<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:42 GMT -->
<?php include("header.php"); ?>
<body>
    <!-- Content Wrapper -->
    <div class="login-wrapper">
        <div class="back-link">
            <a href="index.html" class="btn btn-success">Back to Dashboard</a>
        </div>
        <div class="container-center lg">
            <div class="panel panel-bd">
                <div class="panel-heading">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe-7s-unlock"></i>
                        </div>
                        <div class="header-title">
                            <h3>Register</h3>
                            <small><strong>Please enter your data to register.</strong></small>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="http://healthadmin.thememinister.com/index.html" id="loginForm" novalidate>
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label>Username</label>
                                <input type="text" value="" id="username" class="form-control" name="username">
                                <span class="help-block small">Your unique username to app</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Password</label>
                                <input type="password" value="" id="password" class="form-control" name="password">
                                <span class="help-block small">Your hard to guess password</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Repeat Password</label>
                                <input type="password" value="" id="repeatpassword" class="form-control" name="repeatpassword">
                                <span class="help-block small">Please repeat your pasword</span>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Email Address</label>
                                <input type="text" value="" id="email" class="form-control" name="email">
                                <span class="help-block small">Your address email to contact</span>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-warning">Register</button>
                            <a class="btn btn-primary" href="login.html">Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <!-- jQuery -->
    <?php include("footer.php"); ?>
</body>

<!-- Mirrored from healthadmin.thememinister.com/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:42 GMT -->
</html>