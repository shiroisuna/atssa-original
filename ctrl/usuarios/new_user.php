<?php

if (isset($_POST)) {
	include("../../models/conn.php");
	include("../../models/users.php");
	include("../../models/equipo_salud.php");
	$users = new Users;
	$usuario = new EquipoSalud;

	extract($_POST);
	if ($nombres == '' || $apellidos == '' || $t_doc == '' || $nro_doc == '' || $email == '' || $departamento == '' || $direccion == '' || $tlf == '' || $login == '' || $pass == '' || $pass2 == '' ) {
		
		// Alerta dede completar todos los campos requeridos para continuar
		echo 'Debe completar todos los datos';
		exit();
	}

	// El escript continua validando las contraseña
	if ($pass != $pass2) {
		echo 'contraseña no coinciden';
		exit();
	}

	// validar que el usuario se encuentre disponible
	$userExits = $users->valUser($login);

	// continuar con el registro del usuario
	if (count($userExits) == 0) {
		$password = sha1($pass);

		$usuario->set_data('nombres', $nombres);
		$usuario->set_data('apellidos', $apellidos);
		$usuario->set_data('t_doc', $t_doc);
		$usuario->set_data('nro_doc', $nro_doc);
		$usuario->set_data('email', $email);
		$usuario->set_data('depto', $departamento);
		$usuario->set_data('municipio', $direccion);
		$usuario->set_data('tlf', $tlf);

		if (isset($nit_empresa)){$usuario->set_data('nit_empresa', $nit_empresa);}
		if (isset($nombre_empresa)){$usuario->set_data('nombre_empresa', $nombre_empresa);}
		if (isset($cod_habilitacion)){$usuario->set_data('cod_habilitacion', $cod_habilitacion);}
		if (isset($profesion)){$usuario->set_data('profesion', $profesion);}
		if (isset($fecha_nac)){$usuario->set_data('fecha_nac', $fecha_nac);}

		$id_usuario = $users->register($login, $password, $email, '1', '1');

		$usuario->set_data('id_equipo', $id_usuario);
		echo $usuario->add();
		
	} else {
		echo 'El usuario no esta disponible';
		exit();
	}
	

} else {
	echo 'devolver atras despues de un alert';
}

?>