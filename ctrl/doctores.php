<?php 
	// By JAMP 08/11/2018
	 // instanciamos nuestra conexión
session_start();
	include("../models/conn.php");
	$con = new Conn;

	$con->connect();

	include("../models/doctores.php");

	$doctores = new Doctores;

	include("../models/pacienteReferencia.php");

	$pacienteReferencia = new PacienteReferencia;


	if (isset($_POST['op'])) {
				
		$op = $_POST['op'];
		
	}else{

		$op = $_GET['i'];
	}

	switch ($op) {
		
		case 1: // listar doctores

			$listDoc = $doctores->listDoc();

			$response = json_encode($listDoc);

			include("../views/doctores/listado_doctores.php");
		
		break;

		case 2: // agregar un doctor

			$doctor_nom = $_POST['nom'];
			$doctor_apell = $_POST['apell'];
			$email = $_POST['email'];
			// $password = $_POST['pass'];
			$departamento = $_POST['depto'];
			$direccion = $_POST['direccion'];
			$tlf_cell = $_POST['tlf_cell'];
			$tlf_hab = $_POST['tlf_hab'];
			$sueldo = $_POST['sueldo'];
			$foto = $_POST['foto'];
			$biografia = $_POST['biografia'];
			$fecha_nac = $_POST['f_nac'];
			// $precio = $_POST['precio'];

			// var_dump($_POST);

			// Busca en la bd si existe un doctor con alguna caracteristica por ejemplo el nombre.
			// if(isset($_POST)){

			$search = $doctores->searchDoctor($doctor_nom);
			// var_dump($_POST);

				// var_dump($search);

			
			if ($search == NULL) {
				
			// 	// El doctor ha sido agregado exitosamente...
				$addDoctor = $doctores->addDoctor($doctor_nom, $doctor_apell, $email, $departamento, $direccion, $tlf_cell, $tlf_hab, $sueldo, $foto, $biografia, $fecha_nac);
			// 	// header("location: ../table.php?add=1");
				echo "Doctor registrado";


			}else{
				
				// El doctor que intenta agregar ya existe en la base de datos
				echo "Ya existe ese doctor en nuestra base de datos.";
			// 	// header("location: ../table.php?error=1");
			}

		// }else{

		// 	echo "ha ocurrido algun error";
		// }

		break;

		case 3:

			$id = $_POST['id'];

			// var_dump($id);

			$listaDoc = $doctores->listDocParams($id);

			// $response = json_encode($listaDoc);
			// var_dump($listaDoc);
			include("../views/doctores/modal_edit_doctor.php");

		break;

		case 4:

			$id_doc = $_POST['id'];
			$email = $_POST['email'];
			$depto = $_POST['depto'];
			$direccion = $_POST['direccion'];
			$tlf_movil = $_POST['tlf_movil'];
			$tlf_casa = $_POST['tlf_casa'];
			$sueldo = $_POST['sueldo'];
			$foto = $_POST['foto'];

			// var_dump($_POST);
			if (isset($_POST)) {
				
				$response = $doctores->updateDoctor($id_doc, $email, $depto, $direccion, $tlf_movil, $tlf_casa, $sueldo, $foto);

				
			}else{

				echo "Error al actualizar la informacion";
			}

		break;

		case 5:

			$id_doc = $_POST['id'];
			$status = $_POST['status'];

			if (isset($_POST)) {

				$response = $doctores->eliminarDoctor($id_doc, $status);
				
			}else{

				echo "Ha ocurrido un error en la operación";
			}

		break;

		case 7: // agregar un paciente con riesgo a la lista de captacion

			

			//$fecha_format = $_POST['fecha_format'];
			$fecha_format = date('Y-m-d'); //dia actual
			$cod_upgd = $_POST['cod_upgd'];
			$nom_upgd = $_POST['nom_upgd'];
			$t_riesgo = $_POST['t_riesgo'];
			$nom_evento = "develop-ricardo"; //esto es un valor arbitrario para que guarde
			$nom1 = $_POST['nom1'];
			$apell1 = $_POST['apell1'];
			$t_doc = $_POST['t_doc'];
			$fech_nac = $_POST['fech_nac'];
			$edad = $_POST['edad'];
			$dpto_res = $_POST['dep'];
			$dir_res = $_POST['dir_res'];
			$barrio_res = $_POST['barrio_res'];
			$nom2 = $_POST['nom2'];
			$apell2 = $_POST['apell2'];
			$nr_identidad = $_POST['nr_identidad'];
			$tlf = $_POST['tlf'];
			$sex = $_POST['sex'];
			$mun_res = $_POST['municipio'];
			$location_res = $_POST['location_res'];
			$otro_barrio = $_POST['otro_barrio'];
			$pers_etnica = $_POST['pers_etnica'];
			$g_poblacional = $_POST['g_poblacional'];
			$estrato = $_POST['estrato'];
			$t_aseg = $_POST['t_aseg'];
			$n_educat = $_POST['n_educat'];
			$edo_civil = $_POST['edo_civil'];
			$eps = $_POST['eps'];
			$regimen = $_POST['regimen'];
			$eqSaludAfilia = $_POST['eqSaludAfilia'];
			$peso = $_POST['peso'];
			$altura = $_POST['altura'];
			$IMC = $_POST['imc'];
			$tipo_riesgo = $_POST['t_riesgo'];

			//var_dump($_POST);

			//formateo a fecha de nacimiento antes de insertar
			$fechaNacimiento = date("Y-m-d", strtotime($fech_nac));

			//echo $fechaNacimiento;
			$response = $doctores->captacionPacienteRiesgo2($fecha_format, $nom_upgd, $nom_evento, $t_doc, $nr_identidad, $fechaNacimiento, $edad, $tlf, $sex, $peso, $altura, $IMC, $nom1, $nom2, $apell1, $apell2, $dpto_res, $dir_res, $barrio_res, $mun_res, $location_res, $otro_barrio, $pers_etnica, $g_poblacional, $estrato, $t_aseg, $n_educat, $edo_civil, $eps, $regimen, $eqSaludAfilia);

			if ($response > 0) {
			 	//echo "guardado" + " id guardado : " + $response;
			 	header("location: ../list_capt_pacientes.php");

			}else{
				echo "error";
				header("location: ../pt_riesgos_form.php?error=1");
			} 

			// variables paraRiesgo cardiovascular
						

			/*if (empty($_POST['nr_identidad'])) {
				
				header("location: ../pt_riesgos_form.php?error=1");

			}else{

				$response = $doctores->captacionPacienteRiesgo($fecha_format, $nom_upgd, $nom_evento, $t_doc, $nr_identidad, $fech_nac, $edad, $tlf, $sex, $nom1, $nom2, $apell1, $apell2, $dpto_res, $dir_res, $barrio_res, $mun_res, $location_res, $otro_barrio, $pers_etnica, $g_poblacional, $estrato, $t_aseg, $n_educat, $edo_civil, $peso, $altura, $IMC);

				echo "id guardado " . $response . "<br>";
				
				var_dump($response);

				
			}*/




		break;

		case 8: //lista de captacion de pacientes con riesgo

			$listCapParams = $doctores->listCaptacion();

			include("../views/doctores/listado_pacientes_riesgos.php");

		break;

		case 9: // mostrar pdf de paciente con riesgo

			if (isset($_POST['id'])) {
				
				$id_paciente = $_POST['id'];
				
			}else{

				$id_paciente = $_GET['id'];
			}

			$listCapParams = $doctores->buscarPacCaptacion($id_paciente);

			$list_riesgosCard = $doctores->buscarRiesgosCard($id_paciente);

			$list_riesgosSalud = $doctores->buscarRiesgosSalud($id_paciente);

			$list_riesgosInfecc = $doctores->buscarRiesgosInfecc($id_paciente);

			$list_riesgosGestante = $doctores->buscarRiesgosGestante($id_paciente);

			$list_riesgosCancer = $doctores->buscarRiesgosCancer($id_paciente);

			include("../rpt-captacion.php");

		break;

		case 10: //listar equipos de salud

			// $list_equipos = $doctores->listEqipos();

			include("../views/doctores/lista_voluntarios.php");

		break;

		case 11: // agregar agregar nuevo equipo de voluntario

			$pacientes = $pacienteReferencia->listaReferencia();

			//include("../views/afiliaciones/lista_afiliados.php");

			include("../views/doctores/lista_referencia_pacientes.php");

		break;

		case 12: //add equipo

			$nombres = $_POST['nom1']." ".$_POST['nom2'];
			$apellidos = $_POST['apell']." ".$_POST['apell2'];
			$tipo_doc = $_POST['tipo_doc'];
			$nro_doc = $_POST['nro_doc'];
			$profesion = $_POST['profesion'];
			$sexo = $_POST['sexo'];
			$depto = $_POST['depto'];
			$munic = $_POST['munic'];
			$tlf = $_POST['tlf'];
			$email = $_POST['email'];
			$f_nac = $_POST['f_nac'];

			var_dump($_POST);
			//$add_equipo = $doctores->addEquipo($nombres, $apellidos, $tipo_doc, $nro_doc, $profesion, $sexo, $depto, $munic, $tlf, $email, $f_nac);
		break;

		case 13: //add referencia de paciente
			$nombre = $_POST['nombre'];
			$apellido = $_POST['apellido'];
			$tipo_identidad = $_POST['tipo_identidad'];
			$nro_doc = $_POST['nro_doc'];
			$eps = $_POST['eps'];
			$munic_orig = $_POST['munic_orig'];
			$munic_dest = $_POST['munic_dest'];
			$tlf = $_POST['tlf'];
			$nombre_acomp = $_POST['nombre_acomp'];
			$tlf_acomp = $_POST['tlf_acomp'];
			$email_acomp = $_POST['email_acomp'];
			$razon_social = $_POST['razon_social'];
			$nit = $_POST['nit'];
			$val_medica = $_POST['val_medica'];
			$at_enfermeria = $_POST['at_enfermeria'];
			$serv_rehab = $_POST['serv_rehab'];
			$medicinas = $_POST['medicinas'];
			$insumos = $_POST['insumos'];
			
			var_dump($_POST);

			// $add_equipo = $doctores->addReferencia($nombre, $apellido, $tipo_identidad, $nro_doc, $eps, $munic_orig, $munic_dest, $tlf, $nombre_acomp, $tlf_acomp, $email_acomp, $razon_social, $nit, $val_medica, $at_enfermeria, $serv_rehab, $medicinas, $insumos);


		break;

		case 14: // buscar voluntario para mostrar en el pdf

			// $id_voluntario = $_POST['id_equipo'];

			// $response = $doctores->buscarVoluntario($id_voluntario);

			include("../rpt_equipo_salud.php");

		break;

		case 15: // buscar referencia de paciente para mostrar en el pdf

			// $id_paciente_ref = $_POST['id_paciente_ref'];

			// $response = $doctores->buscarVoluntario($id_paciente_ref);

			include("../rpt_referenci_paciente.php");

		break;

		default:

			echo "No se han recibido parametros";
		
		break;
	}


	/*if ($op = 16) {
		
	}*/


 ?>