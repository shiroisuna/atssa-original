<?php

	session_start();
	include("../models/conn.php");
	$con = new Conn;

	$con->connect();

	include("../models/afiliaciones.php");

	$afiliacion = new afiliaciones;

	$afiliaciones =  $afiliacion->listAfiliaciones();


/*$cuenta = ( isset($_POST['cuenta']) ) ? $_POST['cuenta'] : '' ;
echo $cuenta;*/

//Obtenemos los datos de los input
$nombre = 'data 1';
$apellido = 'data 2';
$afiliaciones = count($afiliaciones);

//Hacemos las comprobaciones que sean necesarias... (sanitizar los textos para evitar XSS e inyecciones de código, comprobar que la edad sea un número, etc.)
//Omitido para la brevededad del código
//PERO NO OLVIDES QUE ES ALGO IMPORTANTE.

//Seteamos el header de "content-type" como "JSON" para que jQuery lo reconozca como tal
header('Content-Type: application/json');
//Guardamos los datos en un array
$datos = array(
'estado' => 'ok',
'nombre' => $nombre, 
'apellido' => $apellido, 
'afiliaciones' => $afiliaciones
);
//Devolvemos el array pasado a JSON como objeto
echo json_encode($datos, JSON_FORCE_OBJECT);

?>