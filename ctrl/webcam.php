<?php 

	class Webcam {

		public function __construct(){

			parent::__construct();
			$fotos = new fotos;
		}

		public function index(){

			include("../views/fotos/photos.php");
		}

		public function ajax(){

			$src = $_POST['src'];
			$response = $fotos->grabarFoto($src);
			$foto = $fotos->getLastFoto();
			$this->output->set_output($foto);
		}
	}
 ?>