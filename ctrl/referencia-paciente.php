<?php 
	
	session_start();
	include("../models/conn.php");
	$con = new Conn;

	$con->connect();

	include("../models/pacienteReferencia.php");

	$pacienteReferencia = new PacienteReferencia;

	$op = $_POST['op'];

	switch ($op){

		case 1:

			#USUARIO
			$t_atencion = ( isset($_POST['t_atencion']) ) ? $_POST['t_atencion'] : '' ;
			$nombre = ( isset($_POST['nombre']) ) ? $_POST['nombre'] : '' ; 		
			$apellido = ( isset($_POST['apellido']) ) ? $_POST['apellido'] : '' ; 	
			$tipo_identidad = ( isset($_POST['tipo_identidad']) ) ? $_POST['tipo_identidad'] : '' ; 
			$nro_identidad = ( isset($_POST['nro_identidad']) ) ? $_POST['nro_identidad'] : '' ; 
			$eps = ( isset($_POST['eps']) ) ? $_POST['eps'] : '' ; 
			$dep = ( isset($_POST['dep']) ) ? $_POST['dep'] : '' ; 
			$municipio_origen = ( isset($_POST['municipio']) ) ? $_POST['municipio'] : '' ; 
			$municipio_destino = ( isset($_POST['select_munic_destino']) ) ? $_POST['select_munic_destino'] : '' ; 
			$tlf = ( isset($_POST['tlf']) ) ? $_POST['tlf'] : '' ; 

			#Datos del acompañante
			$nombre_acompanante = ( isset($_POST['nombre_acomp']) ) ? $_POST['nombre_acomp'] : '' ; 
			$tlf_acompanante = ( isset($_POST['tlf_acomp']) ) ? $_POST['tlf_acomp'] : '' ; 
			$email_acompanante = ( isset($_POST['email_acomp']) ) ? $_POST['email_acomp'] : '' ; 

			#Datos del prestador de Servicio a Atención Domiciliaria
			$razon_social = ( isset($_POST['razon_social']) ) ? $_POST['razon_social'] : '' ; 
			$nit = ( isset($_POST['nit']) ) ? $_POST['nit'] : '' ; 

			#Plan Domiciliario
			$val_medica = ( isset($_POST['val_medica']) ) ? $_POST['val_medica'] : '' ; 
			$at_enfermeria = ( isset($_POST['at_enfermeria']) ) ? $_POST['at_enfermeria'] : '' ; 
			$serv_rehab = ( isset($_POST['serv_rehab']) ) ? $_POST['serv_rehab'] : '' ; 
			$medicinas = ( isset($_POST['medicinas']) ) ? $_POST['medicinas'] : '' ; 
			$insumos = ( isset($_POST['insumos']) ) ? $_POST['insumos'] : '' ; 

			#anexos
			if ( isset($_FILES['archivo']) ) {

				move_uploaded_file($_FILES['archivo']['tmp_name'], '../uploads/' . $_FILES['archivo']['name'] );
				$directorio_archivo = 'uploads/' . $_FILES['archivo']['name'] ;

			}else{
				$directorio_archivo = '';
			}
			
			$response = $pacienteReferencia->addPacienteRefencia($t_atencion, $nombre, $apellido, $tipo_identidad, $nro_identidad, $eps, $dep, $municipio_origen, $municipio_destino, $tlf, $nombre_acompanante, $tlf_acompanante, $email_acompanante, $razon_social, $nit, $val_medica, $at_enfermeria, $serv_rehab, $medicinas, $insumos, $directorio_archivo);

			header('Location: ../lista_referencia_pt.php');

		break;

		case 2:

			$id_dpto = $_POST['id_dpto'];

			//$response = $pacienteReferencia->selectIPS($id_dpto);

			$response = $pacienteReferencia->selectTablaIPS($id_dpto);

			include("../views/referencia/select_ips.php");

		break;

		default:

		break;
	}
