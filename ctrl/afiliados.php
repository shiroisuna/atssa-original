<?php 
	
	session_start();
	include("../models/conn.php");
	$con = new Conn;

	$con->connect();

	include("../models/afiliaciones.php");

	$afiliacion = new afiliaciones;

	if (isset($_GET['i'])) {
		
		$op = $_GET['i'];

	}else{

		$op = $_POST['op'];
		
	}


	switch ($op){

		case 1:
			// listar afiliaciones
			$response = $afiliacion->listAfiliaciones();

			include("../views/afiliaciones/lista_afiliados.php");

		break;

		case 2:
		//agregar una afiliacion

		//Campos Tipo de Trámite
		$eps = $_POST['eps'];// NO ESTABA
		$tipo_tramite = $_POST['tipo_tramite'];
		$tipo_afiliacion = $_POST['tipo_afiliacion'];
		$sub_tipo_afil = $_POST['sub_tipo_afil'];
		$regimen = $_POST['regimen'];
		$tp_afiliado = $_POST['tp_afiliado'];
		$tp_cotizante = $_POST['tp_cotizante'];
		$codigo = $_POST['codigo'];
		$fech_sol = $_POST['fech_sol'];

		// echo $eps . "<br>";
		// echo $tipo_tramite . "<br>";
		// echo $tipo_afiliacion . "<br>";
		// echo " sub_tipo " . $sub_tipo_afil . "<br>";
		// echo $regimen . "<br>";
		// echo $tp_afiliado . "<br>";
		// echo $tp_cotizante . "<br>";
		// echo$codigo . "<br>";
		// echo $fech_sol . "<br>";
		/*----------------------------------------------*/
		//DATOS BASICOS DE IDENTIFICACION (del cotizante o cabeza de familia)
		$nom1_solic = $_POST['nom1_solic'];
		$nom2_solic = $_POST['nom2_solic'];
		$apell1_solic = $_POST['apell1_solic'];
		$apell2_solic = $_POST['apell2_solic'];
		$tipo_id_solic = $_POST['tipo_id_solic'];
		$n_identidad = $_POST['n_identidad'];
		$sexo_solic = $_POST['sexo_solic'];

		$f_nac_solic = $_POST['f_nac_solic'];
		$fecha_nacimiento = strtotime ( $f_nac_solic ) ;
		$format_fecha_nacimiento = date ( 'Y-m-d' , $fecha_nacimiento );
		$fecha_hoy = date('Y-m-d');
		$date1 = new DateTime($format_fecha_nacimiento);
		$date2 = new DateTime($fecha_hoy);
		$diff = $date1->diff($date2);
		$edad_solic = $diff->y;
		
		//Datos Complementarios
		$etnia = $_POST['etnia'];
		$discapacidad = $_POST['discapacidad'];
		$t_discapacidad = $_POST['t_discapacidad']; //NO ESTABA
		$condic = $_POST['condic'];
		$pt_sisben = $_POST['pt_sisben'];
		$g_poblacion_esp = $_POST['g_poblacion_esp']; //no estaba
		$ad_riesgos = $_POST['ad_riesgos'];
		$ad_pension = $_POST['ad_pension'];
		$ing_cotizacion = $_POST['ing_cotizacion'];
		$direccionsolic = $_POST['direccionsolic'];
		$zona = $_POST['zona'];
		$localidad = $_POST['localidad'];
		$barrio = $_POST['barrio'];
		$dep = $_POST['dep'];
		$municipio = $_POST['municipio'];
		$tlf_f = $_POST['tlf_f'];
		$tlf_m = $_POST['tlf_m'];
		$email = $_POST['email'];
		
		//Datos de Identificación de los miembros del Núcleo Familiar
		$p_nom_c = $_POST['p_nom_c'];
		$nom_c = $_POST['p_nom_c']." ".$_POST['s_nom_c'];
		$apell_c = $_POST['p_apell_c']." ".$_POST['s_apell_c'];
		$t_doc_con = $_POST['t_doc_con'];
		$nro_doc_con = $_POST['nro_doc_con'];
		$sexo_con = $_POST['sex_con'];
		$f_nac_con = $_POST['f_nac_con'];
		$nom_ben = $_POST['p_nombre_ben']." ".$_POST['s_nombre_ben'];
		$apell_ben = $_POST['p_apellido_ben']." ".$_POST['s_apellido_ben'];
		$t_doc_ben = $_POST['t_doc_ben'];
		$n_identidad_ben = $_POST['n_identidad_ben'];
		$sexo_ben = $_POST['sexo_ben'];
		$fecha_nac_ben = $_POST['f_nac_ben'];
		$parentesco = $_POST['parentesco'];
		$etnia_ben = $_POST['etnia_ben'];
		$discapacidad_ben = $_POST['discapacidad_ben'];
		$t_discapacidad_ben = $_POST['t_discapacidad_ben']; //NO ESTABA
		$condicion_ben = $_POST['condicion_ben']; //NO ESTABA
		$dep_ben = $_POST['dep_ben'];
		$munic = $_POST['munic'];
		$zona_ben = $_POST['zona_ben'];
		$tlf_m_ben = $_POST['tlf_m_ben'];
		$tlf_f_ben = $_POST['tlf_f_ben'];
		$upc = $_POST['upc'];
		$ins_salud = $_POST['ins_salud'];
		
		//DATOS DE IDENTIFICACIÓN DEL EMPLEADOR Y OTROS APORTANTES O DE LAS ENTIDADES RESPONSABLES DE LA AFILIACIÓN COLECTIVA, INSTITUCIONAL O DE OFICIO
		$rif = $_POST['rif'];
		$t_doc_ot = $_POST['t_doc_ot'];
		$identidad_ot = $_POST['identidad_ot'];
		$t_aportante = $_POST['t_aportante'];
		$direccion = $_POST['direccion'];
		$dep5 = $_POST['dep5'];
		$municipio5 = $_POST['municipio5'];
		$tlf_mo = $_POST['tlf_mo'];
		$tlf_f_aport = $_POST['tlf_f_aport'];
		$email_aport = $_POST['email_aport'];
		$fech_ing_t = $_POST['fech_ing_t'];
		$cargo_t = $_POST['cargo_t'];
		$salario_t = $_POST['salario_t'];
		
		//Reporte de Novedades
		$tipo_novedad = $_POST['tipo_novedad'];
		$movilidad = $_POST['movilidad'];
		$traslado = $_POST['traslado'];
		$rep_nom1 = $_POST['rep_nom1'];
		$rep_nom2 = $_POST['rep_nom2'];
		$rep_apell1 = $_POST['rep_apell1'];
		$rep_apell2 = $_POST['rep_apell2'];
		$t_doc_rep = $_POST['t_doc_rep'];
		$n_identidad_rep = $_POST['n_identidad_rep'];
		$rep_fech_nac = $_POST['rep_fech_nac'];
		$sexo_rep = $_POST['sexo_rep'];
		$rep_fecha_ini = $_POST['rep_fecha_ini'];
		$eps_ant = $_POST['eps_ant'];
		$cod_traslado_rep = $_POST['cod_traslado_rep'];
		$compensacion_rep = $_POST['compensacion_rep'];
		
		//Declaraciones y Autorizaciones		
		$dep_economica = $_POST['dep_economica'];
		$oblig_afiliacion = $_POST['oblig_afiliacion'];
		$entrega_docs = $_POST['entrega_docs'];
		$interm_cotizante = $_POST['interm_cotizante'];
		$eps_datos_cotizante = $_POST['eps_datos_cotizante'];
		$reporte_info_eps = $_POST['reporte_info_eps'];
		$manejo_datoseps_cotizante = $_POST['manejo_datoseps_cotizante'];
		$eps_envio_info_correo = $_POST['eps_envio_info_correo'];
		
		//Anexos
		$cop_doc_identidad = $_POST['cop_doc_identidad'];
		$dictamen_incapac = $_POST['dictamen_incapac'];
		$acta_matrimonio = $_POST['acta_matrimonio'];
		$sent_divorcio = $_POST['sent_divorcio'];
		$adopcion = $_POST['adopcion'];
		$acta_custodia = $_POST['acta_custodia'];
		$perd_patria_potestad = $_POST['perd_patria_potestad'];
		$auth_traslado = $_POST['auth_traslado'];
		$cert_afili_colect = $_POST['cert_afili_colect'];
		$acta_admin_calidad_benef = $_POST['acta_admin_calidad_benef'];
		
		//Datos a ser Diligenciados por la Entidad Territorial
		$id_entidad_terr = $_POST['id_entidad_terr'];
		$ficha = $_POST['ficha'];
		$puntaje = $_POST['puntaje'];
		$nivel = $_POST['nivel'];
		$radicacion = $_POST['radicacion'];
		$validacion = $_POST['validacion'];
		$nom1_func = $_POST['nom1_func'];
		$nom2_func = $_POST['nom2_func'];
		$apell1_func = $_POST['apell1_func'];
		$apell2_func = $_POST['apell2_func'];		
		$identidad_func = $_POST['identidad_func'];
		$observ_func = $_POST['observ_func'];
		
		//Autorización de Mensajes de Texto
		$auth_derechos_deberes = $_POST['auth_derechos_deberes'];
		$mod_datos = $_POST['mod_datos'];
		$auth_desempeño_eps = $_POST['auth_desempeño_eps'];
		$auth_contenido_desempeño = $_POST['auth_contenido_desempeño'];
		$auth_dudas = $_POST['auth_dudas'];
		$auth_entender = $_POST['auth_entender'];
		$auth_canales_disp = $_POST['auth_canales_disp'];
		

		$directorio = "../docs/";

		$file1=$_FILES['dni']['name'];
		$guardado1=$_FILES['dni']['tmp_name'];

		$file2=$_FILES['FotoconDni']['name'];
		$guardado2=$_FILES['FotoconDni']['tmp_name'];
		
		
		if(!file_exists($directorio)){
			mkdir($directorio,0777,true);
			if(file_exists($directorio)){
				if(move_uploaded_file($guardado1, $directorio.$file1) && move_uploaded_file($guardado2, $directorio.$file2)){
					echo "El archivo fue subido con exito";
				}else{
					echo "error al subir el archivo.";
				}
			}
		}else{
			if(move_uploaded_file($guardado1, $directorio.$file1) && move_uploaded_file($guardado2, $directorio.$file2)){
				echo "El archivo fue subido con exito";
			}else{
				echo "error al subir el archivo.";
			}
		}


		if (empty($_POST)) {
			header("location: ../form_afiliacion.php?error=1");
		}else{

		// agregamos una afiliacion
			$addAfiliacion = $afiliacion->addAfiliacion($eps, $tipo_tramite, $tipo_afiliacion, $sub_tipo_afil, $regimen, $tp_afiliado, $tp_cotizante, $codigo, $fech_sol);

			$id_afiliacion = $addAfiliacion;

			// var_dump($id_afiliacion);

		
		// validamos que haya insertado y nos traiga el id del ultimo insertado
			if($id_afiliacion != NULL){
			//buscamos la afiliacion segun el id
				$buscaAfiliacion = $afiliacion->buscarAfiliacion($id_afiliacion);

				$response = $buscaAfiliacion[0]['id_afiliacion'];
				$id_afiliacion = $response;
				// agregamos un solicitante pasandole el id_afiliacion que dentro ejecuta la funcion updateAfiliacion
				$addSolicitante = $afiliacion->addSolicitante($id_afiliacion,$nom1_solic, $nom2_solic, $apell1_solic, $apell2_solic, $tipo_id_solic, $n_identidad, $sexo_solic, $f_nac_solic, $etnia, $condic, $discapacidad, $pt_sisben, $ad_riesgos, $ad_pension, $ing_cotizacion, $direccionsolic, $zona, $localidad, $barrio, $municipio, $dep, $tlf_f, $tlf_m, $email, 1234);
				//retornamos el id del solicitante agregado
				$id_solic = $addSolicitante;
				if (isset($p_nom_c)) {
					$parent = "conyugue";
					$addConyugue = $afiliacion->addConyugue($nom_c, $apell_c, $t_doc_con, $nro_doc_con, $sexo_con, $f_nac_con, $parent, $id_solic);
				}
			
				for ($i=0; $i<count($nom_ben); $i++) {
					// recorre cada uno de los valores del post contandolos pasandoles $i
					$nom = $nom_ben[$i];
					$apell = $apell_ben[$i];
					$t_doc = $t_doc_ben[$i];
					$nro_identidad = $n_identidad_ben[$i];
					$sexo = $sexo_ben[$i];
					$fecha_nac = $fecha_nac_ben[$i];
					$parent = $parentesco[$i];
					$etnia = $etnia_ben[$i];
					$discapacidad = $discapacidad_ben[$i];
					$municipio = $munic[$i];
					$dep = $dep_ben[$i];
					$zona_b = $zona_ben[$i];
					$tlf_m = $tlf_m_ben[$i];
					$tlf_f = $tlf_f_ben[$i];
					$upc_b = $upc[$i];
					$ins_salud_b = $ins_salud[$i];
					$n_sisben = $n_sisben_ben[$i];
					$g_poblacional_b = $g_poblacional[$i];
					// valida que no esten vacios para que guarde
					if ($nom != "" && $apell != "" && $t_doc != "" && $nro_identidad != "" && $sexo != "" && $fecha_nac != "" && $parent != "" && $etnia != "" && $discapacidad != "" && $municipio != "" && $dep != "" && $zona_b != "" && $tlf_m != "" && $tlf_f != "" && $upc_b != "" && $ins_salud_b != "" ) {
						// validamos si existe un conyugue en el registro de beneficiarios
						
						//Guarda en la bd tabla nucleo_familiar
						$addBeneficiarios = $afiliacion->addBeneficiarios($nom, $apell, $t_doc, $nro_identidad, $sexo, $fecha_nac, $parent, $etnia, $discapacidad, $municipio, $dep, $zona_b, $tlf_m, $tlf_f, $upc_b, $ins_salud_b, $n_sisben, $g_poblacional_b, $id_solic);
						$addBeneficiarios = $afiliacion->addBeneficiarios($nom, $apell, $t_doc, $nro_identidad, $sexo, $fecha_nac, $parent, $etnia, $discapacidad, $municipio, $dep, $zona_b, $tlf_m, $tlf_f, $upc_b, $ins_salud_b, $id_solic);

					}
					
				}
				// Guarda en la bd tabla aportantes
				$addAportante = $afiliacion->addAportante($rif, $t_doc_ot, $identidad_ot, $t_aportante, $municipio5, $dep5, $tlf_mo, $tlf_f_aport, $email_aport, $fech_ing_t, $cargo_t, $salario_t, $id_solic); //listo
				// Guarda en la bd tabla novedades
				$addNovedad = $afiliacion->addNovedad($tipo_novedad, $movilidad, $traslado, $id_afiliacion); //listo
				// Guarda en la bd tabla data_novedad
				$addDataNovedad = $afiliacion->addDataNovedad($rep_nom1, $rep_nom2, $rep_apell1, $rep_apell2, $t_doc_rep, $n_identidad_rep, $sexo_rep, $rep_fech_nac, $rep_fecha_ini, $eps_ant, $cod_traslado_rep, $compensacion_rep, $addNovedad); //listo
				// Guarda en la bd tabla entidad_territ
				$addEntidadTerritorial = $afiliacion->addEntidadTerritorial($id_entidad_terr, $ficha, $puntaje, $nivel, $radicacion, $validacion, $id_afiliacion);//listo
				// Guarda en la bd tabla funcionario_validacion
				$addFuncValid = $afiliacion->addFuncValid($nom1_func, $nom2_func, $apell1_func, $apell2_func, $identidad_func, $observ_func, $addEntidadTerritorial);//listo

				$addAuthMsn = $afiliacion->addAuthMsn($auth_derechos_deberes, $auth_desempeño_eps, $auth_contenido_desempeño, $auth_dudas, $auth_entender, $auth_canales_disp, $id_afiliacion);//listo


				$addAuthDeclar = $afiliacion->addAuthDeclar($dep_economica, $entrega_docs, $interm_cotizante, $eps_datos_cotizante, $reporte_info_eps, $manejo_datoseps_cotizante, $eps_envio_info_correo, $oblig_afiliacion, $id_afiliacion);//listo

				$response = $afiliacion->rptAfiliacionedit($id_afiliacion);

				$id_eps = $response[0]['id_eps'];

				$eps = $afiliacion->buscarEPS($id_eps);

				include("../rpt-afiliacion.php");
				
			}
			

		}


		
		


		break;

		case 3:

			$id_afiliacion = $_POST['id'];

			$response = $afiliacion->rptAfiliacionedit($id_afiliacion);

			

			// $resp = json_encode($response);

			// echo $resp;

			include("../views/afiliaciones/form_afiliacion_update.php");


		break;

		case 4: // generar pdf desde el listado de afiliaciones

			if (isset($_POST['id'])) {
				
				$id_afiliacion = $_POST['id'];
				
			}else{

				$id_afiliacion = $_GET['id'];
			}

			$response = $afiliacion->rptAfiliacionedit($id_afiliacion);

			$id_eps = $response[0]['id_eps'];

			$eps = $afiliacion->buscarEPS($id_eps);

			include("../rpt-afiliacion.php");

		break;

		case 5: // Actualizar una afiliacion

			$tipo_tramite = $_POST['tipo_tramite'];
			$tipo_afiliacion = $_POST['tipo_afiliacion'];
			$sub_tipo_afil = $_POST['sub_tipo_afil'];
			$regimen = $_POST['regimen'];
			$tp_afiliado = $_POST['tp_afiliado'];
			$tp_cotizante = $_POST['tp_cotizante'];
			$codigo = $_POST['codigo'];
			$fech_sol = $_POST['fech_sol'];
			$nom1_solic = $_POST['nom1_solic'];
			$nom2_solic = $_POST['nom2_solic'];
			$apell1_solic = $_POST['apell1_solic'];
			$apell2_solic = $_POST['apell2_solic'];
			$tipo_id_solic = $_POST['tipo_id_solic'];
			$n_identidad = $_POST['n_identidad'];
			$sexo_solic = $_POST['sexo_solic'];
			$f_nac_solic = $_POST['f_nac_solic'];
			$etnia = $_POST['etnia'];
			$discapacidad = $_POST['discapacidad'];
			$condic = $_POST['condic'];
			$pt_sisben = $_POST['pt_sisben'];
			$ad_riesgos = $_POST['ad_riesgos'];
			$ad_pension = $_POST['ad_pension'];
			$ing_cotizacion = $_POST['ing_cotizacion'];
			$direccionsolic = $_POST['direccionsolic'];
			$zona = $_POST['zona'];
			$localidad = $_POST['localidad'];
			$barrio = $_POST['barrio'];
			$municipio = $_POST['municipio'];
			$dep = $_POST['dep'];
			$tlf_f = $_POST['tlf_f'];
			$tlf_m = $_POST['tlf_m'];
			$email = $_POST['email'];
			$cod = $_POST['cod'];
			$nom_ben = $_POST['nom_ben'];
			$apell_ben = $_POST['apell_ben'];
			$t_doc_ben = $_POST['t_doc_ben'];
			$n_identidad_ben = $_POST['n_identidad_ben'];
			$parentesco = $_POST['parentesco'];
			$etnia_ben = $_POST['etnia_ben'];
			$discapacidad_ben = $_POST['discapacidad_ben'];
			$munic = $_POST['munic'];
			$dep_ben = $_POST['dep_ben'];
			$zona_ben = $_POST['zona_ben'];
			$tlf_m_ben = $_POST['tlf_m_ben'];
			$tlf_f_ben = $_POST['tlf_f_ben'];
			$upc = $_POST['upc'];
			$ins_salud = $_POST['ins_salud'];
			$n_sisben_ben = $_POST['n_sisben_ben'];
			$g_poblacional = $_POST['g_poblacional'];
			$rif = $_POST['rif'];
			$t_doc_ot = $_POST['t_doc_ot'];
			$identidad_ot = $_POST['identidad_ot'];
			$t_aportante = $_POST['t_aportante'];
			$ciudad = $_POST['ciudad'];
			$departa_aport = $_POST['departa_aport'];
			$tlf_mo = $_POST['tlf_mo'];
			$tlf_f_aport = $_POST['tlf_f_aport'];
			$email_aport = $_POST['email_aport'];
			$fech_ing_t = $_POST['fech_ing_t'];
			$cargo_t = $_POST['cargo_t'];
			$salario_t = $_POST['salario_t'];
			$correc_datos = $_POST['correc_datos'];
			$act_identidad = $_POST['act_identidad'];
			$act_complementarios = $_POST['act_complementarios'];
			$afil_colectivas = $_POST['afil_colectivas'];
			$desafil_colectivas = $_POST['desafil_colectivas'];
			$movilidad = $_POST['movilidad'];
			$traslado = $_POST['traslado'];
			$rep_fallecimiento = $_POST['rep_fallecimiento'];
			$tramite_cesante = $_POST['tramite_cesante'];
			$rep_prepensionados = $_POST['rep_prepensionados'];
			$rep_pensionados = $_POST['rep_pensionados'];
			$rep_nom1 = $_POST['rep_nom1'];
			$rep_nom2 = $_POST['rep_nom2'];
			$rep_apell1 = $_POST['rep_apell1'];
			$rep_apell2 = $_POST['rep_apell2'];
			$t_doc_rep = $_POST['t_doc_rep'];
			$n_identidad_rep = $_POST['n_identidad_rep'];
			$sexo_rep = $_POST['sexo_rep'];
			$rep_fech_nac = $_POST['rep_fech_nac'];
			$rep_fecha_ini = $_POST['rep_fecha_ini'];
			$eps_ant = $_POST['eps_ant'];
			$cod_traslado_rep = $_POST['cod_traslado_rep'];
			$compensacion_rep = $_POST['compensacion_rep'];
			$id_entidad_terr = $_POST['id_entidad_terr'];
			$ficha = $_POST['ficha'];
			$puntaje = $_POST['puntaje'];
			$nivel = $_POST['nivel'];
			$radicacion = $_POST['radicacion'];
			$validacion = $_POST['validacion'];
			$nom1_func = $_POST['nom1_func'];
			$nom2_func = $_POST['nom2_func'];
			$apell1_func = $_POST['apell1_func'];
			$apell2_func = $_POST['apell2_func'];
			$identidad_func = $_POST['identidad_func'];
			$observ_func = $_POST['observ_func'];
			$id_afiliacion = $_POST['idafil'];

			$updateAfil = $afiliacion->updateAfiliacion($tipo_tramite, $tipo_afiliacion, $sub_tipo_afil, $regimen, $tp_afiliado, $tp_cotizante, $codigo, $fech_sol, $id_afiliacion);

			$update_sol = $afiliacion->updateSolicitante($nom1_solic, $nom2_solic, $apell1_solic, $apell2_solic, $tipo_id_solic, $n_identidad, $sexo_solic, $f_nac_solic, $etnia, $discapacidad, $condic, $pt_sisben, $ad_riesgos, $ad_pension, $ing_cotizacion, $direccionsolic, $zona, $localidad, $barrio, $municipio, $dep, $tlf_f, $tlf_m, $email, $cod, $id_afiliacion);

			$id_solic = $update_sol[0]['id_data_solicitante'];

			for ($i=0; $i<count($nom_ben); $i++) {
					// recorre cada uno de los valores del post contandolos pasandoles $i
				$nom = $nom_ben[$i];
				$apell = $apell_ben[$i];
				$t_doc = $t_doc_ben[$i];
				$nro_identidad = $n_identidad_ben[$i];
				$parent = $parentesco[$i];
				$etnia = $etnia_ben[$i];
				$discapacidad = $discapacidad_ben[$i];
				$municipio = $munic[$i];
				$dep = $dep_ben[$i];
				$zona_b = $zona_ben[$i];
				$tlf_m = $tlf_m_ben[$i];
				$tlf_f = $tlf_f_ben[$i];
				$upc_b = $upc[$i];
				$ins_salud_b = $ins_salud[$i];
				$n_sisben = $n_sisben_ben[$i];
				$g_poblacional_b = $g_poblacional[$i];
				// valida que no esten vacios para que guarde
				if ($nom != "" && $apell != "" && $t_doc != "" && $nro_identidad != "" && $parent != "" && $etnia != "" && $discapacidad != "" && $municipio != "" && $dep != "" && $zona_b != "" && $tlf_m != "" && $tlf_f != "" && $upc_b != "" && $ins_salud_b != "" && $n_sisben != "" && $g_poblacional_b != "") {
					// actualiza en la bd tabla nucleo_familiar
					$update_ben = $afiliacion->updateBeneficiario($nom, $apell, $t_doc, $nro_identidad, $parent, $etnia, $discapacidad, $municipio, $dep, $zona_b, $tlf_m, $tlf_f, $upc_b, $ins_salud_b, $n_sisben, $g_poblacional_b, $id_solic);
				}
					
			}


			$updateAportante = $afiliacion->updateAportante($rif, $t_doc_ot, $identidad_ot, $t_aportante, $ciudad, $departa_aport, $tlf_mo, $tlf_f_aport, $email_aport, $fech_ing_t, $cargo_t, $salario_t, $id_solic);

			// actualiza en la bd tabla novedades
			$updateNovedad = $afiliacion->updateNovedad($tipo_novedad, $movilidad, $traslado, $idafiliacion);
			
			// actualiza en la bd tabla data_novedad
			$updateDataNovedad = $afiliacion->updateDataNovedad($rep_nom1, $rep_nom2, $rep_apell1, $rep_apell2, $t_doc_rep, $n_identidad_rep, $sexo_rep, $rep_fech_nac, $rep_fecha_ini, $eps_ant, $cod_traslado_rep, $compensacion_rep, $idnovedad);
			// actualiza en la bd tabla entidad_territ
			$updateEntidadTerritorial = $afiliacion->updateEntidadTerritorial($id_entidad_terr, $ficha, $puntaje, $nivel, $radicacion, $validacion, $idafiliacion);
			// actualiza en la bd tabla funcionario_validacion
			$updateFuncValid = $afiliacion->updateFuncValid($nom1_func, $nom2_func, $apell1_func, $apell2_func, $identidad_func, $observ_func, $id_data_entidad_territ);

				
			include("../rpt-afiliacion.php");
			// header("location: ../afiliaciones.php?uppdate=true");

		break;

		case 6:

			$response = $afiliacion->listarEps();

			// var_dump($response);

			include("../views/afiliaciones/listado_eps.php");

		break;

		case 7: //listar dptos de colombia

			$dptos = $afiliacion->selectDptos();

			// var_dump($dptos);

			include("../views/afiliaciones/selec_dptos.php");


		break;

		case 8:

			$id_dpto = $_POST['id_dpto'];

			$response = $afiliacion->selectMunicipios($id_dpto);

			include("../views/afiliaciones/selec_municipios.php");

		break;

		case 9://listar dptos de colombia para la pagina nucleo familiar

			$dptos = $afiliacion->selectDptos();
			include("../views/afiliaciones/opcionSelect_dpto_ben.php");

		break;

		case 10:

			$id_dpto = $_POST['id_dpto'];

			$response = $afiliacion->selectMunicipios($id_dpto);
			include("../views/afiliaciones/opcionSelect_munic_ben.php");

			break; //end nucleo familiar------------------------

		case 11://listar dptos de colombia para la pagina datos del empleador

			$dptos = $afiliacion->selectDptos();
			include("../views/afiliaciones/opcionSelect_dptosVista5.php");

		break;

		case 12:

			$id_dpto = $_POST['id_dpto'];

			$response = $afiliacion->selectMunicipios($id_dpto);
			include("../views/afiliaciones/opcionSelect_municVista5.php");
			
			break;//end datos del empleador--------------------

		default:

		break;
	}
	/*esto es para traer los listados de compesacion*/

	/*esto es para traer los listados de compesacion*/

	if ($op == 13) {

		$compesacion = $afiliacion->compensacion();

		include("../views/afiliaciones/optionSelectCompensacion.php");
		
	}
	/*______________________________________________*/


	//llamar a listado ips correspondiente segun el departamento selecionado
	if( $op == 14) {
		$id_dpto = $_POST['id_dpto'];

		$response = $afiliacion->selectIpssegunIdDpto($id_dpto);
		include("../views/afiliaciones/optionSelectIps.php");

	}

 ?>