<?php 
	session_start();
	include ("../models/conn.php");

	$con = new Conn;

	$con->connect();

	include("../models/pacientes.php");

	$pacientes = new Pacientes;

	$op = $_POST['op'];

	switch ($op) {
		
		case 1: // Lista los pacientes
		
			$response = $pacientes->listPacientes();

			include("../views/pacientes/listado-pacientes.php");
		
		break;

		case 2: // buscar un paciente segun su nombre o su nro de identidad

			
			$id = $_POST['nro_identidad'];
			$nombre = $_POST['nombres'];

			$response = $pacientes->searchPaciente($id, $nombre);

			if (isset($response)) { //valida si el paciente se encuentra en la BD
				
				include("../views/pacientes/busqueda_paciente.php"); //muestra el paciente buscado
				
			}else{ // si no, me vas a retornar a la vista principal con id 0 para alerta

				$data['pt-error'] = "Este Paciente no se encuentra registrado en nuestro sistema.";
				echo $data;
			}


		break;

		case 3: // agregar un paciente

			$nombre = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$email = $_POST['email'];
			$tlf = $_POST['tlf'];
			$direccion = $_POST['direccion'];
			$sexo = $_POST['sexo'];
			$g_sanguineo = $_POST['g_sanguineo'];
			$fecha_nac = $_POST['fecha_nac'];
			$nro_identidad = $_POST['nro_identidad'];


			 if ($_POST != "") { // valida si esta llegando las variables al ctrl
				// var_dump($_POST);
				
				$response = $pacientes->addPaciente($nombre, $apellidos, $fecha_nac, $tlf, $sexo, $g_sanguineo, $direccion, $nro_identidad);

				echo $response;

			 }else{

				header("location: ../add-patient.php?error=1");
			}


		break;

		case 4: // actualizar los datos de un paciente

			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$email = $_POST['email'];
			$tlf = $_POST['tlf'];
			$direccion = $_POST['direccion'];
			$sexo = $_POST['sexo'];
			$g_sanguineo = $_POST['g_sanguineo'];
			$fecha_nac = $_POST['fecha_nac'];
			$nro_identidad = $_POST['nro_identidad'];

			$response = $pacientes->updatePaciente($nombres, $apellidos, $email, $tlf, $direccion, $sexo, $g_sanguineo, $fecha_nac, $nro_identidad);

		break;

		case 5: // eliminar un paciente

			$nro_identidad = $_POST['nro_identidad'];

			$response = $pacientes->deletePaciente($nro_identidad);

		break;

		default:
			# code...
			break;
	}
 ?>