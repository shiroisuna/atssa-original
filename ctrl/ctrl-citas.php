<?php 
	include("../models/conn.php");

	$conn = new Conn;

	$conn->connect();

	include("../models/citas-medicas.php");

	$cita = new CitasMedicas;


	$op = $_POST['op'];

	switch ($op) {
		case 1: // listar las citas medicas
			
			$response = $cita->listCitas();

			include("../views/citas/listado_citas.php");

		break;
		
		case 2: // buscar una cita segun el paciente

			$id_paciente = $_POST['id_pac'];

			$response = $cita->buscarCita($id_paciente);

			echo $response;

		break;

		case 3: //agregar una nueva cita

			$id_paciente = $_POST['id_paciente'];
			$departamento = $_POST['especialidad'];
			$doctor = $_POST['doctor'];
			$fecha_cita = $_POST['fecha_cita'];
			$problema = $_POST['problema'];
			$status = $_POST['status_cita'];

			var_dump($_POST);

			// $response = $cita->addCita($id_paciente, $departamento, $doctor, $fecha_cita, $problema, $status);

			// echo $response;

		break;

		case 4: // actualizar una cita

			$id_paciente = $_POST['id_paciente'];
			$departamento = $_POST['departamento'];
			$doctor = $_POST['doctor'];
			$fecha_cita = $_POST['fecha_cita'];
			$problema = $_POST['problema'];
			$status = $_POST['status'];

			$response = $cita->updateCita($id_paciente, $departamento, $doctor, $fecha_cita, $problema, $status);

			echo $response;

		break;

		default:
			echo "No se ha recibido ningún parámetro.";
		break;
	}

 ?>