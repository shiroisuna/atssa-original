<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/add-payment.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
         <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
            <!-- Header Navbar -->
            <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                        <?php include("menu-left.php"); ?>

            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                        <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>  
                        <h1>Payment</h1>
                        <small>payment list</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Payment</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- Form controls -->
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="btn-group"> 
                                        <a class="btn btn-primary" href="table.html"> <i class="fa fa-list"></i>  Payment List</a>  
                                    </div>
                                </div>
                                <div class="panel-body">
                                 
                                    <form class="col-sm-6" action="ctrl/payment.php" method="post">
                                        <div class="form-group">
                                            <label>Nombre de Cuenta</label>
                                            <input type="text" name="cuenta" id="cuenta" class="form-control" placeholder="Enter Account name" required>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Metodo de Pago</label>
                                            <select class="form-control" name="metodo_pago" id="metodo_pago">
                                                <option value="debito">Tarjeta de debito</option>
                                                <option value="credito">Tarjeta de credito</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>description</label>
                                            <textarea class="form-control" rows="6" required></textarea>
                                        </div>
                                        <div class="form-check">
                                          <label>Status</label><br>
                                          <label class="radio-inline">
                                              <input type="radio" name="status" value="1" checked="checked">Active</label> 
                                              <label class="radio-inline">
                                                  <input type="radio" name="status" value="0" >Inctive</label>  
                                              </div>                                    
                                              <div class="reset-button">
                                                 <!-- <a href="#" class="btn btn-warning">Reset</a>-->
                                                 <input type="submit" class="btn btn-success" name="save" value="Guardar">
                                                 <a href="#" class="btn btn-success">Save</a>
                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </section> <!-- /.content -->
                 </div> <!-- /.content-wrapper -->
                 
            </div> <!-- ./wrapper -->
        <?php include("footer.php"); ?>
        
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/add-payment.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
</html>
