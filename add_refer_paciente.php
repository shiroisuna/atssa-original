
<html>

<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">

        <div class="wrapper">
            
           <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>

                <?php include("menu-top.php"); ?>
            </header>

                    <?php include("menu-left.php"); ?>

            <div class="content-wrapper">

                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                           <!--  <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div> -->
                        </form>  
                        <h1>Pacientes</h1>
                        <small>lista de pacientes</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="btn-group"> 
                                        <a class="btn btn-primary" href="lista_referencia_pt.php"> 
                                            <i class="fa fa-list"></i>  Volver a Lista Pacientes </a>  
                                    </div>
                                </div>

                            <form id="fomulario-referencia-paciente" name="fomulario-referencia-paciente" action="ctrl/referencia-paciente.php" enctype="multipart/form-data" method="post">
                                
                            
                                <div class="panel-body">

                                    <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">

                                          <li class="nav-item">

                                            <a class="nav-link active" id="usuario-tab" data-toggle="tab" href="#usuario" role="tab" aria-controls="usuario" aria-selected="true">Usuario</a>

                                          </li>

                                          <li class="nav-item">

                                            <a class="nav-link" id="acompanante-tab" data-toggle="tab" href="#acompanante" role="tab" aria-controls="acompanante" aria-selected="false">Datos del Acompañante</a>

                                          </li>

                                          <li class="nav-item">

                                            <a class="nav-link" id="domiciliaria-tab" data-toggle="tab" href="#domiciliaria" role="tab" aria-controls="domiciliaria" aria-selected="false">Datos del prestador de Servicio a Atención Domiciliaria</a>

                                          </li>

                                          <li class="nav-item">

                                            <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Plan Domiciliario</a>

                                          </li>

                                          <li class="nav-item">

                                            <a class="nav-link" id="anexos-tab" data-toggle="tab" href="#anexos" role="tab" aria-controls="anexos" aria-selected="false">Documentos Anexos</a>

                                          </li>

                                    </ul>

                                    <div class="tab-content" id="myTabContent">

                                         <div class="tab-pane fade active in" id="usuario" role="tabpanel" aria-labelledby="usuario-tab">
                                            <hr>
                                            <div class="form-group">
                                                    <label>Tipo de Atención</label>
                                                    <select name="t_atencion" id="t_atencion" class="form-control">
                                                        <option value=""></option>
                                                        <option value="Atencion domiciliaria">Atención domiciliaria</option>
                                                        <option value="Urgencia vital">Urgencia vital</option>
                                                        <option value="Gestion del riesgo">Gestión del riesgo</option>
                                                        <option value="Promocion y proteccion especifica">Promoción y protección específica</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label>Nombres</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Enter Name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Apellidos</label>
                                                    <input type="text" name="apellido" id="apellido" class="form-control" placeholder="Enter last Name" required>

                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de Documento de Identidad</label>
                                                    
                                                    <select name="tipo_identidad" id="tipo_identidad" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">R.C.</option>

                                                        <option value="2">T.I.</option>

                                                        <option value="3">C.C.</option>

                                                        <option value="4">C.E.</option>

                                                        <option value="5">P.A.</option>

                                                        <option value="6">C.D.</option>

                                                        <option value="7">S.C.</option>

                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nro Identidad</label>
                                                    <input type="text" id="nro_identidad" name="nro_identidad" class="form-control" placeholder="Ej: x701505-E" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>EPS</label>
                                                    <div id="lista_eps"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Departamento</label><br>
                                                    <div id="select_dpto"></div>

                                                </div>

                                                <div class="form-group">
                                                    <label>Municipio de Origen</label><br>
                                                    <div id="select_munic"></div>

                                                </div>

                                                <div class="form-group">
                                                    
                                                    <label>Municipio de destino</label><br>
                                                    <div id="select_munic_destino"></div>

                                                </div>

                                                <div class="form-group">
                                                    <label>Tlf</label>
                                                    <input type="tel" name="tlf" id="tlf" class="form-control" placeholder="Enter Mobile" required>
                                                </div>

                                        </div>

                                        <div class="tab-pane fade" id="acompanante" role="tabpanel" aria-labelledby="acompanante-tab">
                                                
                                            <hr>
                                            <h4>Datos del acompañante</h4>
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input type="text" id="nombre_acomp" name="nombre_acomp" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tlf</label><br>
                                                <input type="tel" id="tlf_acomp" name="tlf_acomp" class="form-control" placeholder="" required>

                                            </div>
                                            <div class="form-group">
                                                <label>correo</label><br>
                                               <input type="email" id="email_acomp" name="email_acomp" class="form-control" placeholder="" required>

                                            </div>

                                        </div>

                                        <div class="tab-pane fade" id="domiciliaria" role="tabpanel" aria-labelledby="domiciliaria-tab">
                                                
                                            <hr>
                                                <h4>Datos del prestador de Servicio a Atención Domiciliaria</h4>
                                                <div class="form-group">
                                                    <label>Razon Social</label><br>
                                                    
                                                    <div id="select_ips"></div>
                                                    
                                                    <label>NIT</label><br>
                                                    <input type="text" id="nit" name="nit" class="form-control" placeholder="" required>
                                                </div>
                                        </div>

                                        <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
                                                
                                            <hr>
                                                <h4>Plan Domiciliario</h4>
                                                <div class="form-group">
                                                    <label>Valoración Médica</label><br>
                                                    <textarea name="val_medica" class="form-control" id="val_medica" cols="30" rows="10"></textarea>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label>Atención por Enfermería</label><br>
                                                    <textarea name="at_enfermeria" class="form-control" id="at_enfermeria" cols="30" rows="10"></textarea>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label>Servicios de Rehabilitación</label><br>
                                                    <textarea name="serv_rehab" class="form-control" id="serv_rehab" cols="30" rows="10"></textarea>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label>Medicamentos</label><br>
                                                    <textarea name="medicinas" class="form-control" id="medicinas" cols="30" rows="10"></textarea>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label>Insumos</label><br>
                                                    <textarea name="insumos" class="form-control" id="insumos" cols="30" rows="10"></textarea>
                                                    
                                                </div>

                                        </div>

                                        <div class="tab-pane fade" id="anexos" role="tabpanel" aria-labelledby="anexos-tab">
                                                
                                             <hr>
                                                <h4>Documentos Anexos</h4>
                                                <div class="form-group">
                                                    
                                                    <label>Docs</label><br>
                                                    <input type="file" id="" name="archivo" id="archivo" class="form-control" name="" class="form-control" placeholder="" required>
                                                </div>
                                        </div>

                                    </div>

                                    <div class="reset-button">
                                        <input type="hidden" id="op" name="op" value="1">
                                        <a href="#" id="guardar" class="btn btn-success" >Guardar</a>
                                    </div>
                                
                                </div>
                            </form>
                     </div>
                 </div>
             </div>
             
         </section> <!-- /.content -->
     </div> <!-- /.content-wrapper -->
    
</div> <!-- ./wrapper -->
       <?php include("footer.php"); ?>
        

        <script type="text/javascript">

            $(document).ready(function(){

                // Para listar los EPS
                $.post('ctrl/afiliados.php',
                    {
                        op: 6
                    }, function(data){
                        $('#lista_eps').html(data);
                    });
                // listar dptos en colombia
                $.post('ctrl/afiliados.php',
                    {
                        op: 7
                    }, function(data){
                        $('#select_dpto').html(data);
                    
                    });

            });

            function selectmunicipio(id_dpto){

                var id_dpto = id_dpto;
                // lista municipios segun dpto
                $.post('ctrl/afiliados.php',
                    {
                        op: 8,
                        id_dpto: id_dpto
                    }, function(data){
                    $('#select_munic').html(data);
                });

                $.post('ctrl/referencia-paciente.php',
                    {
                        op: 2,
                        id_dpto: id_dpto
                    }, function(data){
                    $('#select_ips').html(data);
                });

                
                
            }

            var verificar = false;
            $( "div#select_munic" ).click(function() {
                $("div#select_munic_destino").empty();
                $("div#select_munic").clone().appendTo("div#select_munic_destino");
                $('div#select_munic_destino div#select_munic select').attr('name', 'select_munic_destino');
                $('div#select_munic_destino div#select_munic select').attr('id', 'select_munic_destino');         
               
            });

            $("div.reset-button #guardar").click(function(){
                //alert('hola');
                $("#fomulario-referencia-paciente").submit();
            });

            $("div#select_dpto select").click(function(){
                alert('hola');
            });

            
        </script>

        
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/add-patient.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
</html>
