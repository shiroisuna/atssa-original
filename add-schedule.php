<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/add-schedule.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
                <!-- Header Navbar -->
                <?php include("menu-top.php"); ?>
                            </header>
                            <!-- =============================================== -->
                            <!-- Left side column. contains the sidebar -->
                            <?php include("menu-left.php"); ?>
                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-note2"></i>
                        </div>
                        <div class="header-title">
                            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>  
                            <h1>Historias</h1>
                            <small>Lista de Historias</small>
                            <ol class="breadcrumb hidden-xs">
                                <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                                <li class="active">Historias</li>
                            </ol>
                        </div>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <!-- Form controls -->
                            <div class="col-sm-12">
                                <div class="panel panel-bd lobidrag">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-primary" href="table.html"> <i class="fa fa-list"></i> Lista de Historia</a>  
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <form >
                                            <div class="row">

                                                <h2 class="fs-title text-center">Agregar Historia</h2><hr>

                                                <div class="col-md-3 text-center">
                                                    <label for="nombre1Hist" >1er Nombre</label>
                                                    <input type="text" name="nombre1Hist" id="nombre1Hist" class="form-control">
                                                </div>

                                                <div class="col-md-3 text-center ">
                                                    <label for="nombre2Hist">2do Nombre</label>
                                                    <input type="text" name="nombre2Hist" id="nombre2Hist" class="form-control">
                                                </div>

                                                <div class="col-md-3 text-center ">
                                                    <label for="apellido1Hist">1er Apellido</label>
                                                    <input type="text" name="apellido1Hist" id="apellido1Hist" class="form-control">
                                                </div>

                                                <div class="col-md-3 text-center ">
                                                    <label for="apellido2Hist">2do Apellido</label>
                                                    <input type="text" name="apellido2Hist" id="apellido2Hist" class="form-control">
                                                </div>

                                            </div>
                                            
                                            <div class="row">

                                                <div class="col-md-3">
                                                    <label for="fechaNacimientoHist">Fecha de Nacimiento</label><br>

                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker10'>
                                                            <input type='text' class="form-control" onblur="calculaEdad()" id="fechaNacimientoHist"/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar">
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                            
                                                </div>


                                                <div class="col-md-3 text-center">
                                                    <label for="edadHist">Edad</label>
                                                    <input type="text" name="edadHist" id="edadHist" class="form-control">
                                                </div>

                                                <div class="col-md-6 text-center ">
                                                    <label for="direccionHist">Dirección</label>
                                                    <input type="text" name="direccionHist" id="direccionHist" class="form-control" />
                                                </div>

                                            </div>

                                            <div class="col-md-12 text-center">
                                                <label for="historiaClinica">Historia Clínicas</label><br>

                                                <textarea name="historiaClinica" id="historiaClinica" cols="100" rows="3"></textarea>
                                            </div>   

                                            <div class="col-md-12 text-center" style="padding-top:20px;">
                                                <!--<input type="submit" name="guardar" value="Guardar">-->
                                                <input type="submit" class="btn btn-primary" type="button" value="Guardar">
                                            </div>
                                            
                                            <!--<div class="form-group">
                                                <label>Doctor Name</label>
                                                <input type="text" class="form-control" placeholder="Doctor Name" required>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Available days</label>
                                                <select class="form-control">
                                                    <option>sunday</option>
                                                    <option>monday</option>
                                                    <option>tuesday</option>
                                                    <option>wednesday</option>
                                                    <option>thrusday</option>
                                                    <option>friday</option>
                                                    <option>saturday</option>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Available time</label>
                                                <input name="date_of_birth" class="datepicker form-control hasDatepicker" type="text" placeholder="available time" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>per patient time</label>
                                                <input name="date_of_birth" class="datepicker form-control hasDatepicker" type="text" placeholder="time">
                                            </div>
                                            <div class="form-group">
                                                <label>Serial visibility</label>
                                                <select class="form-control">
                                                    <option>sequential</option>
                                                    <option>timestamp</option>
                                                </select>
                                            </div>
                                            <div class="form-check">
                                              <label>Status</label><br>
                                              <label class="radio-inline"><input type="radio" name="status" value="1" checked="checked">Active</label> <label class="radio-inline"><input type="radio" name="status" value="0">Inctive</label>                                       
                                          </div>

                                          <div class="reset-button">
                                             <a href="#" class="btn btn-warning">Reset</a>
                                             <a href="#" class="btn btn-success">Save</a>
                                         </div>-->
                                         
                                     </form>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </section> <!-- /.content -->
             </div> <!-- /.content-wrapper -->
            
        </div> <!-- ./wrapper -->
       <?php include("footer.php"); ?>

       <script type="text/javascript">

            $(function () {
                $('#datetimepicker10').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });


            /*funtion que calcula la edad segun su fecha de nacimiento*/

            function calculaEdad() {
                var fechaNacimiento = $('#fechaNacimientoHist').val();

                var dias = "";
                var mes = "";
                var ano = "";

                var longitud = fechaNacimiento.length;

                /*extraigo los dias, meses y año de la cadena como fecha que ingresa el usuario*/
                for (var i = 0 ; i < longitud ; i++) {

                    if( i <= 1) {
                         dias += fechaNacimiento.charAt(i);
                    }

                    if( i > 2 && i <= 4) {
                        mes += fechaNacimiento.charAt(i);
                    }

                    if (i > 5 && i <= 9) {
                        ano += fechaNacimiento.charAt(i);
                    };
                    
                };

                /*Convierto los string a entero*/
                var dias2 = parseInt(dias);
                var mes2 = parseInt(mes) - 1;
                var ano2 = parseInt(ano);

                /*coloco el mes actual*/
                var objetoDate = new Date();
                var mesActual = objetoDate.getMonth();

                if( mesActual == mes2 ) {
                    /*esto me arroja la edad segun el mes de su cumleaños*/
                    edad = 2019 - ano2;
                    $("#edadHist").val(edad);

                }else {
                    /*esta me arroja la edad cuando no ha llehgado al mes de su cumpleaños*/
                    edad = 2019 - ano2 -1;
                    $("#edadHist").val(edad);
                }

            }
        </script>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/add-schedule.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
</html>
