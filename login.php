
<html>

<!-- Mirrored from healthadmin.thememinister.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->
<?php include("header.php"); ?>
<body>
    <!-- Content Wrapper -->
    <div class="login-wrapper">
        <!-- <div class="back-link">
            <a href="index.html" class="btn btn-success">Back to Dashboard</a>
        </div> -->
        <div class="container-center">
            <div class="panel panel-bd">
                <div class="panel-heading">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe-7s-unlock"></i>
                        </div>
                        <div class="header-title">
                            <h3>Acceso</h3>
                            <small><strong>Por favor, introduzca sus credenciales aquí.</strong></small>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form id="loginForm" action="ctrl/access.php" method="post">
                        <div class="form-group">
                            <label class="control-label" for="user">Usuario</label>
                            <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" name="user" id="user" class="form-control">
                            <span class="help-block small">Su usuario es único en la app</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="pass">Clave</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" name="pass" id="pass" class="form-control">
                            <span class="help-block small">Contraseña Fuerte</span>
                        </div>
                        <div>
                            <input type="submit" value="Entrar" class="btn btn-success">
                            <a class="btn btn-warning" href="register.html">Registrate</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <!-- jQuery -->
    <?php include("footer.php"); ?>
</body>

<!-- Mirrored from healthadmin.thememinister.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->
</html>