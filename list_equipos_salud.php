<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:55 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
                <!-- Header Navbar -->
               <?php include("menu-top.php"); ?>
                            </header>
                            <!-- =============================================== -->
                            <!-- Left side column. contains the sidebar -->
                            <?php include("menu-left.php"); ?>
                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-box1"></i>
                        </div>
                        <div class="header-title">
                            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>   
                            <h1>Equipos de Salud</h1>
                            <small>lista de Equipos de Salud</small>
                            <ol class="breadcrumb hidden-xs">
                                <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
                                <li class="active">Equipos de Salud</li>
                            </ol>
                        </div>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-bd lobidrag">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-success" href="add_equipo_voluntario.php"> <i class="fa fa-plus"></i> Agregar Equipo
                                            </a>  
                                        </div>        
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="panel-header">
                                                <div class="col-sm-4 col-xs-12">
                                                    <div class="dataTables_length">
                                                        <label>Display 
                                                            <select name="example_length">
                                                                <option value="10">10</option>
                                                                <option value="25">25</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select> records per page</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <div class="dataTables_length">
                                                         <a class="btn btn-default buttons-copy btn-sm" tabindex="0">
                                                             <span>Copy</span></a>
                                                             <a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0"><span>CSV</span></a>
                                                             <a class="btn btn-default buttons-excel buttons-html5 btn-sm" tabindex="0"><span>Excel</span></a>
                                                             <a class="btn btn-default buttons-pdf buttons-html5 btn-sm" tabindex="0"><span>PDF</span></a>
                                                             <a class="btn btn-default buttons-print btn-sm" tabindex="0"><span>Print</span></a>
                                                             
                                                         </div>
                                                     </div>
                                                     <div class="col-sm-4 col-xs-12">
                                                        <div class="dataTables_length">
                                                            <div class="input-group custom-search-form">
                                                                <input type="search" class="form-control" placeholder="search..">
                                                                <span class="input-group-btn">
                                                                  <button class="btn btn-primary" type="button">
                                                                      <span class="glyphicon glyphicon-search"></span>
                                                                  </button>
                                                              </span>
                                                          </div><!-- /input-group -->
                                                      </div>
                                                  </div>
                                              </div>

                                          </div>

                                          <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>N°</th>
                                                        <th>Nombre</th>
                                                        <th>Profesion</th>
                                                        <th>Ubicación</th>
                                                        <th>Tlf</th>
                                                        <th>Correo</th>
                                                        <th>status </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="voluntarios-table">
                                                    
                                                    <tr >
                                                        
                                                        
                                                    </tr>
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                          
                    <div class="page-nation text-right">
                        <ul class="pagination pagination-large">
                            <li class="disabled"><span>«</span></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li class="disabled"><span>...</span></li><li>
                            <li><a rel="next" href="#">Next</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
</section> <!-- /.content -->



<div id="ordine" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Actualizar Doctor</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                            <a class="btn btn-primary" href="table.php"> <i class="fa fa-list"></i>  Doctor List </a>  
                        </div>
                    </div>
                    <div id="vistaeditdoctores"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" onclick="updateDoctor()">Guardar</button>
            </div>
        </div>
    </div>
</div>




</div> <!-- /.content-wrapper -->
</div> <!-- ./wrapper -->
<!-- ./wrapper -->
       <?php include("footer.php"); ?>
      <script src="assets/doctores.js" type="text/javascript"></script>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
</html>

<script type="text/javascript">
            

    $('docuemnt').ready(function () {  

        $.ajax({
            url: "ctrl/doctores.php",
            type: "POST",
            data: {'op':17}
        }).done(function(respuesta){
            //console.log(JSON.stringify(respuesta));
            var mostrar = '';

            for (let i in respuesta) {
                 console.log(respuesta[i].segundonombre);

                mostrar += '<tr><td><input type="radio" name="radioGroup"><label>'+respuesta[i].id+'</label></td><td>'+respuesta[i].primernombre+' '+ respuesta[i].primerapellido+'</td><td>'+respuesta[i].profesion+'</td><td>'+respuesta[i].direccion+'</td><td>'+respuesta[i].celular+'</td><td><span class="label-success label label-default">Active</span></td><td><button class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button><a class="btn btn-info btn-xs" href="ctrl/doctores.php?id=1&i=14">PDF</a></td></tr>';
            }

            if(mostrar == ''){
                mostrar += '<tr><td><input type="radio" name="radioGroup"><label></label></td><td></td><td></td><td></td><td></td><td><span class="label-success label label-default">Active</span></td><td><button class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button><a class="btn btn-info btn-xs" href="ctrl/doctores.php?id=1&i=14">PDF</a></td></tr>';
            }

            $("tbody#voluntarios-table").html(mostrar);
        });

        

    });


</script>