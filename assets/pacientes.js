$.post('ctrl/ctrl-pacientes.php',
	{
		op: 1
	}, 
	function(data){
    	$('#lista_pacientes').html(data);

	}
);

function savePatient(){

	var nombres = document.getElementById('nombres').value;
	var apellidos = document.getElementById('apellidos').value;
	var email = document.getElementById('email').value;
	var tlf = document.getElementById('tlf').value;
	var direccion = document.getElementById('direccion').value;
	var sexo = document.getElementById('sex').value;
	var g_sanguineo = document.getElementById('g_sang').value;
	var fecha_nac = document.getElementById('fecha_nac').value;
	var nro_identidad = document.getElementById('nro_identidad').value;

	$.post('ctrl/ctrl-pacientes.php',
		{
			op: 3,
			nombres: nombres,
			apellidos: apellidos,
			email: email,
			tlf: tlf,
			direccion: direccion,
			sexo: sexo, 
			g_sanguineo: g_sanguineo,
			fecha_nac: fecha_nac,
			nro_identidad: nro_identidad
			
		}, function(data){

	    if(data){
	    	window.location.href = "pt-list.php";
	    	document.getElementById('alert-pt').style.display = "";
	    }else{
	    	var error = data;
	    	document.getElementById('alert-pt-error').style.display = "";
	    	document.getElementById('alert-pt-error').value = data;
	    }

		});
}