//function que me ayuda a mostrar las graficas de desnutricion de niños y niñas
function mostrarGraficas(){

	var tipoRiesgo = $("#t_riesgo").val();
	var edad = $("#edad").val();
	var sexo =  $("#sex").val();

	if( tipoRiesgo == 5) {

		if (sexo == 2) {

			if (edad < 24 ) {
	        	document.getElementById('niños-0-a-2años').style.display = '';
			}else if(edad > 24) {
				document.getElementById('niños-2-a-5años').style.display = "none"
			}
		}else if( sexo == 1) {
			if (edad < 24 ) {
	        	document.getElementById('niñas-0-a-2años').style.display = '';
			}else if(edad > 24) {
				document.getElementById('niñas-2-a-5años').style.display = "none"
			}
		}	
	}
}


function gestanteMayor35(edad){

	if ( edad < 35) {
        document.getElementById('GestanteMayor35').style.display = 'none';
    }else if( edad > 35){
        document.getElementById('GestanteMayor35').style.display = '';
    }
}

/*esta funcion ayuda oarq eu cuando se selecione tipo de riego de desnutricion el sistema*/
//ya reconozca que al calcular la edad y mostrarla en meses
/*function selecNinoMenor5() {

	var tipoRiesgo = $('#t_riesgo').val();

	if(tipoRiesgo == 5) {
		alert("mostrar edad en meses");
	}
}*/

function calcularEdad(fechaNacimiento, menor5) {
	    var dias = "";
	    var mes = "";
	    var ano = "";

	    var longitud = fechaNacimiento.length;

	                /*extraigo los dias, meses y año de la cadena como fecha que ingresa el usuario*/
	    for (var i = 0 ; i < longitud ; i++) {

	        if( i <= 1) {
	             dias += fechaNacimiento.charAt(i);
	        }

	        if( i > 2 && i <= 4) {
	            mes += fechaNacimiento.charAt(i);
	        }

	        if (i > 5 && i <= 9) {
	            ano += fechaNacimiento.charAt(i);
	        };                    
	    };

	    /*Convierto los string a entero*/
	    var dias2 = parseInt(dias);
	    var mes2 = parseInt(mes);
	    var ano2 = parseInt(ano);

	    /*coloco el mes, dia, año actual*/
	    var objetoDate = new Date();
	    var mesActual = objetoDate.getMonth() + 1//mes
	    var diaAcutal = objetoDate.getDate(); //dia
	    var anoActual = objetoDate.getFullYear() //año

	    /*valido que sea un menor o un adulto*/
	    if (menor5) {

	    	//valido que la edad sea menor o iagual a 5
	    	var anoValidar = anoActual-ano2;

	    	if (anoValidar <= 5) {

	    		if (mes2 != mesActual) {

			    	//edad en meses

			    	var edad = anoActual - ano2 -1;

			    	var convertida = edad*12;

			    	/*calculo meses*/
			    	var meses = (mesActual - mes2) + 12;

			    	//sumo edad en meses mas meses antes del cumpleaños
			    	var edadMeses = convertida + meses;

			    	return edadMeses;
			    	//$("#edad").val(edadMeses);

			    }else {

			    	var nuevaEdad = anoActual - ano2;
			    	var convertida = nuevaEdad*12;

			    	return convertida;
			    	//$("#edad").val(convertida );
			    }

	    	}else {

	    		return false;
	    	}

	    }else{

	    	if( mesActual == mes2 ) {

	       	 	/*esto me arroja la edad segun el mes de su cumleaños*/
	       	 	edad = anoActual - ano2;
	       	 	return edad;

	    	}else {

	       	 	/*esta me arroja la edad cuando no ha llehgado al mes de su cumpleaños*/
	        	edad = anoActual - ano2 -1;
	        	return edad;
	    	}
	    }    
}


function calculaEdadmayor5() {

	var tipoRiesgo = $('#t_riesgo').val();

	if (tipoRiesgo != 5 && tipoRiesgo != 4) {

		var fechaNacimiento = $('#fech_nac').val();
		var edad = calcularEdad(fechaNacimiento, false); //llamo a la function que calcula la edad
		$("#edad").val(edad);
		//$("#edad").prop('disabled', true);

	}else if (tipoRiesgo == 5){
	
		var fechaNacimiento = $('#fech_nac').val();
		var edad = calcularEdad(fechaNacimiento, true); //llamo a la function que calcula la edad
		
		if (edad == false) {
			$('#fech_nac').val('');
			alert("Esa edad no corresponde con el Tipo de riesgo selecionado, Desnutricion es solo para niños menores o iguales a 5.");
			$('#t_doc').trigger('focus');
		}else {
			$("#edad").val(edad);
			$("#meses").html(" (meses) ");
			//$("#edad").prop('disabled', true);
		}
	}else if(tipoRiesgo == 4){
		var fechaNacimiento = $('#fech_nac').val();
		var edad = calcularEdad(fechaNacimiento, false);
		$("#edad").val(edad);
		//$("#edad").prop('disabled', true);
		gestanteMayor35(edad);
	}
}


//esta function ayudara a ver si la femenina esta en estado de embarazo
//esto se activa en el select de sexo
function verEmberazo(){
    var sexo = $('#sex').val();
    var edad = $("#edad").val();
    var tipoRiesgo = $('#t_riesgo').val();

    if (tipoRiesgo == 4) {
    	if (sexo == 1  && edad > 10) {
        document.getElementById('enEstado').style.display= '';
	    }else if (edad < 10 && sexo == 1 || sexo == 2) {
	        document.getElementById('enEstado').style.display= 'none';
	    }

    };
    
}



/*funcion que me ayuda a calular la edad gesatcional

function calculaEdadGestacional() {

	var fechaUltimoPeriodo = $("#fechaUlitmoP").val();

		var dias = "";
	    var mes = "";
	    var ano = "";

	    var longitud = fechaUltimoPeriodo.length;

	    ///*extraigo los dias, meses y año de la cadena como fecha que ingresa el usuario
	    for (var i = 0 ; i < longitud ; i++) {
	        if( i <= 1) {
	             dias += fechaUltimoPeriodo.charAt(i);
	        }
	        if( i > 2 && i <= 4) {
	            mes += fechaUltimoPeriodo.charAt(i);
	        }
	        if (i > 5 && i <= 9) {
	            ano += fechaUltimoPeriodo.charAt(i);
	        };                    
	    };

	    //Convierto los string a entero
	    var dias2 = parseInt(dias);
	    var mes2 = parseInt(mes);
	    var ano2 = parseInt(ano);

	    //año mes dia actual
	    var objetoDate = new Date();
	    var diaActual = objetoDate.getDate();
	    var mesActual = objetoDate.getMonth() + 1;
	    var anoActual = objetoDate.getFullYear();

	    if (dias2 < diaActual) {
	    	var meses =  (mesActual - mes2) + 12 - 1;
	    	var diasDespFechaMes = 30 - diaActual;
	    	var semanas = meses*4;
	    }else {
			//calculo meses si ya paso el dia que cumple cierto mes
			var meses = (mesActual - mes2) + 12;
			var diasDespFechaMes = diaActual - dias2;
			diasDespFechaMes *= -1;
			//llevo los meses a semanas
			var semanas = meses*4;
		}
		$('#edad_embrion').val(semanas + "  semanas, " + diasDespFechaMes + " dias");
		$('#edad_embrion').prop('disabled', true);

}*/