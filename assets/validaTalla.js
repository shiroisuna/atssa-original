//function que me valida de que la edad introducida sea en meses

function validaEdadMeses() {
            
    var edadValidar = $("#edadValidar").prop("checked");
    var edad = $('#edad').val();

    if( edadValidar == true ) {
                
        if (edad >= 1 && edad <= 15) {
        	$('#edad').val('');
            $('#msgEdadMese').html('Tienes que ingresar la edad en meses');
            $('#edad').trigger('focus');
        }
        //valido que la edad sea menor a 5  y hago una conversion
        var  edadConvertidad = edad / 12;

        if ( edadConvertidad > 5){
            $('#edad').val('');
            alert("segun la opcion que seleciono la edad tiene que ser menor o igual a 5, de lo contrario desactive esa opcion");
            $('#edad').trigger('focus');

        }else if( edadConvertidad >= 2 && edadConvertidad <= 5) {
            $('#msgEdadMese').html('');
        }

    }
 }
/*realizo todas la validaciones*/

function validaTalla() {

	var edad = $('#edad').val();
	var talla = $('#altura').val();

	//niño 2 años
	if (edad == 24) {

		if (talla > 76  && talla < 79.3){
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if (talla <= 76 ) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	};

	//niños 2 años y un mes
	if(edad == 25) {

		if (talla > 76.8 && talla < 80) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 76.8) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 26) {

		if (talla > 77.5 && talla < 80.8) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 77.5) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 27) {

		if (talla > 78.1 && talla < 81.5) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 78.1) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 28) {

		if (talla > 78.8 && talla < 82.2) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 78.8) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 29) {

		if (talla > 79.5 && talla < 82.9) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 79.5) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 30) {

		if (talla > 80.1 && talla < 83.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 80.1) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 31) {

		if (talla > 80.7 && talla < 84.3) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 80.7) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 32) {

		if (talla > 81.3 && talla < 84.9) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 81.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 33) {

		if (talla > 81.9 && talla < 85.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 81.9) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 34) {

		if (talla > 82.5 && talla < 86.2) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 82.5) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 35) {

		if (talla > 83.1 && talla < 86.8) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 83.1) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 36) {

		if (talla > 83.6 && talla < 87.4) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 83.6) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 37) {

		if (talla > 84.2 && talla < 88) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 84.2) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 38) {

		if (talla > 84.7 && talla < 88.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 84.7) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 39) {

		if (talla > 85.3 && talla < 89.2) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 85.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 40) {

		if (talla > 85.8 && talla < 89.8) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 85.8) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 41) {

		if (talla > 86.3 && talla < 90.4) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 86.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 42) {

		if (talla > 86.8 && talla < 90.9) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 86.8) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 43) {

		if (talla > 87.4 && talla < 91.5) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 87.4) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 44) {

		if (talla > 87.9 && talla < 92) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 87.9) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 45) {

		if (talla > 88.4 && talla < 92.5) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 88.4) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 46) {

		if (talla > 88.9 && talla < 93.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 88.9) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 47) {

		if (talla > 89.3 && talla < 93.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 89.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 48) {

		if (talla > 89.8 && talla < 94.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 89.8) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 49) {

		if (talla > 90.3 && talla < 94.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 90.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 50) {

		if (talla > 90.7 && talla < 95.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 90.7) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 51) {

		if (talla > 91.2 && talla < 95.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 91.2) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 52) {

		if (talla > 91.7 && talla < 96.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 91.7) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 53) {

		if (talla > 92.1 && talla < 96.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 92.1) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 54) {

		if (talla > 92.6 && talla < 97.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 92.6) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 55) {

		if (talla > 93.0 && talla < 97.6) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 93.0) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 56) {

		if (talla > 93.4 && talla < 98.1) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 93.4) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 57) {

		if (talla > 93.9 && talla < 98.5) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 93.9) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 58) {

		if (talla > 94.3 && talla < 99) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 94.3) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 59) {

		if (talla > 94.7 && talla < 99.5) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 94.7) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

	if(edad == 60) {

		if (talla > 95.2 && talla < 99.9) {
            $.growl.warning({title: "EL PACIENTE TIENE", message: "DEFICIT DE TALLA", duration: 5000 });
		};

		if(talla <= 95.2) {
            $.growl.error({title: "EL PACIENTE TIENE ", message: "UN DEFICIT SEVERO DE TALLA", duration: 5000 });
		};
	}

}