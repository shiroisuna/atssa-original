$(document).ready(function(){

	// Para Listar doctores
	$.post('ctrl/afiliados.php',
		{
			op: 1
		}, function(data){
	    $('#lista_afiliaciones').html(data);

		});
	// Para listar los EPS
	$.post('ctrl/afiliados.php',
		{
			op: 6
		}, function(data){
	    $('#lista_eps').html(data);

		});
	// listar dptos en colombia
	$.post('ctrl/afiliados.php',
		{
			op: 7
		}, function(data){
	    $('#select_dpto').html(data);
		});

});

function editAfiliacion(idafil){
	var id = idafil;

	$.post("ctrl/afiliados.php",
			{
				op: 3, 
				id:id

			}, function(data){

				$('#vistaeditafiliacion').html(data);
				$('#ordine').modal('show');

			}
	);
}


function selectmunicipio(id_dpto){

	var id_dpto = id_dpto;
	// lista municipios segun dpto
	$.post('ctrl/afiliados.php',
		{
			op: 8,
			id_dpto: id_dpto
		}, function(data){
	    $('#select_munic').html(data);
		});

	
}

function select_tipoafil(tipo_afil){

	var tipo_afil = tipo_afil;

	if(tipo_afil == "Individual"){

		document.getElementById('sub_tipo_afil').style.display = "";
	}else{
		document.getElementById('sub_tipo_afil').style.display = "none";
	}

}

// function generaPDF(id_afiliacion){

// 	var id = id_afiliacion;

// 	$.post("ctrl/afiliados.php",
// 			{ 
// 				op: 4,
// 				id:id

// 			}, function(data){
// 				alert(data);
// 			}
// 	);

// }
