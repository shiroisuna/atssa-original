$(document).ready(function(){

	// Para Listar doctores
	$.post("ctrl/doctores.php",
		{
			op: 1
		}, function(data){

			$('#mostrarDoctores').html(data);

		});

	$.post("ctrl/doctores.php",
		{
			op: 8
		}, function(data){

			$('#mostrarCaptPacientes').html(data);

		});

		$.post("ctrl/doctores.php",
			{
				op: 10
				
			}, function(data){

				$('#voluntarios').html(data);
			}
		);
		$.post("ctrl/doctores.php",
			{
				op: 11
				
			}, function(data){

				$('#refer_pacientes').html(data);
			}
		);

});
// Para agregar un doctor
function registrarDoctor(){
	var nom = document.getElementById('nom').value;
	var apell = document.getElementById('apellido').value;
	var email = document.getElementById('email').value;
	// var pass = document.getElementById('pass').value;
	var depto = document.getElementById('departamento').value;
	var direccion = document.getElementById('direccion').value;
	var tlf_cell = document.getElementById('tlf_cell').value;
	var tlf_hab = document.getElementById('tlf_hab').value;
	var sueldo = document.getElementById('sueldo').value;
	var foto = document.getElementById('picture').value;
	var biografia = document.getElementById('biografia').value;
	var f_nac = document.getElementById('f_nac').value;
	// var status = document.getElementById('status').checked;

	$.post("ctrl/doctores.php",
		{
			op: 2,
			nom: nom,
			apell: apell,
			email: email,
			// pass: pass,
			depto: depto,
			direccion: direccion,
			tlf_cell: tlf_cell,
			tlf_hab: tlf_hab,
			sueldo: sueldo,
			foto: foto,
			biografia: biografia,
			f_nac: f_nac,
		}, function(data){
			alert(data);
		});
	
}

// // Para actualizar los datos de un doctor

function modalEditDoctor(iddoctor){

	var id = iddoctor;

	$.post("ctrl/doctores.php",
			{
				op: 3, 
				id:id
			}, function(data){

				$('#vistaeditdoctores').html(data);
				$('#ordine').modal('show');
			}
	);

}

function updateDoctor(){

	var id = document.getElementById('id_doc').value;
	var email = document.getElementById('email').value;
	var depto = document.getElementById('departamento').value;
	var direccion = document.getElementById('direccion').value;
	var tlf_movil = document.getElementById('tlf_cell').value;
	var tlf_casa = document.getElementById('tlf_hab').value;
	var sueldo = document.getElementById('sueldo').value;
	var foto = document.getElementById('picture').value;

	$.post("ctrl/doctores.php",
			{
				op: 4, 
				id: id,
				email: email,
				depto: depto,
				direccion: direccion,
				tlf_movil: tlf_movil,
				tlf_casa:tlf_casa,
				sueldo: sueldo,
				foto: foto

			}, function(data){

				window.location.href = "table.php?update=1";
			}
	);


}

function updateStatus(id_doctor, status){

	var id = id_doctor;
	var status = status;

	$.post("ctrl/doctores.php",
			{
				op: 5, 
				id:id,
				status: status
			}, function(data){

				$.post("ctrl/doctores.php",
					{
						op: 1
					}, function(data){

						$('#mostrarDoctores').html(data);

					}
				);
			}
	);
}




function saveVoluntario(){ // regitrar un voluntario
	var nom1 = document.getElementById('nombre1').value;
	var nom2 = document.getElementById('nombre2').value;
	var apell = document.getElementById('apellido1').value;
	var apell2 = document.getElementById('apellido2').value;
	var tipo_doc = document.getElementById('tipo_identidad').value;
	var nro_doc = document.getElementById('nro_doc').value;
	var profesion = document.getElementById('profesion').value;
	var sexo = document.getElementById('sex').value;
	// var pass = document.getElementById('pass').value;
	var depto = document.getElementById('departamento').value;
	var munic = document.getElementById('direccion').value;
	var tlf = document.getElementById('tlf_hab').value;
	var email = document.getElementById('email').value;
	var f_nac = document.getElementById('f_nac').value;
	// var status = document.getElementById('status').checked;

	$.post("ctrl/doctores.php",
		{
			op: 12,
			nom1: nom1,
			nom2: nom2,
			apell: apell,
			apell2: apell2,
			tipo_doc: tipo_doc,
			nro_doc: nro_doc,
			profesion: profesion,
			sexo: sexo,
			depto: depto,
			munic: munic,
			tlf: tlf,
			email: email,
			f_nac
			
		}, function(data){
			alert(data);
		}
	);
	
}

function saveReferencia(){ // regitrar una referencia
	
	var nombre = document.getElementById("nombre").value;
	var apellido = document.getElementById("apellido").value;
	var tipo_identidad = document.getElementById("tipo_identidad").value;
	var nro_doc = document.getElementById("nro_doc").value;
	var eps = document.getElementById("eps").value;
	var munic_orig = document.getElementById("munic_orig").value;
	var munic_dest = document.getElementById("munic_dest").value;
	var tlf = document.getElementById("tlf").value;
	var nombre_acomp = document.getElementById("nombre_acomp").value;
	var tlf_acomp = document.getElementById("tlf_acomp").value;
	var email_acomp = document.getElementById("email_acomp").value;
	var razon_social = document.getElementById("razon_social").value;
	var nit = document.getElementById("nit").value;
	var val_medica = document.getElementById("val_medica").value;
	var at_enfermeria = document.getElementById("at_enfermeria").value;
	var serv_rehab = document.getElementById("serv_rehab").value;
	var medicinas = document.getElementById("medicinas").value;
	var insumos = document.getElementById("insumos").value;


	$.post("ctrl/doctores.php",
		{
			op: 12,
			nombre: nombre,
			apellido: apellido,
			tipo_identidad: tipo_identidad,
			nro_doc: nro_doc,
			eps: eps,
			munic_orig: munic_orig,
			munic_dest: munic_dest,
			tlf: tlf,
			nombre_acomp: nombre_acomp,
			tlf_acomp: tlf_acomp,
			email_acomp: email_acomp,
			razon_social: razon_social,
			nit: nit,
			val_medica: val_medica,
			at_enfermeria: at_enfermeria,
			serv_rehab: serv_rehab,
			medicinas: medicinas,
			insumos: insumos
			
		}, function(data){
			alert(data);
		}
	);
	
}