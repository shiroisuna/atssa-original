<footer class="main-footer">
        
        <strong>Copyright &copy; 2016-2017 Realizado con todo el corazon por: <a href="#">lacasacreativa.co</a>.</strong> Todos los derechos reservados.
</footer>
<script src="assets/plugins/jQuery/jquery-1.12.4.min.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/plugins/moment/moment.js"></script>

        <!-- jquery-ui --> 
        <script src="assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- lobipanel -->
        <script src="assets/plugins/lobipanel/lobipanel.min.js" type="text/javascript"></script>
        <!-- Pace js -->
        <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
        <!-- Hadmin frame -->
        <script src="assets/dist/js/custom1.js" type="text/javascript"></script>
        <!-- End Core Plugins
        =====================================================================-->
        <!-- Start Page Lavel Plugins
        =====================================================================-->
        <!-- Toastr js -->
        <script src="assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <!-- Sparkline js -->
        <script src="assets/plugins/sparkline/sparkline.min.js" type="text/javascript"></script>
        <!-- Data maps js -->
        <script src="assets/plugins/datamaps/d3.min.js" type="text/javascript"></script>
        <script src="assets/plugins/datamaps/topojson.min.js" type="text/javascript"></script>
        <script src="assets/plugins/datamaps/datamaps.all.min.js" type="text/javascript"></script>
        <!-- Counter js -->
        <script src="assets/plugins/counterup/waypoints.js" type="text/javascript"></script>
        <script src="assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <!-- ChartJs JavaScript -->
        <script src="assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>
        <script src="assets/plugins/emojionearea/emojionearea.min.js" type="text/javascript"></script>
        <!-- Monthly js -->
        <script src="assets/plugins/monthly/monthly.js" type="text/javascript"></script>
        <!-- Data maps -->
        <script src="assets/plugins/datamaps/d3.min.js" type="text/javascript"></script>
        <script src="assets/plugins/datamaps/topojson.min.js" type="text/javascript"></script>
        <script src="assets/plugins/datamaps/datamaps.all.min.js" type="text/javascript"></script>
        <!--libreria para notificaciones-->
        <script type="text/javascript" src="assets/plugins/jquery.growl/js/jquery.growl.js"></script>
        <!--esto es los js para los calendarios
        <script src="assets/plugins/dataTimePicker/jquery.datetimepicker.full.js"></script>-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="assets/validaTalla.js"></script>

        <!-- End Page Lavel Plugins
        =====================================================================-->
        <!-- Start Theme label Script
        =====================================================================-->
        <!-- Dashboard js -->
        <script src="assets/dist/js/custom.js" type="text/javascript"></script>

        <!-- Datapicker js -->
        <script src="assets/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.js"></script>


        <script type="text/javascript">
            

            $('docuemnt').ready(function () {
                var num = 0;
                function ejecutar() {
                    $('span#notification-payment').text(num);
                    num++;
                }   

                function paymentNotification() {
                    $.ajax({
                        url: "ctrl/payment.php",
                        type: "GET"
                    }).done(function(respuesta){
                        if (respuesta.estado === "ok") {
                            console.log(JSON.stringify(respuesta));

                           /* var nombre = respuesta.nombre,
                            apellido = respuesta.apellido,
                            edad = respuesta.edad;*/

                            var td = '';                         
                            var i;
                            var count = 0;
                            
                            if(respuesta.afiliaciones > 0){
                                
                                for (i = 1; i < respuesta.afiliaciones+1; i++) { 
                                    td += '<tr>';
                                    td += '<td>'+i+'</td>';
                                    td += '<td>Afiliación</td>';
                                    td += '</tr>';
                                }


                                $("tbody#payment").html(td);
                                $('span#notification-payment').text(respuesta.afiliaciones);
                            }else{
                                td += '<tr>';
                                td += '<td colspan="2" align="center">Sin datos</td>';
                                td += '</tr>';

                                $("tbody").html(td);
                                $('span#notification-payment').text(count);
                            }
                            
                            //$(".respuesta").html("Servidor:<br><pre>"+JSON.stringify(respuesta, null, 2)+"</pre>");
                        }
                    });
                }

                setInterval(paymentNotification, 5000);
                
            });
</script>