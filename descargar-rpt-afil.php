<?php 

ob_start(); 
include ("rpt-afiliacion.php"); 
$rpt=ob_get_contents(); 
ob_end_clean();  

$nombrearchivo="Reporte-Afiliacion";

require_once __DIR__ . '/mpdf2/vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf([
	'format' => 'Legal',
	'mode' => 'utf-8',
	'orientation' => 'P',
    'setAutoTopMargin' => 'stretch',
    'margin_left' => 5,
	'margin_right' => 5,
	'margin_top' => 10,
	'margin_bottom' => 10,
	'margin_header' => 0,
	'margin_footer' => 0
]);

$html = $rpt;
$mpdf->autoPageBreak = true;
$mpdf->AddPage('P');
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);

$mpdf->Output($nombrearchivo.'.pdf','D'); exit;

?>