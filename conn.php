<?php
/* Conectar a una base de datos de MySQL invocando al controlador */
class Conn {

	function connect(){


		$dsn = 'mysql:dbname=bd_atssa;host=localhost';
		$usuario = 'root';
		$contraseña = '';

		try {
		    $con = new PDO($dsn, $usuario, $contraseña);
		} catch (PDOException $e) {
		    echo 'Falló la conexión: ' . $e->getMessage();
		}

		return $con;
	}

}
?>