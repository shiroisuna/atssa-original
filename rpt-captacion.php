<?php
	ini_set('memory_limit', '256M');

$fecha=date("d/m/Y", strtotime($listCapParams[0]['fecha']));
$codigo =$listCapParams[0]['cod_upgd'];
$nom_upgd = $listCapParams[0]['nom_upgd'];
$nom_evento = $listCapParams[0]['nom_evento'];
$tipo_doc = $listCapParams[0]['t_doc'];
$nro_doc = $listCapParams[0]['nro_identidad'];
$fecha_nac = $listCapParams[0]['fech_nacimiento'];
$edad = $listCapParams[0]['edad'];
$tlf = $listCapParams[0]['tlf'];
$sexo = $listCapParams[0]['sexo'];
$nom1 = $listCapParams[0]['nombre1'];
$nom2 = $listCapParams[0]['nombre2'];
$apell1 = $listCapParams[0]['apellido1'];
$apell2 = $listCapParams[0]['apellido2'];
$dpto = $listCapParams[0]['dpto_res'];
$munic = $listCapParams[0]['mun_res'];
$direccion = $listCapParams[0]['dir_res'];
$localidad = $listCapParams[0]['location_res'];
$barrio = $listCapParams[0]['barrio_res'];
$otro = $listCapParams[0]['otro_barrio'];
$etnia = $listCapParams[0]['pers_etnica'];
$g_poblacional = $listCapParams[0]['grupo_poblacional'];
$estrato = $listCapParams[0]['estrato'];
$t_aseg = $listCapParams[0]['t_aseg'];
$n_educativo = $listCapParams[0]['nivel_educativo'];
$edo_civil = $listCapParams[0]['edo_civil'];
$peso = $listCapParams[0]['peso'];
$estatura = $listCapParams[0]['estatura'];
$imc = $listCapParams[0]['imc'];

// Analisis Riesgos
// var_dump($list_riesgosCard);
if ($list_riesgosCard == NULL) {
	$tipo_riesgo = '';
	$cigarrillo = '';
	$antescedente_familiar = '';
	$familiar = '';
	$hta = '';
	$irc = '';
	$dislipidemia = '';
	$diabetes_dm2 = '';
	$glucosa = '';
	$sedentarismo = '';
	$insuf_card = '';
	$insuf_card_chag = '';
	$ar_cardiacas = '';
	$infartos_previos = '';
	$antesc_cerebvas = '';
	$tep_tvp = '';
	$coagulopatias = '';
}else{

	$tipo_riesgo = $list_riesgosCard[0]['tipo_de_riesgo'];
	$cigarrillo = $list_riesgosCard[0]['cigarrillo'];
	$antescedente_familiar = $list_riesgosCard[0]['antescedente_familiar'];
	$hta = $list_riesgosCard[0]['hta'];
	$irc = $list_riesgosCard[0]['irc'];
	$dislipidemia = $list_riesgosCard[0]['dislipidemia'];
	$diabetes_dm2 = $list_riesgosCard[0]['diabetes_dm2'];
	$glucosa = $list_riesgosCard[0]['glucosa'];
	$sedentarismo = $list_riesgosCard[0]['sedentarismo'];
	$insuf_card = $list_riesgosCard[0]['insuf_card'];
	$insuf_card_chag = $list_riesgosCard[0]['insuf_card_chag'];
	$ar_cardiacas = $list_riesgosCard[0]['ar_cardiacas'];
	$infartos_previos = $list_riesgosCard[0]['infartos_previos'];
	$antesc_cerebvas = $list_riesgosCard[0]['antesc_cerebvas'];
	$tep_tvp = $list_riesgosCard[0]['tep_tvp'];
	$coagulopatias = $list_riesgosCard[0]['coagulopatias'];

	if ($cigarrillo == "on" && $antescedente_familiar == "on" && $hta == "on" && $irc == "on" && $dislipidemia == "on" && $diabetes_dm2 == "on" && $glucosa == "on" && $sedentarismo == "on" && $insuf_card == "on" && $insuf_card_chag == "on" && $ar_cardiacas == "on" && $infartos_previos == "on" && $antesc_cerebvas == "on" && $tep_tvp == "on" && $coagulopatias == "on") {
		$cigarrillo = "SI";
		$antescedente_familiar = "SI";
		$hta = "SI";
		$irc = "SI";
		$dislipidemia = "SI";
		$diabetes_dm2 = "SI";
		$glucosa = "SI";
		$sedentarismo = "SI";
		$insuf_card = "SI";
		$insuf_card_chag = "SI";
		$ar_cardiacas = "SI";
		$infartos_previos = "SI";
		$antesc_cerebvas = "SI";
		$tep_tvp = "SI";
		$coagulopatias = "SI";
	}else{
		$cigarrillo = "NO";
		$antescedente_familiar = "NO";
		$hta = "NO";
		$irc = "NO";
		$dislipidemia = "NO";
		$diabetes_dm2 = "NO";
		$glucosa = "NO";
		$sedentarismo = "NO";
		$insuf_card = "NO";
		$insuf_card_chag = "NO";
		$ar_cardiacas = "NO";
		$infartos_previos = "NO";
		$antesc_cerebvas = "NO";
		$tep_tvp = "NO";
		$coagulopatias = "NO";
	}


	
}

if($list_riesgosSalud == NULL){

	$cons_psicoactivas = "";
	$antesc_tras_psiquis = "";
	$esquizofrenia = "";
	$antes_cond_suicid = "";
	$tras_depresivo = "";
	$antes_violencia = "";
	$tras_personalidad = "";
	$abuso_alcohol = "";
	$tras_bipolar = "";
	$estres_posttr = "";
	$plan_suicida = "";
}else{

	$cons_psicoactivas = $list_riesgosSalud[0]['cons_psicoactivas'];
	$antesc_tras_psiquis = $list_riesgosSalud[0]['antesc_tras_psiquis'];
	$esquizofrenia = $list_riesgosSalud[0]['esquizofrenia'];
	$antes_cond_suicid = $list_riesgosSalud[0]['antes_cond_suicid'];
	$tras_depresivo = $list_riesgosSalud[0]['tras_depresivo'];
	$antes_violencia = $list_riesgosSalud[0]['antes_violencia'];
	$tras_personalidad = $list_riesgosSalud[0]['tras_personalidad'];
	$abuso_alcohol = $list_riesgosSalud[0]['abuso_alcohol'];
	$tras_bipolar = $list_riesgosSalud[0]['tras_bipolar'];
	$estres_posttr = $list_riesgosSalud[0]['estres_posttr'];
	$plan_suicida = $list_riesgosSalud[0]['plan_suicida'];

	if($cons_psicoactivas == "on" && $antesc_tras_psiquis == "on" && $esquizofrenia == "on" && $antes_cond_suicid == "on" && $tras_depresivo == "on" && $antes_violencia == "on" && $tras_personalidad == "on" && $abuso_alcohol == "on" && $tras_bipolar == "on" && $estres_posttr == "on" && $plan_suicida == "on"){

		$cons_psicoactivas = "SI";
		$antesc_tras_psiquis = "SI";
		$esquizofrenia = "SI";
		$antes_cond_suicid = "SI";
		$tras_depresivo = "SI";
		$antes_violencia = "SI";
		$tras_personalidad = "SI";
		$abuso_alcohol = "SI";
		$tras_bipolar = "SI";
		$estres_posttr = "SI";
		$plan_suicida = "SI";

	}else{

		$cons_psicoactivas = "NO";
		$antesc_tras_psiquis = "NO";
		$esquizofrenia = "NO";
		$antes_cond_suicid = "NO";
		$tras_depresivo = "NO";
		$antes_violencia = "NO";
		$tras_personalidad = "NO";
		$abuso_alcohol = "NO";
		$tras_bipolar = "NO";
		$estres_posttr = "NO";
		$plan_suicida = "NO";

	}

}


if($list_riesgosInfecc == NULL){

	$vih = "";
	$hepa_a = "";
	$sifilis = "";
	$esputar = "";
	$rayosx_esputos = "";
	$antes_contacto = "";
	$sintomas_b = "";
	$leishmaniasis = "";
	$dengue = "";
	$malaria = "";
	$chagas = "";
}else{

	$vih = $list_riesgosInfecc[0]['vih'];
	$hepa_a = $list_riesgosInfecc[0]['hepa_a'];
	$sifilis = $list_riesgosInfecc[0]['sifilis'];
	$esputar = $list_riesgosInfecc[0]['esputar'];
	$rayosx_esputos = $list_riesgosInfecc[0]['rayosx_esputos'];
	$antes_contacto = $list_riesgosInfecc[0]['antes_contacto'];
	$sintomas_b = $list_riesgosInfecc[0]['sintomas_b'];
	$leishmaniasis = $list_riesgosInfecc[0]['leishmaniasis'];
	$dengue = $list_riesgosInfecc[0]['dengue'];
	$malaria = $list_riesgosInfecc[0]['malaria'];
	$chagas = $list_riesgosInfecc[0]['chagas'];

	if($vih == "on" && $hepa_a == "on" && $sifilis == "on" && $esputar == "on" && $rayosx_esputos == "on" && $antes_contacto == "on" && $sintomas_b == "on" && $leishmaniasis == "on" && $dengue == "on" && $malaria == "on" && $chagas == "on"){

		$vih = "SI";
		$hepa_a = "SI";
		$sifilis = "SI";
		$esputar = "SI";
		$rayosx_esputos = "SI";
		$antes_contacto = "SI";
		$sintomas_b = "SI";
		$leishmaniasis = "SI";
		$dengue = "SI";
		$malaria = "SI";
		$chagas = "SI";

	}else{

		$vih = "NO";
		$hepa_a = "NO";
		$sifilis = "NO";
		$esputar = "NO";
		$rayosx_esputos = "NO";
		$antes_contacto = "NO";
		$sintomas_b = "NO";
		$leishmaniasis = "NO";
		$dengue = "NO";
		$malaria = "NO";
		$chagas = "NO";

	}

}


if($list_riesgosGestante == NULL){

	$n_controls_gest = "";
	$edad_embrion = "";
	$gest_mayor = "";
	$preeclamsia = "";
	$hta_gest = "";
	$eclampsia = "";
	$sobrepeso_gest = "";
	$leishmaniasis = "";
	$obeso_gest = "";
	$toxoplasmosis = "";
	$sifilis_gest = "";
	$vih_gest = "";
	$hepatitis_b_gest = "";
	$emb_gemelos = "";
	$hipotiroidismo = "";
	$parto_prematuro = "";
	$cardio_madre = "";
	$drogas_alcohol = "";
	$malform_feto = "";
	$placenta_previa = "";
	$anemia = "";
	$presen_transv = "";
	$p_podalica = "";


}else{

	$n_controls_gest = $list_riesgosGestante[0]['n_controls_gest'];
	$edad_embrion = $list_riesgosGestante[0]['edad_embrion'];
	$gest_mayor = $list_riesgosGestante[0]['gest_mayor'];
	$preeclamsia = $list_riesgosGestante[0]['preeclamsia'];
	$hta_gest = $list_riesgosGestante[0]['hta_gest'];
	$eclampsia = $list_riesgosGestante[0]['eclampsia'];
	$sobrepeso_gest = $list_riesgosGestante[0]['sobrepeso_gest'];
	$leishmaniasis = $list_riesgosGestante[0]['leishmaniasis_gest'];
	$obeso_gest = $list_riesgosGestante[0]['obeso_gest'];
	$toxoplasmosis = $list_riesgosGestante[0]['toxoplasmosis'];
	$sifilis_gest = $list_riesgosGestante[0]['sifilis_gest'];
	$vih_gest = $list_riesgosGestante[0]['vih_gest'];
	$hepatitis_b_gest = $list_riesgosGestante[0]['hepatitis_b_gest'];
	$emb_gemelos = $list_riesgosGestante[0]['emb_gemelos'];
	$hipotiroidismo = $list_riesgosGestante[0]['hipotiroidismo'];
	$parto_prematuro = $list_riesgosGestante[0]['parto_prematuro'];
	$cardio_madre = $list_riesgosGestante[0]['cardio_madre'];
	$drogas_alcohol = $list_riesgosGestante[0]['drogas_alcohol'];
	$malform_feto = $list_riesgosGestante[0]['malform_feto'];
	$placenta_previa = $list_riesgosGestante[0]['placenta_previa'];
	$anemia = $list_riesgosGestante[0]['anemia'];
	$presen_transv = $list_riesgosGestante[0]['presen_transv'];
	$p_podalica = $list_riesgosGestante[0]['p_podalica'];
	$rubeola = $list_riesgosGestante[0]['rubeola'];
	$estreptococo = $list_riesgosGestante[0]['estreptococo'];
	$citomegalovirus = $list_riesgosGestante[0]['citomegalovirus'];
	$diabetes_prege = $list_riesgosGestante[0]['diabetes_prege'];
	$diabetes_ge = $list_riesgosGestante[0]['diabetes_ge'];


	if($n_controls_gest == "on" && $edad_embrion == "on" && $gest_mayor == "on" && $preeclamsia == "on" && $hta_gest == "on" && $eclampsia == "on" && $sobrepeso_gest == "on" && $leishmaniasis == "on" && $obeso_gest == "on" && $toxoplasmosis == "on" && $sifilis_gest == "on" && $vih_gest == "on" && $hepatitis_b_gest == "on" && $emb_gemelos == "on" && $hipotiroidismo == "on" && $parto_prematuro == "on" && $cardio_madre == "on" && $drogas_alcohol == "on" && $malform_feto == "on" && $placenta_previa == "on" && $anemia == "on" && $presen_transv == "on" && $p_podalica == "on" && $rubeola == "on" && $estreptococo == "on" && $citomegalovirus == "on" && $diabetes_prege == "on" && $diabetes_ge == "on"){

		$n_controls_gest = "SI";
		$edad_embrion = "SI";
		$gest_mayor = "SI";
		$preeclamsia = "SI";
		$hta_gest = "SI";
		$eclampsia = "SI";
		$sobrepeso_gest = "SI";
		$leishmaniasis = "SI";
		$obeso_gest = "SI";
		$toxoplasmosis = "SI";
		$sifilis_gest = "SI";
		$vih_gest = "SI";
		$hepatitis_b_gest = "SI";
		$emb_gemelos = "SI";
		$hipotiroidismo = "SI";
		$parto_prematuro = "SI";
		$cardio_madre = "SI";
		$drogas_alcohol = "SI";
		$malform_feto = "SI";
		$placenta_previa = "SI";
		$anemia = "SI";
		$presen_transv = "SI";
		$p_podalica = "SI";
		$rubeola = "SI";
		$estreptococo = "SI";
		$citomegalovirus = "SI";
		$diabetes_prege = "SI";
		$diabetes_ge = "SI";


	}else{

		$n_controls_gest = "NO";
		$edad_embrion = "NO";
		$gest_mayor = "NO";
		$preeclamsia = "NO";
		$hta_gest = "NO";
		$eclampsia = "NO";
		$sobrepeso_gest = "NO";
		$leishmaniasis = "NO";
		$obeso_gest = "NO";
		$toxoplasmosis = "NO";
		$sifilis_gest = "SI";
		$vih_gest = "NO";
		$hepatitis_b_gest = "NO";
		$emb_gemelos = "NO";
		$hipotiroidismo = "NO";
		$parto_prematuro = "NO";
		$cardio_madre = "NO";
		$drogas_alcohol = "NO";
		$malform_feto = "NO";
		$placenta_previa = "NO";
		$anemia = "NO";
		$presen_transv = "NO";
		$p_podalica = "NO";
		$rubeola = "NO";
		$estreptococo = "NO";
		$citomegalovirus = "NO";
		$diabetes_prege = "NO";
		$diabetes_ge = "NO";


	}

}

if($list_riesgosCancer == NULL){

	$cancer_mama = "";
	$cancer_cervix = "";
	$cancer_prostata = "";
	
}else{

	$cancer_mama = $list_riesgosCancer[0]['cancer_mama'];
	$cancer_cervix = $list_riesgosCancer[0]['cancer_cervix'];
	$cancer_prostata = $list_riesgosCancer[0]['cancer_prostata'];
	

	if($cancer_mama == "on" && $cancer_cervix == "on" && $cancer_prostata == "on"){


		$cancer_mama = "SI";
		$cancer_cervix = "SI";
		$cancer_prostata = "SI";
		

	}else{

		$cancer_mama = "NO";
		$cancer_cervix = "NO";
		$cancer_prostata = "NO";

	}

}


$rpt='';
$rpt.="<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>

	<style type='text/css'>
		body{
			font-family: 'Open Sans', sans-serif;
			margin-right:0;
			margin-left:0;
			width: 100%;
		}

		td{
			padding: 0;
		}

		.border {
		    border: 1px solid gray;
		    border-radius: 3px;
		}

		.bl {
			border-left: 1px solid gray;
		}

		.br {
			border-right: 1px solid gray;
		}

		.bt {
			border-top: 1px solid gray;
		}

		.pp1{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 8px;
			font-size: 10px;
		}

		.pp2{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 5px;
		}

		.pp3{
			margin-top: 0;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 7px;
		}

		.pp4{
			margin-top: 3px;
			margin-bottom: 2px;
			font-weight: 400;
			font-size: 10px;
			line-height: 20px;
		}
		.pp5{
			margin-top: 3px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.pp6{
			margin-top: 70px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.h2{
			font-size: 13px;
			line-height: 13px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.h1{
			font-size: 18px;
			line-height: 14px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.pt-20{
			padding-top: -25px;
		}

		.pb-20{
			padding-bottom: -10px;
		}

		.pb-10{
			padding-bottom: 10px;
		}

		.mb-20{
			margin-bottom: 20px;
		}

		.mb-10{
			margin-bottom: 10px;
		}

		.mb-15{
			margin-bottom: 8px;
		}

		.mt-20{
			margin-top:20px;
		}

		.mt-15{
			margin-top:15px;
		}

		.logomin{
			max-width: 100px;
			margin-left:auto;
			margin-right:auto;
		}

		.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-0,.col-xs-1-5 {
		  float: left;
		  position: relative;
		  min-height: 1px;
		}

		.col-xs-12 {
		  width: 100%;
		}
		.col-xs-11 {
		  width: 91.66666667%;
		}
		.col-xs-10 {
		  width: 83.33333333%;
		}
		.col-xs-9 {
		  width: 75%;
		}
		.col-xs-8 {
		  width: 66.66666667%;
		}
		.col-xs-7 {
		  width: 58.33333333%;
		}
		.col-xs-6 {
		  width: 50%;
		}
		.col-xs-5 {
		  width: 41.66666667%;
		}
		.col-xs-4 {
		  width: 33.33333333%;
		  width: 30%;
		}
		.col-xs-3 {
		  width: 20%;
		}
		.col-xs-2 {
		  width: 15%;
		}

		.col-xs-1-5 {
		  width: 11%;
		}
		.col-xs-1 {
		  width: 8.33333333%;
		}

		.col-xs-0 {
		  width: 5%;
		}

		.p-5{
			padding: 5px;
		}

		.text-center{
			text-align: center;
		}

		.text-right{
			text-align: right;
		}

		.title1{
			font-size: 7px;
			margin-top:0;
			margin-bottom:5px;
		}

		.title3{
			font-size: 12px;
			margin-top:0;
			margin-bottom:2px;
		}

		.autorizacion{
			font-size: 8px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.title2{
			font-size: 7px;
			margin-top:0;
			margin-bottom:0;
		}

		.capi{
			text-transform: capitalize;
		}

		.cortext{
			 text-overflow:ellipsis;
			  white-space:nowrap; 
			  overflow:hidden; 
			  max-width: 100%;
		}
	</style>
	<body>
	<div class='col-xs-12'>
			<div class='col-xs-4 text-center '>
				<p class='pp1'>REPUBLICA DE COLOMBIA</p>
				<p class='pp2'>SISTEMA GENERAL DE SEGURIDAD SOCIAL EN SALUD</p>
				<p class='pp2'>SUPERINTENDENCIA NACIONAL DE SALUD</p>
				<!-- <p class='pp3 mt-15'>'Código Únicamente para dependientes y afiliaciones al Régimen Subsidiado'</p> -->
			</div>

			<div class='col-xs-5 text-center'>
				<h2 class='h2'>FORMULARIO ÚNICO DE Captación de Pacientes con Riesgo</h2>
			</div>

			<div class='col-xs-3 text-right'>
				<img class='logomin' src='../assets/img/logo-atssa.jpg' alt=''>
				<!-- <h2>LOGO</h2> -->
			</div>
	</div>

	<div class='col-xs-12 pt-20 pb-10 text-center'>

		<h1 class='h1'>FORMULARIO ELECTRÓNICO</h1>
	</div>

	<div class='col-xs-12 border mb-15'>
		<h6 class='title1' align='center'>INFORMACION GENERAL</h6>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12 p-5'>
			<div class='col-xs-3 p-5'>
				<h6 class='title1'>Fecha</h6>
				<p class='pp3 cortext'>".$fecha."</p>
			</div>
			<div class='col-xs-8 p-5 bl'>
				<div class='col-xs-5 p-5'>
					<h6 class='title1'>Codigo UPGD ".$codigo."</h6>
					
				</div>
				<div class='col-xs-6 p-5'>
					<h6 class='title1'>Nombre UPGD ".$nom_upgd."</h6>
					
				</div>
				<div class='col-xs-12 p-5'>
					<h6 class='title1'>Nombre del Evento ".$nom_evento."</h6>
					
				</div>
			</div>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<h6 class='title1' align='center'>C.N.V. CERTIFICADO NACIDO VIVO | R.C. REGISTRO CIVIL | T.I. TARJETA DE IDENTIDAD | C.C. CEDULA DE CIUDADANÍA | C.E. CEDULA DE EXTRANJERÍA | P.A. PASAPORTE | M.S. MENOR SIN ID | A.I. ADULTO SIN ID</h6>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<div class='col-xs-5 p-5 bl'>
				<h6 class='title1'>Tipo de Doc ".$tipo_doc."</h6>
				
			</div>
			
			<div class='col-xs-6 p-5 bl'>
				<h6 class='title1'>N° de Identificación ".$nro_doc."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5 bl'>
				<h6 class='title1'>Primer Nombre: ".$nom1."</h6>
			</div>
			
			<div class='col-xs-6 p-5 bl'>
				<h6 class='title1'>Segundo Nombre ".$nom2."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Primer Apellido ".$apell1."</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Segundo Apellido ".$apell2."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Depatamento de Residencia ".$dpto."</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Municipio de Residencia ".$munic."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Direccion de Residencia ".$direccion."</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Localidad de Residencia ".$localidad."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Barrio de Residencia ".$barrio."</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Otro ".$otro."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Fecha de Nacimiento ".$fecha_nac."</h6>
				
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>Edad ".$edad."</h6>
				
			</div>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>Tlf ".$tlf."</h6>
				
			</div>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>Sexo ".$sexo."</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>Estado Civil:</h6>
				<p class='pp3 cortext'>".$edo_civil."</p>
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Pertenencia Etnica:</h6>
				<p class='pp3 cortext'>".$etnia."</p>
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Grupo Poblacional:</h6>
				<p class='pp3 cortext'>".$g_poblacional."</p>
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>Estrato:</h6>
				<p class='pp3 cortext'>".$estrato."</p>
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Tipo de Aseguramiento:</h6>
				<p class='pp3 cortext'>".$t_aseg."</p>
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Nivel Educativo:</h6>
				<p class='pp3 cortext'>".$n_educativo."</p>
			</div>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<h6 class='title1' align='center'>INFORMACION MEDICA DEL PACIENTE</h6>
	</div>

	<div class='col-xs-12 border mb-15'>
		
		<div class='col-xs-12'>

			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Peso ".$peso."</h6>
				
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>Estatura ".$estatura."</h6>
				
			</div>
			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>IMC ".$imc."</h6>
				
			</div>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<h5 class='title1'>Factor de Riesgo </h5>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<h5 class='title1'>Riesgo Cardiovascular</h5>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Consume cigarrillo ".$cigarrillo."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Antecedente familiar ".$antescedente_familiar."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>HTA (Hipertensión arterial) ".$hta."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>IRC (Insuficiencia renal) ".$irc."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Dislipidemia ".$dislipidemia."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>DM2 (Diabetes mellitus 2) ".$diabetes_dm2."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Glucometría > 200 mg/dl  ".$glucosa."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Sedentarismo ".$sedentarismo."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Antescedente de Insuficiencia cardíaca ".$insuf_card."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Insuficiencia cardíaca chagásica ".$insuf_card_chag."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Arritmias cardíacas ".$ar_cardiacas."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Antecedente de infartos previa ".$infartos_previos."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Antecedente de accidente cerebrovasculares ".$antesc_cerebvas."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 c3rtext'>Antecedente de TEP (Tromboembolismo pulmonar) o TVP (Trombosis venosa profunda) o Uso de anticoagulantes.  ".$tep_tvp."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Coagulopatias ".$coagulopatias."</p>
			</div>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<h5 class='title1'>Riesgo de Salud Mental</h5>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Consumo de sustancias psicoactivas ".$cons_psicoactivas."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Antescedente de trastorno psiquiátricos ".$antesc_tras_psiquis."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Esquizofrenia ".$esquizofrenia."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Antescedentes familiares de conducta suicida ".$antes_cond_suicid."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Trastorno depresivo ".$tras_depresivo."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Antescedente de violencia ".$antes_violencia."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Trastorno de personalidad  ".$tras_personalidad."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Abuso de alcohol ".$abuso_alcohol."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Trastorno bipolar ".$tras_bipolar."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Estrés postraumático ".$estres_posttr."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Plan organizado suicida ".$plan_suicida."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Otro  </p>
			</div>
		</div>
	</div>

	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<h5 class='title1'>Riesgo Infectocontagioso</h5>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>VIH: Serología positiva ".$vih."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Hepatitis exceptuando A: Anticuerpo positivos ".$hepa_a."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Sífilis: VDRL positivo ".$sifilis."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Esputo seriado positivo o negativo ".$esputar."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Radiografía de tórax con hallazgos sugestivos de TBC ".$rayosx_esputos."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Antescedente de contacto ".$antes_contacto."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Síntomas B  ".$sintomas_b."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Leishmaniasis ".$leishmaniasis."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Dengue ".$dengue."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Malaria ".$malaria."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Chagas ".$chagas."</p>
			</div>
			
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<h5 class='title1'>Riesgo en Gestante</h5>
		</div>
		<div class='col-xs-12'>

			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Número de controles prenatales ".$n_controls_gest."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Edad gestacional ".$edad_embrion."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Gestante > 35 años ".$gest_mayor."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Preeclampsia ".$preeclamsia."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Hipertensión gestacional ".$hta_gest."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Eclampsia ".$eclampsia."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Sobrepeso  ".$sobrepeso_gest."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Obesidad ".$obeso_gest."</p>
			</div>
			<div class='col-xs-3 '>
				<p class='pp3 cortext'>Toxoplasmosis ".$toxoplasmosis."</p>
			</div>
			<div class='col-xs-2 '>
				<p class='pp3 cortext'>Sífilis ".$sifilis_gest."</p>
			</div>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>VIH ".$vih_gest."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Hepatitis B ".$hepatitis_b_gest."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Citomegalovirus ".$citomegalovirus."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Rubéola ".$rubeola."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Estreptococo beta ".$estreptococo."</p>
			</div>
			
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Leishmaniasis concomitante ".$leishmaniasis."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Diabetes pregestacional ".$diabetes_prege."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Diabetes gestacional ".$diabetes_ge."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Embarazo gemelar o múltiple ".$emb_gemelos."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Hipotiroidismo ".$hipotiroidismo."</p>
			</div>
			
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Amenaza de parto prematuro ".$parto_prematuro."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Cardiopatías en la madre ".$cardio_madre."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Drogadicción y/o alcoholismo ".$drogas_alcohol."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Malformación fetal confirmada ".$malform_feto."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Placenta previa  ".$placenta_previa."</p>
			</div>
			
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Anemia ".$anemia."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Presentación transversa ".$presen_transv."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<p class='pp3 cortext'>Presentación podálica completa o incompleta. ".$p_podalica."</p>
			</div>
			
		</div>
	</div>
	
	
	</body>
</html>";

	// var_dump($rpt);

		$nombrearchivo='Reporte-Captacion-Paciente-con-Riesgo';

		require_once __DIR__ . '/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'Legal',
			'mode' => 'utf-8',
			'orientation' => 'P',
		    'setAutoTopMargin' => 'stretch',
		    'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 10,
			'margin_header' => 0,
			'margin_footer' => 0
		]);

		$html = $rpt;
		$mpdf->autoPageBreak = true;
		$mpdf->AddPage('P');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		$mpdf->Output($nombrearchivo.'.pdf','I'); 
		exit;
?>

