/*
SQLyog Professional v12.5.1 (32 bit)
MySQL - 10.1.33-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `equipos_de_salud` (
	`id` int (11),
	`primernombre` varchar (300),
	`segundonombre` varchar (300),
	`primerapellido` varchar (300),
	`segundoapellido` varchar (300),
	`tipo_identidad` varchar (300),
	`nro_identidad` varchar (300),
	`profesion` varchar (300),
	`departamento` varchar (300),
	`municipio` varchar (300),
	`celular` varchar (300),
	`Whatsapp` varchar (300),
	`email` varchar (300),
	`direccion` varchar (300),
	`horario` varchar (300)
); 
