    <?php session_start(); ?>

    <html>
    <?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">        
    <!-- Site wrapper -->
    <div class="wrapper">
    <header class="main-header">
    <a href="index.html" class="logo"> <!-- Logo -->
    <span class="logo-mini">
    <!--<b>A</b>H-admin-->
    <img src="assets/dist/img/mini-logo.png" alt="">

    </span>
    <span class="logo-lg">
    <!--<b>Admin</b>H-admin-->
    <h4>ATSSA</h4>
    <!-- <img src="assets/dist/img/logo.png" alt=""> -->
    </span>
    </a>
    <!-- Header Navbar -->
    <?php include("menu-top.php"); ?>
    </header>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <?php include("menu-left.php"); ?>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="header-icon">
    <i class="pe-7s-note2"></i>
    </div>
    <div class="header-title">
    <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
    <div class="input-group">
    <input type="text" name="q" class="form-control" placeholder="Search...">
    <span class="input-group-btn">
    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
    </span>
    </div>
    </form>  
    <h1>Captación de Paciente Con Riesgo</h1>
    <small>Formulario Captación</small>
    <ol class="breadcrumb hidden-xs">
    <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
    <li class="active">Dashboard</li>
    </ol>
    </div>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <!-- Form controls -->
    <?php if (isset($_GET['error'])==1) { ?>

    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    Algunos Campos estan incorrectos, verifique.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>

    <?php } ?>
    <div class="col-lg-12">
    <div class="panel panel-bd lobidrag">
    <div class="panel-heading">
    <div class="btn-group"> 
    <a class="btn btn-primary" href="afiliaciones.php"> <i class="fa fa-return"></i>  Volver </a>  
    </div>
    </div>
    <div class="panel-body">

    <form id="msform" method="post" action="ctrl/doctores.php">
    <!-- <form id="msform" method="post" onsubmit="return crearAfiliacion();"> -->
    <input type="hidden" id="op" name="op" value="7">
    <ul id="progressbar">
    <li class="active">INFORMACION GENERAL</li>
    <li>IDENTIFICACION DEL PACIENTE</li>
    <li>DOCUMENTACION</li>


    </ul>
    <fieldset>
    <h2 class="fs-title">Información General</h2>
    <div class="row">
    <div class="col-md-12">
    <!--<div class="col-md-4">
    <label for="fecha_format">Fecha de Notificación</label>
    <input type="date" id="fecha_format" name="fecha_format" class="form-control">
    </div>-->
    <div class="col-md-12">
    <div class="col-md-6">
        <label for="cod_upgd">Documento de Identidad</label>
        <input type="text" id="cod_upgd" name="cod_upgd" class="form-control" value="<?php echo $_SESSION['documento']; ?>">
    </div>
    <div class="col-md-6">
        <label for="nom_upgd">Nombre del Profesional</label>
        <input type="text" id="nom_upgd" name="nom_upgd" class="form-control" value="<?php echo $_SESSION['nombre']; ?>">

    </div>
    </div>
    <div class="col-md-6" id="tipo_riesgo" >
    <label for="t_riesgo">Tipo de Riesgo</label>
    <select name="t_riesgo" id="t_riesgo" class="form-control" onchange="selectRiesgoCardiovascular();">
        <option value="0" selected></option>
        <option value="1">Riesgo Cardiovascular</option>
        <option value="2">Salud Mental</option>
        <option value="3">Infectocontagioso</option>
        <option value="4">Riesgo en Gestante</option>
        <option value="5">Riesgo por Desnutrición</option>
        <option value="6">Trasmitida por Vectores</option>
    </select>
    </div>
    <!--<div class="col-md-12">
    <label for="nom_evento">Nombre del Evento</label>
    <input type="text" id="nom_evento" name="nom_evento" class="form-control">
    </div>--> 

    </div>
    </div>

    <input type="button" name="next" class="next action-button" value="Siguiente" />
    </fieldset>
    <fieldset>
    <h2 class="fs-title">C.N.V. CERTIFICADO NACIDO VIVO | R.C. REGISTRO CIVIL | T.I. TARJETA DE IDENTIDAD | C.C. CEDULA DE CIUDADANÍA | C.E. CEDULA DE EXTRANJERÍA | P.A. PASAPORTE | M.S. MENOR SIN ID | A.I. ADULTO SIN ID</h2>

    <div class="row">

    <div class="col-md-12">
    <div class="col-md-6">
    <label for="nom1">Primer Nombre</label>
    <input type="text" id="nom1" name="nom1" class="form-control">
    <label for="apell1">Primer Apellido</label>
    <input type="text" id="apell1" name="apell1" class="form-control">
    <label for="t_doc">Tipo de Doc</label>
    <select name="t_doc" id="t_doc" class="form-control">
    <option value="0"></option>
    <option value="1">CNV</option>
    <option value="2">RC</option>
    <option value="3">TI</option>
    <option value="4">CC</option>
    <option value="5">CE</option>
    <option value="6">PA</option>
    <option value="7">MSI</option>
    <option value="8">ASI</option>
    </select>
    <label for="fech_nac">Fecha Nacimiento</label>
    <!--<input type="date" id="fech_nac" name="fech_nac" class="form-control">-->
    <div class="form-group">
    <div class='input-group date' id='datetimepickerRiesgo'>
        <input type='text' class="form-control" id="fech_nac" name="fech_nac" onblur="calculaEdadmayor5()"/>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar">
            </span>
        </span>
    </div>
    </div>

    <!--<div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="edadValidar">LA EDAD DEL PACIENTE ES IGUAL O MENOR A 5 AÑOS.</label>
    <div class="onoffswitch text-center">
        
        <input type="checkbox" name="edadValidar" class="onoffswitch-checkbox" id="edadValidar">
        <label class="onoffswitch-label" for="edadValidar">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>-->
    <label for="edad">Edad</label>
    <!--<input type="text" id="edad" name="edad" class="form-control" onblur="validaEdadMeses(); validaOpcionMenor5()">-->
    <input type="text" id="edad" name="edad" class="form-control">
    <!--<span id="msgEdadMese" style="color: red;"></span><br><br>-->
    <label for="dep">Dpto de Residencia del Paciente</label>
    <div id="select_dpto"></div>
    <!--<input type="text" id="dpto_res" name="dpto_res" class="form-control" >-->
    <label for="dir_res">Dirección de Residencia</label>
    <input type="text" id="dir_res" name="dir_res" class="form-control">
    <label for="barrio_res">Barrio de Residencia del Paciente</label>
    <input type="text" id="barrio_res" name="barrio_res" class="form-control">
    </div>
    <div class="col-md-6">
    <label for="nom2">Segundo Nombre</label>
    <input type="text" id="nom2" name="nom2" class="form-control">
    <label for="apell2">Segundo Apellido</label>
    <input type="text" id="apell2" name="apell2" class="form-control">
    <label for="nr_identidad">N° Identificación</label>
    <input type="text" id="nr_identidad" name="nr_identidad" class="form-control">
    <label for="tlf">Tlf</label>
    <input type="tel" id="tlf" name="tlf" class="form-control">
    <label for="sex">Sexo</label>
    <select name="sex" id="sex" class="form-control" onchange="verEmberazo()">
    <option value="0"></option>
    <option value="1">Femenino</option>
    <option value="2">Masculino</option>
    </select>
    <!--<div class="my-4 col-md-12" id="enEstado" style="display: none;" onchange="r_gestante()">
    <div class="col-md-12 text-left align-self-center"  >
        <label class="spanlabel" for="embarazo">Esta en estado de Embarazo</label>
        <div class="onoffswitch text-center">
            <input type="checkbox" name="embarazo" class="onoffswitch-checkbox" id="embarazo">
            <label class="onoffswitch-label" for="embarazo">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
    </div>
    </div>-->

    <label for="municipio">Municipio de Residencia del Paciente</label>
    <div id="select_munic"></div>
    <!--<input type="text" id="mun_res" name="mun_res" class="form-control">-->
    <label for="location_res">Localidad de Residencia del Paciente</label>
    <input type="text" id="location_res" name="location_res" class="form-control">
    <label for="otro_barrio">Otro (¿Cuál Barrio?)</label>
    <input type="text" id="otro_barrio" name="otro_barrio" class="form-control">
    </div>

    </div>
    </div><hr>
    <div class="row">
    <div class="col-md-12">
    <div class="col-md-6">
    <label for="pers_etnica">Pertenencia Etnica</label>
    <select name="pers_etnica" id="pers_etnica" class="form-control">
    <option value="0"></option>
    <option value="1">Indígena</option>
    <option value="2">Rom</option>
    <option value="3">Raizal</option>
    <option value="4">Palenquero</option>
    <option value="5">Mulato</option>
    <option value="6">Afrocolombiano</option>
    <option value="7">Otros</option>
    </select>
    </div>
    <div class="col-md-6">
    <label for="g_poblacional">Grupo Poblacional</label>
    <select name="g_poblacional" id="g_poblacional" class="form-control">
    <option value="0"></option>
    <option value="1">Discapacitado</option>
    <option value="2">Desmovilizado</option>
    <option value="3">Indigente</option>
    <option value="4">Migrante</option>
    <option value="5">Vict. Violencia Arm.</option>
    <option value="6">Madre Comunitaria</option>
    <option value="7">Gestante</option>
    <option value="8">Desplazado</option>
    <option value="9">Centro Psiquiátrico</option>
    <option value="10">P. Infantil a cargo de ICBF</option>
    <option value="11">Carcelario</option>
    <option value="12">Otros</option>
    </select>
    </div>

    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="col-md-3">
    <label for="estrato">Estrato</label>
    <select name="estrato" id="estrato" class="form-control">
    <option value="0"></option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    </select>
    </div>
    <div class="col-md-3">
    <label for="t_aseg">Tipo de Aseguramiento</label>
    <select name="t_aseg" id="t_aseg" class="form-control">
    <option value="0"></option>
    <option value="1">Contributivo</option>
    <option value="2">Subsidiado</option>
    <option value="3">Vinculado</option>
    <option value="4">Regimen Especial</option>
    <option value="5">Particular</option>
    <option value="6">Medicina Prepagada</option>
    </select>
    </div>
    <div class="col-md-3">
    <label for="n_educat">Nivel Educativo</label>
    <select name="n_educat" id="n_educat" class="form-control">
    <option value="0"></option>
    <option value="1">No fue a la Escuela</option>
    <option value="2">Preescolar</option>
    <option value="3">Primaria Incompleta</option>
    <option value="4">Primaria Completa</option>
    <option value="5">Secundaria Incompleta</option>
    <option value="6">Secundaria Completa</option>
    <option value="7">Tec. post-Secund Incomp</option>
    <option value="8">Tec. post-Secund Comp</option>
    <option value="9">Universidad Incompleta</option>
    <option value="10">Universidad Completa</option>
    <option value="11">Postgrado Incompleto</option>
    <option value="12">Postgrado Completo</option>
    </select>
    </div>
    <div class="col-md-3">
    <label for="edo_civil">Estado Civil</label>
    <select name="edo_civil" id="edo_civil" class="form-control">
    <option value="0"></option>
    <option value="1">Soltero(a)</option>
    <option value="2">Casado(a)</option>
    <option value="3">Divorciado(a)</option>
    <option value="4">Separado(a)</option>
    <option value="5">Viudo(a)</option>
    <option value="6">Unión Libre</option>
    </select>
    </div>
    </div>
    </div><hr>

    <div class="row">
    <div class="col-md-12">

    <div class="col-md-6">

    <label for="tipo_tramite">EPS a la que pertenece</label>

    <div id="lista_eps"></div>

    </div>

    <div class="col-md-6">
    <label for="regimen">Régimen</label>

    <select name="regimen" id="regimen" class="form-control">

    <option value="0"></option>

    <option value="Contributivo">C) Contributivo</option>

    <option value="Subsidio">D) Subsidiado</option>

    </select>

    </div>

    <!--<div class="col-md-4">

    <label for="compensacion_rep">Caja de Compensación Familiar o Pagador de Pensiones</label>
    <div id="select_compensacion"></div>
    <input type="text" id="compensacion_rep" name="compensacion_rep" class="form-control">

    </div>-->
    </div>
    </div>

    <input type="button" name="previous" class="previous action-button" value="Atrás" />
    <input type="button" name="next" class="next action-button" value="Siguiente" />
    </fieldset>
    <fieldset>
    <h2 class="fs-title">Identificación del Paciente</h2>

    <div class="row">
    <div class="col-md-12">


    <div class="col-md-12">

    <div class="col-md-3">
    <label for="peso">Peso en Kg.</label>
    <input type="text" id="peso" name="peso" class="form-control">
    </div>

    <div class="col-md-3">
    <label for="altura">Altura en cm.</label>
    <input type="text" id="altura" name="altura" class="form-control">
    </div>
    <div class="col-md-3" id="idIMC">
    <label for="imc">IMC</label>
    <input type="text" id="imc" name="imc" readonly="true">
    </div>
    <!--<div class="col-md-3" id="tipo_riesgo" >
    <label for="t_riesgo">Tipo de Riesgo</label>
    <select name="t_riesgo" id="t_riesgo" class="form-control" onchange="selectRiesgoCardiovascular()">
        <option value="0" selected></option>
        <option value="1">Riesgo Cardiovascular</option>
        <option value="2">Salud Mental</option>
        <option value="3">Infectocontagioso</option>
        <option value="4">Riesgo en Gestante</option>
        <option value="5">Riesgo por Desnutrición</option>
        <option value="6">Trasmitida por Vectores</option>
    </select>
    </div>-->

    </div>
    <div class="col-md-6">   
    <div class="col-md-3">   

    <button type="button" onclick="calculoIMC()" class="btn btn-primary">Calcular IMC</button>

    </div> 
    </div> 
    <div class="col-md-12">   
    <div class="col-md-9">   
    <label for="imc">Observaciones</label>
    <textarea class="form-control" rows="4"></textarea>

    </div> 
    </div>
    <hr>
    <div class="col-md-12" id="r_cardiovascular" style="display: none;">

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="c_cigarrillo"> Consume cigarrillo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="c_cigarrillo" class="onoffswitch-checkbox" id="c_cigarrillo" >
        <label class="onoffswitch-label" for="c_cigarrillo">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antesc_familiar"> Antecedente familiar</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antesc_familiar" class="onoffswitch-checkbox" id="antesc_familiar" >
        <label class="onoffswitch-label" for="antesc_familiar">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="hta"> HTA (Hipertensión arterial)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="hta" class="onoffswitch-checkbox" id="hta" >
        <label class="onoffswitch-label" for="hta">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="irc"> IRC (Insuficiencia renal)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="irc" class="onoffswitch-checkbox" id="irc" >
        <label class="onoffswitch-label" for="irc">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="dislipidemia"> Dislipidemia</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="dislipidemia" class="onoffswitch-checkbox" id="dislipidemia" >
        <label class="onoffswitch-label" for="dislipidemia">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    </div>

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="dm2"> DM2 (Diabetes mellitus 2)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="dm2" class="onoffswitch-checkbox" id="dm2" >
        <label class="onoffswitch-label" for="dm2">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="glucosa"> Glucometría > 200 mg/dl</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="glucosa" class="onoffswitch-checkbox" id="glucosa" >
        <label class="onoffswitch-label" for="glucosa">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="sedentarismo"> Sedentarismo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="sedentarismo" class="onoffswitch-checkbox" id="sedentarismo" >
        <label class="onoffswitch-label" for="sedentarismo">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="insuf_card"> Antecedente de Insuficiencia cardíaca</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="insuf_card" class="onoffswitch-checkbox" id="insuf_card" >
        <label class="onoffswitch-label" for="insuf_card">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="insuf_card_chag"> Insuficiencia cardíaca chagásica</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="insuf_card_chag" class="onoffswitch-checkbox" id="insuf_card_chag" >
        <label class="onoffswitch-label" for="insuf_card_chag">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    </div>


    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="ar_cardiacas"> Arritmias cardíacas</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="ar_cardiacas" class="onoffswitch-checkbox" id="ar_cardiacas" >
        <label class="onoffswitch-label" for="ar_cardiacas">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="infartos_previos"> Antescedente de infartos previa</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="infartos_previos" class="onoffswitch-checkbox" id="infartos_previos">
        <label class="onoffswitch-label" for="infartos_previos">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antesc_cerebvas"> Antescedente de accidente cerebrovasculares</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antesc_cerebvas" class="onoffswitch-checkbox" id="antesc_cerebvas">
        <label class="onoffswitch-label" for="antesc_cerebvas">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="tep_tvp"> Antescedente de TEP (Tromboembolismo pulmonar) o TVP (Trombosis venosa profunda) o Uso de anticoagulantes.</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="tep_tvp" class="onoffswitch-checkbox" id="tep_tvp" >
        <label class="onoffswitch-label" for="tep_tvp">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="coagulopatias"> Coagulopatias</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="coagulopatias" class="onoffswitch-checkbox" id="coagulopatias">
        <label class="onoffswitch-label" for="coagulopatias">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    </div>
    </div><hr>

    <div class="col-md-12" id="r_salud_mental" style="display: block;">
    <h4>Salud Mental</h4>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="cons_psicoactivas"> Consumo de sustancias psicoactivas</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="cons_psicoactivas" class="onoffswitch-checkbox" id="cons_psicoactivas">
        <label class="onoffswitch-label" for="cons_psicoactivas">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antesc_tras_psiquis">   Antescedente de trastorno psiquiátricos</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antesc_tras_psiquis" class="onoffswitch-checkbox" id="antesc_tras_psiquis">
        <label class="onoffswitch-label" for="antesc_tras_psiquis">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="esquizofrenia"> Esquizofrenia</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="esquizofrenia" class="onoffswitch-checkbox" id="esquizofrenia">
        <label class="onoffswitch-label" for="esquizofrenia">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antes_cond_suicid"> Antescedentes familiares de conducta suicida</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antes_cond_suicid" class="onoffswitch-checkbox" id="antes_cond_suicid">
        <label class="onoffswitch-label" for="antes_cond_suicid">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="tras_depresivo">  Trastorno depresivo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="tras_depresivo" class="onoffswitch-checkbox" id="tras_depresivo" >
        <label class="onoffswitch-label" for="tras_depresivo">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    </div>

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antes_violencia"> Antescedente de violencia</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antes_violencia" class="onoffswitch-checkbox" id="antes_violencia">
        <label class="onoffswitch-label" for="antes_violencia">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="tras_personalidad"> Trastorno de personalidad</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="tras_personalidad" class="onoffswitch-checkbox" id="tras_personalidad" >
        <label class="onoffswitch-label" for="tras_personalidad">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="abuso_alcohol"> Abuso de alcohol</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="abuso_alcohol" class="onoffswitch-checkbox" id="abuso_alcohol">
        <label class="onoffswitch-label" for="abuso_alcohol">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="tras_bipolar">   Trastorno bipolar</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="tras_bipolar" class="onoffswitch-checkbox" id="tras_bipolar" >
        <label class="onoffswitch-label" for="tras_bipolar">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="estres_posttr"> Estrés postraumático</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="estres_posttr" class="onoffswitch-checkbox" id="estres_posttr" >
        <label class="onoffswitch-label" for="estres_posttr">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    </div>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="plan_suicida"> Plan organizado suicida</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="plan_suicida" class="onoffswitch-checkbox" id="plan_suicida" >
        <label class="onoffswitch-label" for="plan_suicida">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="plan_suicida"> Otro</label>
    <input type="text" id="plan_suicida_otro" name="plan_suicida_otro" class="form-control">
    </div>
    </div>
    </div>

    <div class="col-md-12" id="r_por_Vectores" style="display: none;">
    <div class="col-md-4">
    <h4>Transmitidas por vectores</h4>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="leishmaniasis"> Leishmaniasis</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="leishmaniasis" class="onoffswitch-checkbox" id="leishmaniasis">
        <label class="onoffswitch-label" for="leishmaniasis">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="dengue"> Dengue</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="dengue" class="onoffswitch-checkbox" id="dengue" >
        <label class="onoffswitch-label" for="dengue">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="malaria"> Malaria</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="malaria" class="onoffswitch-checkbox" id="malaria" >
        <label class="onoffswitch-label" for="malaria">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="chagas"> Chagas</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="chagas" class="onoffswitch-checkbox" id="chagas" >
        <label class="onoffswitch-label" for="chagas">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>
    </div>

    <div class="col-md-12" id="r_infectocontagiosas" style="display: none;">
    <h4>Infectocontagiosas</h4>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="vih"> VIH: Serología positiva</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="vih" class="onoffswitch-checkbox" id="vih" >
        <label class="onoffswitch-label" for="vih">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="hepa_a"> Hepatitis exceptuando A: Anticuerpo positivos</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="hepa_a" class="onoffswitch-checkbox" id="hepa_a">
        <label class="onoffswitch-label" for="hepa_a">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="sifilis"> Sífilis: VDRL positivo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="sifilis" class="onoffswitch-checkbox" id="sifilis" >
        <label class="onoffswitch-label" for="sifilis">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>

    <div class="col-md-4">
    <h4>TBC</h4>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="esputar"> Esputo seriado positivo o negativo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="esputar" class="onoffswitch-checkbox" id="antes_violencia">
        <label class="onoffswitch-label" for="esputar">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="rayosx_esputos"> Radiografía de tórax con hallazgos sugestivos de TBC</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="rayosx_esputos" class="onoffswitch-checkbox" id="rayosx_esputos">
        <label class="onoffswitch-label" for="rayosx_esputos">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="antes_contacto"> Antescedente de contacto</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="antes_contacto" class="onoffswitch-checkbox" id="antes_contacto">
        <label class="onoffswitch-label" for="antes_contacto">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="sintomas_b"> Síntomas B</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="sintomas_b" class="onoffswitch-checkbox" id="sintomas_b">
        <label class="onoffswitch-label" for="sintomas_b">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>
    </div>

    <div class="col-md-12" id="r_gestante" style="display: none;">
    <h4>Riesgo en Gestante</h4>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="n_controls_gest"> Número de controles prenatales</label>
    <input type="number" id="n_controls_gest" name="n_controls_gest" class="form-control">
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="fechaUlitmoP">Fecha del ultimo Periodo</label>
    <div class="form-group">
        <div class='input-group date' id='datetimepickerPeriodoUltimo'>
            <input type='text' class="form-control" id="fechaUlitmoP" name="fechaUlitmoP" onblur="calculaEdadGestacional()"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar">
                </span>
            </span>
        </div>
    </div>
    <!--<input type="text" id="fechaUlitmoP" name="fechaUlitmoP" class="form-control">-->
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="edad_embrion"> Edad gestacional</label>
    <input type="text" id="edad_embrion" name="edad_embrion" class="form-control">
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="gest_mayor"> Gestante > 35 años</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="gest_mayor" class="onoffswitch-checkbox" id="gest_mayor" >
        <label class="onoffswitch-label" for="gest_mayor">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="preeclamsia"> Preeclampsia</label>
    <select name="preeclamsia" id="preeclamsia" class="form-control">
        <option value="0"></option>
        <option value="1">Proteinuria > 300 mg/dl en 24 horas</option>
        <option value="2">TAS > 140 mmHg</option>
        <option value="3">TAD > 90 mmHg en dos lecturas separadas</option>
        <option value="4">Uroanálisis – Proteínas ++ o > 30 mg/dl</option>
        <option value="5"> > 30 mg/dl</option>
    </select>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="hta_gest"> Hipertensión gestacional</label>
    <select name="hta_gest" id="hta_gest" class="form-control">
        <option value="0"></option>
        <option value="1">Proteinuria > 300 mg/dl en 24 horas</option>
        <option value="2">TAS > 140 mmHg</option>
        <option value="3">TAD > 90 mmHg en dos lecturas separadas</option>
       
    </select>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="eclampsia"> Eclampsia</label>
     <select name="eclampsia" id="eclampsia" class="form-control">
        <option value="0"></option>
        <option value="1">TAS > 140 mmHg</option>
        <option value="2">TAD > 90 mmHg. Convulsión</option>
    </select>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="sobrepeso_gest"> Sobrepeso</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="sobrepeso_gest" class="onoffswitch-checkbox" id="sobrepeso_gest">
        <label class="onoffswitch-label" for="sobrepeso_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="obeso_gest"> Obesidad</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="obeso_gest" class="onoffswitch-checkbox" id="obeso_gest">
        <label class="onoffswitch-label" for="obeso_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>


    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="toxoplasmosis"> Toxoplasmosis (Serología positiva)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="toxoplasmosis" class="onoffswitch-checkbox" id="toxoplasmosis" >
        <label class="onoffswitch-label" for="toxoplasmosis">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="sifilis_gest"> Sífilis (VDRL positivo)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="sifilis_gest" class="onoffswitch-checkbox" id="sifilis_gest" >
        <label class="onoffswitch-label" for="sifilis_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="vih_gest"> VIH (Serología positiva)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="vih_gest" class="onoffswitch-checkbox" id="vih_gest">
        <label class="onoffswitch-label" for="vih_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="hepatitis_b_gest"> Hepatitis B (Anticuerpos positivos)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="hepatitis_b_gest" class="onoffswitch-checkbox" id="hepatitis_b_gest">
        <label class="onoffswitch-label" for="hepatitis_b_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="citomegalovirus"> Citomegalovirus (Serología positiva)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="citomegalovirus" class="onoffswitch-checkbox" id="citomegalovirus">
        <label class="onoffswitch-label" for="citomegalovirus">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="rubeola_gest"> Rubéola (Serología positiva)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="rubeola_gest" class="onoffswitch-checkbox" id="rubeola_gest" >
        <label class="onoffswitch-label" for="rubeola_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="estrep_gest"> Estreptococo beta-hemolítico (Frotis positivo)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="estrep_gest" class="onoffswitch-checkbox" id="estrep_gest" >
        <label class="onoffswitch-label" for="estrep_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="leish_gest"> Leishmaniasis concomitante</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="leish_gest" class="onoffswitch-checkbox" id="leish_gest" >
        <label class="onoffswitch-label" for="leish_gest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>

    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="diabetes_pregest"> Diabetes pregestacional</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="diabetes_pregest" class="onoffswitch-checkbox" id="diabetes_pregest" >
        <label class="onoffswitch-label" for="diabetes_pregest">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="diabetes_gest"> Diabetes gestacional (PTOG 75 gramos)</label>
    <select name="diabetes_gest" id="diabetes_gest" class="form-control">
        <option value="0"></option>
        <option value="1">92 mg/dl en ayunas</option>
        <option value ="1"> >180 mg/dl a la hora</option> 
        <option value="2"> >153 mg/dl a las dos horas</option> 
        
    </select>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="emb_gemelos">  Embarazo gemelar o múltiple</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="emb_gemelos" class="onoffswitch-checkbox" id="emb_gemelos" >
        <label class="onoffswitch-label" for="emb_gemelos">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="hipotiroidismo">  Hipotiroidismo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="hipotiroidismo" class="onoffswitch-checkbox" id="hipotiroidismo" >
        <label class="onoffswitch-label" for="hipotiroidismo">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="parto_prematuro"> Amenaza de parto prematuro</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="parto_prematuro" class="onoffswitch-checkbox" id="parto_prematuro" >
        <label class="onoffswitch-label" for="parto_prematuro">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="cardio_madre"> Cardiopatías en la madre </label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="cardio_madre" class="onoffswitch-checkbox" id="cardio_madre" >
        <label class="onoffswitch-label" for="cardio_madre">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="drogas_alcohol">  Drogadicción y/o alcoholismo</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="drogas_alcohol" class="onoffswitch-checkbox" id="drogas_alcohol" >
        <label class="onoffswitch-label" for="drogas_alcohol">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="malform_feto"> Malformación fetal confirmada</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="malform_feto" class="onoffswitch-checkbox" id="malform_feto" >
        <label class="onoffswitch-label" for="malform_feto">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>
    <div class="col-md-4">
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="placenta_previa"> Placenta previa</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="placenta_previa" class="onoffswitch-checkbox" id="placenta_previa" >
        <label class="onoffswitch-label" for="placenta_previa">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="anemia"> Anemia (Hemoglobina < 11 g/dl)</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="anemia" class="onoffswitch-checkbox" id="anemia" >
        <label class="onoffswitch-label" for="anemia">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="presen_transv">  Presentación transversa</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="presen_transv" class="onoffswitch-checkbox" id="presen_transv" >
        <label class="onoffswitch-label" for="presen_transv">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="p_podalica"> Presentación podálica completa o incompleta. </label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="p_podalica" class="onoffswitch-checkbox" id="p_podalica" >
        <label class="onoffswitch-label" for="p_podalica">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>

    </div>

    <div class="col-md-12" id="r_cancer" style="display: none;">
    <div class="col-md-4">
    <h4>Detección temprana de cáncer</h4>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="cancer_mama"> Cáncer de mama</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="cancer_mama" class="onoffswitch-checkbox" id="cancer_mama" >
        <label class="onoffswitch-label" for="cancer_mama">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="cancer_cervix"> Cáncer de cérvix</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="cancer_cervix" class="onoffswitch-checkbox" id="cancer_cervix" >
        <label class="onoffswitch-label" for="cancer_cervix">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>
    <div class="col-md-12 text-left align-self-center">
    <label class="spanlabel" for="cancer_prostata">  Cáncer de próstata</label>
    <div class="onoffswitch text-center">
        <input type="checkbox" name="cancer_prostata" class="onoffswitch-checkbox" id="cancer_prostata" >
        <label class="onoffswitch-label" for="cancer_prostata">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
    </div>

    </div>
    </div>  

    </div>
    </div>
    <input type="button" name="previous" class="previous action-button" value="Atrás" />
    <input type="submit" name="submit" class="submit action-button" value="Guardar" />

    <input type="button" name="vistaPreviaRiesgo" id="vistaPreviaRiesgo" value="Vista Previa">

    </fieldset>



    </form>
    </div>
    </div>
    </div>
    </div> <!-- /.row -->

    </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->

    </div> <!-- ./wrapper -->
    <?php include("footer.php"); ?>
    <script src="assets/bootstrap/js/select2.full.js"></script>
    <script type="text/javascript" src="assets/pt_riesgos_form.js"></script>
    <script>
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
    //as the opacity of current_fs reduces to 0 - stored in "now"
    //1. scale current_fs down to 80%
    scale = 1 - (1 - now) * 0.2;
    //2. bring next_fs from the right(50%)
    left = (now * 100)+"%";
    //3. increase opacity of next_fs to 1 as it moves in
    opacity = 1 - now;
    current_fs.css({'transform': 'scale('+scale+')'});
    next_fs.css({'left': left, 'opacity': opacity});
    }, 
    duration: 0, 
    complete: function(){
    current_fs.hide();
    animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
    });
    });

    $(".previous").click(function(){
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
    //as the opacity of current_fs reduces to 0 - stored in "now"
    //1. scale previous_fs from 80% to 100%
    scale = 0.8 + (1 - now) * 0.2;
    //2. take current_fs to the right(50%) - from 0%
    left = ((1-now) * 100)+"%";
    //3. increase opacity of previous_fs to 1 as it moves in
    opacity = 1 - now;
    current_fs.css({'left': left});
    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
    }, 
    duration: 0, 
    complete: function(){
    current_fs.hide();
    animating = false;
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
    });
    });

    // $(".submit").click(function(){
    //     return false;
    // })
    </script>

    <script type="text/javascript">
    $.fn.select2.defaults.set( "theme", "bootstrap" );

    var placeholder = "";


    $( ".select2-single, .select2-multiple" ).select2( {
    placeholder: placeholder,
    width: null,
    containerCssClass: ':all:'
    } );

    $( ".select2-allow-clear" ).select2( {
    allowClear: true,
    placeholder: placeholder,
    width: null,
    containerCssClass: ':all:'
    } );
    </script>

    <script>

    function selectRiesgoCardiovascular(){
    var tipo_riesgo = document.getElementById('t_riesgo').value;
    //var sexo = document.getElementById('sex').value;
    var edad = $('#edad').val();
    var enEstado = $('#embarazo').prop('checked');


    if (tipo_riesgo == 1) {

    document.getElementById('r_cardiovascular').style.display = '';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = 'none';
    document.getElementById('r_salud_mental').style.display = 'none';
    document.getElementById('r_por_Vectores').style.display = 'none';
    $('#sex > option[value="2"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);


    }else if (tipo_riesgo == 3) {

    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = '';
    document.getElementById('r_gestante').style.display = 'none';
    document.getElementById('r_salud_mental').style.display = 'none';
    document.getElementById('r_por_Vectores').style.display = 'none';
    $('#sex > option[value="0"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);

    }else if (tipo_riesgo == 4 ) {
    //$("#sexo option[value=1]").attr("selected",true);
    $('#sex > option[value="1"]').attr('selected', 'selected');
    $('#sex').prop("disabled", true);
    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = '';
    document.getElementById('r_por_Vectores').style.display = 'none';
    document.getElementById('r_salud_mental').style.display = 'none';

    }else if( tipo_riesgo == 5) {
    $.growl.error({title: "RIESGO POR DESNUTRICION", message: "SE AÑADIRAN POSIBLES RIESGOS", duration: 5000 });
    $('#sex > option[value="0"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);

    }else if (tipo_riesgo == 2){
    document.getElementById('r_salud_mental').style.display = '';
    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = 'none';
    document.getElementById('r_por_Vectores').style.display = 'none';
    $('#sex > option[value="0"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);

    }else if( tipo_riesgo == 6) {
    document.getElementById('r_por_Vectores').style.display = '';
    document.getElementById('r_salud_mental').style.display = 'none';
    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = 'none';
    $('#sex > option[value="0"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);

    }else if(tipo_riesgo == 0){
    document.getElementById('r_por_Vectores').style.display = 'none';
    document.getElementById('r_salud_mental').style.display = 'none';
    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = 'none';
    $('#sex > option[value="0"]').attr('selected', 'selected');
    $('#sex').prop("disabled", false);

    }
    }

    /*function r_gestante(){
    var tipo_riesgo = document.getElementById('t_riesgo').value;
    var sexo = document.getElementById('sex').value;
    var edad = $('#edad').val();
    var enEstado = $('#embarazo').prop('checked');

    /*if (tipo_riesgo == 4) {
    //precargamos el sexo al selecionar riesgo gestante
    $("#sex option[value=1]").attr("selected",true);
    };

    if (tipo_riesgo == 4 && sexo == 1 && enEstado == true) {

    document.getElementById('r_cardiovascular').style.display = 'none';
    document.getElementById('r_infectocontagiosas').style.display = 'none';
    document.getElementById('r_gestante').style.display = '';
    document.getElementById('r_por_Vectores').style.display = 'none';
    document.getElementById('r_salud_mental').style.display = 'none';

    }
    }*/

    $('document').ready(function(){

    selectRiesgoCardiovascular();

    });

    function calculoIMC(){
    var peso = document.getElementById('peso').value;
    var altura = document.getElementById('altura').value;
    var edad = document.getElementById('edad').value;



    with (Math) {
    imc = peso / pow(altura / 100,2);
    imc = round(imc * 100) / 100;  // redondea con 2 decimales.
    }


    document.getElementById('imc').value = imc;
    document.getElementById('idIMC').style.display = '';
    /*if (imc < 18.5) {
    alert("Insuficiencia Ponderada");

    // document.getElementById('insufic').style.display = '';
    }
    if (imc > 18.5 && imc < 24.9) {
    alert("IMC Adecuado");
    // document.getElementById('adecuado').style.display = '';
    }
    if (imc > 25 && imc < 29.9) {
    alert("Sobrepeso");
    // document.getElementById('sobrepeso').style.display = '';
    }
    if (imc > 30) {
    alert("Obeso");
    document.getElementById('r_cardiovascular').style.display = 'none';
    if (edad >= 35) {
    alert("Puede tener Riesgo Cardiovascular");
    document.getElementById('r_cardiovascular').style.display = '';

    }
    }*/
    }
    //para niño de 5 años, tiene una talla de 106.40 cm y un peso 22,5 kg
    //para niños de 4 años, tiene una talla de 100.13 cm y un peso de 16.5kg
    //para niños de 3 años, tiene una talla de 96.5 cm y un peso de 14kg
    //para niños de 2 años, tiene una talla de 88 cm y un peso de 12.9kg
    //para niños de 1 años, tiene una talla de 76 cm y un peso de 9.5kg
    //CONSTANTES PARA CALCU DEL (DS)
    //M = MEDIA DE ALTURA DE NIÑOS PARA CIERTA EDAD, O PESO
    //L = (-0.06) poder para normalizar los datos
    // S = 0.12 (coeficiente de variación)

    function calculoDS(M ,talla){

    L = 0.06;
    S = 0.12;

    //CALCULO1 = VALOR OBSERVADO - MEDIA
    var calulo1 = talla / M;
    //calculo2 = calculo1 a la 2
    var calculo2 = 1 / Math.pow(calulo1, L);
    //CALCULO 3 = calculo2 -1
    var calculo3 = calculo2-1;
    //caculo4 = -L*S
    var calculo4 = -0.06*S;
    // RESULTADO TOTAL DS
    var DS = calculo3/calculo4;
    return DS;
    }

    function valorDsporEdadMas(edad,altura,sexo){

    //convierto la edad de meses a años
    var edadConversion = edad/12;

    //para niños
    if (sexo == 2) {
    if( edadConversion == 5) {
    return calculoDS(106.40 ,altura);
    }else if( edadConversion == 4){
    return calculoDS(100.13 ,altura);
    }else if(edadConversion == 3){
    return calculoDS(96.5 ,altura);
    }else if(edadConversion == 2){
    return calculoDS(88 ,altura);
    }else if(edadConversion == 1){
    return calculoDS(76 ,altura);
    }
    }

    //para niñas
    if( sexo == 1) {
    if( edadConversion == 5) {
    return calculoDS(105.95 ,altura);
    }else if( edadConversion == 4){
    return calculoDS(99.14 ,altura);
    }else if(edadConversion == 3){
    return calculoDS(95 ,altura);
    }else if(edadConversion == 2){
    return calculoDS(86 ,altura);
    }else if(edadConversion == 1){
    return calculoDS(74 ,altura);
    }
    }

    }

    function desnutricion(){

    L = 0.06;
    S = 0.12;

    //var tipo_riesgo = $('#t_riesgo').val();

    /*Analisis de desnutricion para niños y niñas de 1 a 5 años*/
    var peso = $("#peso").val();
    var altura = $("#altura").val();
    var edad = $('#edad').val();
    var sexo = $('#sex').val();
    //var imc = ('#imc').val();

    var DS = valorDsporEdadMas(edad,altura,sexo);

    if (sexo == 2 || sexo ==  1) {

    if ( DS  > 3) {
    $.growl.error({title: "OBESIDAD", message: "SE AÑADIRAN POSIBLES RIESGOS", duration: 5000 });
    }else if(DS > 2 && DS <= 3) {
    $.growl.error({title: "SOBREPESO", message: "SE AÑADIRAN POSIBLES RIESGOS", duration: 5000 });    
    }else if(DS > 1 && DS <= 2){
    $.growl.warning({title: "RIESGO DE SOBREPESO", message: "SE AÑADIRAN POSIBLES RIESGOS", duration: 5000 });
    }else if(DS >= -1 &&  DS<= 1){
    $.growl.notice({title: "PESO", message: "ADECUADO PARA LA EDAD", duration: 5000 });   
    }else if (DS >= -2  && DS < -1){
    $.growl.error({title: "RIESGO", message: "POR DESNUTRICION", duration: 5000 });   
    }else if(DS >= -3 &&  DS < -2){
    $.growl.error({title: "DESNUTRICION", message: "AGUDA MODERADA", duration: 5000 });   
    }else if( DS < -3){
    $.growl.error({title: "DESNUTRICION", message: "AGUDA SEVERA", duration: 5000 });   
    }
    };

    }

    //funciton uqe me valida que si se va a ingresar una edad del 1 al 5 obligar al uduario
    //que seleccione la opcion correspondiente
    /*function validaOpcionMenor5() {

    var edad = $('#edad').val();

    if (edad >= 1 && edad <= 5) {
    $('#edad').val('');
    alert("Tienes que selecionar la opcion (LA EDAD DEL PACIENTE ES IGUAL O MENOR A 5 AÑOS) , para poder ingresar esa edad.");
    $('#edad').trigger('focus');
    }
    }*/

    /*esto es para el calendario de la vista 2 del campo edad*/
    $(function () {
    $('#datetimepickerRiesgo').datetimepicker({
    viewMode: 'years',
    format: 'DD/MM/YYYY'
    });
    });

    $(function () {
    $('#datetimepickerPeriodoUltimo').datetimepicker({
    viewMode: 'years',
    format: 'DD/MM/YYYY'
    });
    });


    /*$('#nom_upgd').val(sesion.s_nombre);*/

    // Para listar los EPS
    $.post('ctrl/afiliados.php',
    {
    op: 6
    }, function(data){
    $('#lista_eps').html(data);

    });

    /*esto es para selecionar departamentos y municipios*/
    // listar dptos en colombia
    $.post('ctrl/afiliados.php',
    {
    op: 7
    }, function(data){
    $('#select_dpto').html(data);
    }); 

    function selectmunicipio(id_dpto){

    var id_dpto = id_dpto;
    // lista municipios segun dpto
    $.post('ctrl/afiliados.php',
    {
    op: 8,
    id_dpto: id_dpto
    }, function(data){
    $('#select_munic').html(data);
    });    
    } 

    /*funcion que me ayuda a calular la edad gesatcional*/

    function calculaEdadGestacional() {

    var fechaUltimoPeriodo = $("#fechaUlitmoP").val();

    var dias = "";
    var mes = "";
    var ano = "";

    var longitud = fechaUltimoPeriodo.length;

    /*extraigo los dias, meses y año de la cadena como fecha que ingresa el usuario*/
    for (var i = 0 ; i < longitud ; i++) {
    if( i <= 1) {
    dias += fechaUltimoPeriodo.charAt(i);
    }
    if( i > 2 && i <= 4) {
    mes += fechaUltimoPeriodo.charAt(i);
    }
    if (i > 5 && i <= 9) {
    ano += fechaUltimoPeriodo.charAt(i);
    };                    
    };

    /*Convierto los string a entero*/
    var dias2 = parseInt(dias);
    var mes2 = parseInt(mes);
    var ano2 = parseInt(ano);

    /*año mes dia actual*/
    var objetoDate = new Date();
    var diaActual = objetoDate.getDate();
    var mesActual = objetoDate.getMonth() + 1;
    var anoActual = objetoDate.getFullYear();

    if (dias2 < diaActual) {
    var meses =  (mesActual - mes2) + 12 - 1;
    var diasDespFechaMes = 30 - diaActual;
    var semanas = meses*4;
    }else {
    //calculo meses si ya paso el dia que cumple cierto mes
    var meses = (mesActual - mes2) + 12;
    var diasDespFechaMes = diaActual - dias2;
    diasDespFechaMes *= -1;
    //llevo los meses a semanas
    var semanas = meses*4;
    }
    $('#edad_embrion').val(semanas + "  semanas, " + diasDespFechaMes + " dias");
    $('#edad_embrion').prop('disabled', true);

    }     

    </script>
    </body>
    <!-- BY JAMP 21-11-2018 7.13pm -->
    </html>
