<!DOCTYPE html>

<html lang="en">



<!-- Mirrored from healthadmin.thememinister.com/forms_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->

<?php include("header.php"); ?>

    <body class="hold-transition sidebar-mini">        

        <!-- Site wrapper -->

        <div class="wrapper">

            <header class="main-header">

                <a href="index.html" class="logo"> <!-- Logo -->

                    <span class="logo-mini">

                        <!--<b>A</b>H-admin-->

                        <img src="assets/dist/img/mini-logo.png" alt="">



                    </span>

                    <span class="logo-lg">

                        <!--<b>Admin</b>H-admin-->

                        <h4>ATSSA</h4>

                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->

                    </span>

                </a>

                <!-- Header Navbar -->

                <?php include("menu-top.php"); ?>

                            </header>

                            <!-- =============================================== -->

                            <!-- Left side column. contains the sidebar -->

                            <?php include("menu-left.php"); ?>

                <!-- =============================================== -->

                <!-- Content Wrapper. Contains page content -->

                <div class="content-wrapper">

                    <!-- Content Header (Page header) -->

                    <section class="content-header">

                        <div class="header-icon">

                            <i class="pe-7s-note2"></i>

                        </div>

                        <div class="header-title">

                            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">

                                <div class="input-group">

                                    <input type="text" name="q" class="form-control" placeholder="Search...">

                                    <span class="input-group-btn">

                                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>

                                    </span>

                                </div>

                            </form>  

                            <h1>Afiliaciones</h1>

                            <small>Listado de Afiliaciones</small>

                            <ol class="breadcrumb hidden-xs">

                                <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>

                                <li class="active">Dashboard</li>

                            </ol>

                        </div>

                    </section>

                    <!-- Main content -->

            <section class="content">

                <div class="row">

                            <!-- Form controls -->

                    <div class="col-lg-12">

                        <div class="panel panel-bd lobidrag">

                            <div class="panel-heading">

                                <div class="btn-group"> 

                                    <a class="btn btn-primary" href="afiliaciones.php"> <i class="fa fa-return"></i>  Volver </a>  

                                </div>

                            </div>

                            <div class="panel-body">

                                <form id="msform" method="post" enctype="multipart/form-data" action="ctrl/afiliados.php">

                                <!-- ESTO ES LO QUE VAS A USAR RICARDO -->

                                <ul class="nav nav-tabs" id="myTab" role="tablist">

                                  <li class="nav-item">

                                    <a class="nav-link active" id="tramite-tab" data-toggle="tab" href="#tramite" role="tab" aria-controls="tramite" aria-selected="true">TRAMITE</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">DATOS PERSONALES</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="complement-tab" data-toggle="tab" href="#complement" role="tab" aria-controls="complement" aria-selected="false">DATOS COMPLEMENTARIOS</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="familia-tab" data-toggle="tab" href="#familia" role="tab" aria-controls="familia" aria-selected="false">NÚCLEO FAMILIAR</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="empleador-tab" data-toggle="tab" href="#empleador" role="tab" aria-controls="empleador" aria-selected="false">DATOS DEL EMPLEADOR</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="novedad-tab" data-toggle="tab" href="#novedad" role="tab" aria-controls="novedad" aria-selected="false">REPORTE DE NOVEDAD</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="declar-tab" data-toggle="tab" href="#declar" role="tab" aria-controls="declar" aria-selected="false">DECLARACIONES Y AUTORIZACIONES</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="anexos-tab" data-toggle="tab" href="#anexos" role="tab" aria-controls="anexos" aria-selected="false">ANEXOS</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="entidad-tab" data-toggle="tab" href="#entidad" role="tab" aria-controls="entidad" aria-selected="false">DATOS POR LA ENTIDAD</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="auth-tab" data-toggle="tab" href="#auth" role="tab" aria-controls="auth" aria-selected="false">AUTORIZACION</a>

                                  </li>

                                  <li class="nav-item">

                                    <a class="nav-link" id="docs-tab" data-toggle="tab" href="#docs" role="tab" aria-controls="docs" aria-selected="false">CARGA DE DOCUMENTOS</a>

                                  </li>

                                </ul>

                                <div class="tab-content" id="myTabContent">

                                  <div class="tab-pane fade " id="tramite" role="tabpanel" aria-labelledby="tramite-tab">

                                        <h2 class="fs-title">DATOS DEL TRÁMITE</h2><hr>

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="col-md-6">

                                                            <label for="tipo_tramite">A qué EPS se desea afiliar</label>

                                                            <div id="lista_eps"></div>

                                                        </div>

                                                        <div class="col-md-6">

                                                            <label for="tipo_tramite">Tipo de Trámite</label>

                                                            <select name="tipo_tramite" id="tipo_tramite" class="form-control">

                                                                <option value="0"></option>

                                                                <option value="Afiliación">A. Afiliación</option>

                                                                <option value="Reporte de Novedad">B. Reporte de Novedad</option>
                                                                <option value="Reporte de Novedad">C. Traslados</option>

                                                            </select>

                                                        </div>

                                                       <div class="col-md-6">

                                                            <div id="tipo_afiliacion">

                                                            <label for="tipo_afiliacion">Tipo de Afiliación</label>

                                                            <select name="tipo_afiliacion" id="tipo_afiliacion" onchange="select_tipoafil(this.value)" class="form-control">

                                                                <option value="0"></option>

                                                                <option value="Individual">A) Individual</option>

                                                                <option value="Colectiva">B) Colectiva</option>

                                                                <option value="Institucional">C) Institucional</option>

                                                                <option value="de Oficio">D) de Oficio</option>

                                                            </select><br>

                                                            </div>

                                                            <div id="sub_tipo_afil">

                                                            <label for="sub_tipo_afil">Caracteristica del tipo de afiliación</label>

                                                            <select name="sub_tipo_afil" id="sub_tipo_afil" class="form-control">

                                                                <option value="0"></option>

                                                                <option value="Cotizante o cabeza de Familia">a) Cotizante o cabeza de Familia</option>

                                                                <option value="2">b) Beneficiario o afiliado adicional</option>

                                                                

                                                            </select>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="col-md-6">

                                                            

                                                            <label for="regimen">Régimen</label>

                                                            <select name="regimen" id="regimen" class="form-control">

                                                                <option value="0"></option>

                                                                <option value="Contributivo">C) Contributivo</option>

                                                                <option value="Subsidio">D) Subsidiado</option>

                                                            </select>

                                                        </div>

                                                    </div>

                                                </div>



                                                



                                                <div class="row">

                                                    

                                                    <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">

                                                        <h4 align="left">Tipo de Afiliado</h4>

                                                        <select name="tp_afiliado" class="form-control">

                                                            <option selected disabled="">Seleccione</option>

                                                            <option value="co" >CO cotizante</option>

                                                            <option value="cf">CF Cabeza de Familia</option>

                                                            <option value="be">BE Beneficiario</option>

                                                        </select>

                                                    </div>

                                                    

                                                    <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">

                                                        <h4 align="left">Tipo de Cotizante</h4>

                                                        <select name="tp_cotizante" class="form-control">

                                                            <option selected disabled="">Seleccione</option>

                                                            <option value="dep" >Dependiente</option>

                                                            <option value="ind">Independiente</option>

                                                            <option value="pen">Pensionado</option>

                                                        </select>

                                                    </div>

                                                    <div class="col-md-2 col-sm-2 col-md-2 col-lg-2">

                                                        <h4>Código (Lo diligencia la EPS):</h4>

                                                        <input type="text" name="codigo" placeholder="" />

                                                        

                                                    </div>

                                                    <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">

                                                        <h4>Fecha Solicitud:</h4>

                                                        <!-- <input class="date" data-provide="datepicker" name="fech_sol"  /> -->
                                                        <div class="form-group">
                                                            <div class='input-group date' id="fech_sol">
                                                                    <input type='text' class="form-control" id="fech_sol" name="fech_sol" />
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar">
                                                                        </span>
                                                                    </span>
                                                            </div>
                                                        </div>

                                                        

                                                    </div>

                                                </div>


                                                     

                                  </div>

                                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                        <h2 class="fs-title">DATOS BASICOS DE IDENTIFICACION (del cotizante o cabeza de familia)</h2><hr>

                                            <div class="row">

                                                <div class="col-md-3">

                                                    <label for="apell1_solic">1er Apellido</label>

                                                    <input type="text" id="apell1_solic" name="apell1_solic" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="apell2_solic">2do Apellido</label>

                                                    <input type="text" id="apell2_solic" name="apell2_solic" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="nom1_solic">1er Nombre</label>

                                                    <input type="text" id="nom1_solic" name="nom1_solic" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="nom2_solic">2do Nombre</label>

                                                    <input type="text" id="nom2_solic" name="nom2_solic" class="form-control">

                                                </div>

                                            

                                            </div>

                                            <div class="row">

                                                <div class="col-md-3">

                                                    <label for="tipo_id_solic">Documento de Identificacion</label>

                                                    <select name="tipo_id_solic" id="tipo_id_solic" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">R.C.</option>

                                                        <option value="2">T.I.</option>

                                                        <option value="3">C.C.</option>

                                                        <option value="4">C.E.</option>

                                                        <option value="5">P.A.</option>

                                                        <option value="6">C.D.</option>

                                                        <option value="7">S.C.</option>

                                                    </select>



                                                </div>

                                                <div class="col-md-3">

                                                    <label for="n_identidad">Numero de Identificación</label>

                                                    <input type="text" id="n_identidad" name="n_identidad" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="sexo_solic">Sexo</label>

                                                    <select name="sexo_solic" id="sexo_solic" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">Femenino</option>

                                                        <option value="2">Masculino</option>

                                                    </select>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="f_nac_solic">Fecha de Nacimiento</label><br>
                                                        
                                                        <div class="form-group">

                                                            <!--<div class='input-group date' id='datetimepicker1'>
                                                                <input type='text' class="form-control" id="f_nac_solic" name="f_nac_solic" />-->

                                                            <div class='input-group date' id='datetimepicker10'>
                                                                <input type='text' class="form-control" id="f_nac_solic" name="f_nac_solic"/>

                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar">
                                                                    </span>
                                                                </span>
                                                            </div>

                                                            
                                                        </div>
                  
                                                   <!-- <input type="date" id="f_nac_solic" name="f_nac_solic" class="form-control">-->

                                                </div>

                                            </div>   
                                            

                                  </div>

                                  <div class="tab-pane fade" id="complement" role="tabpanel" aria-labelledby="complement-tab">

                                        <h2 class="fs-title">Datos Complementarios</h2><hr>

                                            <div class="row">

                                                <div class="col-md-2">

                                                    <label for="etnia">Origen Étnico</label>

                                                    <select id="etnia" name="etnia" class="form-control">

                                                        <option value=""></option>

                                                        <option value="01">Indigena</option>

                                                        <option value="02">Rrom (Gitano)</option>

                                                        <option value="03">Raizal (San Andres y Providencia)</option>

                                                        <option value="04">Palenquero (San Basilio de Palenque)</option>

                                                        <option value="05">Negro(a), Afrocolombiano(a)</option>

                                                    </select>

                                                </div>

                                                <div class="col-md-8">

                                                    <div class="col-md-6">

                                                        <div class="col-md-6">

                                                            <label for="discapacidad">Discapacidad</label>

                                                            <input type="text" id="discapacidad" name="discapacidad" class="form-control">

                                                            

                                                        </div>

                                          

                                                        <div class="col-md-6">

                                                            <label for="t_discapacidad_ben">Tipo de Discapacidad</label>

                                                        

                                                            <select id="t_discapacidad" name="t_discapacidad" class="form-control">

                                                                <option value=""></option>

                                                                <option value="F">Física</option>

                                                                <option value="NS">Neuro-sensorial</option>

                                                                <option value="M">Mental</option>

                                                            </select>

                                                            

                                                        </div>

                                                        

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="condic">Condición</label>

                                                        <select name="condic" id="condic" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">Temporal</option>

                                                            <option value="2">Permanente</option>

                                                        </select>

                                                    </div>

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="pt_sisben">Puntaje del Sisben</label>

                                                    <input type="text" id="pt_sisben" name="pt_sisben" class="form-control">

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="g_poblacion_esp">Grupo Poblacional Especial</label>

                                                    <!--<input type="text" id="g_poblacion_esp" name="g_poblacion_esp" class="form-control">-->
                                                    <select id="g_poblacion_esp" name="g_poblacion_esp" class="form-control">

                                                                    <option value=""></option>

                                                                    <option value="02">02 Población Infantil abandonada aa cargo del Instituto Colombiano de Bienestar Familiar</option>

                                                                    <option value="06">06 Menore desvinculados del conflicto armado, a cargo del ICBF</option>

                                                                    <option value="08">08 Población Desmovilizada</option>

                                                                    <option value="09">09 Victimas de Conflicto armado</option>

                                                                    <option value="10">10 Población Infantil vulnerable bajo protección de instituciones diferentes al ICBF</option>

                                                                    <option value="11">11 Personas incluidas al programa de prottección a testigos.</option>

                                                                    <option value="16">16 Adultos mayores en centros de protección</option>

                                                                    <option value="17">17 Comunidad Indigena</option>

                                                                    <option value="18">18 Poblacion Rrom</option>

                                                                    <option value="22">22 Población privada de libertad que no este a cargo del Fondo Nacional de Salud de las personas privadas de libertad</option>

                                                                    <option value="23">23 Personas que dejen de ser madres comunitarias y sean beneficiarias del subsidio de la subcuenta de subsistencia del fondo de solidaridad pensional</option>

                                                                    <option value="24">24 Personas incluidas en el registro único de Damnificados por la deportación, expulsión, repatriación o retorno desde el territorio Venezolano</option>

                                                                </select>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-4">

                                                    <label for="ad_riesgos">Administradora de Riesgos Laborales</label>

                                                    <select class="form-control" name="ad_riesgos" id="ad_riesgos">
                                                        <option value=""></option>
                                                        <option value="SSS COLMENA RIESGOS PROFESIONALES S.A.">SSS COLMENA RIESGOS PROFESIONALES S.A.</option>
                                                        <option value="SSS COMPAÑIA AGRICOLA DE SEGUROS DE VIDA S.A.">SSS COMPAÑIA AGRICOLA DE SEGUROS DE VIDA S.A.</option>
                                                        <option value="SSS SEGUROS DE VIDA COLPATRIA S.A.">SSS SEGUROS DE VIDA COLPATRIA S.A.</option>
                                                        <option value="SSS SEGUROS DE VIDA DEL ESTADO S.A.">SSS SEGUROS DE VIDA DEL ESTADO S.A.</option>
                                                        <option value="SSS SEGUROS BOLIVAR S.A.">SSS SEGUROS BOLIVAR S.A.</option>
                                                        <option value="SSS COMPAÑÍA DE SEGUROS DE VIDA AURORA S.A.">SSS COMPAÑÍA DE SEGUROS DE VIDA AURORA S.A.</option>
                                                        <option value="SSS LA PREVISORA VIDA S.A. COMPAÑÍA DE SEGUROS">SSS LA PREVISORA VIDA S.A. COMPAÑÍA DE SEGUROS</option>
                                                        <option value="SSS SEGUROS DE VIDA ALFA S.A.">SSS SEGUROS DE VIDA ALFA S.A.</option>
                                                        <option value="SSS INSTITUTO DE SEGUROS SOCIALES">SSS INSTITUTO DE SEGUROS SOCIALES</option>
                                                        <option value="SSS LIBERTY SEGUROS DE VIDA S.A.">SSS LIBERTY SEGUROS DE VIDA S.A.</option>
                                                        <option value="SSS ASEGURADORA DE VIDA COLSEGUROS S.A.">SSS ASEGURADORA DE VIDA COLSEGUROS S.A.</option>
                                                        <option value="SSS SURATEP SA">SSS SURATEP SA</option>
                                                        <option value="SSS BBVA SEGUROS DE VIDA SA">SSS BBVA SEGUROS DE VIDA SA</option>
                                                        <option value="SSS LA EQUIDAD SEGUROS DE VIDA">SSS LA EQUIDAD SEGUROS DE VIDA</option>
                                                    </select>

                                                    <!--<input type="text" id="ad_riesgos" name="ad_riesgos" class="form-control">-->

                                                </div>

                                                

                                                <div class="col-md-4">

                                                    <label for="ad_pension">Administración de Pensiones</label>

                                                    <select class="form-control" name="ad_pension" id="ad_pension">
                                                        <option value=""></option>
                                                        <option value="SSS ADMINISTRADORA DE FONDOS DE PENSIONES Y CESANTIAS PORVENIR S.A">SSS ADMINISTRADORA DE FONDOS DE PENSIONES Y CESANTIAS PORVENIR S.A</option>
                                                        <option value="SSS INSTITUTO DE SEGUROS SOCIALES">SSS INSTITUTO DE SEGUROS SOCIALES</option>
                                                        <option value="SSS CAPRECOM">SSS CAPRECOM</option>
                                                        <option value="SSS FONDO DE PENSIONES SANTANDER">SSS FONDO DE PENSIONES SANTANDER</option>
                                                        <option value="SSS CAJA DE AUXILIOS Y PRESTACIO NES DE ACDAC CAXDAC">SSS CAJA DE AUXILIOS Y PRESTACIO NES DE ACDAC CAXDAC</option>
                                                        <option value="SSS CAJA NACIONAL DE PREVENSION SOCIAL">SSS CAJA NACIONAL DE PREVENSION SOCIAL</option>
                                                        <option value="SSS FONDO ALTERNATIVO DE PENSIONES SKANDIA">SSS FONDO ALTERNATIVO DE PENSIONES SKANDIA</option>
                                                        <option value="SSS FONDO DE PREVENSION SOCIAL DEL CONGRESO DE LA REPUBLICA">SSS FONDO DE PREVENSION SOCIAL DEL CONGRESO DE LA REPUBLICA</option>
                                                        <option value="SSS FONDO OBLIGATORIO DE PENSIONES SKANDIA">SSS FONDO OBLIGATORIO DE PENSIONES SKANDIA</option>
                                                        <option value="SSS PROTECCION FONDO DE PENSIONES OBLIGATORIAS">SSS PROTECCION FONDO DE PENSIONES OBLIGATORIAS</option>
                                                        <option value="SSS PENSIONES DE ANTIOQUIA">SSS PENSIONES DE ANTIOQUIA</option>
                                                        <option value="SSS FONDO DE PENSIONES HORIZONTES">SSS FONDO DE PENSIONES HORIZONTES</option>
                                                        <option value="SSS FONDO DE PENSIONES OBLIGATORIAS COLFONDOS">SSS FONDO DE PENSIONES OBLIGATORIAS COLFONDOS</option>
                                                    </select>

                                                    <!--<input type="text" id="ad_pension" name="ad_pension" class="form-control">-->

                                                </div>

                                                <div class="col-md-4">

                                                    <label for="ing_cotizacion">Ingreso Base de Cotización</label>

                                                    <input type="text" id="ing_cotizacion" name="ing_cotizacion" class="form-control">

                                                </div>

                                                

                                            </div>

                                            <div class="row">

                                                

                                                <div class="col-md-3">

                                                    <label for="direccionsolic">Dirección</label>

                                                    <input type="text" id="direccionsolic" name="direccionsolic" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="zona">Zona</label>

                                                    <select name="zona" id="zona" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">Rural</option>

                                                        <option value="2">Urbana</option>

                                                    </select>

                                                </div>

                                            

                                            

                                                <div class="col-md-3">

                                                    <label for="localidad">Localidad / Comuna</label>

                                                    <input type="text" id="localidad" name="localidad" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="barrio">Barrio</label>

                                                    <input type="text" id="barrio" name="barrio" class="form-control">

                                                </div>

                                                

                                            </div>

                                            <div class="row">

                                                <div class="col-md-5">

                                                    <label for="dep">Departamento</label>

                                                    <div id="select_dpto"></div>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="municipio">Municipio</label>

                                                    <div id="select_munic"></div>

                                                </div>

                                                <div class="col-md-4">

                                                    <div class="col-md-6">

                                                        <label for="tlf_f">Teléfono Fijo</label>

                                                        <input type="tel" id="tlf_f" name="tlf_f" class="form-control">

                                                    </div>

                                                    <div class="col-md-6">

                                                        <label for="tlf_m">Teléfono Movil</label>

                                                        <input type="tel" id="tlf_m" name="tlf_m" class="form-control">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-5">

                                                    <label for="email">Correo Electrónico</label>

                                                    <input type="email" id="email" name="email" class="form-control">

                                                </div>

                                                

                                            </div>

                                  </div>

                                  <div class="tab-pane fade" id="familia" role="tabpanel" aria-labelledby="familia-tab">

                                        <h2 class="fs-title">Datos de Identificación de los miembros del Núcleo Familiar</h2><hr>

                                            <div class="row">

                                                <h4>DATOS BASICOS DE IDENTIFICACION DEL CÓNYUGE O COMPAÑERO (A) PERMANENTE COTIZANTE</h4>

                                                <div class="col-md-3">

                                                    <label for="p_nom_c">Primer Nombre</label>

                                                    <input type="text" id="p_nom_c" name="p_nom_c" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="s_nom_c">Segundo Nombre</label>

                                                    <input type="text" id="s_nom_c" name="s_nom_c" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="p_apell_c">Primer Apellido</label>

                                                    <input type="text" id="p_apell_c" name="p_apell_c" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="s_apell_c">Segundo Apellido</label>

                                                    <input type="text" id="s_apell_c" name="s_apell_c" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="t_doc_con">Tipo Doc</label>

                                                    <select name="t_doc_con" id="t_doc_con" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">CN</option>

                                                            <option value="2">RC</option>

                                                            <option value="3">TI</option>

                                                            <option value="4">CC</option>

                                                            <option value="5">CE</option>

                                                            <option value="6">PA</option>

                                                            <option value="7">SC</option>



                                                        </select><br>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="nro_doc_con">N° Doc</label>

                                                    <input type="text" id="nro_doc_con" name="nro_doc_con" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="sex_con">Sexo</label>

                                                    <select name="sex_con" id="sex_con" class="form-control">

                                                        <option value=""></option>

                                                        <option value="1">Femenino</option>

                                                        <option value="2">Masculino</option>

                                                    </select>

                                                    

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="f_nac_con">Fecha de nacimiento</label>                                                 

                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' class="form-control" id="f_nac_con" name="f_nac_con" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar">
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                     <!--<input type="date" id="f_nac_con" name="f_nac_con" class="form-control">-->
                                                </div>



                                            </div><hr>

                                            <div class="row">

                                                <h4>Datos Básicos de Identificación de los Beneficiarios y Adicionales</h4>

                                                

                                                <div class="col-md-3">

                                                    <label for="p_nombre_ben">1er Nombre</label>

                                                    <div class="row">

                                                        

                                                        <?php for ($i=1; $i < 5; $i++) { ?>

                                                            <div class="col-md-2">

                                                                <p><?php echo $i; ?></p>

                                                            </div>

                                                            <div class="col-md-10">

                                                                <input type="text" id="p_nombre_ben[]" name="p_nombre_ben[]" class="form-control">

                                                                

                                                            </div>

                                                        <?php } ?>

                                                

                                                    </div>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="s_nombre_ben">2do Nombre</label>

                                                    <?php for ($i=1; $i < 5; $i++) { ?>

                                                        

                                                        <input type="text" id="s_nombre_ben[]" name="s_nombre_ben[]" class="form-control">

                                                    <?php } ?>

                                                

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="p_apellido_ben">1er Apellido</label>

                                                    <?php for ($i=1; $i < 5; $i++) { ?>

                                                        

                                                        <input type="text" id="p_apellido_ben[]" name="p_apellido_ben[]" class="form-control">

                                                    <?php } ?>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="s_apellido_ben">2do Apellido</label>

                                                    <?php for ($i=1; $i < 5; $i++) { ?>

                                                        

                                                        <input type="text" id="s_apellido_ben[]" name="s_apellido_ben[]" class="form-control">

                                                    <?php } ?>

                                                </div>

                                            </div>

                                                

                                            <div class="row">

                                                <div class="col-md-12">

                                                        

                                                    <div class="col-md-3">

                                                        <label for="t_doc_ben">Tipo de Doc</label>

                                                        <?php for($i=1; $i<5; $i++){ ?> 

                                                        <select name="t_doc_ben[]" id="t_doc_ben[]" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">CN</option>

                                                            <option value="2">RC</option>

                                                            <option value="3">TI</option>

                                                            <option value="4">CC</option>

                                                            <option value="5">CE</option>

                                                            <option value="6">PA</option>

                                                            <option value="7">SC</option>



                                                        </select><br>

                                                        <?php } ?>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="n_identidad_ben">Numero de Identidad</label>

                                                         <?php for($i=1; $i<5; $i++){ ?> 

                                                        <input type="text" id="n_identidad_ben[]" name="n_identidad_ben[]" class="form-control">

                                                        

                                                        <?php } ?>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="sexo_ben[]">Sexo</label>

                                                        <?php for($i=1; $i<5; $i++){ ?> 

                                                        <select name="sexo_ben[]" id="sexo_ben[]" class="form-control">

                                                            <option value=""></option>

                                                            <option value="1">Femenino</option>

                                                            <option value="2">Masculino</option>

                                                        </select><br>

                                                        <?php } ?>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="f_nac_ben[]">Fecha de nacimiento</label>

                                                        <?php $contador = 1; for($i=1; $i<5; $i++){ 
                                                            if ($contador == $i) {
                                                                $id= $i;
                                                                $contador += 1;
                                                            }
                                                        ?> 

                                                         <!--<input id="f_nac_ben[]" name="f_nac_ben[]" class="format-date form-control" data-provide="datepicker">-->
                                                         <div class="form-group">
                                                                <div class='input-group date' id='<?php echo $id; ?>'>
                                                                    <input type='text' class="form-control" id="f_nac_con" name="f_nac_con" />
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar">
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                        <?php } ?>

                                                    </div>

                                                </div>

                                            </div>

                                                

                                            <div class="row">

                                                <div class="col-md-3">

                                                

                                                    <div class="col-md-6">

                                                        <label for="parentesco">Parentesco</label>

                                                         <?php for($i=1; $i<5; $i++){ ?> 

                                                        <input type="text" id="parentesco[]" name="parentesco[]" class="form-control">

                                                        

                                                        <?php } ?>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <label for="etnia_ben">Etnia</label>

                                                         <?php for($i=1; $i<5; $i++){ ?> 

                                                        

                                                        <select id="etnia_ben[]" name="etnia_ben[]" class="form-control">

                                                            <option value=""></option>

                                                            <option value="01">Indigena</option>

                                                            <option value="02">Rrom (Gitano)</option>

                                                            <option value="03">Raizal (San Andres y Providencia)</option>

                                                            <option value="04">Palenquero (San Basilio de Palenque)</option>

                                                            <option value="05">Negro(a), Afrocolombiano(a)</option>

                                                        </select>

                                                        <br>

                                                        <?php } ?>

                                                    </div>

                                               

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="discapacidad_ben">Discapacidad</label>

                                                     <?php for($i=1; $i<5; $i++){ ?> 

                                                        <input type="text" id="discapacidad_ben[]" name="discapacidad_ben[]" class="form-control">

                                                    

                                                    <?php } ?>

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="t_discapacidad_ben">Tipo de Discapacidad</label>

                                                     <?php for($i=1; $i<5; $i++){ ?> 

                                                        <select id="t_discapacidad_ben[]" name="t_discapacidad_ben[]" class="form-control">

                                                            <option value=""></option>

                                                            <option value="F">Física</option>

                                                            <option value="NS">Neuro-sensorial</option>

                                                            <option value="M">Mental</option>

                                                        </select><br>

                                                    <?php } ?>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="condicion_ben">Condicion</label>

                                                     <?php for($i=1; $i<5; $i++){ ?>

                                                        <select id="condicion_ben[]" name="condicion_ben[]" class="form-control">

                                                            <option value=""></option>

                                                            <option value="T">Temporal</option>

                                                            <option value="P">Permanente</option>

                                                        </select><br>

                                                    <?php } ?>

                                                </div>

                                            </div>

                                                <div class="row">

                                                    <h4>Datos de Residencia</h4>

                                                    <div class="col-md-12">

                                                        <div class="col-md-2">

                                                            <label for="dep_ben">Departamento</label>

                                                             <?php for($i=1; $i<5; $i++){ ?>

                                                                <div id="select_dpto1"></div>

                                                             <?php } ?>

                                                        </div>

                                                        <div class="col-md-2">

                                                            <label for="munic">Municipio</label>

                                                            <?php for($i=1; $i<5; $i++){ ?> 

                                                                <div id="select_munic1"></div>

                                                            <?php } ?>

                                                        </div>

                                                        <div class="col-md-2">

                                                            <label for="zona_ben">Zona</label>

                                                             <?php for($i=1; $i<5; $i++){ ?> 

                                                                <select name="zona_ben[]" id="zona_ben[]" class="form-control">

                                                                    <option value="0"></option>

                                                                    <option value="1">Rural</option>

                                                                    <option value="2">Urbana</option>

                                                                </select><br>

                                                             <?php } ?>

                                                        </div>

                                                        <div class="col-md-2">

                                                            <label for="tlf_m_ben">Teléfono Movil</label>

                                                            <?php for($i=1; $i<5; $i++){ ?> 

                                                                <input type="text" id="tlf_m_ben[]" name="tlf_m_ben[]" class="form-control">

                                                             <?php } ?>

                                                        </div>

                                                        <div class="col-md-2">

                                                            <label for="tlf_f_ben">Teléfono Fijo</label>

                                                            <?php for($i=1; $i<5; $i++){ ?> 

                                                                <input type="text" id="tlf_f_ben[]" name="tlf_f_ben[]" class="form-control">

                                                             <?php } ?>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="col-md-3">

                                                                <label for="upc">Valor UPC afiliado adicional (Lo diligencia la EPS)</label>

                                                                <?php for($i=1; $i<5; $i++){ ?> 

                                                                <input type="text" id="upc[]" name="upc[]" class="form-control">

                                                             <?php } ?>

                                                            </div>

                                                            <div class="col-md-5">

                                                                <label for="Ins_salud">Nombre del Inst. Prestador de servicios de salud (IPS)</label>

                                                                <?php for($i=1; $i<5; $i++){ ?> 

                                                                    <div id="select_ips"></div>

                                                                <?php } ?>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                  </div>

                                  <div class="tab-pane fade" id="empleador" role="tabpanel" aria-labelledby="empleador-tab">

                                        <h2 class="fs-title">DATOS DE IDENTIFICACIÓN DEL EMPLEADOR Y OTROS APORTANTES O DE LAS ENTIDADES RESPONSABLES DE LA AFILIACIÓN COLECTIVA, INSTITUCIONAL O DE OFICIO</h2><hr>



                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-4">

                                                        <label for="rif">Nombre o Razón Social</label>

                                                        <input type="text" id="rif" name="rif" class="form-control">

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="t_doc_ot">Tipo Documento</label>

                                                        <select name="t_doc_ot" id="t_doc_ot" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">C.C.</option>

                                                            <option value="2">C.E.</option>

                                                            <option value="3">P.A.</option>

                                                            <option value="4">C.D.</option>

                                                            <option value="5">N.I.</option>

                                                        </select>

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="identidad_ot">Numero de Identidad</label>

                                                        <input type="text" id="identidad_ot" name="identidad_ot" class="form-control">

                                                    </div>

                                                    <div class="col-md-4">

                                                        <label for="t_aportante">Tipo de Aportante o pagador de pensiones</label>

                                                        <input type="text" id="t_aportante" name="t_aportante" class="form-control">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                

                                                <div class="col-md-12">



                                                    <div class="col-md-4">

                                                        <h4>Ubicación o Dirección</h4>

                                                        <div class="col-md-6">

                                                            <div id="select_dpto2"></div>

                                                        </div>

                                                        <div class="col-md-6">

                                                            <div id="select_munic2"></div>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="dirección">Dirección</label>

                                                        <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección ">
                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="tlf_mo">Teléfono Movil</label>

                                                        <input type="text" id="tlf_mo" name="tlf_mo" class="form-control" placeholder="Teleono Movil">

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="tlf_f_aport">Teléfono Fijo</label>

                                                        <input type="text" id="tlf_f_aport" name="tlf_f_aport" class="form-control" placeholder="Telefono Fijo">

                                                    </div>
                                                </div>

                                                    <div class="col-md-4">

                                                        <label for="email_aport">Correo</label>

                                                        <input type="email" id="email_aport" name="email_aport" class="form-control" placeholder="Email">

                                                    </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-4">

                                                        <label for="fech_ing_t">Fecha Inicio de Relacion Laboral</label>

                                                        <input id="fech_ing_t" name="fech_ing_t" class="format-date form-control" data-provide="datepicker">

                                                    </div>

                                                    <div class="col-md-4">

                                                        <label for="cargo_t">Cargo</label>

                                                        <input type="text" id="cargo_t" name="cargo_t" class="form-control">

                                                    </div>

                                                    <div class="col-md-4">

                                                        <label for="salario_t">Salario</label>

                                                        <input type="text" id="salario_t" name="salario_t" class="form-control">

                                                    </div>

                                                </div>

                                            </div>

                                  </div>

                                  <div class="tab-pane fade" id="novedad" role="tabpanel" aria-labelledby="novedad-tab">

                                        <h2 class="fs-title">Reporte de Novedades</h2><hr>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <label for="tipo_novedad">Tipo de Novedad</label>

                                                    <select name="tipo_novedad" id="tipo_novedad" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="Modificación de Datos Básicos de Identificación">Modificación de Datos Básicos de Identificación</option>

                                                        <option value="Corrección de Datos Básicos de Identificación">Corrección de Datos Básicos de Identificación</option>

                                                        <option value="Actualización de Documento de Identidad">Actualización de Documento de Identidad</option>

                                                        <option value="Actualización y/o corrección de Datos complementarios">Actualización y/o corrección de Datos complementarios</option>

                                                        <option value="Terminación de la Inscripción en la EPS (Código)">Terminación de la Inscripción en la EPS (Código)</option>

                                                        <option value="Reinscripción en la EPS">Reinscripción en la EPS</option>

                                                        <option value="Inclusión de Beneficiario o de Afiliados adicionales">Inclusión de Beneficiario o de Afiliados adicionales</option>

                                                        <option value="Exclusión de Beneficiarios o Afiliados adicionales">Exclusión de Beneficiarios o Afiliados adicionales</option>

                                                        <option value="Inicio de Relación Laboral o Adquisición de Condiciones para Cotizar">Inicio de Relación Laboral o Adquisición de Condiciones para Cotizar</option>

                                                        <option value="Terminación de la Relación Laboral o pérdida de condiciones para seguir cotizando">Terminación de la Relación Laboral o pérdida de condiciones para seguir cotizando</option>

                                                        <option value="Vinculación a una Entidad autorizada para realizar las afiliaciones colectivas">Vinculación a una Entidad autorizada para realizar las afiliaciones colectivas</option>

                                                        <option value="Desvinculación a una Entidad autorizada para realizar las afiliaciones colectivas">Desvinculación a una Entidad autorizada para realizar las afiliaciones colectivas</option>

                                                        <option value="Movilidad">Movilidad</option>

                                                        <option value="Traslado">Traslado</option>

                                                        <option value="Reporte de Fallecimiento">Reporte de Fallecimiento</option>

                                                        <option value="Reporte del trámite de protección al cesante">Reporte del trámite de protección al cesante</option>

                                                        <option value="Reporte de la calidad de Prepensionado">Reporte de la calidad de Prepensionado</option>

                                                        <option value="Reporte de la calidad de Pensionado">Reporte de la calidad de Pensionado</option>

                                                    </select><br>

                                                    

                                                    <div class="col-md-12 text-left align-self-center" id="movilidad" style="display:none;">

                                                         <label class="spanlabel" for="movilidad">Movilidad</label>

                                                        <select name="movilidad" id="movilidad" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">Al régimen contributivo</option>

                                                            <option value="2">Al régimen subsidiado</option>

                                                        </select>

                                                    </div>

                                                    <div class="col-md-12 text-left align-self-center" id="traslado" style="display:none;">

                                                        <label class="spanlabel" for="traslado">Traslado</label>

                                                        <select name="traslado" id="traslado" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">Al mismo régimen</option>

                                                            <option value="2">Diferente régimen</option>

                                                        </select>

                                                    </div>

                                                </div>

                                            </div>

                                            <hr>

                                            <h2 class="fs-title">Datos para el reporte de la novedad</h2><hr>

                                            <div class="row">

                                                <div class="col-md-3">

                                                    <label for="rep_nom1">1er Nombre</label>

                                                    <input type="text" id="rep_nom1" name="rep_nom1" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="rep_nom2">2er Nombre</label>

                                                    <input type="text" id="rep_nom2" name="rep_nom2" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="rep_apell1">1er Apellido</label>

                                                    <input type="text" id="rep_apell1" name="rep_apell1" class="form-control">

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="rep_apell2">2do Apellido</label>

                                                    <input type="text" id="rep_apell2" name="rep_apell2" class="form-control">

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-2">

                                                    <label for="t_doc_rep">Tipo de Documento</label>

                                                    <select name="t_doc_rep" id="t_doc_rep" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">R.C.</option>

                                                        <option value="2">T.I.</option>

                                                        <option value="3">C.C.</option>

                                                        <option value="4">C.E.</option>

                                                        <option value="5">P.A.</option>

                                                        <option value="6">C.D.</option>

                                                        <option value="7">S.C.</option>

                                                    </select>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="n_identidad_rep">Número de Identificación</label>

                                                    <input type="text" id="n_identidad_rep" name="n_identidad_rep" class="form-control">

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="sexo_rep">Sexo</label>

                                                    <select name="sexo_rep" id="sexo_rep" class="form-control">

                                                        <option value="0"></option>

                                                        <option value="1">Femenino</option>

                                                        <option value="2">Masculino</option>

                                                    </select>

                                                </div>

                                                <div class="col-md-3">

                                                    <label for="rep_fech_nac">Fecha de Nacimiento</label>

                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker4'>
                                                            <input type='text' class="form-control" id="rep_fech_nac" name="rep_fech_nac" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar">
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <!--<input type="date" id="rep_fech_nac" name="rep_fech_nac" class="form-control">-->


                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-3">

                                                    <label for="rep_fecha_ini">Fecha (a partir de)</label>

                                                    <input id="rep_fecha_ini" name="rep_fecha_ini" class="format-date form-control" data-provide="datepicker">

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="eps_ant">EPS anterior (Código)</label>

                                                    <!--<input type="text" id="eps_ant" name="eps_ant" class="form-control">-->
                                                    <select name="eps_ant" class="form-control" id="eps_ant">
                                                            
                                                            <option value="" >Seleccione</option>

                                                            <option value="EPS030" >EPS030</option>

                                                            <option value="EPS035">EPS035</option>

                                                        </select>

                                                </div>

                                                <div class="col-md-2">

                                                    <label for="cod_traslado_rep">Traslado (Código)</label>

                                                    <input type="text" id="cod_traslado_rep" name="cod_traslado_rep" class="form-control">

                                                </div>

                                                <div class="col-md-5">

                                                    <label for="compensacion_rep">Caja de Compensación Familiar o Pagador de Pensiones</label>
                                                    <div id="select_compensacion"></div>
                                                    <!--<input type="text" id="compensacion_rep" name="compensacion_rep" class="form-control">-->

                                                </div>

                                            </div>
        

                                  </div>

                                  <div class="tab-pane fade" id="declar" role="tabpanel" aria-labelledby="declar-tab">

                                        <h2 class="fs-title">Declaraciones y Autorizaciones</h2><hr>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-9" align="left">

                                                        <label for="dep_economica">.- Declaración de dependencia económica de los beneficiarios y afiliados adicionales: Declaro bajo la gravedad de juramento que el(los) Beneficiario(s) reportado(s) dependen económicamente de mi</label>

                                                        

                                                        <label for="oblig_afiliacion" >.- Declaración de la no obligación de afiliarse al Régimen Contributivo, Especial o de Excepción</label>

                                                        

                                                        <label for="entrega_docs">.- Declaración de existencia de razones de fuerza mayor o caso fortuito que impiden la entrega de los documentos que acreditan la condición de beneficiarios</label>

                                                        <label for="interm_cotizante">.- Declaración de no intermediación del cotizante, cabeza de familia, beneficiarios o afiliados adicionales en una Institución Prestadora de Servicios de Salud</label>

                                                        <label for="eps_datos_cotizante">.- Autorización para que la EPS solicite y obtenga datos y copia de la historia clínica del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales.</label>

                                                        <label for="reporte_info_eps">.- Autorización para que la EPS reporte la información que se genere de la afiliación o del reporte de novedades a la base de datos de afiliados vigentes y a las entidades públicas que por sus funciones la requieran</label>

                                                        <label for="manejo_datoseps_cotizante">.- Autorización para que la EPS maneje los datos personales del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales, de acuerdo con lo previsto en la Ley 1581 de 2012 y el Decreto 1377 de 2013</label>

                                                        <label for="eps_envio_info_correo">.- Autorización para que la EPS envíe información al correo electrónico o al celular como mensajes de texto.</label>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="dep_economica" class="onoffswitch-checkbox" id="dep_economica" checked>

                                                            <label class="onoffswitch-label" for="dep_economica">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="oblig_afiliacion" class="onoffswitch-checkbox" id="oblig_afiliacion" checked>

                                                            <label class="onoffswitch-label" for="oblig_afiliacion">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="entrega_docs" class="onoffswitch-checkbox" id="entrega_docs" checked>

                                                            <label class="onoffswitch-label" for="entrega_docs">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="interm_cotizante" class="onoffswitch-checkbox" id="interm_cotizante" checked>

                                                            <label class="onoffswitch-label" for="interm_cotizante">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="eps_datos_cotizante" class="onoffswitch-checkbox" id="eps_datos_cotizante" checked>

                                                            <label class="onoffswitch-label" for="eps_datos_cotizante">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="reporte_info_eps" class="onoffswitch-checkbox" id="reporte_info_eps" checked>

                                                            <label class="onoffswitch-label" for="reporte_info_eps">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="manejo_datoseps_cotizante" class="onoffswitch-checkbox" id="manejo_datoseps_cotizante" checked>

                                                            <label class="onoffswitch-label" for="manejo_datoseps_cotizante">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="eps_envio_info_correo" class="onoffswitch-checkbox" id="eps_envio_info_correo" checked>

                                                            <label class="onoffswitch-label" for="eps_envio_info_correo">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        

                                                    </div>

                                                </div>

                                            </div>  

                                  </div>

                                  <div class="tab-pane fade" id="anexos" role="tabpanel" aria-labelledby="anexos-tab">

                                        <h2 class="fs-title">Anexos</h2>

                                                

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-9" align="left">

                                                        <label for="cop_doc_identidad">.- Anexo copia del documento de identidad</label>

                                                        

                                                        <label for="dictamen_incapac" >.- Copia del dictamen de incapacidad permanente emitido por la autoridad competente.</label>

                                                        

                                                        <label for="acta_matrimonio">.- . Copia del registro civil de matrimonio, o de la Escritura pública, acta de conciliación o sentencia judicial que declare la unión marital.</label>

                                                        <label for="sent_divorcio">.- Copia de la escritura pública o sentencia judicial que declare el divorcio, sentencia judicial que declare la separación de cuerpos y escritura pública, acta de conciliación o sentencia judicial que declare la terminación de la unión marital.</label>

                                                        <label for="adopcion">.- Copia del certificado de adopción o acta de entrega del menor.</label>

                                                        <label for="acta_custodia">.- Copia de la orden judicial o del acto administrativo de custodia.</label>

                                                        <label for="perd_patria_potestad">.- . Documento en que conste la pérdida de la patria potestad o el certificado de defunción de los padres o la declaración suscrita por el cotizante sobre la ausencia de los dos padres.</label>

                                                        <label for="auth_traslado">.- Copia de la autorización de traslado por parte de la Superintendencia Nacional de Salud.</label>

                                                        <label for="cert_afili_colect">.- Certificación de vinculación a una entidad autorizada para realizar afiliaciones colectivas.</label>

                                                        <label for="acta_admin_calidad_benef">.- Copia del acto administrativo o providencia de las autoridades competentes en la que conste la calidad de beneficiario o se ordene la afiliación de oficio.</label>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="cop_doc_identidad" class="onoffswitch-checkbox" id="cop_doc_identidad" checked value="1">

                                                            <label class="onoffswitch-label" for="cop_doc_identidad">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="dictamen_incapac" class="onoffswitch-checkbox" id="dictamen_incapac" checked value="1">

                                                            <label class="onoffswitch-label" for="dictamen_incapac">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="acta_matrimonio" class="onoffswitch-checkbox" id="acta_matrimonio" checked value="1">

                                                            <label class="onoffswitch-label" for="acta_matrimonio">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="sent_divorcio" class="onoffswitch-checkbox" id="sent_divorcio" checked value="1">

                                                            <label class="onoffswitch-label" for="sent_divorcio">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="adopcion" class="onoffswitch-checkbox" id="adopcion" checked value="1">

                                                            <label class="onoffswitch-label" for="adopcion">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="acta_custodia" class="onoffswitch-checkbox" id="acta_custodia" checked value="1">

                                                            <label class="onoffswitch-label" for="acta_custodia">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="perd_patria_potestad" class="onoffswitch-checkbox" id="perd_patria_potestad" checked value="1">

                                                            <label class="onoffswitch-label" for="perd_patria_potestad">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_traslado" class="onoffswitch-checkbox" id="auth_traslado" checked value="1">

                                                            <label class="onoffswitch-label" for="auth_traslado">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="cert_afili_colect" class="onoffswitch-checkbox" id="cert_afili_colect" checked value="1">

                                                            <label class="onoffswitch-label" for="cert_afili_colect">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="acta_admin_calidad_benef" class="onoffswitch-checkbox" id="acta_admin_calidad_benef" checked value="1">

                                                            <label class="onoffswitch-label" for="acta_admin_calidad_benef">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                        

                                                    </div>

                                                </div>

                                            </div> 

                                  </div>

                                  <div class="tab-pane fade" id="entidad" role="tabpanel" aria-labelledby="entidad-tab">

                                        <h2 class="fs-title">Datos a ser Diligenciados por la Entidad Territorial</h2><hr>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-2">

                                                        <label for="id_entidad_terr">Identificación de la Entidad Territorial</label>

                                                        <select name="id_entidad_terr" id="id_entidad_terr" class="form-control">

                                                            <option value="0"></option>

                                                            <option value="1">Código del Municipio</option>

                                                            <option value="2">Código del Departamento</option>

                                                        </select>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <h4>Datos del SISBEN</h4>

                                                        <div class="col-md-4">

                                                            <label for="ficha">Número de Ficha</label>

                                                            <input type="text" id="ficha" name="ficha" class="form-control">

                                                        </div>  

                                                        <div class="col-md-4">

                                                            <label for="puntaje">Puntaje</label>

                                                            <input type="text" id="puntaje" name="puntaje" class="form-control">

                                                        </div>

                                                        <div class="col-md-4">

                                                            <label for="nivel">Nivel</label>

                                                            <input type="text" id="nivel" name="nivel" class="form-control">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="radicacion">Fecha de Radicación</label>

                                                        <input id="radicacion" name="radicacion" class="format-date form-control" data-provide="datepicker">

                                                    </div>

                                                    <div class="col-md-2">

                                                        <label for="validacion">Fecha de Validación</label>

                                                        <input id="validacion" name="validacion" class="format-date form-control" data-provide="datepicker">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <h4>Datos del Funcionario que realiza la validación</h4>

                                                    <div class="col-md-3">

                                                        <label for="nom1_func">1er Nombre</label>

                                                        <input type="text" id="nom1_func" name="nom1_func" class="form-control">

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="nom2_func">2do Nombre</label>

                                                        <input type="text" id="nom2_func" name="nom2_func" class="form-control">

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="apell1_func">1er Apellido</label>

                                                        <input type="text" id="apell1_func" name="apell1_func" class="form-control">

                                                    </div>

                                                    <div class="col-md-3">

                                                        <label for="apell2">2do Apellido</label>

                                                        <input type="text" id="apell2_func" name="apell2_func" class="form-control">

                                                    </div>

                                                </div>

                                            </div>

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="col-md-3">

                                                            <label for="identidad_func">Número de Identificación</label>

                                                            <input type="text" id="identidad_func" name="identidad_func" class="form-control">

                                                        </div>

                                                        <div class="col-md-9">

                                                            <label for="observ_func">Observaciones</label>

                                                            <textarea name="observ_func" id="observ_func" cols="30" rows="2"></textarea>

                                                        </div>

                                                    </div>

                                                </div>     

                                  </div>

                                <div class="tab-pane fade" id="auth" role="tabpanel" aria-labelledby="auth-tab">

                                    <h2 class="fs-title">Autorización de Mensajes de Texto</h2>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_derechos_deberes">He recibido la carta de derechos y deberes</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_derechos_deberes" class="onoffswitch-checkbox" id="auth_derechos_deberes" checked value="1">

                                                            <label class="onoffswitch-label" for="auth_derechos_deberes">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>





                                                        <label for="mod_datos" class="spanlabel">Modificación de Datos Básicos de Identificación</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="mod_datos" class="onoffswitch-checkbox" id="mod_datos" checked value="1">

                                                            <label class="onoffswitch-label" for="mod_datos">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_desempeño_eps">He recibido la carta de desempeño con el ranking de la EPS</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_desempeño_eps" class="onoffswitch-checkbox" id="auth_desempeño_eps" checked value="1">

                                                            <label class="onoffswitch-label" for="auth_desempeño_eps">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                     <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_contenido_desempeño">Leí el contenido de la carta de desempeño</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_contenido_desempeño" class="onoffswitch-checkbox" id="auth_contenido_desempeño" checked>

                                                            <label class="onoffswitch-label" for="auth_contenido_desempeño">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                     <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_dudas">Me fueron resueltas las dudas sobre el contenido de las cartas de derechos y deberes y la carta de desempeño</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_dudas" class="onoffswitch-checkbox" id="auth_dudas" checked value="1">

                                                            <label class="onoffswitch-label" for="auth_dudas">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                     <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_entender">Entendí y comprendí lo enunciado en la carta de derechos y deberes y la carta de desempeño</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_entender" class="onoffswitch-checkbox" id="auth_entender" checked>

                                                            <label class="onoffswitch-label" for="auth_entender">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                     

                                                     <div class="col-md-12 text-left align-self-center">

                                                        <label class="spanlabel" for="auth_canales_disp"> La EPS cuenta con canales disponibles y eficaces para resolver las dudas sobre el contenido de las cartas</label>

                                                        <div class="onoffswitch text-center">

                                                            <input type="checkbox" name="auth_canales_disp" class="onoffswitch-checkbox" id="auth_canales_disp" checked>

                                                            <label class="onoffswitch-label" for="auth_canales_disp">

                                                                <span class="onoffswitch-inner"></span>

                                                                <span class="onoffswitch-switch"></span>

                                                            </label>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div> 

                                  </div>

                                  <div class="tab-pane fade" id="docs" role="tabpanel" aria-labelledby="docs-tab">

                                        <h2 class="fs-title">Carga de Documentos</h2><hr>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    

                                                    <label for="docs">Adjuntar documentos para anexar a la afiliación</label>

                                                

                                                    <input type="hidden" name="MAX_FILE_SIZE" value="100000" />

                                                    <input name="dni" id="dni" type="file" class="form-control file-input"/>

                                                    <input name="FotoconDni" id="FotoconDni" type="file" class="form-control file-input1"/>

                                                   <br />

                                                   <img id="mostrarDni" width="20%" height="20%" src="" />

                                                   <img id="mostrarImg" src="" width="20%" heigth="20%" >

                                                   
                                                <div>

                                                    <!-- Tomar Foto -->

                                               <!--  <video id="video"></video>



                                                <br>

                                                <button id="boton">Tomar foto</button>

                                                <p id="estado"></p>

                                                <canvas id="canvas" style="display: none;"></canvas> -->

                                  </div>

                                </div>

                                <!-- HASTA ACA ES ESA ESTRUCTURA -->

                                <!-- <form id="msform" method="post" onsubmit="return crearAfiliacion();"> -->

                                    <input type="hidden" id="op" name="op" value="2">

                                    <input type="button" name="vista-previa" id="vista-previa" value="Vista Previa">

                                    <input type="submit" name="guardar" value="Guardar">

                            </form>

                        </div>

                    </div>

                </div>

            </div> <!-- /.row -->

               <div id="ordine" class="modal fade" role="dialog">

                <div class="modal-dialog">



                <!-- Modal content-->

                    <div class="modal-content ">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal">×</button>

                            <h4 class="modal-title">Actualizar Afiliación</h4>

                        </div>

                        <div class="modal-body">

                            <div class="panel panel-bd lobidrag">

                                <div class="panel-heading">

                                    <div class="btn-group"> 

                                        <a class="btn btn-primary" href="afiliaciones.php"> <i class="fa fa-list"></i>  Lista de Afiliaciones </a>  

                                    </div>

                                </div>

                                <div id="vistaeditafiliacion"></div>



                            </div>

                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                            <button type="button" class="btn btn-success" onclick="updateAfiliacion()">Guardar</button>

                        </div>

                    </div>

                </div>

            </div>       

        </section> <!-- /.content -->

                 </div> <!-- /.content-wrapper -->

                 

            </div> <!-- ./wrapper -->

        <?php include("footer.php"); ?>

        <script src="assets/afiliados.js"></script>

        <script src="assets/bootstrap/js/select2.full.js"></script>

        <script src="assets/say-cheese.js"></script>

        <script type="text/javascript">
            $('.format-date').datepicker({
                format: 'yyyy/mm/dd'
                
            });
        </script>

        <script type="text/javascript">
            $("#vista-previa").click(function(){
                $('form#msform').attr('action', 'rpt-afiliacion-previa.php');
                $('form#msform').attr('target', '_blank');
                $('form#msform').submit();
            });
        </script>

        <script>

           $(document).ready(function(){
            

                var sayCheese = SayCheese('#video', {snapshots: true});



                SayCheese.start();

           });
                

           

        </script>

        <script>

            var current_fs, next_fs, previous_fs; //fieldsets

    var left, opacity, scale; //fieldset properties which we will animate

    var animating; //flag to prevent quick multi-click glitches



   /* $(".next").click(function(){

        if(animating) return false;

        animating = true;

        

        current_fs = $(this).parent();

        next_fs = $(this).parent().next();

        

        //activate next step on progressbar using the index of next_fs

        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        

        //show the next fieldset

        next_fs.show(); 

        //hide the current fieldset with style

        current_fs.animate({opacity: 0}, {

            step: function(now, mx) {

                //as the opacity of current_fs reduces to 0 - stored in "now"

                //1. scale current_fs down to 80%

                scale = 1 - (1 - now) * 0.2;

                //2. bring next_fs from the right(50%)

                left = (now * 100)+"%";

                //3. increase opacity of next_fs to 1 as it moves in

                opacity = 1 - now;

                current_fs.css({'transform': 'scale('+scale+')'});

                next_fs.css({'left': left, 'opacity': opacity});

            }, 

            duration: 0, 

            complete: function(){

                current_fs.hide();

                animating = false;

            }, 

            //this comes from the custom easing plugin

            easing: 'easeInOutBack'

        });

    });



    $(".previous").click(function(){

        if(animating) return false;

        animating = true;

        

        current_fs = $(this).parent();

        previous_fs = $(this).parent().prev();

        

        //de-activate current step on progressbar

        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        

        //show the previous fieldset

        previous_fs.show(); 

        //hide the current fieldset with style

        current_fs.animate({opacity: 0}, {

            step: function(now, mx) {

                //as the opacity of current_fs reduces to 0 - stored in "now"

                //1. scale previous_fs from 80% to 100%

                scale = 0.8 + (1 - now) * 0.2;

                //2. take current_fs to the right(50%) - from 0%

                left = ((1-now) * 100)+"%";

                //3. increase opacity of previous_fs to 1 as it moves in

                opacity = 1 - now;

                current_fs.css({'left': left});

                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});

            }, 

            duration: 0, 

            complete: function(){

                current_fs.hide();

                animating = false;

            }, 

            //this comes from the custom easing plugin

            easing: 'easeInOutBack'

        });

    });*/



    // $(".submit").click(function(){

    //     return false;

    // })

</script>



<script type="text/javascript">

    $.fn.select2.defaults.set( "theme", "bootstrap" );



    var placeholder = "";





    $( ".select2-single, .select2-multiple" ).select2( {

        placeholder: placeholder,

        width: null,

        containerCssClass: ':all:'

    } );



    $( ".select2-allow-clear" ).select2( {

        allowClear: true,

        placeholder: placeholder,

        width: null,

        containerCssClass: ':all:'

    } );

 </script>



<script>

    $(window).load(function(){



    $(function() {

        $('#dni').change(function(e) {

            addImage(e); 

        });



     function addImage(e){

      var file = e.target.files[0],

      imageType = /image.*/;

    

      if (!file.type.match(imageType))

       return;

  

      var reader = new FileReader();

      reader.onload = fileOnload;

      reader.readAsDataURL(file);

     }

  

     function fileOnload(e) {

      var result=e.target.result;

      $('#mostrarDni').attr("src",result);

      

     }

    });



    $(function() {

        $('#FotoconDni').change(function(e) {

            addImage(e); 

        });



     function addImage(e){

      var file1 = e.target.files[0],

      imageType1 = /image.*/;

    

      if (!file1.type.match(imageType1))

       return;

  

      var reader = new FileReader();

      reader.onload = fileOnload;

      reader.readAsDataURL(file1);

     }

  

     function fileOnload(e) {

      var result=e.target.result;

      $('#mostrarImg').attr("src",result);

      

     }

    });

  });



</script>



<script type="text/javascript">



    /*muestra segun movilidad o traslado los select correspondiente a cada opcion*/

    $("#tipo_novedad").change(function(){

                

        var options = $('#tipo_novedad').val();



        if(options == "Movilidad"){       

            document.getElementById('movilidad').style.display = "";

            document.getElementById('traslado').style.display = "none";

        }



        if(options == "Traslado"){      

            document.getElementById('traslado').style.display = "";

            document.getElementById('movilidad').style.display = "none";

        }



        if(options != "Movilidad" && options != "Traslado") {

            document.getElementById('traslado').style.display = "none";

            document.getElementById('movilidad').style.display ="none";

        }

    });


    $(document).ready(function(){

        // listar dptos en colombia nucleo familiar
        $.post('ctrl/afiliados.php',
            {
                op: 9
            }, function(data){
            $('#select_dpto1').html(data);
            });
        /*___________________________________________*/       

         /*esto es para mostrar select en la vista 5 ubicacion*/
        $.post('ctrl/afiliados.php',
        {
            op: 11
        },function(data){
            $('#select_dpto2').html(data);
        });
        /*Para traerme los datos de compensacion*/
            $.post('ctrl/afiliados.php',
                {
                    op: 13
                }, function(data){
                    $('#select_compensacion').html(data);
                });
        /*###########################################*/
    });

//selecionar municipios nucleo familiar
function selectmunicipio3(){
    var id_dpto = $("#dep_ben").val();
            // lista municipios segun dpto
     $.post('ctrl/afiliados.php',
     {
        op: 10,
        id_dpto: id_dpto
     }, function(data){
         $('#select_munic1').html(data);
     });


     //busco en relacion co el id la ips relacionada para mostrar un select en el campo
     //Nombre del Inst. Prestador de servicios de salud (IPS)

     $.post('ctrl/afiliados.php',
     {
        op: 14,
        id_dpto: id_dpto
     }, function(data){
         $('#select_ips').html(data);
     });
 }
/*________________________________________*/ 

    function selectmunicipio2(id_dpto){

        console.log(id_dpto);

    $.post('ctrl/afiliados.php',

        {

            op: 12,

            id_dpto: id_dpto

        }, function(data){

            $('#select_munic2').html(data);   

        });

    }

</script>

<script>

    /*esto es para activar la primera pestaña de afiliaciones*/

  $(function () {

    $('#myTab li:nth-child(1) a').tab('show')

  })

  /*esto ayuda para usar la navegacion usuando los botones y activar 
  cada opcion del munu correspondiente*/
  $(".next").click(function(){

  });

</script>

<!--esto es para el calendario-->
    <script type="text/javascript">
            /*vista 1*/
            fech_sol
            $(function () {
                $('#fech_sol').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });
            /*vista 2*/
            $(function () {
                $('#datetimepicker10').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });
            /*vista 4*/
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

            /*vista 6 reporte de novedad*/
            $(function () {
                $('#datetimepicker4').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

            $(function () {
                $('#1').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

            $(function () {
                $('#2').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

            $(function () {
                $('#3').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

            $(function () {
                $('#4').datetimepicker({
                    viewMode: 'years',
                    format: 'DD/MM/YYYY'
                });
            });

    </script>

</body>

<!-- By JAMP 14-12-2018 9:04am -->

</html>

