<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/dep-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
          <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                <span class="logo-mini">
                    <!--<b>A</b>BD-->
                    <img src="assets/dist/img/mini-logo.png" alt="">
                </span>
                <span class="logo-lg">
                    <!--<b>Admin</b>BD-->
                    <img src="assets/dist/img/logo.png" alt="">
                </span>
            </a>
            <!-- Header Navbar -->
            <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                       <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-box1"></i>
                    </div>
                    <div class="header-title">
                        <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>  
                        <h1>Department</h1>
                        <small>department list</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Department</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">

                                    <div class="btn-group"> 
                                        <a class="btn btn-success" href="add-department.html"> <i class="fa fa-plus"></i>  Add Department</a>  
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                     <div class="panel-header">
                                        <div class="col-sm-4">
                                            <div class="dataTables_length">
                                                <label>Display <select name="example_length">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> records per page</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="dataTables_length">
                                               <a class="btn btn-default buttons-copy btn-sm" tabindex="0"><span>Copy</span></a>
                                               <a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0"><span>CSV</span></a>
                                               <a class="btn btn-default buttons-excel buttons-html5 btn-sm" tabindex="0"><span>Excel</span></a>
                                               <a class="btn btn-default buttons-pdf buttons-html5 btn-sm" tabindex="0"><span>PDF</span></a>
                                               <a class="btn btn-default buttons-print btn-sm" tabindex="0"><span>Print</span></a>
                                               
                                           </div>
                                       </div>
                                       <div class="col-sm-4">
                                        <div class="dataTables_length">
                                            <div class="input-group custom-search-form">
                                                <input type="search" class="form-control" placeholder="search..">
                                                <span class="input-group-btn">
                                                  <button class="btn btn-primary" type="button">
                                                      <span class="glyphicon glyphicon-search"></span>
                                                  </button>
                                              </span>
                                          </div><!-- /input-group -->
                                      </div>
                                  </div>
                              </div>

                          </div>
                          <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Serial No</th>
                                        <th>Department Name</th>
                                        <th>Description</th>
                                        <th>status</th>
                                        <th>Update</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr >
                                      <td><input type="radio" name="radioGroup">
                                       <label>1</label>   
                                   </td>
                                   <td>General Surgery</td>
                                   <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                                   <td><span class="label-default label label-danger">Inactive</span></td>
                                   <td>
                                    <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                    <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                            
                            <tr>
                             <td><input type="radio" name="radioGroup">
                               <label>2</label>   
                           </td>
                           <td>Heart Surgery</td>
                           <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                           <td><span class="label-success label label-default">Active</span>
                           </td>
                           <td>
                            <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </td>
                    </tr>
                    <tr >
                      <td>
                          <input type="radio" name="radioGroup">
                          <label>3</label>   
                      </td>
                      <td>Neurology</td>
                      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                      <td><span class="label-default label label-danger">Inactive</span></td>
                      <td>
                        <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </td>
                </tr>
                <tr >
                 <td><input type="radio" name="radioGroup">
                   <label>4</label>   
               </td>
               <td>Radiotherapy</td>
               <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
               <td><span class="label-default label label-danger">Inactive</span></td>
               <td>
                <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
            </td>
        </tr>
        <tr >
         <td><input type="radio" name="radioGroup">
           <label>5</label>   
       </td>
       <td>Gynaecology</td>
       <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
       <td><span class="label-success label label-default">Active</span></td>
       <td>
        <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
    </td>
</tr>
</tbody>
</table>
</div>
<div class="page-nation text-right">
    <ul class="pagination pagination-large">
        <li class="disabled"><span>«</span></li>
        <li class="active"><span>1</span></li>
        <li><a href="#">2</a></li>
        <li class="disabled"><span>...</span></li><li>
        <li><a rel="next" href="#">Next</a></li>
    </ul>
</div>
</div>
</div>
</div>
</div>
</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->

</div> <!-- ./wrapper -->
<!-- ./wrapper -->
        <!-- Start Core Plugins
        =====================================================================-->
        <!-- jQuery -->
       <?php include("footer.php"); ?>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/dep-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
</html>