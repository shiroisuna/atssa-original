<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/forms_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">        
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
                <!-- Header Navbar -->
                <?php include("menu-top.php"); ?>
                            </header>
                            <!-- =============================================== -->
                            <!-- Left side column. contains the sidebar -->
                            <?php include("menu-left.php"); ?>
                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-note2"></i>
                        </div>
                        <div class="header-title">
                            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>  
                            <h1>Doctor</h1>
                            <small>Doctor list</small>
                            <ol class="breadcrumb hidden-xs">
                                <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <!-- Form controls -->
                            <div class="col-sm-12">
                                <div class="panel panel-bd lobidrag">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-primary" href="table.php"> <i class="fa fa-list"></i>  Doctor List </a>  
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <!-- <form class="col-sm-12"> -->
                                            <div class="col-sm-6 form-group">
                                                <label>Nombre</label>
                                                <input type="text" id="nom" name="nom" class="form-control" placeholder="Escriba su nombre" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Apellido</label>
                                                <input type="text" id="apellido" name="apellido" class="form-control" placeholder="Escriba su apellido" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="email@remail.com" required>
                                            </div>
                                           <!--  <div class="col-sm-6 form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" id="pass" name="pass" placeholder="clave" required>
                                            </div> -->
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Departamento</label>
                                                <select class="form-control" id="departamento" name="departamento" size="1">
                                                    <option  selected class="test">Neurology</option>
                                                    <option>Gynaecology</option>
                                                    <option>Microbiology</option>
                                                    <option>Pharmacy</option>
                                                    <option>Neonatal</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-6 form-group">
                                                <label>Dirección</label>
                                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="direccion" required>
                                            </div>
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Número de Tlf Movil</label>
                                                <input type="number" id="tlf_cell" name="tlf_cell" class="form-control" placeholder="Escriba su tlf" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Número de Tlf Residencial</label>
                                                <input type="number" id="tlf_hab" name="tlf_hab" class="form-control" placeholder="Escriba su tlf Residencial" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Sueldo</label>
                                                <input type="text" id="sueldo" name="sueldo" class="form-control" placeholder="Monto" required>
                                            </div>

                                            <div class="col-sm-6 form-group">
                                                <label>Foto</label>
                                                <input type="file" name="picture" id="picture">
                                            </div>  
                                            <div class="col-sm-12 form-group">
                                                <label>Biografia</label>
                                                <textarea id="biografia" name="biografia" class="form-control" rows="3" placeholder="Descripción"></textarea>
                                            </div>        
                                            <div class="col-sm-6 form-group">
                                                <label>Fecha de Nacimiento</label>
                                                <input id="f_nac" name="f_nac" class="datepicker form-control hasDatepicker" type="date" placeholder="cumpleaños">
                                            </div>
                                            

                                           
                                             <!-- <div class="col-sm-6 form-check">
                                              <label>Estado</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="status" value="1" checked="checked">Activo
                                                </label> 
                                              <label class="radio-inline">
                                                  <input type="radio" name="status" value="0" >Inactivo</label>  
                                              </div>  -->
                                              <div class="col-sm-12 reset-button">
                                                 <a href="#" class="btn btn-warning">Volver</a>
                                                 <a href="#" class="btn btn-success" onclick="registrarDoctor()">Guardar</a>
                                             </div>
                                         <!-- </form> -->
                                     </div>
                                 </div>
                             </div>
                         </div>
                         
                     </section> <!-- /.content -->
                 </div> <!-- /.content-wrapper -->
                 
            </div> <!-- ./wrapper -->
        <?php include("footer.php"); ?>
       <script src="assets/doctores.js" type="text/javascript"></script>

    </body>
    
<!-- BY JAMP 13-11-2018 -->
</html>
