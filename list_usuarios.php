<?php session_start(); ?>

<?php include 'ctrl/usuarios/list.php'; ?>;

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from healthadmin.thememinister.com/profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->
<?php include("header.php"); ?>

    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
         <header class="main-header">
             <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
            <!-- Header Navbar -->
            <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                        <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon"><i class="pe-7s-user-female"></i></div>
                    <div class="header-title">
                        <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                        <h1>Listado de usuarios</h1>
                        <div class="btn-group"> 
                                        <a class="btn btn-success" href="form_usuario.php"> <i class="fa fa-plus"></i> Nuevo</a>  
                                    </div> 
                        <small></small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashboard.php"><i class="pe-7s-home"></i>Home</a></li>
                            <li class="active">listado de usuarios</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <?php
                            if (count($usuarios) > 0) {
                                foreach ($usuarios as $u) {
                                ?>

                                <div class="col-sm-12 col-md-12 col-12">
                                    <div class="review-block">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="review-block-img">
                                                    <img src="assets/dist/img/avatar.png" class="img-rounded" alt="">
                                                </div>
                                                <!--<div class="review-block-name"><a href="#">nktailor</a></div>
                                                <div class="review-block-date">January 29, 2016<br/>1 day ago</div>-->
                                            </div>
                                            <div class="col-sm-9">
                                                <!--<div class="review-block-rate">
                                                    <button type="button" class="btn btn-success btn-xs" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-success btn-xs" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-success btn-xs" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                    </button>
                                                </div>-->
                                                <div class="review-block-title"><?php echo $u['nombres'].' '.$u['apellidos']; ?></div>
                                                <div class="review-block-description">
                                                    <ul style="list-style: none; padding-left: 0px;">
                                                        <li>
                                                            <i class="fa fa-envelope"></i> 
                                                            <span>Correo: <?php echo $u['email']; ?></span>
                                                        </li>
                                                        <li> 
                                                            <i class="fa fa-phone"></i> 
                                                            <span>Teléfono: <?php echo $u['tlf']; ?></span>
                                                        </li>
                                                        <li>
                                                            <i class="fa fa-address-book"></i> 
                                                            <span>Dirección: <?php echo $u['municipio']; ?></span>
                                                        </li>
                                                        <li class="mb-10">
                                                            <i class="fa fa-group"></i> 
                                                            <span><?php echo $u['depto']; ?></span>
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                            </button>
                                                            <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Eliminar ">
                                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                            </button>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <?php
                                    
                                }
                            } else {
                                echo '<h3>Lista de usuario se encuentra vacia</h3>';
                            }
                            

                        ?>


                        
                    </div> 
                </section> <!-- /.content -->
            </div> <!-- /.content-wrapper -->
            
        </div> <!-- ./wrapper -->
        <!-- Start Core Plugins
        =====================================================================-->
       <?php include("footer.php"); ?>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:54 GMT -->
</html>