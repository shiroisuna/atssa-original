/*
SQLyog Professional v12.5.1 (32 bit)
MySQL - 10.1.33-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `referencia_paciente` (
	`id` int (11),
	`t_atencion` varchar (300),
	`nombre` varchar (300),
	`apellido` varchar (300),
	`tipo_identidad` varchar (300),
	`nro_identidad` varchar (300),
	`eps` varchar (300),
	`dep` varchar (300),
	`municipio_origen` varchar (300),
	`municipio_destino` varchar (300),
	`tlf` varchar (300),
	`nombre_acompanante` varchar (300),
	`tlf_acompanante` varchar (300),
	`email_acompanante` varchar (300),
	`razon_social` varchar (300),
	`nit` varchar (300),
	`val_medica` varchar (300),
	`at_enfermeria` varchar (300),
	`serv_rehab` varchar (300),
	`medicinas` varchar (300),
	`insumos` varchar (300),
	`anexo` varchar (300)
); 
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('1','Atencion domiciliaria','Pedro','Guzman',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razon Social',NULL,NULL,NULL,NULL,'Medicinas',NULL,NULL);
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('2','Atencion domiciliaria','Javier','Torres','','','','','','','','','','','Razon Social','','','','','Medicinas','','');
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('3','Atencion domiciliaria','Vizquel','Martinez','','','','','','','','','','','Razon Social','','','','','Medicinas','','');
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('4','Atencion domiciliaria','David','Zambrano','','','','','','','','','','','Razon Social','','','','','Medicinas','','');
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('5','Urgencia vital','Alejandro','Perez','CI','14852369','19','17','737','735','41259963','Jose Sepulveda','41258963','jose@mail.com','Razon Social','123456789','valoracion medica','atencion por enfermeria','rehabilitacion','medicamentos','insumos','');
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('6','','','','','','','','','','','','','','','','','','','','','');
insert into `referencia_paciente` (`id`, `t_atencion`, `nombre`, `apellido`, `tipo_identidad`, `nro_identidad`, `eps`, `dep`, `municipio_origen`, `municipio_destino`, `tlf`, `nombre_acompanante`, `tlf_acompanante`, `email_acompanante`, `razon_social`, `nit`, `val_medica`, `at_enfermeria`, `serv_rehab`, `medicinas`, `insumos`, `anexo`) values('7','Atencion domiciliaria','Ejemplo','camacaro zambrano','','','','','','','','','','','','','','','','','','uploads/apn movistar.txt');
