<?php 
$fecha=date("d/m/Y", strtotime($response[0]['fecha_solicitud']));
$tipotramite=$response[0]['tipo_tramite'];
$tipoafiliacion= $response[0]['tipo_afiliacion'];
$subtipoafili=$response[0]['sub_tipo_afiliacion'];
$regimen=$response[0]['regimen'];
$tipoafili=$response[0]['tipo_afiliado'];
$cotizante=$response[0]['tipo_cotizante'];
$codigo=$response[0]['codigo'];
$nomsolicompleto= $response[0]['apellido1_solic']." ".$response[0]['apellido2_solic'].", ". $response[0]['nom1_solic']." ".$response[0]['nom2_solic'];
$tipodoc=$response[0]['tipo_doc_id'];
$numerodocidentidad=$response[0]['nro_doc_id'];
$sexo=$response[0]['sexo'];
$fechanac=$response[0]['fecha_nac'];
$codetnico=$response[0]['etnia'];
$discapacidad= $response[0]['discapacidad'];
$condiciondiscapaci=$response[0]['condicion'];
$ptsnivel=$response[0]['pts_nivel_sisben'];
$grupopoblacion=
$adminriesgos=$response[0]['admin_riesgos'];
$adminpension=$response[0]['admin_pension'];
$ingresocotiza=$response[0]['ingreso_cotiz'];
$direccion=$response[0]['direccion'];
$zona=$response[0]['zona'];
$localidad=$response[0]['localidad'];
$barrio=$response[0]['barrio']; 
$municipio=$response[0]['municipio'];
$departamento=$response[0]['departamento'];
$tlfcell=$response[0]['tlf_cell'];
$tlfhab=$response[0]['tlf_hab'];
$correo=$response[0]['correo'];
$codigoips=$response[0]['codigo'];
$nombeneficiario=$response[0]['apellidos'].' '.$response[0]['nombres'];
$tipodocbene=$response[0]['t_doc_id'];
$numdocidenbene=$response[0]['nro_doc_id'];
$sexobene=$response[0]['sexo'];
$fechanacbene=$response[0][''];
$codigoipsbene=$response[0][''];
$nomapellfamili=$response[0]['apellidos'].' '.$response[0]['nombres'];
$tipodocfamili=$response[0]['t_doc_id'];
$numdocfamili=$response[0]['nro_doc_id'];
$sexofamili=$response[0]['sexo'];
$fechanacfamili=$response[0]['fecha_nac'];
$parentescofamili=$response[0]['parentesco'];
$etniafamili=$response[0]['etnia'];
$discapacidadfamili=$response[0]['discapacidad'];
$condicionfamili=$response[0]['condicion'];

$gestor_eps = $eps[0]['gestor'];
$nom_comercial = $eps[0]['nombre_comercial'];
$ciudad_eps = $eps[0]['ciudad'];
$fecha_eps = $eps[0]['fecha'];




$rpt='';
$rpt.="<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>

	<style type='text/css'>
		body{
			font-family: 'Open Sans', sans-serif;
			margin-right:0;
			margin-left:0;
			width: 100%;
		}

		td{
			padding: 0;
		}

		.border {
		    border: 1px solid gray;
		    border-radius: 3px;
		}

		.bl {
			border-left: 1px solid gray;
		}

		.br {
			border-right: 1px solid gray;
		}

		.bt {
			border-top: 1px solid gray;
		}

		.pp1{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 8px;
			font-size: 10px;
		}

		.pp2{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 5px;
		}

		.pp3{
			margin-top: 0;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 7px;
		}

		.pp4{
			margin-top: 3px;
			margin-bottom: 2px;
			font-weight: 400;
			font-size: 10px;
			line-height: 20px;
		}
		.pp5{
			margin-top: 3px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.pp6{
			margin-top: 70px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.h2{
			font-size: 13px;
			line-height: 13px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.h1{
			font-size: 18px;
			line-height: 14px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.pt-20{
			padding-top: -50px;
		}

		.pb-20{
			padding-bottom: -10px;
		}

		.pb-10{
			padding-bottom: 10px;
		}

		.mb-20{
			margin-bottom: 20px;
		}

		.mb-10{
			margin-bottom: 10px;
		}

		.mb-15{
			margin-bottom: 8px;
		}

		.mt-20{
			margin-top:20px;
		}

		.mt-15{
			margin-top:15px;
		}

		.logomin{
			max-width: 350px;
			margin-left:auto;
			margin-right:auto;
		}

		.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-0,.col-xs-1-5 {
		  float: left;
		  position: relative;
		  min-height: 1px;
		}

		.col-xs-12 {
		  width: 100%;
		}
		.col-xs-11 {
		  width: 91.66666667%;
		}
		.col-xs-10 {
		  width: 83.33333333%;
		}
		.col-xs-9 {
		  width: 75%;
		}
		.col-xs-8 {
		  width: 66.66666667%;
		}
		.col-xs-7 {
		  width: 58.33333333%;
		}
		.col-xs-6 {
		  width: 50%;
		}
		.col-xs-5 {
		  width: 41.66666667%;
		}
		.col-xs-4 {
		  width: 33.33333333%;
		  width: 30%;
		}
		.col-xs-3 {
		  width: 20%;
		}
		.col-xs-2 {
		  width: 15%;
		}

		.col-xs-1-5 {
		  width: 11%;
		}
		.col-xs-1 {
		  width: 8.33333333%;
		}

		.col-xs-0 {
		  width: 5%;
		}

		.p-5{
			padding: 5px;
		}

		.text-center{
			text-align: center;
		}

		.text-right{
			text-align: right;
		}

		.title1{
			font-size: 7px;
			margin-top:0;
			margin-bottom:5px;
		}

		.title3{
			font-size: 12px;
			margin-top:0;
			margin-bottom:2px;
		}

		.autorizacion{
			font-size: 8px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.title2{
			font-size: 7px;
			margin-top:0;
			margin-bottom:0;
		}

		.capi{
			text-transform: capitalize;
		}

		.cortext{
			 text-overflow:ellipsis;
			  white-space:nowrap; 
			  overflow:hidden; 
			  max-width: 100%;
		}
	</style>
	<body>
	<div class='col-xs-12'>
			<div class='col-xs-4 text-center '>
				<p class='pp1'>REPUBLICA DE COLOMBIA</p>
				<p class='pp2'>SISTEMA GENERAL DE SEGURIDAD SOCIAL EN SALUD</p>
				<p class='pp2'>SUPERINTENDENCIA NACIONAL DE SALUD</p>
				<!-- <p class='pp3 mt-15'>'Código Únicamente para dependientes y afiliaciones al Régimen Subsidiado'</p> -->
			</div>

			<div class='col-xs-5 text-center'>
				<h2 class='h2'>FORMULARIO ÚNICO DE AFILIACIÓN Y REGISTRO DE NOVEDADES</h2>
			</div>

			<div class='col-xs-3 text-right'>
				<img class='logomin' src='../assets/img/logo-atssa.jpg' alt=''>
				<!--<h2>LOGO</h2>-->
			</div>
	</div>

	<div class='col-xs-12 pt-20 pb-10 text-center'>

		<h1 class='h1'>FORMULARIO ELECTRÓNICO</h1>
	</div>

	<div class='col-xs-12'>

		<div class='col-xs-12 text-right'>
			<h6 class='title1'>FECHA DE SOLICITUD</h6>
			<p class='pp3'>". $fecha."</p>
		</div>

		<h6 class='title1'>DATOS DEL TRAMITE</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-2 p-5'>
				<h6 class='title1'>TIPO DE TRAMITE</h6>
				<p class='pp3'>". $tipotramite ."</p>
			</div>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>TIPO DE AFILIACION</h6>
				<p class='pp3'>". $tipoafiliacion.":".$subtipoafili." </p>
			</div>

			
			<div class='col-xs-1 p-5 bl'>
				<h6 class='title1'>RÉGIMEN</h6>
				<p class='pp3'>".$regimen."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>TIPO DE AFILIADO</h6>
				<p class='pp3'>".$tipoafili."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>TIPO DE COTIZANTE</h6>
				<p class='pp3'>". $cotizante ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO</h6>
					<p class='pp3'>". $codigo ."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS BÁSICOS DE IDENTIFICACIÓN ( del cotizante o cabeza de familia)</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $nomsolicompleto ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $tipodoc ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $numerodocidentidad ."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $sexo ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $fechanac ."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS COMPLEMENTARIOS</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-1 p-5'>
				<h6 class='title1' style='width: 50px; margin-bottom: 2px;'>CÓD. ORIGEN ETNICO</h6>
				<p class='pp3 text-right' style='margin-top: -13px;'>". $codetnico ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>DISCAPACIDAD</h6>
				<p class='pp3'><strong>Tipo: </strong>". $discapacidad ." | <strong>Condición:</strong> ". $condiciondiscapa ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>PUNTAJE Y NIVEL DEL SISBEN</h6>
				<p class='pp3 cortext'>". $ptsnivel ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>GRUPO DE POBLACIÓN ESPECIAL</h6>
				<p class='pp3 cortext'>".$grupopoblacion."</p>
			</div>

			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>ADMINISTRADORA DE RIESGOS LABORALES</h6>
				<p class='pp3 cortext'>". $adminriesgos ."</p>
			</div>

			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>ADMINISTRADORA DE PENSIONES</h6>
				<p class='pp3 cortext'>". $ingresocotiza ."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>INGRESO BASE DE COTIZACIÓN - IBC</h6>
					<p class='pp3'>". $ingresocotiza ."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>UBICACIÓN O DIRECCIÓN DE RESIDENCIA</h6>
					<p class='pp3 cortext'>". $direccion ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>ZONA</h6>
					<p class='pp3 cortext'>". $zona ."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>LOCALIDAD / COMUNA</h6>
					<p class='pp3 cortext'>". $localidad ."</p>
				</div>

				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>BARRIO</h6>
					<p class='pp3 cortext'>". $barrio ."</p>
				</div>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-2 p-5'>
					<h6 class='title1'>CIUDAD / MUNICIPIO</h6>
					<p class='pp3 cortext'>". $municipio ."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>DEPARTAMENTO</h6>
					<p class='pp3 cortext'>". $departamento ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO FIJO</h6>
					<p class='pp3 cortext'>". $tlfhab ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO MÓVIL</h6>
					<p class='pp3 cortext'>". $tlfcell ."</p>
				</div>

				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>CORREO ELECTRÓNICO</h6>
					<p class='pp3 cortext'>". $correo ."</p>
				</div>

				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO IPS</h6>
					<p class='pp3 cortext'>". $codigoips ."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>DATOS DE IDENTIFICACIÓN DE LOS MIEMBROS DEL NÚCLEO FAMILIAR</h6>
		<h6 class='title1'>Datos de identificación del beneficiario (Cónyuge o compañero (a) permanente del cotizante)</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-4 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $nombeneficiario ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $tipodocbene ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $numdocidenbene ."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $sexobene ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $fechanacbene ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO IPS</h6>
					<p class='pp3 cortext'>". $codigoipsbene ."</p>
				</div>
		</div>

		<h6 class='title1'>Datos básicos de identificación de los beneficiarios y de los afiliados adicionales</h6>
		<table class='border mb-10' style='width: 100%;'>
			<tr>
				<td class='text-center p-5' style='width: 0.5%;'>
				</td>
				<td class='text-center p-5' style='width: 29%;'>
					<h6 class='title2'>APELLIDOS Y NOMBRES COMPLETOS DE LOS BENEFICIARIOS</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TIPO DOCUMENTO DE IDENTIDAD</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>NÚMERO DE IDENTIDAD</h6>
				</td>
				<td class='bl text-center p-5' style='width: 5%;'>
					<h6 class='title2'>SEXO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>FECHA DE NACIMIENTO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>PARENTESCO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>ETNIA</h6>
				</td>
				<td class='bl text-center p-5' style='width: 15.5%;'>
					<h6 class='title2'>DISCAPACIDAD</h6>
				</td>
			</tr>

			<tr>
				<td class='text-center bt p-5'>
					<p class='pp3'>1</p>
				</td>

				<td class='bl text-center bt p-5' >
					<p class='pp3 capi cortext'>". $nomapellfamili ."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $tipodocfamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $numdocfamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $sexofamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $fechanacfamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $parentescofamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'>". $etniafamili."</p>
				</td>
				<td class='bl text-center bt p-5' >
					<p class='pp3'><strong>Tipo: </strong>". $discapacidadfamili."<strong> | Condición:</strong> ". $condicionfamili."</p>
				</td>
			</tr>
		</table>

		<h6 class='title1'>Datos de Residencia</h6>
		<table class='border mb-15' style='width: 100%;'>
			<tr>
				<td class='text-center p-5' style='width: 0.5%;'>
				</td>
				<td class='text-center p-5' style='width: 10%;'>
					<h6 class='title2'>CIUDAD/MPIO.</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>DEPARTAMENTO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 5%;'>
					<h6 class='title2'>ZONA</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TELEFÓNO FIJO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TELEFÓNO MOVIL</h6>
				</td>
				<td class='bl text-center p-5' style='width: 15%;'>
					<h6 class='title2'>VALOR UPC AFILIADO ADICIONAL (Lo diligencia la EPS)</h6>
				</td>
				<td class='bl text-center p-5' style='width: 21%;'>
					<h6 class='title2'>NOMBRE DE LA INSTITUCIÓN PRESTADORA DE SERVICIOS DE SALUD (IPS)</h6>
				</td>
				<td class='bl text-center p-5' style='width: 8%;'>
					<h6 class='title2'>NIVEL DE SISBÉN</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10.5%;'>
					<h6 class='title2'>GRUPO POBLACIONAL</h6>
				</td>
			</tr>	

			<tr>
				<td class='bt text-center bt p-5'>
					<p class='pp3'>1</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 cortext capi'>". $response[0]['municipio']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 capi cortext'>". $response[0]['depto']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['zona']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['tlf_cell']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['tlf_hab']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['valor_upc_dicional']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 cortext'>". $response[0]['inst_servicios_salud']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['nivel_sisben']."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $response[0]['grupo_poblacional']."</p>
				</td>
			</tr>
		</table>
		
		<h6 class='title1'>DATOS DE IDENTIFICACIÓN DEL EMPLEADOR Y OTROS APORTANTES O DE LAS ENTIDADES RESPONSABLES DE LA AFILIACIÓN COLECTIVA, INSTITUCIONAL O DE OFICIO</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-4 p-5'>
				<h6 class='title1'>NOMBRE O RAZÓN SOCIAL</h6>
				<p class='pp3 capi cortext'>". $response[0]['razon_social']."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $response[0]['t_doc']."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $response[0]['nro_doc_id']."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>DV</h6>
				<p class='pp3 cortext'>1</p>
			</div>

			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>TIPO DE APORTANTE O PAGADOR DE PENSIONES</h6>
				<p class='pp3'>". $response[0]['tipo_aportante']."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-4 p-5'>
					<h6 class='title1'>UBICACIÓN O DIRECCIÓN</h6>
					<p class='pp3 cortext'>". $response[0]['ciudad']."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>CIUDAD / MUNICIPIO</h6>
					<p class='pp3 cortext'>". $response[0]['ciudad']."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>DEPARTAMENTO</h6>
					<p class='pp3 cortext'>". $response[0]['dpto']."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO FIJO</h6>
					<p class='pp3 cortext'>". $response[0]['tlf_cell']."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO MÓVIL</h6>
					<p class='pp3 cortext'>". $response[0]['tlf_hab']."</p>
				</div>

				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CORREO ELECTRÓNICO</h6>
					<p class='pp3 cortext'>". $response[0]['correo']."</p>
				</div>
			</div>
			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>FECHA INICIO DE RELACIÓN LABORAL</h6>
					<p class='pp3 cortext'>". $response[0]['fecha_ini_relacion_lab']."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>CARGO</h6>
					<p class='pp3 cortext'>". $response[0]['cargo']."</p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>SALARIO</h6>
					<p class='pp3 cortext'>". $response[0]['salario']."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>. REPORTE DE NOVEDADES</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>TIPO DE NOVEDAD</h6>
				<p class='pp3 capi cortext'>". $response[0]['tipo_novedad']."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS PARA EL REPORTE DE LA NOVEDAD</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $response[0]['nom1_novedad'].' '.$response[0]['nom2_novedad'].' '.$response[0]['apellido1_novedad'].' '.$response[0]['apellido2_novedad']."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $response[0]['t_doc_novedad']."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $response[0]['nro_doc_novedad']."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $response[0]['sexo']."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $response[0]['fecha_nac']."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-1 p-5'>
					<h6 class='title1'>FECHA (a partir de)</h6>
					<p class='pp3 cortext'>". $response[0]['fecha_ini']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO EPS ANTERIOR</h6>
					<p class='pp3 cortext'>". $response[0]['eps_anterior']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>MOTIVO DE TRASLADO CÓDIGO</h6>
					<p class='pp3 cortext'>". $response[0]['traslado']."</p>
				</div>
				<div class='col-xs-6 p-5 bl'>
					<h6 class='title1'>CAJA DE COMPENSACIÓN FAMILIAR O PAGADOR DE PENSIONES</h6>
					<p class='pp3 cortext'>". $response[0]['compensacion_familiar']."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>DECLARACIONES Y AUTORIZACIONES</h6>
			
		<div class='col-xs-12 border mb-15'>
			<p class='pp3 cortext'> - Declaración Juramentada de convivencia: Declaro que convivo con el(la) Señor(a): _______________________________________________ identificado(a) con _________________________ N° ___________________ <br>desde el dia _____ del mes ___________ del año ______</p>
			<p class='pp3 cortext'> - Declaración de dependencia económica de los beneficiarios y afiliados adicionales: Declaro bajo la gravedad de juramento que el(los) Beneficiario(s) reportado(s) dependen económicamente de mi.</p>
			<p class='pp3 cortext'> - Declaración de la no obligación de afiliarse al Régimen Contributivo, Especial o de Excepción.</p>
			<p class='pp3 cortext'> - Declaración de existencia de razones de fuerza mayor o caso fortuito que impiden la entrega de los documentos que acreditan la condición de beneficiarios. Anexo soporte de la Entidad.</p>
			<p class='pp3 cortext'> - Declaración de no intermediación del cotizante, cabeza de familia, beneficiarios o afiliados adicionales en una Institución Prestadora de Servicios de Salud</p>
			<p class='pp3 cortext'> - Autorización para que la EPS solicite y obtenga datos y copia de la historia clínica del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS reporte la información que se genere de la afiliación o del reporte de novedades a la base de datos de afiliados vigentes y a las entidades públicas que por sus funciones la requieran.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS maneje los datos personales del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales, de acuerdo con lo previsto en la Ley 1581 de 2012 y el Decreto 1377 de 2013.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS envíe información al correo electrónico o al celular como mensajes de texto.</p>
			
		</div>

		<h6 class='title1'>FIRMAS</h6>
		<div class='col-xs-12 border mb-15' align='center'>
			<div class='col-xs-5 p-5'>
				<p class='pp3 cortext' align='center' style='margin-top:20px;'> Firma de cotizante, cabeza de familia o beneficiario</p>
				
			</div>
			<div class='col-xs-6 p-5'>
				<p class='pp3 cortext' align='center' style='margin-top:20px;'> Firma y sello del empleador, aportante o entidad responsable de la afiliación colectiva, institucional o de oficio.</p>
				
			</div>
		</div>

		<h6 class='title1'>ANEXOS</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-6 p-5'>
				<p class='pp3 cortext'> Anexo copia del documento de identidad:</p>
				
			</div><br>
			<p class='pp3 cortext'> - Copia del dictamen de incapacidad permanente emitido por la autoridad competente.</p>
				<p class='pp3 cortext'> - Copia del registro civil de matrimonio, o de la Escritura pública, acta de conciliación o sentencia judicial que declare la unión marital</p>
				<p class='pp3 cortext'> - . Copia de la escritura pública o sentencia judicial que declare el divorcio, sentencia judicial que declare la separación de cuerpos y escritura pública, acta de conciliación o sentencia judicial que declare la terminación de la unión marital</p>
				<p class='pp3 cortext'> - Copia del certificado de adopción o acta de entrega del menor.</p>
				<p class='pp3 cortext'> - Copia de la orden judicial o del acto administrativo de custodia.</p>
				<p class='pp3 cortext'> - Documento en que conste la pérdida de la patria potestad o el certificado de defunción de los padres o la declaración suscrita por el cotizante sobre la ausencia de los dos padres.
</p>
				<p class='pp3 cortext'> - Copia de la autorización de traslado por parte de la Superintendencia Nacional de Salud.</p>
				<p class='pp3 cortext'> - Certificación de vinculación a una entidad autorizada para realizar afiliaciones colectivas</p>
				<p class='pp3 cortext'> - Copia del acto administrativo o providencia de las autoridades competentes en la que conste la calidad de beneficiario o se ordene la afiliación de oficio.</p>
		</div>

		<h6 class='title1'>DATOS A SER DILIGENCIADOS POR LA ENTIDAD TERRITORIAL</h6>
		<div class='col-xs-12 border mb-15'>
				<div class='col-xs-4 p-5'>
					<h6 class='title1'>IDENTIFICACIÓN DE LA ENTIDAD TERRITORIAL</h6>
					<p class='pp3 cortext'><strong>CÓDIGO DEL MUNICIPIO: </strong>000 | <strong>CÓDIGO DEL DEPARTAMENTO: </strong>". $response[0]['tipo_id_entidad_territ']."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>DATOS DEL SISBÉN</h6>
					<p class='pp3 cortext'><strong>NÚMERO DE FICHA: </strong>". $response[0]['nro_ficha']." | <strong>PUNTAJE:</strong> ". $response[0]['puntaje']." | <strong>NIVEL: </strong>". $response[0]['nivel']."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>FECHA DE RADICACIÓN</h6>
					<p class='pp3 cortext'>". $response[0]['fecha_radicacion']."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>FECHA DE VALIDACIÓN</h6>
					<p class='pp3 cortext'>". $response[0]['fecha_validacion']."</p>
				</div>

			<div class='col-xs-12 bt'>

				<div class='col-xs-4 p-5'>
					<h6 class='title1'>DATOS DEL FUNCIONARIO QUE REALIZA LA VALIDACIÓN</h6>
					<p class='pp3 capi cortext'>". $response[0]['nom1_func'].' '.$response[0]['nom2_func'].' '.$response[0]['apell1_func'].' '.$response[0]['apell2_func']."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>			
					<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
					<p class='pp3'>C.C.</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
			</div>
			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>OBSERVACIONES:</h6>
					<p class='pp3 cortext'>". $response[0]['observaciones']."</p>
				</div>
			</div>
		</div>
		<h6 class='title3'>AUTORIZACIÓN DE MENSAJES DE TEXTO.</h6>
		<div class='col-xs-12 border mb-15'>
			<p class='pp4 cortext'>Yo,_______________________________________________identificado (a) con ___________________ otro _________________ número _________________ de _________________, <br> certifico que:</p>
			<div class='col-xs-6 p-5'>
				<div class='col-xs-7 p-5'>
					<p class='pp4 cortext'> He recibido la carta de derechos y deberes.  </p>
					<p class='pp4 cortext'> He recibido la carta de desempeño con el ranking de la EPS.  </p>
					<p class='pp4 cortext'> Leí el contenido de la carta de derechos y deberes.  </p>
					<p class='pp4 cortext'> Leí el contenido de la carta de desempeño.  </p>
					<p class='pp4 cortext'> Me fueron resueltas las dudas sobre el contenido de las cartas 	de derechos y deberes y la carta de desempeño.  </p>
					<p class='pp4 cortext'> Entendí y comprendí lo enunciado en la carta de derechos y 	deberes y la carta de desempeño.  </p>
					<p class='pp4 cortext'> La EPS cuenta con canales disponibles y eficaces para resolver las dudas sobre el contenido de las cartas.  </p>
				</div>
				<div class='col-xs-4 p-5'>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p><br>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p><br>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
				</div>
				<div class='co-xs-5'>
					
				</div>
			</div>
			<div class='col-xs-5 p-5'>
				<p class='pp4 cortext'>Así mismo autorizo a NUEVA EPS, para que envíe información al
teléfono celular No. ____________________
y/o correo electrónico: _______________________________________________</p>
				<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
			</div>
		</div>
		<div class='col-xs-12 mb-15'>
			<div class='col-xs-5'>
				<p class='pp4 cortext'>Codialmente,</p>

			</div>
			<div class='col-xs-6'>
				<p class='pp4 cortext'>Afiliado manifiesta que no sabe o no puede firmar, autoriza a:</p>

			</div>
			<div class='col-xs-5 p-5'>
				<div class='col-xs-9'>
				<p class='pp4 cortext'>Firma: _________________________________________</p>
				<p class='pp4 cortext'>Tipo ID: ______________ No. ID: ____________________</p>
				<p class='pp4 cortext'>Fecha (dd/mm/aaaa): _____________________________</p>
				<p class='pp4 cortext'>Dirección: _________________________________________</p>
				<p class='pp4 cortext'>Tel: _______________ Municipio: _____________________</p>
				<p class='pp4 cortext'>Departamento: ___________________________________</p>
				</div>
				<div class='col-xs-3 border'>
					<p class='pp6 cortext' align='center'>Huella</p>
				</div>
			</div>
			<div class='col-xs-6 p-5'>
				<div class='col-xs-9'>
				<p class='pp4 cortext'>Firma: _________________________________________</p>
				<p class='pp4 cortext'>Tipo ID: ______________ No. ID: ____________________</p>
				<p class='pp4 cortext'>Fecha (dd/mm/aaaa): _____________________________</p>
				<p class='pp4 cortext'>Dirección: _________________________________________</p>
				<p class='pp4 cortext'>Tel: _______________ Municipio: _____________________</p>
				<p class='pp4 cortext'>Departamento: ___________________________________</p>
				</div>
				<div class='col-xs-3 border'>
					<p class='pp6 cortext' align='center'>Huella</p>
				</div>
			</div>
			<div class='col-xs-12 mb-15'>
				<p class='pp3 cortext'>Manifiesto con mi firma la aceptación de todas las declaraciones y términos incorporados en este formato y el consentimiento expreso, previo e informado en relación con el tratamiento de mis datos personales que hará
NUEVA EPS S.A., en sujeción a la política para el tratamiento de datos personales de la NUEVA EPS S.A. consúltela al reverso de este documento.</p>
			</div>
		</div>
		<div class='col-xs-12 border mb-15' style='border-radius: 5px;'>
			<p class='pp4 cortext'>  Información de la Nueva EPS</p>
			<p class='pp4 cortext'>     Asesor: __________________________________________________________</p>
			<p class='pp4 cortext'>     Código: ____________________ Oficina: _____________________________</p>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-6'>
				<h3>Autorizaciones</h3>
				<p class='pp3 cortext'>En virtud de la anterior certificación, autorizo expresamente y conforme a lo dispuesto en los artículos 14 y 15 de la Ley 527 de 1995, a NUEVA EPS S.A. Para que me remita información en forma de mensajes de datos, a través de mecanismos como sms -short message service - , correos electrónicos y redes sociales, relacionada con los servicios de NUEVA EPS S.A. y la carta de derecho de deberes.</p>
				<p class='pp3 cortext'>Autorizo de manera expresa a NUEVA EPS S.A. y/o a la persona natural o jurídica a quien ésta encargue, para que recolecte, almacene, use, haga circular, actualice o suprima mis datos personales, para acceder a los servicios de la NUEVA EPS S.A. y para el cumplimiento de la carta de derechos y deberes.</p><br>
				<h3>Aviso de privacidad</h3>
				<p class='pp3 cortext'>Hago constar expresamente que NUEVA EPS S.A. me informó sobre los siguientes derechos que me asisten como titular de datos personales:</p>
				<p class='pp3 cortext'>• Acceder los datos personales que hayan sido objeto de tratamiento conforme a lo dispuesto en la Ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen.</p>
				<p class='pp3 cortext'>• Conocer, actualizar y rectificar los datos personales frente al responsable del tratamiento y al encargado del tratamiento. Este derecho se podrá ejercer, entre otros, datos, en relación con datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos datos cuyo tratamiento esté expresamente prohibido o no haya sido autorizado. </p>
				<p class='pp3 cortext'>• solicitar prueba de la autorización otorgada al responsable del tratamiento, salvo cuando expresamente se exceptúe como requisito para el tratamiento, de conformidad con lo previsto en el artículo 10 de la Ley 1581 de 2012.</p>
				<p class='pp3 cortext'>• Ser informado por el responsable del tratamiento o el encargado del tratamiento, previa solicitud, con respecto del uso que le ha dado a los datos personales</p>
				<p class='pp3 cortext'>• Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la Ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen.</p>

			</div>
			<div class='col-xs-5'><br><br>
				<p class='pp3 cortext'>• Revocar la autorización y/o solicitar la supresión del dato cuando en el tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o supresión procederá cuando la Superintendencia de Industria y  comercio haya determinado que en el tratamiento el responsable o encargado han ncurrido en conductas contrarias a esta ley y a la constitución. En cumplimiento de la Ley 1581 de 2012 y el Decreto 1377 de 2013 y las demás normas que los modifiquen, adicionen o complementen, le informamos que usted puede conocer la política de tratamiento de los datos personales de NUEVA EPS S.S., a través de los siguientes mecanismos que ponemos a su disposición:</p><br>
				<p class='pp3 cortext'>Sitio web: www.nuevaeps.com.co <br> Oficinas de atención al afiliado de NUEVA EPS S.A. <br> Línea Gratuita: 01 8000 954400 <br> Línea de atención en Bogotá: 307 7022 <br> Conmutador: (57 + 1) 4193000 <br> Dirección Administrativa: Carrera 85k No. 46A – 66 Piso 2</p>
				<p class='pp3 cortext'>Sr.(a) afiliado(a), la información por usted suministrada en este formato, así como la relacionada en el formulario de afiliación y/o demás documentos anexos, serán utilizados por NUEVA EPS S.A. como responsable del tratamiento de la información únicamente para los fines específicos relacionados con la administración y prestación de servicios en salud de Plan Obligatorio de Salud, de acuerdo con la Ley 1581 de 2012 ‘’Hábeas Data’’; dicho tratamiento podrá implicar la trasferencia, trasmisión y/o recepción de los datos, y el cual se realizará a través de sí misma, terceros encargados de tratamiento de información o de sus aliados comerciales, para fines comerciales y para la correcta prestación del servicio, de la forma indicada en los Términos y Condiciones de cada uno de sus portales y de la Política de Confidencialidad y Protección de Datos.</p>
			</div>
		</div>
		<h6 class='title1'>INFORMACIÓN PARA SER DILIGENCIADA POR EL TRABAJADOR INDEPENDIENTE CUYO INGRESO SEA IGUAL O SUPERIOR A 1 SMMLV</h6>
		<div class='col-xs-12 mb-15'>
			<div class='col-xs-12 border'>
				
				<div class='col-xs-2  p-5 bl'>
					<h6 class='title1'>ACTIVIDAD ECONÓMICA</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ADMINISTRADORA DE RIESGOS LABORALES</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ADMINISTRADORA DE FONDOS DE PENSIONES</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>INGRESO BASE DE COTIZACIÓN</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>POSEE VIVIENDA PROPIA</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ACTIVIDAD ECONÓMICA</h6>
					<p class='pp3'>". $response[0]['nro_identidad']."</p>
				</div>
			</div>
			<div class='col-xs-6 border p-5 bl'>
				<p class='pp3 cortext'>Nivel de Escolaridad</p>
				<p class='pp3'></p>
			</div>
			<div class='col-xs-5 p-5 bl'>
				<div class='col-xs-12 p-5'>
					<p class='pp3'>Posición Ocupacional</p>
				</div>
				<div class='col-xs-6 p-5'>
					<p class='pp3'>Inicio de Contrato</p>
				</div>
				<div class='col-xs-5 p-5'>
					<p class='pp3'>Fin de Contrato</p>
				</div>
			</div>
			<div class='col-xs-12 border p-5 bl'>
				<p class='pp3'>SEÑOR USUARIO: TRABAJADOR INDEPENDIENTE O CONTRATISTA, RECUERDE REPORTAR LA NOVEDAD DE RETIRO EN LA PLANILLA DE PAGO A PARTIR DEL MOMENTO QUE DECIDA NO CONTINUAR CON LOS SERVICIOS DE SALUD, EL NO HACERLO  LE GENERA MORA, LA CUAL TENDRÁ VIGENCIA HASTA EL MOMENTO EN EL QUE REPORTE SU RETIRO AL SISTEMA. DECRETO 806 ART. 57 DE 1998 Y ART. 59 DECRETO 1406 DE 1999. </p>
			</div>
			<div class='col-xs-12 border p-5 bt'>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>SÓLO DILIGENCIAR SI ES COTIZANTE INDEPENDIENTE PAGO POR TERCERO</h6>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre del Tercero</h6>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Identificación</h6>

				</div>
			</div>
		</div>
		<h6 class='title1'>INFORMACIÓN PARA SER SUMINISTRADA POR LA EPS</h6>
		<div class='col-xs-12 boder mb-15'>
			<div class='col-xs-12 border p-5 bt'>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre del GESTOR o PROMOTOR</h6>
					<p class='pp3 cortext'>".$gestor_eps."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO GESTOR o PROMOTOR</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre DIRECTOR COMERCIAL</h6>
					<p class='pp3 cortext'>".$nom_comercial."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO DIRECTOR</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Causal Inconsistencia y/o Observaciones</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1' align='center'>CIUDAD Y FECHA</h6>
					<p class='pp3 cortext'>".$ciudad_eps." ".$fecha_eps."</p>
				</div>
			</div>
			
		</div>
	</body>
</html>";

	// var_dump($rpt);

		$nombrearchivo='Reporte-afiliacion';

		require_once __DIR__ . '/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'Legal',
			'mode' => 'utf-8',
			'orientation' => 'P',
		    'setAutoTopMargin' => 'stretch',
		    'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 10,
			'margin_header' => 0,
			'margin_footer' => 0
		]);

		$html = $rpt;
		$mpdf->autoPageBreak = true;
		$mpdf->AddPage('P');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		$mpdf->Output($nombrearchivo.'.pdf','I'); 
		exit;
?>

