
<html>

<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            
           <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
            <!-- Header Navbar -->
                <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                    <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                           <!--  <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div> -->
                        </form>  
                        <h1>Equipos de Salud (Voluntarios)</h1>
                        <small>Lista de Voluntarios</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- Form controls -->
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="btn-group"> 
                                        <a class="btn btn-primary" href="list_equipos_salud.php"> 
                                            <i class="fa fa-list"></i>  Volver a Lista Voluntarios</a>  
                                    </div>
                                </div>
                           <!--  <form id="fomulario-voluntario" name="fomulario-voluntario" action="ctrl/referencia-paciente.php" enctype="multipart/form-data" method="post"> -->

                            <form id="fomulario-voluntario" name="fomulario-voluntario" action="ctrl/doctores.php" enctype="multipart/form-data" method="post">

                                
                            
                                <div class="panel-body">


                                    <div class="form-group col-md-6">
                                        <label>Primer Nombre</label>
                                        <input type="text" id="primernombre" name="primernombre" class="form-control" placeholder="Ingresa Primer Nombre" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Segundo Nombre</label>
                                        <input type="text" id="segundonombre" name="segundonombre" class="form-control" placeholder="Ingresa Segundo Nombre" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Primer Apellido</label>
                                        <input type="text" name="primerapellido" id="primerapellido" class="form-control" placeholder="Ingresa Primer Apellido" required>
                                    </div>
                                      <div class="form-group col-md-6">
                                        <label>Segundo Apellido</label>
                                        <input type="text" name="segundoapellido" id="segundoapellido" class="form-control" placeholder="Ingresa Segundo Apellido" required>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Documento de Identidad</label>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Tipo</label>
                                        
                                        <select name="tipo_identidad" id="tipo_identidad" class="form-control" required="">

                                            <option value="0"></option>
                                            <option value="1">R.C.</option>
                                            <option value="2">T.I.</option>
                                            <option value="3">C.C.</option>
                                            <option value="4">C.E.</option>
                                            <option value="5">P.A.</option>
                                            <option value="6">C.D.</option>
                                            <option value="7">S.C.</option>

                                        </select>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label>Número</label>
                                        <input type="text" id="nro_identidad" name="nro_identidad" class="form-control" placeholder="Ej: x701505-E" required>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Profesión</label>
                                        <select name="profesion" id="profesion" class="form-control" required="">
                                            <option value="" selected="" disabled="">Selecciona una Profesión</option>
                                            <option value="Médico">Médico</option>
                                            <option value="">Enfermera Superior</option>
                                            <option value="Odontólogo">Odontólogo</option>
                                            <option value="Fisioterapista">Fisioterapista</option>
                                            <option value="Nutricionista">Nutricionista</option>
                                            <option value="Psicólogo">Psicólogo</option>
                                            <option value="Higienista Oral">Higienista Oral</option>
                                            <option value="Auxiliar de Enfermería">Auxiliar de Enfermería</option>
                                            <option value="Promotora de Salud">Promotora de Salud</option>
                                            <option value="Agente Educativo en Salud">Agente Educativo en Salud</option>
                                            <option value="Partera">Partera</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Departamento</label><br>
                                        <div id="select_dpto"></div>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Municipio</label><br>
                                        <div id="select_munic"></div>

                                    </div>


                                    <div class="form-group col-md-6">
                                        
                                        <label>Celular</label><br>
                                        <input type="tel" name="celular" id="celular" class="form-control" placeholder="Ingresa N° Celular" required>

                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Whatsapp</label>
                                        <input type="tel" name="Whatsapp" id="Whatsapp" class="form-control" placeholder="Ingresa N° Whatsapp" required>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Correo</label>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Ingresa Correo" required>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Dirección</label>
                                        <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingresa Dirección" required>
                                    </div>
                    

                                    <!-- <div class="reset-button col-md-12"> -->
                                        <!-- <input type="hidden" id="op" name="op" value="1">
                                        <a href="#" id="guardar" class="btn btn-success" >Guardar</a> -->

                                    <div class="form-group col-md-12">
                                        <label>Horario</label>
                                        <input type="text" name="horario" id="horario" class="form-control" placeholder="Ingresa Horario de Disponibilidad" required>
                                    </div>

                                   

                                    <div class="reset-button col-md-12">
                                        <input type="hidden" id="op" name="op" value="16">
                                        <input type="submit" name="guardar" value="Guardar" class="btn btn-success">

                                    </div>
                                
                                </div>
                            </form>
                     </div>
                 </div>
             </div>
             
         </section> <!-- /.content -->
     </div> <!-- /.content-wrapper -->
    
</div> <!-- ./wrapper -->
       <?php include("footer.php"); ?>
        

        <script type="text/javascript">

            $(document).ready(function(){

                // Para listar los EPS
                $.post('ctrl/afiliados.php',
                    {
                        op: 6
                    }, function(data){
                        $('#lista_eps').html(data);
                    });
                // listar dptos en colombia
                $.post('ctrl/afiliados.php',
                    {
                        op: 7
                    }, function(data){
                        $('#select_dpto').html(data);
                    
                    });

            });

            function selectmunicipio(id_dpto){

                var id_dpto = id_dpto;
                // lista municipios segun dpto
                $.post('ctrl/afiliados.php',
                    {
                        op: 8,
                        id_dpto: id_dpto
                    }, function(data){
                    $('#select_munic').html(data);
                });

                $.post('ctrl/referencia-paciente.php',
                    {
                        op: 2,
                        id_dpto: id_dpto
                    }, function(data){
                    $('#select_ips').html(data);
                });

                
                
            }

            var verificar = false;
            $( "div#select_munic" ).click(function() {
                $("div#select_munic_destino").empty();
                $("div#select_munic").clone().appendTo("div#select_munic_destino");
                $('div#select_munic_destino div#select_munic select').attr('name', 'select_munic_destino');
                $('div#select_munic_destino div#select_munic select').attr('id', 'select_munic_destino');         
               
            });

            $("div.reset-button #guardar").click(function(){
                //alert('hola');
                $("#fomulario-referencia-paciente").submit();
            });

            $("div#select_dpto select").click(function(){
                alert('hola');
            });

            
        </script>

        
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/add-patient.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
</html>
