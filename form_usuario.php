<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/add-department.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
          <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                <span class="logo-mini">
                    <!--<b>A</b>BD-->
                    <img src="assets/dist/img/mini-logo.png" alt="">
                </span>
                <span class="logo-lg">
                    <!--<b>Admin</b>BD-->
                    <img src="assets/dist/img/logo.png" alt="">
                </span>
            </a>
            <!-- Header Navbar -->
            <?php include("menu-top.php"); ?>
          </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                       <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon"><i class="pe-7s-user-female"></i></div>
                    <div class="header-title">
                        <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                        <h1>Listado de usuarios</h1>
                        <small></small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashboard.php"><i class="pe-7s-home"></i>Home</a></li>
                            <li><a href="list_usuarios.php"><i class="pe-7s-home"></i>Usuarios</a></li>
                            <li class="active">Nuevo</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <form action="ctrl/usuarios/new_user.php" method="post" id="add_user" >
                        <div class="row">
                            <!-- Form controls -->
                            <div class="col-sm-12">
                                <div class="panel panel-bd lobidrag">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-primary" href="list_usuarios.php"> <i class="fa fa-list"></i>  Lista de Usuarios </a>  
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <!-- <form class="col-sm-12"> -->
                                            <div class="col-sm-6 form-group">
                                                <label>Nombres *</label>
                                                <input type="text" id="nombres" name="nombres" class="form-control" placeholder="Nombres del usuario" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Apellidos *</label>
                                                <input type="text" id="apellidos" name="apellidos" class="form-control" placeholder="Apellidos del usuario" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Tipo de documento *</label>
                                                <input type="text" id="t_doc" name="t_doc" class="form-control" placeholder="Tipo de documento" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Número de documento *</label>
                                                <input type="text" id="nro_doc" name="nro_doc" class="form-control" placeholder="Numero de documento del usuario" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Email *</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="email@remail.com" required>
                                            </div>
                                           <!--  <div class="col-sm-6 form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" id="pass" name="pass" placeholder="clave" required>
                                            </div> -->
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Departamento *</label>
                                                <select class="form-control" id="departamento" name="departamento" size="1">
                                                    <option  selected class="test">Neurology</option>
                                                    <option>Gynaecology</option>
                                                    <option>Microbiology</option>
                                                    <option>Pharmacy</option>
                                                    <option>Neonatal</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-6 form-group">
                                                <label>Dirección *</label>
                                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección" required>
                                            </div>
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Número de Tlf Movil *</label>
                                                <input type="number" id="tlf" name="tlf" class="form-control" placeholder="Escriba el teléfono del usuario" required>
                                            </div>

                                            <div class="col-sm-4 form-group">
                                                <label>Nombre de la empresa</label>
                                                <input type="text" id="nombre_empresa" name="nombre_empresa" class="form-control" placeholder="Nombre de la empresa" >
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>NIT de la empresa</label>
                                                <input type="text" id="nit_empresa" name="nit_empresa" class="form-control" placeholder="NIT de la empresa" >
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>Código de habilitación</label>
                                                <input type="text" id="cod_habilitacion" name="cod_habilitacion" class="form-control" placeholder="Código de habilitación" >
                                            </div>

                                            <div class="col-sm-4 form-group">
                                                <label>Foto</label>
                                                <input type="file" name="picture" id="picture">
                                            </div>      
                                            <div class="col-sm-4 form-group">
                                                <label>Fecha de Nacimiento</label>
                                                <input id="fecha_nac" name="fecha_nac" class="datepicker form-control hasDatepicker" type="date" placeholder="Cumpleaños">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>Sexo</label>
                                                <select class="form-control" id="departamento" name="departamento" size="1">
                                                    <option  selected class="test">Masculino</option>
                                                    <option>Femenino</option>
                                                </select>
                                            </div>
                                            
                                            <hr/>
                                            <div class="col-sm-4 form-group">
                                                <label>Usuario de ingreso al sistema *</label>
                                                <input type="text" id="login" name="login" class="form-control" placeholder="Usuario " required>
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>Contraseña *</label>
                                                <input type="password" id="pass" name="pass" class="form-control" placeholder="*******" maxlength="15" required>
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>Confirmar contraseña *</label>
                                                <input type="password" id="pass2" name="pass2" class="form-control" placeholder="Nombres del usuario" maxlength="15" required>
                                            </div>

                                           
                                             <!-- <div class="col-sm-6 form-check">
                                              <label>Estado</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="status" value="1" checked="checked">Activo
                                                </label> 
                                              <label class="radio-inline">
                                                  <input type="radio" name="status" value="0" >Inactivo</label>  
                                              </div>  -->
                                              <div class="col-sm-12 reset-button">
                                                 <a href="#" class="btn btn-warning">Volver</a>
                                                 <button type="submit" class="btn btn-success">Guardar</a>
                                             </div>
                                         <!-- </form> -->
                                     </div>
                                 </div>
                             </div>
                         </div>
                         
                    </form>     
                </section> <!-- /.content -->
           </div> <!-- /.content-wrapper -->
           
    </div> <!-- ./wrapper -->
        <!-- Start Core Plugins
        =====================================================================-->
        <!-- jQuery -->
        <?php include("footer.php"); ?>

        <script type="text/javascript">
            $("#add_user").submit(function(){
                console.log('se envia el form');
                $.post($(this).attr('action'), $(this).serialize(), function(res){
                    if (res == 1) {
                        location.href = "list_usuarios.php";
                    } else {
                        alert(res);
                    }
                    console.log(res);
                });
                return false;
            });
        </script>
      
    </body>

<!-- Mirrored from healthadmin.thememinister.com/add-department.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
</html>
