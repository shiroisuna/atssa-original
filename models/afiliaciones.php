<?php 
	
	class Afiliaciones {


		public function listAfiliaciones(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			// $query = $conn->prepare("SELECT * FROM afiliaciones");
			$query = $conn->prepare("SELECT * FROM afiliaciones as a INNER JOIN data_solicitante_afiliacion as b ON b.id_afiliacion = a.id_afiliacion");

			$query->execute();

			return $query->fetchAll();
		}

		public function buscarAfiliacion($id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM afiliaciones WHERE id_afiliacion = $id_afiliacion");
			
			$query->execute();

			return $query->fetchAll();
		}

		public function ListarEPS(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM eps_afiliacion");
			
			$query->execute();

			return $query->fetchAll();
		}

		/*funcion que yuda a traer todos los datos de compensacion para afiliacion*/

		public function compensacion(){
			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM compensacion");

			$query->execute();

			return $query->fetchAll();
		}
		/*___________________________________________________________________________*/

		
		public function buscarEPS($id_eps){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM eps_afiliacion WHERE id_eps = '$id_eps'");
			
			$query->execute();

			return $query->fetchAll();

		}



		public function addAfiliacion($id_eps, $tipo_tramite, $tipo_afiliacion, $sub_tipo_afil, $regimen, $tp_afiliado, $tp_cotizante, $codigo, $fech_sol){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO afiliaciones (id_eps, tipo_tramite, tipo_afiliacion, sub_tipo_afiliacion, regimen, tipo_de_afiliado, tipo_cotizante, codigo, fecha_solicitud) VALUES ('$id_eps', '$tipo_tramite', '$tipo_afiliacion', '$sub_tipo_afil', '$regimen', '$tp_afiliado', '$tp_cotizante', '$codigo', '$fech_sol')");

			// var_dump($query);
			$query->execute();

			$idafiliacion = $conn->lastinsertId();

			return $idafiliacion;
		}

		// public function updateDataSolic($id_solic, $id_afiliacion){

		// 	$modelo = new Conn();
		// 	$conn = $modelo->Connect();

		// 	$query = $conn->prepare("UPDATE afiliaciones SET id_data_solicitante = '$id_solic' WHERE id_afiliacion = '$id_afiliacion'");

		// 	$query->execute();

		// 	return $query->execute();
		// }

		public function addSolicitante($id_afiliacion, $nom1_solic, $nom2_solic, $apell1_solic, $apell2_solic, $tipo_id_solic, $n_identidad, $sexo_solic, $f_nac_solic, $edad_solic, $etnia, $condic, $discapacidad, $pt_sisben, $ad_riesgos, $ad_pension, $ing_cotizacion, $direccionsolic, $zona, $localidad, $barrio, $municipio, $dep, $tlf_f, $tlf_m, $email, $cod){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO data_solicitante_afiliacion (id_afiliacion, nom1_solic, nom2_solic, apellido1_solic, apellido2_solic, tipo_doc_id, nro_doc_id, fecha_nac, edad, sexo, etnia, condicion, discapacidad, pts_nivel_sisben, admin_riesgos, admin_pension, ingreso_cotiz, direccion, zona, localidad, barrio, municipio, departamento, tlf_cell, tlf_hab, correo, codigo) VALUES ('$id_afiliacion', '$nom1_solic', '$nom2_solic', '$apell1_solic', '$apell2_solic', '$tipo_id_solic', '$n_identidad', '$f_nac_solic', '$edad', '$sexo_solic', '$etnia', '$condic' ,'$discapacidad', '$pt_sisben', '$ad_riesgos', '$ad_pension', '$ing_cotizacion', '$direccionsolic', '$zona', '$localidad', '$barrio', '$municipio', '$dep', '$tlf_f', '$tlf_m', '$email', '$cod')");

			$query->execute();

			return $conn->lastinsertId();


		}

		public function addConyugue($nom_c, $apell_c, $t_doc_con, $nro_doc_con, $sexo_con, $f_nac_con, $parent, $id_solic){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO nucleo_familiar_afiliacion (nombres, apellidos, t_doc_id, nro_doc_id, sexo, fecha_nac, parentesco, id_data_solicitante) VALUES ('$nom_c', '$apell_c', '$t_doc_con', '$nro_doc_con', '$sexo_con', '$f_nac_con', '$parent', '$id_solic')");

			$query->execute();

			return 1;
			
		}


		public function addBeneficiarios($nom, $apell, $t_doc, $nro_identidad, $sexo, $fecha_nac, $parent, $etnia, $discapacidad, $municipio, $dep, $zona_b, $tlf_m, $tlf_f, $upc_b, $ins_salud_b, $id_solic){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO nucleo_familiar_afiliacion (nombres, apellidos, t_doc_id, nro_doc_id, sexo, fecha_nac, parentesco, etnia, discapacidad, municipio, depto, zona, tlf_cell, tlf_hab, valor_upc_dicional, inst_servicios_salud, id_data_solicitante) VALUES ('$nom', '$apell', '$t_doc', '$nro_identidad', '$sexo', '$fecha_nac', '$parent', '$etnia', '$discapacidad', '$municipio', '$dep', '$zona_b', '$tlf_m', '$tlf_f', '$upc_b', '$ins_salud_b', '$id_solic')");

			$query->execute();

			return 1;
			
		}


		public function addAportante($rif, $t_doc_ot, $identidad_ot, $t_aportante, $ciudad, $departa_aport, $tlf_mo, $tlf_f_aport, $email_aport, $fech_ing_t, $cargo_t, $salario_t, $id_solic){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO aportantes_afiliacion (razon_social, t_doc, nro_doc_id, tipo_aportante, ciudad, dpto, tlf_cell, tlf_hab, correo, fecha_ini_relacion_lab, cargo, salario, id_data_solicitante) VALUES ('$rif', '$t_doc_ot', '$identidad_ot', '$t_aportante', '$ciudad', '$departa_aport', '$tlf_mo', '$tlf_f_aport', '$email_aport', '$fech_ing_t', '$cargo_t', '$salario_t', '$id_solic')");

			$query->execute();

			return 1;
		}

		public function addNovedad($tipo_novedad, $movilidad, $traslado, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO novedades_afiliaciones (tipo_novedad, movilidad, traslado, id_afiliacion) VALUES ('$tipo_novedad', '$movilidad', '$traslado', '$id_afiliacion')");

			$query->execute();

			return $conn->lastinsertId();
		}

		public function addDataNovedad($rep_nom1, $rep_nom2, $rep_apell1, $rep_apell2, $t_doc_rep, $n_identidad_rep, $sexo_rep, $rep_fech_nac, $rep_fecha_ini, $eps_ant, $cod_traslado_rep, $compensacion_rep, $id_novedad){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO data_novedad (nom1_novedad, nom2_novedad, apellido1_novedad, apellido2_novedad, t_doc_novedad, nro_doc_novedad, sexo, fecha_nac, fecha_ini, eps_anterior, traslado, compensacion_familiar, id_novedad) VALUES ('$rep_nom1', '$rep_nom2', '$rep_apell1', '$rep_apell2', '$t_doc_rep', '$n_identidad_rep', '$sexo_rep', '$rep_fech_nac', '$rep_fecha_ini', '$eps_ant', '$cod_traslado_rep', '$compensacion_rep', '$id_novedad')");

			$query->execute();

			return 1;
		}

		public function addEntidadTerritorial($id_entidad_terr, $ficha, $puntaje, $nivel, $radicacion, $validacion, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO data_entidad_territ_afiliacion (tipo_id_entidad_territ, nro_ficha, puntaje, nivel, fecha_radicacion, fecha_validacion, id_afiliacion) VALUES ('$id_entidad_terr', '$ficha', '$puntaje', '$nivel', '$radicacion', '$validacion', '$id_afiliacion')");

			$query->execute();

			return $conn->lastinsertId();
		}

		public function addFuncValid($nom1_func, $nom2_func, $apell1_func, $apell2_func, $identidad_func, $observ_func, $id_data_entidad_territ){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO funcionario_validacion_afiliacion (id_data_entidad_territ, nom1_func, nom2_func, apell1_func, apell2_func, nro_identidad, observaciones) VALUES ('$id_data_entidad_territ', '$nom1_func', '$nom2_func', '$apell1_func', '$apell2_func', '$identidad_func', '$observ_func')");

			$query->execute();

			return 1;
		}

		public function addAuthMsn($auth_derechos_deberes, $auth_desempeño_eps, $auth_contenido_desempeño, $auth_dudas, $auth_entender, $auth_canales_disp, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO autorizacion_msn_texto_afiliacion (recibo_carta_derechos, recibo_carta_desempeno, leer_carta_desempeno, dudas_resueltas, comp_renuncia_derechos_desempeno, new_eps_canales_eficaces, id_afiliacion) VALUES ('$auth_derechos_deberes', '$auth_desempeño_eps', '$auth_contenido_desempeño', '$auth_dudas', '$auth_entender', '$auth_canales_disp', '$id_afiliacion')");

			$query->execute();

			return 1;
		}

		public function addAuthDeclar($dep_economica, $entrega_docs, $interm_cotizante, $eps_datos_cotizante, $reporte_info_eps, $manejo_datoseps_cotizante, $eps_envio_info_correo, $oblig_afiliacion, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO declaracion_autorizacion (dep_economica, entrega_docs, interm_cotizante, eps_datos_cotizante, reporte_info_eps, manejo_datoseps_cotizante, eps_envio_info_correo, oblig_afiliacion, id_afiliacion) VALUES ('$dep_economica', '$entrega_docs', '$interm_cotizante', '$eps_datos_cotizante', '$reporte_info_eps', '$manejo_datoseps_cotizante', '$eps_envio_info_correo', '$oblig_afiliacion', $'id_afiliacion')");

			$query->execute();

			return 1;
		}

		public function rptAfiliacionedit($id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT a.*, b.*, c.*, d.*, e.*, f.*, g.*, h.* FROM afiliaciones a 
				INNER JOIN data_solicitante_afiliacion b ON b.id_afiliacion = a.id_afiliacion
				INNER JOIN nucleo_familiar_afiliacion c ON c.id_data_solicitante = b.id_data_solicitante
				INNER JOIN aportantes_afiliacion d ON d.id_data_solicitante = b.id_data_solicitante
				INNER JOIN novedades_afiliaciones e ON e.id_afiliacion = a.id_afiliacion
				INNER JOIN data_novedad f ON f.id_novedad = e.id_novedad
				INNER JOIN data_entidad_territ_afiliacion g ON g.id_afiliacion = a.id_afiliacion
				INNER JOIN funcionario_validacion_afiliacion h ON h.id_data_entidad_territ = g.id_data_entidad_territ
				WHERE a.id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return $query->fetchAll();
			
		}


		public function verAfiliacion($id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT a.*, b.*, c.*, d.*, e.*, f.*, g.*, h.* FROM afiliaciones a 
				INNER JOIN data_solicitante_afiliacion b ON b.id_afiliacion = a.id_afiliacion
				INNER JOIN nucleo_familiar_afiliacion c ON c.id_data_solicitante = b.id_data_solicitante
				INNER JOIN aportantes_afiliacion d ON d.id_data_solicitante = b.id_data_solicitante
				INNER JOIN novedades_afiliaciones e ON e.id_afiliacion = a.id_afiliacion
				INNER JOIN data_novedad f ON f.id_novedad = e.id_novedad
				INNER JOIN data_entidad_territ_afiliacion g ON g.id_afiliacion = a.id_afiliacion
				INNER JOIN funcionario_validacion_afiliacion h ON h.id_data_entidad_territ = g.id_data_entidad_territ
				WHERE a.id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return $query->fetchAll();
			
		}

		public function rptAfiliacion(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM afiliaciones a 
				INNER JOIN data_solicitante_afiliacion b ON b.id_afiliacion = a.id_afiliacion
				INNER JOIN nucleo_familiar_afiliacion c ON c.id_data_solicitante = b.id_data_solicitante
				INNER JOIN aportantes_afiliacion d ON d.id_data_solicitante = b.id_data_solicitante
				INNER JOIN novedades_afiliaciones e ON e.id_afiliacion = a.id_afiliacion
				INNER JOIN data_novedad f ON f.id_novedad = e.id_novedad
				INNER JOIN data_entidad_territ_afiliacion g ON g.id_afiliacion = a.id_afiliacion
				INNER JOIN funcionario_validacion_afiliacion h ON h.id_data_entidad_territ = g.id_data_entidad_territ
				WHERE a.id_afiliacion in(SELECT MAX(id_afiliacion) FROM afiliaciones)");

			$query->execute();

			return $query->fetchAll();
			
		}

		public function updateAfiliacion($tipo_tramite, $tipo_afiliacion, $sub_tipo_afil, $regimen, $tp_afiliado, $tp_cotizante, $codigo, $fech_sol, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE afiliaciones SET tipo_tramite = '$tipo_tramite', tipo_afiliacion = '$tipo_afiliacion', sub_tipo_afiliacion = '$sub_tipo_afil', regimen = '$regimen', tipo_afiliado = '$tp_afiliado', tipo_cotizante = '$tp_cotizante', codigo = '$codigo', fecha_solicitud = '$fech_sol'  WHERE id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return 1;

		}

		public function updateSolicitante($nom1_solic, $nom2_solic, $apell1_solic, $apell2_solic, $tipo_id_solic, $n_identidad, $sexo_solic, $f_nac_solic, $etnia, $discapacidad, $condic, $pt_sisben, $ad_riesgos, $ad_pension, $ing_cotizacion, $direccionsolic, $zona, $localidad, $barrio, $municipio, $dep, $tlf_f, $tlf_m, $email, $cod, $id_afiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE data_solicitante_afiliacion SET nom1_solic = '$nom1_solic', nom2_solic = '$nom2_solic', apellido1_solic = '$apell1_solic', apellido2_solic = '$apell2_solic', tipo_doc_id = '$tipo_id_solic', nro_doc_id = '$n_identidad', fecha_nac = '$f_nac_solic', sexo = '$sexo_solic', etnia = '$etnia', condicion = '$condic', discapacidad = '$discapacidad', pts_nivel_sisben = '$pt_sisben', admin_riesgos = '$ad_riesgos', admin_pension = '$ad_pension', ingreso_cotiz = '$ing_cotizacion', direccion = '$direccionsolic', zona = '$zona', localidad = '$localidad', barrio = '$barrio', municipio = '$municipio', departamento = '$dep', tlf_cell = '$tlf_m', tlf_hab = '$tlf_f', correo = '$email', codigo = '$cod' WHERE id_afiliacion = '$id_afiliacion'");

			
			$query->execute();

			$query = $conn->prepare("SELECT id_data_solicitante FROM data_solicitante_afiliacion WHERE id_afiliacion = '$id_afiliacion'");
			
			$query->execute();			

			return $query->fetchAll();

		}

		public function updateBeneficiario($nom, $apell, $t_doc, $nro_identidad, $parent, $etnia, $discapacidad, $municipio, $dep, $zona_b, $tlf_m, $tlf_f, $upc_b, $ins_salud_b, $n_sisben, $g_poblacional_b, $id_solic){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE nucleo_familiar_afiliacion SET nombres = '$nom', apellidos = '$apell', t_doc_id = '$t_doc', nro_doc_id = '$nro_identidad', parentesco = '$parent', etnia = '$etnia', discapacidad = '$discapacidad', municipio = '$municipio', depto = '$dep', zona = '$zona_b', tlf_cell = '$tlf_m', tlf_hab = '$tlf_f', valor_upc_dicional = '$upc_b', inst_servicios_salud = '$ins_salud_b', nivel_sisben = '$n_sisben', grupo_poblacional = '$g_poblacional_b' WHERE id_data_solicitante = '$id_solic'");

			$query->execute();

			return 1;
		}

		public function updateAportante($rif, $t_doc_ot, $identidad_ot, $t_aportante, $ciudad, $departa_aport, $tlf_mo, $tlf_f_aport, $email_aport, $fech_ing_t, $cargo_t, $salario_t, $id_solic){
			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE afiliaciones SET razon_social = '$rif', t_doc = '$t_doc_ot', nro_doc_id = '$identidad_ot', tipo_aportante = '$t_aportante', ciudad = '$ciudad', dpto = 'departa_aport', tlf_cell = '$tlf_mo', tlf_hab = '$tlf_f_aport', correo = '$email_aport', fecha_ini_relacion_lab = '$fech_ing_t', cargo = '$cargo_t', salario = '$salario_t'  WHERE id_data_solicitante = '$id_solic'");

			$query->execute();

			return 1;

		}
		
		public function updateNovedad($tipo_novedad, $movilidad, $traslado, $idafiliacion){
			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE novedades_afiliaciones SET tipo_novedad = '$tipo_novedad', movilidad = '$movilidad', traslado = '$traslado' WHERE id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return 1;

		}

		public function updateDataNovedad($rep_nom1, $rep_nom2, $rep_apell1, $rep_apell2, $t_doc_rep, $n_identidad_rep, $sexo_rep, $rep_fech_nac, $rep_fecha_ini, $eps_ant, $cod_traslado_rep, $compensacion_rep, $idnovedad){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE data_novedad SET nom1_novedad = '$tipo_tramite', nom2_novedad = '$tipo_afiliacion', apellido1_novedad = '$sub_tipo_afil', apellido2_novedad = '$regimen', t_doc_novedad = '$tp_afiliado', nro_doc_novedad = '$tp_cotizante', sexo = '$codigo', fecha_nac = '$fech_sol', fecha_ini, eps_anterior, traslado, compensacion_familiar WHERE id_novedad = '$idnovedad'");

			$query->execute();

			return 1;

		}

		public function updateEntidadTerritorial($id_entidad_terr, $ficha, $puntaje, $nivel, $radicacion, $validacion, $idafiliacion){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE afiliaciones SET tipo_tramite = '$tipo_tramite', tipo_afiliacion = '$tipo_afiliacion', sub_tipo_afiliacion = '$sub_tipo_afil', regimen = '$regimen', tipo_afiliado = '$tp_afiliado', tipo_cotizante = '$tp_cotizante', codigo = '$codigo', fecha_solicitud = '$fech_sol'  WHERE id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return 1;

		}

		public function updateFuncValid($nom1_func, $nom2_func, $apell1_func, $apell2_func, $identidad_func, $observ_func, $id_data_entidad_territ){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE afiliaciones SET tipo_tramite = '$tipo_tramite', tipo_afiliacion = '$tipo_afiliacion', sub_tipo_afiliacion = '$sub_tipo_afil', regimen = '$regimen', tipo_afiliado = '$tp_afiliado', tipo_cotizante = '$tp_cotizante', codigo = '$codigo', fecha_solicitud = '$fech_sol'  WHERE id_afiliacion = '$id_afiliacion'");

			$query->execute();

			return 1;

		}

		public function selectDptos(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM departamentos");


			$query->execute();

			return $query->fetchAll();

		}

		public function selectMunicipios($id_dpto){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM municipios WHERE id_dpto = '$id_dpto'");


			$query->execute();

			return $query->fetchAll();

		}

		public function selectMunicipiosTodo(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM municipios");


			$query->execute();

			return $query->fetchAll();

		}


		//devuelve los ips segun departamento selecionado
		public function selectIpssegunIdDpto($id_dpto) {

			$modelo = new Conn();
			$conn = $modelo->Connect();



			if( $id_dpto == 1) {
				$query = $conn->prepare("SELECT * FROM antioquia");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 2) {
				$query = $conn->prepare("SELECT * FROM atlantico");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 3) {
				$query = $conn->prepare("SELECT * FROM bogotadc");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 4) {
				$query = $conn->prepare("SELECT * FROM bolivar");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 5) {
				$query = $conn->prepare("SELECT * FROM  boyaca");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 6) {
				$query = $conn->prepare("SELECT * FROM caldas");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 7) {
				$query = $conn->prepare("SELECT * FROM caqueta");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 8) {
				$query = $conn->prepare("SELECT * FROM cauca");

				$query->execute();

				return $query->fetchAll();
			}
			
			if( $id_dpto == 9) {
				$query = $conn->prepare("SELECT * FROM cesar");

				$query->execute();

				return $query->fetchAll();
			}
			
			if( $id_dpto == 10) {
				$query = $conn->prepare("SELECT * FROM cordoba");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 11) {
				$query = $conn->prepare("SELECT * FROM cundinamarca");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 12) {
				$query = $conn->prepare("SELECT * FROM choco");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 13) {
				$query = $conn->prepare("SELECT * FROM huila");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 14) {
				$query = $conn->prepare("SELECT * FROM  laguajira");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 15) {
				$query = $conn->prepare("SELECT * FROM magdalena");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 16) {
				$query = $conn->prepare("SELECT * FROM meta");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 17) {
				$query = $conn->prepare("SELECT * FROM narino");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 18) {
				$query = $conn->prepare("SELECT * FROM nortedesantander");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 19) {
				$query = $conn->prepare("SELECT * FROM quindio");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 20) {
				$query = $conn->prepare("SELECT * FROM risaralda");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 21) {
				$query = $conn->prepare("SELECT * FROM santander");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 22) {
				$query = $conn->prepare("SELECT * FROM sucre");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 23) {
				$query = $conn->prepare("SELECT * FROM  tolima");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 24) {
				$query = $conn->prepare("SELECT * FROM valledelcauca");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 25) {
				$query = $conn->prepare("SELECT * FROM   arauca");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 26) {
				$query = $conn->prepare("SELECT * FROM casanare");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 27) {
				$query = $conn->prepare("SELECT * FROM putumayo");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 28) {
				$query = $conn->prepare("SELECT * FROM sanandresprovidencia");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 29) {
				$query = $conn->prepare("SELECT * FROM amazonas");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 30) {
				$query = $conn->prepare("SELECT * FROM guainia");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 31) {
				$query = $conn->prepare("SELECT * FROM guaviare");

				$query->execute();

				return $query->fetchAll();
			}

			if( $id_dpto == 32) {
				$query = $conn->prepare("SELECT * FROM vaupes");

				$query->execute();

				return $query->fetchAll();

			}

			if( $id_dpto == 33) {
				$query = $conn->prepare("SELECT * FROM vichada");

				$query->execute();

				return $query->fetchAll();
			}

		}

		/*end function __________________________________________*/
	}
 ?>