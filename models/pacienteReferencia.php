<?php 
	
	class PacienteReferencia {

		public function addPacienteRefencia($t_atencion, $nombre, $apellido, $tipo_identidad, $nro_identidad, $eps, $dep, $municipio_origen, $municipio_destino, $tlf, $nombre_acompanante, $tlf_acompanante, $email_acompanante, $razon_social, $nit, $val_medica, $at_enfermeria, $serv_rehab, $medicinas, $insumos, $anexo){

			$modelo = new Conn();
			$conn   = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO referencia_paciente (t_atencion, nombre, apellido, tipo_identidad, nro_identidad, eps, dep, municipio_origen, municipio_destino, tlf, nombre_acompanante, tlf_acompanante, email_acompanante, razon_social, nit, val_medica, at_enfermeria, serv_rehab, medicinas, insumos, anexo) VALUES ('$t_atencion', '$nombre', '$apellido', '$tipo_identidad', '$nro_identidad', '$eps', '$dep', '$municipio_origen', '$municipio_destino', '$tlf', '$nombre_acompanante', '$tlf_acompanante', '$email_acompanante', '$razon_social', '$nit', '$val_medica', '$at_enfermeria', '$serv_rehab', '$medicinas', '$insumos', '$anexo')");

			$query->execute();
			
			return 1;

		}

		public function listaReferencia(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM referencia_paciente");

			$query->execute();

			return $query->fetchAll();
		}

		public function selectIPS($id_dpto){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM ips_departamentos inner join ips on ips_departamentos.id_ips = ips.id inner join departamentos on ips_departamentos.id_departamento = departamentos.id_dpto where ips_departamentos.id_departamento = '$id_dpto'");


			$query->execute();

			return $query->fetchAll();

		}

		public function selectTablaIPS($id_dpto){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM departamentos_ips inner join departamentos on departamentos_ips.departamento_id = departamentos.id_dpto where departamentos_ips.departamento_id = '$id_dpto'");


			$query->execute();

			$tabla = $query->fetchAll();

			if( $tabla[0]['tabla_ips'] ){

				$nombreTabla = $tabla[0]['tabla_ips'];

				$query2 = $conn->prepare("SELECT * FROM $nombreTabla");

				$query2->execute();

				return $query2->fetchAll();

				//return $tabla[0]['tabla_ips'];

			}else{
				return '';
			}
			

			

		}



		

	}
?>

