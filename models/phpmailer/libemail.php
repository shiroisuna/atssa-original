<?php

class LibEmail{
	
	function enviarcorreo($email, $asunto, $htmlcuerpo){
		
		$html = "
		<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<style type='text/css'>
		body {
			font-family: 'Trebuchet MS', sans-serif;
		}
		
		.title {
			font-family: 'Trebuchet MS', sans-serif;
		}
		
		.titlenews {
			line-height: 25px;
			padding-top: 8px;
			padding-left: 16px;
		}
		
		.titlenews a {
			text-decoration: none;
			color: #1F8ADC;
			font-size: 32px;
		}
		
		.backgroundtitlenews {
			width: 100%;
			display: inline-block;
		}
		
		.titledemo {
			font-size: 21px;
			margin-bottom: 10px;
			text-align: center;
			color: #006A85;
		}
		
		.titleemail {
			font-size: 25px;
			text-decoration: none;
			color: #0088AA;
		}
		
		.titlebenefits {
			font-size: 21px;
			margin-bottom: 10px;
			text-align: center;
			color: #006A85;
		}
		
		.titledominio {
			color: #fff;
			text-align: center;
			padding: auto auto;
			font-size: 20px
		}
		
		.subtitlenews {
			font-size: 19px;
			font-weight: 500;
			margin-bottom: 5px;
			text-align: left;
			color: #006A85;
			width: 100%;
			font-family: 'Trebuchet MS', sans-serif;
		}
		
		.textnews {
			font-family: 'Arial', serif;
		}
		
		.date {
			font-size: 15px;
			text-decoration: none;
			color: #fff;
			padding-top: 0px;
		}
		
		.datenews {
			font-size: 14px;	
			text-align: left;
			color: #006A85;
		}
		
		.footer {
			font-family: 'Arial', serif;
			font-size: 13px;
		}
		
		li {
			list-style-type: circle;
			margin-left: -1em;
			margin-right: .100em;
		}
		
		ul {
			padding-left: 20px;
			text-indent: 2px;
			list-style: none;
			list-style-position: outside;
		}
		
		.sep1 {
			background: #00627A;
			height: 1px;
			width: 45%;
			margin-top: 5px
		}
		
		.sep2 {
			background: #E5E8E9;
			height: 1px;
			width: 60%;
			margin: 30px auto 7px auto
		}
		
		.sep3 {
			height: 1px;
			width: 60%;
			margin: 30px auto 7px auto
		}
		
		.sep4 {
			background: #E5E8E9;
			height: 1px;
			width: 60%;
			margin: 10px auto 7px auto
		}
		</style>
		</head>
		<body>
		<center>
			<table width='850px' border='0'
				style='min-height: 110px; width: 850px; border-collapse: collapse; margin: auto auto auto auto'>
				<tr
					style='width: 100%; background: #FFF; overflow: auto; border-bottom: 5px solid #252525;'>
					<td width='28%'
						style='width: 28%; margin: 10px auto auto auto; text-align: center;'>
		
					</td>
					<td width='40%'
						style='width: 40%; margin: 0px auto auto auto; text-align: center'>
						<a href='https://www.systemlider.com/proy/weproyet/' style='text-decoration: none;'> <img
							alt='We Proyet'
							src='https://www.systemlider.com/proy/weproyet/images/logo.png' height='70px' width='200px'></a>
					</td>
					<td width='33%' class='date'
						style='width: 33%; margin: 0px auto auto auto; vertical-align: middle;'>
								
					</td>
				</tr>
			</table>
		
			<table width='850px' border='0'
				style='width: 850px; border-collapse: collapse; margin: auto auto auto auto'>
				<tr width='95%' style='width: 95%; text-align: center;'>
					<td colspan='3' style='padding-top: 20px;'>
						$htmlcuerpo
					</td>
				</tr>
			</table>	
			<br>
			<table width='850px' border='0'
				style='width: 850px; border-collapse: collapse; margin: 10px auto auto auto; background: #252525; height: 33px'>
				<tr width='100%'
					style='width: 100%; background: #252525; overflow: auto;'>
					<td align='center' width='100%'
						style='width: 100%; margin: auto auto auto auto; text-align: center; color: #fff; font-size: 13px; padding-left: 15px'>
						<a align='center' href='https://www.weproyet.com/' target='_blank'
						style='color: #fff; text-decoration: none; text-align: center; font-size: 17px'>
							
								www.weproyet.com
								<br>
							</a>
					</td>
				</tr>
			</table>
			
			<table width='850px' border='0'
				style='min-height: 110px; width: 850px; border-collapse: collapse; margin: auto auto auto auto; padding-bottom: 20px'>
				<tr style='width: 100%; background: #000; overflow: auto;'>
					<td width='50%'
						style='width: 50%; margin: auto auto auto auto; text-align: left; color: #FFF; font-size: 13px; padding-left: 15px'>
						Recibes este email porque eres cliente de We Proyet o<br /> te has
						suscrito a nuestro boletín en <br> <a
							href='https://www.weproyet.com/' target='_blank'
							style='color: #FFF; text-decoration: underline;'> <span
								style='color: #FFF !important;'> www.weproyet.com<br />
							</span>
						</a> 
					</td>
					<td width='50%'
						style='width: 50%; margin: 0px auto auto auto; text-align: right; color: #000; font-size: 13px; padding-right: 15px; padding-top: 10px; padding-bottom: 10px'>
						
							<a
									href='tel:+12345678'
									style='text-decoration: none; color: #FFF;'> <span
										style='color: #FFF !important;'> <img
											src='https://www.systemlider.com/proy/weproyet/images/phone.png'
											width='10px' style='width: 10px' />+12345678
									</span>
								</a> <br> <a href='mailto:info@weproyet.com'
										style='text-decoration: none; color: #FFF;'> <span
											style='color: #FFF !important;'> <img
												src='https://www.systemlider.com/proy/weproyet/images/email.png'
												width='10px' style='width: 15px' /> info@weproyet.com
										</span>
									</a> <br></br>
					
					</td>
				</tr>
			</table>
		</center>
		</body>
		</html>
				";
	
		$to      = $email;
		$subject = $asunto;
						
		$message = $html;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: We Proyet info@weproyet.com' . "\r\n" .
				'Reply-To: We Proyet info@weproyet.com' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
		
		//////mail($to, $subject, $message, $headers);
		
		return true;
	
	}
	
		
}
?>