<?php 

	class Doctores{

		public function searchDoctor($doctor_nom){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM doctores WHERE  doc_nombre = '$doctor_nom'");

			$query->execute();

			return $query->fetchAll();

		}

		public function listDoc(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM doctores");

			$query->execute();

			return $query->fetchAll();
		}

		public function addDoctor($doctor_nom, $doctor_apell, $email, $departamento, $direccion, $tlf_cell, $tlf_hab, $sueldo, $foto, $biografia, $fecha_nac){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO doctores (doc_nombre, doc_apellido, doc_departamento, doc_direccion, doc_email, tlf_cel, tlf_hab, sueldo, doc_img, biografia, fecha_nac) VALUES ('$doctor_nom', '$doctor_apell', '$departamento', '$direccion', '$email', '$tlf_cell', '$tlf_hab', '$sueldo', '$foto', '$biografia', '$fecha_nac')");
			
			$query->execute();

			return 1;

		}

		public function listDocParams($id){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM doctores WHERE id_doctor='$id'");

			$query->execute();

			return $query->fetchAll();
		}

		public function updateDoctor($id_doc, $email, $depto, $direccion, $tlf_movil, $tlf_casa, $sueldo, $foto){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("UPDATE doctores SET doc_email = '$email', doc_departamento = '$depto', doc_direccion = '$direccion', tlf_cel = '$tlf_movil', tlf_hab = '$tlf_casa', sueldo = '$sueldo', doc_img = '$foto' WHERE id_doctor = '$id_doc'");

			$query->execute();

			return $query->fetchAll();

		}

		public function eliminarDoctor($id_doc, $status){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			if ($status == 1) {
				
				$query = $conn->prepare("UPDATE doctores SET status_doctor = '0' WHERE id_doctor='$id_doc'");
				
			}else{

				$query = $conn->prepare("UPDATE doctores SET status_doctor = '1' WHERE id_doctor='$id_doc'");
			}

			$query->execute();

			return $query->fetchAll();

		}

		//function de prueba para guardar formulario de captacion

		public function captacionPacienteRiesgo2($fecha_format, $nom_upgd, $nom_evento, $t_doc, $nr_identidad, $fech_nac, $edad, $tlf, $sex, $peso, $altura, $IMC, $nom1, $nom2, $apell1, $apell2, $dpto_res, $dir_res, $barrio_res, $mun_res, $location_res, $otro_barrio, $pers_etnica, $g_poblacional, $estrato, $t_aseg, $nivel_educativo, $edo_civil, $eps, $regimen, $eqSaludAfilia) {
			$modelo = new Conn();
			$conn = $modelo->Connect();

			$sql = "INSERT INTO rpt_pacientes_riesgos2 (fecha, nom_upgd, nom_evento, t_doc, nro_identidad, fech_nacimiento, edad, tlf, sexo, peso, estatura, imc, nombre1, nombre2, apellido1, apellido2, dpto_res, dir_res, barrio_res, mun_res, location_res, otro_barrio, pers_etnica, grupo_poblacional, estrato, t_aseg, nivel_educativo, edo_civil, eps, regimen, eqSaludAfilia) VALUES ('$fecha_format', '$nom_upgd', '$nom_evento', '$t_doc', '$nr_identidad', '$fech_nac', '$edad', '$tlf', '$sex', '$peso', '$altura', '$IMC', '$nom1', '$nom2', '$apell1', '$apell2', '$dpto_res', '$dir_res', '$barrio_res', '$mun_res', '$location_res', '$otro_barrio', '$pers_etnica', '$g_poblacional', '$estrato', '$t_aseg', '$nivel_educativo', '$edo_civil', '$eps', '$regimen', '$eqSaludAfilia')";

			$insert = $conn->query($sql);

			if ($insert) {
				$id = $conn->lastInsertId();
				return $id;
			} else {
				return 0;
			}
		}

		public function captacionPacienteRiesgo($fecha_format, $nom_upgd, $nom_evento, $t_doc, $nr_identidad, $fech_nac, $edad, $tlf, $sex, $nom1, $nom2, $apell1, $apell2, $dpto_res, $dir_res, $barrio_res, $mun_res, $location_res, $otro_barrio, $pers_etnica, $g_poblacional, $estrato, $t_aseg, $n_educat, $edo_civil, $peso, $altura, $IMC){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO rpt_pacientes_riesgos (fecha, nom_upgd, nom_evento, t_doc, nro_identidad, fech_nacimiento, edad, tlf, sexo, peso, estatura, imc, nombre1, nombre2, apellido1, apellido2, dpto_res, dir_res, barrio_res, mun_res, location_res, otro_barrio, pers_etnica, grupo_poblacional, estrato, t_aseg, nivel_educativo, edo_civil) VALUES ('$fecha_format', '$nom_upgd', '$nom_evento', '$t_doc', '$nr_identidad', '$fech_nac', '$edad', '$tlf', '$sex','$peso', '$altura', '$IMC', '$nom1', '$nom2', '$apell1', '$apell2', '$dpto_res', '$dir_res', '$barrio_res', '$mun_res', '$location_res', '$otro_barrio', '$pers_etnica', '$g_poblacional', '$estrato', '$t_aseg', '$n_educat', '$edo_civil')");

			$query->execute();

			$id_paciente = $conn->lastInsertId();

			return $id_paciente;
		}

		public function listCaptacion(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM rpt_pacientes_riesgos2");

			$query->execute();

			return $query->fetchAll();

		}


		public function buscarPacCaptacion($id_paciente){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM rpt_pacientes_riesgos WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			return $query->fetchAll();

		}

		public function buscarRiesgosCard($id_paciente){


			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM riesgos_cardiovas WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			
			return $query->fetchAll();



		}
		public function buscarRiesgosSalud($id_paciente){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM riesgo_salud_mental WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			
			return $query->fetchAll();

		}

		public function buscarRiesgosInfecc($id_paciente){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM riesgo_infectocontagioso WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			return $query->fetchAll();

		}

		public function buscarRiesgosGestante($id_paciente){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM riesgo_gestante WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			return $query->fetchAll();

			
		}

		public function buscarRiesgosCancer($id_paciente){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM riesgo_cancer WHERE id_paciente_riesgo = '$id_paciente'");

			$query->execute();

			return $query->fetchAll();

		}


		public function addRiesgoCardiovas($tipo_riesgo, $id_paciente_riesgo, $c_cigarrillo, $antesc_familiar, $hta, $irc, $dislipidemia, $dm2, $glucosa, $sedentarismo, $insuf_card, $insuf_card_chag, $ar_cardiacas, $infartos_previos, $antesc_cerebvas, $tep_tvp, $coagulopatias){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO riesgos_cardiovas (tipo_de_riesgo, id_paciente_riesgo, cigarrillo, antescedente_familiar, hta, irc, dislipidemia, diabetes_dm2, glucosa, sedentarismo, insuf_card, insuf_card_chag, ar_cardiacas, infartos_previos, antesc_cerebvas, tep_tvp, coagulopatias) VALUES ('$tipo_riesgo', '$id_paciente_riesgo', '$c_cigarrillo', '$antesc_familiar', '$hta', '$irc', '$dislipidemia', '$dm2', '$glucosa', '$sedentarismo', '$insuf_card', '$insuf_card_chag', '$ar_cardiacas', '$infartos_previos', '$antesc_cerebvas', '$tep_tvp', '$coagulopatias')");

			$query->execute();

			return 1;

		}

		public function addRiesgoSalud($ipo_riesgo, $id_paciente_riesgo, $cons_psicoactivas, $antesc_tras_psiquis, $esquizofrenia, $antes_cond_suicid, $tras_depresivo, $antes_violencia, $tras_personalidad, $abuso_alcohol, $tras_bipolar, $estres_posttr, $plan_suicida){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO riesgo_salud_mental (tipo_de_riesgo, id_paciente_riesgo, cons_psicoactivas, antesc_tras_psiquis, esquizofrenia, antes_cond_suicid, tras_depresivo, antes_violencia, tras_personalidad, abuso_alcohol, tras_bipolar, estres_posttr, plan_suicida) VALUES ('$ipo_riesgo', '$id_paciente_riesgo','$cons_psicoactivas', '$antesc_tras_psiquis', '$esquizofrenia', '$antes_cond_suicid', '$tras_depresivo', '$antes_violencia', '$tras_personalidad', '$abuso_alcohol', '$tras_bipolar', '$estres_posttr', '$plan_suicida')");

			$query->execute();

			return 1;

		}

		public function addRiesgoInfeccion($ipo_riesgo, $id_paciente_riesgo, $vih, $hepa_a, $sifilis, $esputar, $rayosx_esputos, $antes_contacto, $sintomas_b, $leishmaniasis, $dengue, $malaria, $chagas){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO riesgo_infectocontagioso (tipo_de_riesgo, id_paciente_riesgo, vih, hepa_a, sifilis, esputar, rayosx_esputos, antes_contacto, sintomas_b, leishmaniasis, dengue, malaria, chagas) VALUES ('$ipo_riesgo', '$id_paciente_riesgo', '$vih', '$hepa_a', '$sifilis', '$esputar', '$rayosx_esputos', '$antes_contacto', '$sintomas_b', '$leishmaniasis', '$dengue', '$malaria', '$chagas')");

			$query->execute();


			return 1;
		}

		public function addRiesgoGestante($ipo_riesgo, $id_paciente_riesgo, $n_controls_gest, $edad_embrion, $gest_mayor, $preeclamsia, $hta_gest, $eclampsia, $sobrepeso_gest, $obeso_gest, $toxoplasmosis, $sifilis_gest, $vih_gest, $hepatitis_b_gest, $citomegalovirus, $rubeola_gest, $estrep_gest, $leish_gest, $diabetes_pregest, $diabetes_gest, $emb_gemelos, $hipotiroidismo, $parto_prematuro, $cardio_madre, $drogas_alcohol, $malform_feto, $placenta_previa, $anemia, $presen_transv, $p_podalica){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO riesgo_gestante (tipo_de_riesgo, id_paciente_riesgo, n_controls_gest, edad_embrion, gest_mayor, preeclamsia, hta_gest, eclampsia, sobrepeso_gest, obeso_gest, toxoplasmosis, sifilis_gest, vih_gest, hepatitis_b_gest, emb_gemelos, hipotiroidismo, parto_prematuro, parto_prematuro, cardio_madre, drogas_alcohol, malform_feto, placenta_previa, anemia, presen_transv, p_podalica, leishmaniasis_gest, citomegalovirus, rubeola, estreptococo, diabetes_prege, diabetes_ge) VALUES ('$ipo_riesgo', '$id_paciente_riesgo', '$n_controls_gest', '$edad_embrion', '$gest_mayor', '$preeclamsia', '$hta_gest', '$eclampsia', '$sobrepeso_gest', '$obeso_gest', '$toxoplasmosis', '$sifilis_gest', '$vih_gest', '$hepatitis_b_gest', '$emb_gemelos', '$hipotiroidismo', '$parto_prematuro', '$cardio_madre', '$drogas_alcohol', '$malform_feto', '$placenta_previa', '$anemia', '$presen_transv', '$p_podalica', '$leish_gest', '$citomegalovirus', '$rubeola_gest', '$estrep_gest', '$diabetes_pregest', '$diabetes_gest')");

			$query->execute();

			return 1;

		}

		public function addRiesgoCancer($ipo_riesgo, $id_paciente_riesgo, $cancer_mama, $cancer_cervix, $cancer_prostata){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO riesgo_cancer (tipo_de_riesgo, id_paciente_riesgo, cancer_mama, cancer_cervix, cancer_prostata) VALUES ('$ipo_riesgo', '$id_paciente_riesgo', '$cancer_mama', '$cancer_cervix', '$cancer_prostata')");

			$query->execute();

			return 1;

		}

		public function addEquipo($razon_social, $nit, $cod_habilitacion, $nombres, $apellidos, $tipo_doc, $nro_doc, $profesion, $sexo, $depto, $munic, $tlf, $email, $f_nac){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO  equipos_salud (nombre_empresa, nit_empresa, cod_habilitacion, nombres, apellidos, t_doc, nro_doc, profesion, sexo, depto, municipio, tlf, email, fecha_nac) VALUES ('$razon_social', '$nit', '$cod_habilitacion', '$nombres', '$apellidos', '$tipo_doc', '$nro_doc', '$profesion', '$sexo', '$depto', '$munic', '$tlf', '$email', '$f_nac')");

			$query->execute();

			return 1;

		}

		public function addReferencia($id_equipo, $nombre, $apellido, $tipo_identidad, $nro_doc, $eps, $munic_orig, $munic_dest, $direccion, $tlf, $nombre_acomp, $tlf_acomp, $email_acomp, $descrip_diagnostico, $val_medica, $at_enfermeria, $serv_rehab, $medicinas, $insumos){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("INSERT INTO  pacientes_referencias (id_equipo, nombre, apellido tipo_identidad, nro_identidad, eps, munic_origen, munic_destino, direccion, tlf, nomb_acomp, tlf_acomp, correo_acomp, descrip_diagnostico, valor_medica, atenc_enfermeria, serv_rehabilitacion, medicinas, insumos) VALUES ( '$id_equipo', '$nombre', '$apellido', '$tipo_identidad', '$nro_doc', '$eps', '$munic_orig', '$munic_dest', '$direccion', '$tlf', '$nombre_acomp', '$tlf_acomp', '$email_acomp', '$descrip_diagnostico', '$val_medica', '$at_enfermeria', '$serv_rehab', '$medicinas', '$insumos')");

			$query->execute();

			return 1;

		}

		public function listEquipoSalud(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM equipos_salud");

			$query->execute();

			return $query->fetchAll();

		}

		public function listReferPaciente(){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM pacientes_referencias");

			$query->execute();

			return $query->fetchAll();

		}

		public function buscarEquipoSalud($id_equipo){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM equipos_salud WHERE idequipo = '$id_equipo'");

			$query->execute();

			return $query->fetchAll();

		}

		public function buscarReferenciaPaciente($paciente_refer){

			$modelo = new Conn();
			$conn = $modelo->Connect();

			$query = $conn->prepare("SELECT * FROM pacientes_referencias WHERE id_pacientes_refer = '$paciente_refer'");

			$query->execute();

			return $query->fetchAll();

		}

		
	}
 ?>