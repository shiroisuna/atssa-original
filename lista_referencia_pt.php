<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:16:55 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
                <!-- Header Navbar -->
               <?php include("menu-top.php"); ?>
                            </header>
                            <!-- =============================================== -->
                            <!-- Left side column. contains the sidebar -->
                            <?php include("menu-left.php"); ?>
                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-box1"></i>
                        </div>
                        <div class="header-title">
                            
                            <h1>Referencia de Pacientes</h1>
                            <small>Lista de Referencia de Pacientes</small>
                            <ol class="breadcrumb hidden-xs">
                                <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
                                <li class="active">Referencia de Paciente</li>
                            </ol>
                        </div>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-bd lobidrag">
                                    <div class="panel-heading">
                                        <div class="btn-group"> 
                                            <a class="btn btn-success" href="add_refer_paciente.php"> <i class="fa fa-plus"></i> Añadir Referencia de Paciente
                                            </a>  
                                        </div>        
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="panel-header">
                                                <div class="col-sm-4 col-xs-12">
                                                    <div class="dataTables_length">
                                                        <label>Display 
                                                            <select name="example_length">
                                                                <option value="10">10</option>
                                                                <option value="25">25</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select> records per page</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-12">
                                                        <div class="dataTables_length">
                                                         <a class="btn btn-default buttons-copy btn-sm" tabindex="0">
                                                             <span>Copy</span></a>
                                                             <a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0"><span>CSV</span></a>
                                                             <a class="btn btn-default buttons-excel buttons-html5 btn-sm" tabindex="0"><span>Excel</span></a>
                                                             <a class="btn btn-default buttons-pdf buttons-html5 btn-sm" tabindex="0"><span>PDF</span></a>
                                                             <a class="btn btn-default buttons-print btn-sm" tabindex="0"><span>Print</span></a>
                                                             
                                                         </div>
                                                     </div>
                                                     <div class="col-sm-4 col-xs-12">
                                                        <div class="dataTables_length">
                                                            <div class="input-group custom-search-form">
                                                                <input type="search" class="form-control" placeholder="search..">
                                                                <span class="input-group-btn">
                                                                  <button class="btn btn-primary" type="button">
                                                                      <span class="glyphicon glyphicon-search"></span>
                                                                  </button>
                                                              </span>
                                                          </div><!-- /input-group -->
                                                      </div>
                                                  </div>
                                              </div>

                                          </div>

                                          <div id="refer_pacientes"></div>
                                          
                    <div class="page-nation text-right">
                        <ul class="pagination pagination-large">
                            <li class="disabled"><span>«</span></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li class="disabled"><span>...</span></li><li>
                            <li><a rel="next" href="#">Next</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
</section> <!-- /.content -->



<div id="ordine" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Actualizar Doctor</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                            <a class="btn btn-primary" href="table.php"> <i class="fa fa-list"></i>  Doctor List </a>  
                        </div>
                    </div>
                    <div id="vistaeditdoctores"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" onclick="updateDoctor()">Guardar</button>
            </div>
        </div>
    </div>
</div>




</div> <!-- /.content-wrapper -->
</div> <!-- ./wrapper -->
<!-- ./wrapper -->
       <?php include("footer.php"); ?>
      <script src="assets/doctores.js" type="text/javascript"></script>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
</html>