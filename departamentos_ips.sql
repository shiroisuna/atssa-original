/*
SQLyog Professional v12.5.1 (32 bit)
MySQL - 10.1.33-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `departamentos_ips` (
	`id` int (11),
	`departamento_id` int (11),
	`tabla_ips` varchar (135)
); 
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('1','29','amazonas');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('2','1','antioquia');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('3','25','arauca');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('4','2','atlantico');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('5','3','bogotadc');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('6','4','bolivar');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('7','5','boyaca');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('8','6','caldas');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('9','7','caqueta');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('10','26','casanagre');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('11','8','cauca');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('12','9','cesar');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('13','12','choco');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('14','10','cordoba');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('15','11','cundinamarca');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('16','30','guainia');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('17','31','guaviare');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('18','13','huila');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('19','14','laguajira');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('20','15','magdalena');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('21','16','meta');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('22','17','narino');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('23','18','nortedesantander');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('24','27','putumayo');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('25','19','quindio');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('26','20','risaralda');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('27','28','sanandresprovidencia');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('28','21','santander');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('29','22','sucre');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('30','23','tolima');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('31','24','valledelcauca');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('32','32','vaupes');
insert into `departamentos_ips` (`id`, `departamento_id`, `tabla_ips`) values('33','33','vichada');
