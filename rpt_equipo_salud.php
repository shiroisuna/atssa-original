<?php
	ini_set('memory_limit', '256M');




$rpt='';
$rpt.="<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>

	<style type='text/css'>
		body{
			font-family: 'Open Sans', sans-serif;
			margin-right:0;
			margin-left:0;
			width: 100%;
		}

		td{
			padding: 0;
		}

		.border {
		    border: 1px solid gray;
		    border-radius: 3px;
		}

		.bl {
			border-left: 1px solid gray;
		}

		.br {
			border-right: 1px solid gray;
		}

		.bt {
			border-top: 1px solid gray;
		}

		.pp1{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 8px;
			font-size: 10px;
		}

		.pp2{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 5px;
		}

		.pp3{
			margin-top: 0;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 7px;
		}

		.pp4{
			margin-top: 3px;
			margin-bottom: 2px;
			font-weight: 400;
			font-size: 10px;
			line-height: 20px;
		}
		.pp5{
			margin-top: 3px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.pp6{
			margin-top: 70px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.h2{
			font-size: 13px;
			line-height: 13px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.h1{
			font-size: 18px;
			line-height: 14px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.pt-20{
			padding-top: -25px;
		}

		.pb-20{
			padding-bottom: -10px;
		}

		.pb-10{
			padding-bottom: 10px;
		}

		.mb-20{
			margin-bottom: 20px;
		}

		.mb-10{
			margin-bottom: 10px;
		}

		.mb-15{
			margin-bottom: 8px;
		}

		.mt-20{
			margin-top:20px;
		}

		.mt-15{
			margin-top:15px;
		}

		.logomin{
			max-width: 100px;
			margin-left:auto;
			margin-right:auto;
		}

		.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-0,.col-xs-1-5 {
		  float: left;
		  position: relative;
		  min-height: 1px;
		}

		.col-xs-12 {
		  width: 100%;
		}
		.col-xs-11 {
		  width: 91.66666667%;
		}
		.col-xs-10 {
		  width: 83.33333333%;
		}
		.col-xs-9 {
		  width: 75%;
		}
		.col-xs-8 {
		  width: 66.66666667%;
		}
		.col-xs-7 {
		  width: 58.33333333%;
		}
		.col-xs-6 {
		  width: 50%;
		}
		.col-xs-5 {
		  width: 41.66666667%;
		}
		.col-xs-4 {
		  width: 33.33333333%;
		  width: 30%;
		}
		.col-xs-3 {
		  width: 20%;
		}
		.col-xs-2 {
		  width: 15%;
		}

		.col-xs-1-5 {
		  width: 11%;
		}
		.col-xs-1 {
		  width: 8.33333333%;
		}

		.col-xs-0 {
		  width: 5%;
		}

		.p-5{
			padding: 5px;
		}

		.text-center{
			text-align: center;
		}

		.text-right{
			text-align: right;
		}

		.title1{
			font-size: 7px;
			margin-top:0;
			margin-bottom:5px;
		}

		.title3{
			font-size: 12px;
			margin-top:0;
			margin-bottom:2px;
		}

		.autorizacion{
			font-size: 8px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.title2{
			font-size: 7px;
			margin-top:0;
			margin-bottom:0;
		}

		.capi{
			text-transform: capitalize;
		}

		.cortext{
			 text-overflow:ellipsis;
			  white-space:nowrap; 
			  overflow:hidden; 
			  max-width: 100%;
		}
	</style>
	<body>
	<div class='col-xs-12'>
			<div class='col-xs-4 text-center '>
				<p class='pp1'>REPUBLICA DE COLOMBIA</p>
				<p class='pp2'>SISTEMA GENERAL DE SEGURIDAD SOCIAL EN SALUD</p>
				<p class='pp2'>SUPERINTENDENCIA NACIONAL DE SALUD</p>
				<!-- <p class='pp3 mt-15'>'Código Únicamente para dependientes y afiliaciones al Régimen Subsidiado'</p> -->
			</div>

			<div class='col-xs-5 text-center'>
				<h2 class='h2'>FORMULARIO ÚNICO DE EQUIPO DE SALUD (VOLUNTARIOS)</h2>
			</div>

			<div class='col-xs-3 text-right'>
				<img class='logomin' src='../assets/img/logo-atssa.jpg' alt=''>
				<!-- <h2>LOGO</h2> -->
			</div>
	</div>

	<div class='col-xs-12 pt-20 pb-10 text-center'>

		<h1 class='h1'>FORMULARIO ELECTRÓNICO</h1>
	</div>

	<div class='col-xs-12 border mb-15'>
		<h6 class='title1' align='center'>DATOS DEL PRESTADOR DE SERVICIOS</h6>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12 p-5'>
			<div class='col-xs-4 p-5'>
				<h6 class='title1'>Nombre o Razón Social</h6>
				<p class='pp3 cortext'></p>
			</div>
			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>Nit</h6>
				<p class='pp3 cortext'></p>
				
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>Codigo de Habilitación </h6>
				<p class='pp3 cortext'></p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>Tipo de Servicio </h6>
				<p class='pp3 cortext'></p>
			</div>
		</div>
	</div>
	<div class='col-xs-12 border mb-15'>
		<h6 class='title1' align='center'>INSCRIPCION EQUIPOS DE SALUD</h6>
	</div>
	<div class='col-xs-12 border mb-15'>
		<div class='col-xs-12'>
			<div class='col-xs-5 p-5 bl'>
				<h6 class='title1'>Primer Nombre</h6>
				
			</div>
			
			<div class='col-xs-6 p-5 bl'>
				<h6 class='title1'>Segundo Nombre</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5 bl'>
				<h6 class='title1'>Primer Apellido</h6>
			</div>
			
			<div class='col-xs-6 p-5 bl'>
				<h6 class='title1'>Segundo Apellido</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Tipo </h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Doc. Identidad</h6>
				
			</div>
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Tlf</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Profesion </h6>
				
			</div>
			
		</div>
		<div class='col-xs-12 bt'>
			<div class='col-xs-5 p-5  bl'>
				<h6 class='title1'>Municipio</h6>
				
			</div>
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Departamento </h6>
				
			</div>
			
		</div>
		<div class='col-xs-12 bt'>
			
			<div class='col-xs-6 p-5  bl'>
				<h6 class='title1'>Correo Electrónico</h6>
				
			</div>
		</div>
		
		
	</div>
	
	
	</body>
</html>";

	// var_dump($rpt);

		$nombrearchivo='Reporte-Captacion-Paciente-con-Riesgo';

		require_once __DIR__ . '/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'Legal',
			'mode' => 'utf-8',
			'orientation' => 'P',
		    'setAutoTopMargin' => 'stretch',
		    'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 10,
			'margin_header' => 0,
			'margin_footer' => 0
		]);

		$html = $rpt;
		$mpdf->autoPageBreak = true;
		$mpdf->AddPage('P');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		$mpdf->Output($nombrearchivo.'.pdf','I'); 
		exit;
?>

