<div class="table-responsive">
	<table class="table table-bordered table-stripe table-hover">
		<thead>
			<th>N° Identidad</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Edad</th>
			<th>Sexo</th>
			<th>Peso</th>
			<th>Estatura</th>
			<th>IMC</th>
			<th>Tlf</th>
			<th>Evento</th>
		</thead>
		<tbody>
			<?php foreach ($listCapParams as $row) { ?>
				<tr>
					<td><?php echo $row['nro_identidad']; ?> </td>
					<!-- td><img src="assets/dist/img/<?php echo $row['doc_img']; ?>" class="img-circle" alt="User Image" height="50" width="50"> <?php //echo $row['img']; ?> </td> -->
					<td><?php echo $row['nombre1']; ?> </td>
					<td><?php echo $row['apellido1']; ?> </td>
					<td><?php echo $row['edad']; ?> </td>
					<td><?php echo $row['sexo']; ?> </td>
					<td><?php echo $row['peso']; ?></td>
					<td><?php echo $row['estatura']; ?> </td>
					<td><?php echo $row['imc']; ?></td>
					<td><?php echo $row['tlf']; ?></td>
					<td><?php echo $row['nom_evento']; ?> </td>
					<td>
						<!-- <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#ordine"><i class="fa fa-pencil"></i></button> -->
						<a href="ctrl/doctores.php?id=<?php echo $row['id_paciente_riesgo']; ?>&i=9" class="btn btn-info btn-xs">PDF</a>
					</td>
				</tr>
			<?php } ?>
			
		</tbody>
	</table>
</div>