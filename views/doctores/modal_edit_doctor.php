
                    <div class="panel-body">
                        <?php foreach ($listaDoc as $row ) { ?>
                         
                            
                            <div class="col-sm-6 form-group">
                                <label>Nombre</label>
                                <input type="text" id="nom" name="nom" class="form-control" value="<?php echo $row['doc_nombre']; ?>" disabled="true">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Apellido</label>
                                <input type="text" id="apellido" name="apellido" class="form-control" value="<?php echo $row['doc_apellido']; ?>" disabled="true">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $row['doc_email']; ?>" required>
                            </div>
                           
                            
                            <div class="col-sm-6 form-group">
                                <label>Departamento</label>
                                <select class="form-control" id="departamento" name="departamento" size="1">
                                    <option  selected class="test" value="<?php echo $row['doc_departamento']; ?>">Neurology</option>
                                    <option>Gynaecology</option>
                                    <option>Microbiology</option>
                                    <option>Pharmacy</option>
                                    <option>Neonatal</option>
                                </select>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Dirección</label>
                                <input type="text" id="direccion" name="direccion" class="form-control" value="<?php echo $row['doc_direccion']; ?>" required>
                            </div>
                            
                            <div class="col-sm-6 form-group">
                                <label>Número de Tlf Movil</label>
                                <input type="number" id="tlf_cell" name="tlf_cell" class="form-control" value="<?php echo $row['tlf_cel']; ?>" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Número de Tlf Residencial</label>
                                <input type="number" id="tlf_hab" name="tlf_hab" class="form-control" value="<?php echo $row['tlf_hab']; ?>" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Sueldo</label>
                                <input type="text" id="sueldo" name="sueldo" class="form-control" value="<?php echo $row['sueldo']; ?>" required>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Foto</label>
                                <input type="file" name="picture" id="picture" value="<?php echo $row['doc_img']; ?>">
                            </div>  
                            <div class="col-sm-12 form-group">
                                <label>Biografia</label>
                                <textarea id="biografia" name="biografia" class="form-control" rows="3" value="<?php echo $row['biografia']; ?>" disabled="true"></textarea>
                            </div>        
                            <div class="col-sm-6 form-group">
                                <label>Fecha de Nacimiento</label>
                                <input id="f_nac" name="f_nac" class="datepicker form-control hasDatepicker" type="date" value="<?php echo $row['fecha_nac']; ?>" disabled="true">
                            </div>
                            <input type="hidden" value="<?php echo $row['id_doctor']; ?>" id="id_doc" name="id_doc">       
                            <!-- <div class="form-check">
                                <label>Status</label><br>
                                <label class="radio-inline">
                                <input type="radio" name="status" value="1" checked="checked">Active</label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" >Inctive
                                </label>
                            </div>        -->                                
                        <?php } ?>
                          
                    </div>

           