<div class="table-responsive">
	<table class="table table-bordered table-stripe table-hover">
		<thead>
			<th>ID</th>
			<td>Foto</td>
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Departamento</td>
			<td>Email</td>
			<td>Tlf (Movil)</td>
			<td>Tlf (Res.)</td>
			<td>Precio</td>
			<td>Update</td>
		</thead>
		<tbody>
			<?php foreach ($listDoc as $row) { ?>
				<tr>
					<td><?php echo $row['id_doctor']; ?> </td>
					<td><img src="assets/dist/img/<?php echo $row['doc_img']; ?>" class="img-circle" alt="User Image" height="50" width="50"> <?php //echo $row['img']; ?> </td>
					<td><?php echo $row['doc_nombre']; ?> </td>
					<td><?php echo $row['doc_apellido']; ?> </td>
					<td><?php echo $row['doc_departamento']; ?> </td>
					<td><?php echo $row['doc_email']; ?> </td>
					<td><?php echo $row['tlf_cel']; ?></td>
					<td><?php echo $row['tlf_hab']; ?> </td>
					<td><?php echo $row['sueldo']; ?> </td>
					<td>
						<!-- <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#ordine"><i class="fa fa-pencil"></i></button> -->
						<button type="button" class="btn btn-info btn-xs" onclick="modalEditDoctor('<?php echo $row['id_doctor']; ?>')"><i class="fa fa-pencil"></i></button>
						<?php if($row['status_doctor']== 1){ ?> 
                        	<button type="button" class="btn btn-danger btn-xs" tooltip="Desactivar" title="Desactivar" onclick="updateStatus('<?php echo $row['id_doctor']; ?>', '<?php echo $row['status_doctor']; ?>')"><i class="fa fa-toggle-off"></i></button>

						<?php }else{ ?> 
								<button type="button" class="btn btn-success btn-xs" tooltip="activar" title="activar" onclick="updateStatus('<?php echo $row['id_doctor']; ?>', '<?php echo $row['status_doctor']; ?>')"><i class="fa fa-toggle-on"></i></i></button>
						<?php } ?>
					</td>
				</tr>
			<?php } ?>
			
		</tbody>
	</table>
</div>