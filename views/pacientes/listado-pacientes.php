<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead class="success">
        <tr>
            <th>N° Identidad</th>
            <th>Picture</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Dirección</th>
            <th>Sexo</th>
            <th>Grupo Sanguineo</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($response as $row) { ?>
          
        <tr>
          <td>
             <label><?php echo $row['nro_identidad']; ?></label>   
          </td>
          
          <td>
            <img src="assets/dist/img/m1.png" class="img-circle" alt="User Image" height="50" width="50">
          </td>
          <td><?php echo $row['nombres']; ?></td>
          <td><?php echo $row['apellidos']; ?></td>
          <td><?php echo $row['tlf']; ?></td>
          <td><?php echo $row['direccion']; ?></td>
          <td><?php echo $row['sexo']; ?></td>
          <td><?php echo $row['grupo_sanguineo']; ?></td>
        </tr>                            
        <?php } ?>
    </tbody>
  </table>
</div>