<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="success">
            <tr>
                <th>Nro Afiliación</th>
                <th>Nombre del Solicitante</th>
                <th>Tipo de trámite</th>
                <th>Tipo de Afiliación</th>
                <th>Tipo de Cotizante</th>
                <th>Fecha de Solicitud</th>
                <th>Acción</th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($response as $row) { ?>
                
            <tr>
               <td><?php echo $row['id_afiliacion']; ?></td>
               <td><?php echo $row['nom1_solic']; ?></td>
               <td><?php echo $row['tipo_tramite']; ?></td>
               <td><?php echo $row['tipo_afiliacion']; ?></td>
               <td><?php echo $row['tipo_cotizante']; ?></td>
               <td><?php echo $row['fecha_solicitud']; ?></td>
               <td>
                <!-- <a class="btn btn-info btn-xs" href="ctrl/afiliados.php?id=<?php echo $row['id_afiliacion']; ?>&i=3"><i class="fa fa-pencil"></i></a> -->
                 <button type="button" class="btn btn-info btn-xs" onclick="editAfiliacion('<?php echo $row['id_afiliacion']; ?>')"><i class="fa fa-pencil"></i> <a href=""></a></button>
                 <a href="ctrl/afiliados.php?id=<?php echo $row['id_afiliacion']; ?>&i=4" class="btn btn-info btn-xs">PDF</a>
                <!--  <button type="button" class="btn btn-info btn-xs" onclick="generaPDF('<?php echo $row['id_afiliacion']; ?>')">PDF</button> -->
                  <!-- <?php if($row['status_doctor']== 1){ ?> 
                    <button type="button" class="btn btn-danger btn-xs" tooltip="Desactivar" title="Desactivar" onclick="updateStatus('<?php echo $row['id_doctor']; ?>', '<?php echo $row['status_doctor']; ?>')"><i class="fa fa-toggle-off"></i></button>

                  <?php }else{ ?> 
                      <button type="button" class="btn btn-success btn-xs" tooltip="activar" title="activar" onclick="updateStatus('<?php echo $row['id_doctor']; ?>', '<?php echo $row['status_doctor']; ?>')"><i class="fa fa-toggle-on"></i></i></button>
                  <?php } ?> -->
               </td>

            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>