
<div class="panel-body">

    <form id="msform" method="post" action="ctrl/afiliados.php">
    <!-- <form id="msform" method="post" onsubmit="return crearAfiliacion();"> -->
        <input type="hidden" id="idafil" name="idafil" value="<?php echo $response[0]['id_afiliacion']; ?>">
        <input type="hidden" id="op" name="op" value="5">
        <ul id="progressbar">
            <li class="active">TRAMITE</li>
            <li>DATOS PERSONALES</li>
            <li>DATOS COMPLEMENTARIOS</li>
            <li>NÚCLEO FAMILIAR</li>
            <li>DATOS DEL EMPLEADO</li>
            <li>REPORTE DE NOVEDAD</li>
            <li>ANEXOS</li>
            <li>DATOS POR LA ENTIDAD</li>
            <li>AUTORIZACION</li>

        </ul>
                                    

<fieldset>
     
        
    
        <h2 class="fs-title">DATOS DEL TRÁMITE</h2><hr>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <label for="tipo_tramite">Tipo de Trámite</label>
                    <select name="tipo_tramite" id="tipo_tramite" class="form-control">
                        <option value="0"></option>
                        <option <?php if (isset($response[0]['tipo_tramite'])== 'Afiliación') { echo 'selected'; } ?> value="Afiliación">A. Afiliación</option>
                        <option <?php if (isset($response[0]['tipo_tramite'])== 'Reporte de Novedad') { echo 'Selected'; } ?> value="Reporte de Novedad">B. Reporte de Novedad</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="tipo_afiliacion">Tipo de Afiliación</label>
                    <select name="tipo_afiliacion" id="tipo_afiliacion" class="form-control">
                        <option value="0"></option>
                        <option <?php if (isset($response[0]['tipo_afiliacion'])== 'Individual') { echo 'Selected'; } ?> value="Individual">A) Individual</option>
                        <option <?php if (isset($response[0]['tipo_afiliacion'])== 'Colectiva') { echo 'Selected'; } ?> value="Colectiva">B) Colectiva</option>
                    </select><br>
                    <select name="sub_tipo_afil" id="sub_tipo_afil" class="form-control">
                        <option value="0"></option>
                        <option <?php if (isset($response[0]['sub_tipo_afiliacion'])== 'Cotizante o cabeza de Familia') { echo 'Selected'; } ?> value="Cotizante o cabeza de Familia">a) Cotizante o cabeza de Familia</option>
                        <option <?php if (isset($response[0]['sub_tipo_afiliacion'])== 'Beneficiado o afiliado adicional') { echo 'Selected'; } ?> value="Beneficiado o afiliado adicional">b) Beneficiado o afiliado adicional</option>
                        <option <?php if (isset($response[0]['sub_tipo_afiliacion'])== 'Institucional') { echo 'Selected'; } ?> value="Institucional">c) Institucional</option>
                        <option <?php if (isset($response[0]['sub_tipo_afiliacion'])== 'de Oficio') { echo 'Selected'; } ?> value="de Oficio">d) de Oficio</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    
                    <label for="regimen">Régimen</label>
                    <select name="regimen" id="regimen" class="form-control">
                        <option value="0"></option>
                        <option <?php if (isset($response[0]['regimen'])== 'Contributivo') { echo 'Selected'; } ?> value="Contributivo">C) Contributivo</option>
                        <option <?php if (isset($response[0]['regimen'])== 'Subsidio') { echo 'Selected'; } ?> value="Subsidio">D) Subsidio</option>
                    </select>
                </div>
            </div>
        </div>

        

        <div class="row">
            
            <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">
                <h4 align="left">Tipo de Afiliado</h4>
                <select name="tp_afiliado" class="form-control">
                    
                    <option <?php if (isset($response[0]['tipo_afiliado'])== 'co') { echo 'Selected'; } ?> value="co" >CO cotizante</option>
                    <option <?php if (isset($response[0]['tipo_afiliado'])== 'cf') { echo 'Selected'; } ?> value="cf">CF Cabeza de Familia</option>
                    <option <?php if (isset($response[0]['tipo_afiliado'])== 'be') { echo 'Selected'; } ?> value="be">BE Beneficiario</option>
                </select>
            </div>
            
            <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">
                <h4 align="left">Tipo de Cotizante</h4>
                <select name="tp_cotizante" class="form-control">
                    
                    <option <?php if (isset($response[0]['tipo_cotizante'])== 'dep') { echo 'Selected'; } ?> value="dep" >Dependiente</option>
                    <option <?php if (isset($response[0]['tipo_cotizante'])== 'ind') { echo 'Selected'; } ?> value="ind">Independiente</option>
                    <option <?php if (isset($response[0]['tipo_cotizante'])== 'pen') { echo 'Selected'; } ?> value="pen">Pensionado</option>
                </select>
            </div>
            <div class="col-md-2 col-sm-2 col-md-2 col-lg-2">
                <h4>Código:</h4>
                <input type="text" name="codigo" placeholder="" value="<?php echo $response[0]['codigo']; ?>">
                
            </div>
            <div class="col-md-3 col-sm-3 col-md-3 col-lg-3">
                <h4>Fecha Solicitud:</h4>
                <input type="date" name="fech_sol" placeholder="" value="<?php echo $response[0]['fecha_solicitud']; ?>"/>
                
            </div>
        </div>

    
    <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>

    <h2 class="fs-title">Datos Básicos de Identificación</h2><hr>
    
    <div class="row">
        
        <div class="col-md-3">
            <label for="apell1_solic">1er Apellido</label>
            <input type="text" id="apell1_solic" name="apell1_solic" class="form-control" value="<?php echo $response[0]['apellido1_solic']; ?>">
        </div>
        <div class="col-md-3">
            <label for="apell2_solic">2do Apellido</label>
            <input type="text" id="apell2_solic" name="apell2_solic" class="form-control" value="<?php echo $response[0]['apellido2_solic']; ?>">
        </div>
        <div class="col-md-3">
            <label for="nom1_solic">1er Nombre</label>
            <input type="text" id="nom1_solic" name="nom1_solic" class="form-control" value="<?php echo $response[0]['nom1_solic']; ?>">
        </div>
        <div class="col-md-3">
            <label for="nom2_solic">2do Nombre</label>
            <input type="text" id="nom2_solic" name="nom2_solic" class="form-control" value="<?php echo $response[0]['nom2_solic']; ?>">
        </div>
    
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for="tipo_id_solic">Documento de Identificacion</label>
            <select name="tipo_id_solic" id="tipo_id_solic" class="form-control">
                <option value="0"></option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '1') { echo 'Selected'; } ?> value="1">R.C.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '2') { echo 'Selected'; } ?> value="2">T.I.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '3') { echo 'Selected'; } ?> value="3">C.C.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '4') { echo 'Selected'; } ?> value="4">C.E.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '5') { echo 'Selected'; } ?> value="5">P.A.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '6') { echo 'Selected'; } ?> value="6">C.D.</option>
                <option <?php if (isset($response[0]['tipo_doc_id'])== '7') { echo 'Selected'; } ?> value="7">S.C.</option>
            </select>

        </div>
        <div class="col-md-3">
            <label for="n_identidad">Numero de Identificación</label>
            <input type="text" id="n_identidad" name="n_identidad" class="form-control" value="<?php echo $response[0]['nro_doc_id']; ?>">
        </div>
        <div class="col-md-3">
            <label for="sexo_solic">Sexo</label>
            <select name="sexo_solic" id="sexo_solic" class="form-control">
                <option value="0"></option>
                <option <?php if (isset($response[0]['sexo'])== '1') { echo 'Selected'; } ?> value="1">Femenino</option>
                <option <?php if (isset($response[0]['sexo'])== '2') { echo 'Selected'; } ?> value="2">Masculino</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="f_nac_solic">Fecha de Nacimiento</label>
            <input type="date" id="f_nac_solic" name="f_nac_solic" class="form-control" value="<?php echo $response[0]['fecha_nac']; ?>">
        </div>
    </div>
    
     <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Datos Complementarios</h2><hr>
    <div class="row">
        <div class="col-md-2">
            <label for="etnia">Origen Étnico</label>
            <input type="text" id="etnia" name="etnia" class="form-control" value="<?php echo $response[0]['etnia']; ?>">
        </div>
        <div class="col-md-3">
            <div class="col-md-6">
                <label for="discapacidad">Discapacidad</label>
                <select name="discapacidad" id="discapacidad" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['discapacidad'])== '1') { echo 'Selected'; } ?> value="1">F</option>
                    <option <?php if (isset($response[0]['discapacidad'])== '2') { echo 'Selected'; } ?> value="2">N</option>
                    <option <?php if (isset($response[0]['discapacidad'])== '3') { echo 'Selected'; } ?> value="3">M</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="condic">Condición</label>
                <select name="condic" id="condic" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['condicion'])== '1') { echo 'Selected'; } ?> value="1">T</option>
                    <option <?php if (isset($response[0]['condicion'])== '2') { echo 'Selected'; } ?> value="2">P</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <label for="pt_sisben">Puntaje y Nivel del Sisben</label>
            <input type="text" id="pt_sisben" name="pt_sisben" class="form-control" value="<?php echo $response[0]['pts_nivel_sisben']; ?>">
        </div>
        <div class="col-md-4">
            <label for="Ad_riesgos">Administradora de Riesgos Laborales</label>
            <input type="text" id="ad_riesgos" name="ad_riesgos" class="form-control" value="<?php echo $response[0]['admin_riesgos']; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-6">
                <label for="ad_pension">Administración de Pensiones</label>
                <input type="text" id="ad_pension" name="ad_pension" class="form-control" value="<?php echo $response[0]['admin_pension']; ?>">
            </div>
            <div class="col-md-6">
                <label for="ing_cotizacion">Ingreso Base de Cotización</label>
                <input type="text" id="ing_cotizacion" name="ing_cotizacion" class="form-control" value="<?php echo $response[0]['ingreso_cotiz']; ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-8">
                <label for="direccionsolic">Dirección</label>
                <input type="text" id="direccionsolic" name="direccionsolic" class="form-control" value="<?php echo $response[0]['direccion']; ?>">
            </div>
            <div class="col-md-4">
                <label for="zona">Zona</label>
                <select name="zona" id="zona" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['zona'])== '1') { echo 'Selected'; } ?> value="1">Rural</option>
                    <option <?php if (isset($response[0]['zona'])== '2') { echo 'Selected'; } ?> value="2">Urbana</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-6">
                <label for="localidad">Localidad / Comuna</label>
                <input type="text" id="localidad" name="localidad" class="form-control" value="<?php echo $response[0]['localidad']; ?>">
            </div>
            <div class="col-md-6">
                <label for="barrio">Barrio</label>
                <input type="text" id="barrio" name="barrio" class="form-control" value="<?php echo $response[0]['barrio']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label for="municipio">Municipio</label>
            <input type="text" id="municipio" name="municipio" class="form-control" value="<?php echo $response[0]['municipio']; ?>">
        </div>
        <div class="col-md-3">
            <label for="dep">Departamento</label>
            <input type="text" id="dep" name="dep" class="form-control" value="<?php echo $response[0]['departamento']; ?>">
        </div>
        <div class="col-md-4">
            <div class="col-md-6">
                <label for="tlf_f">Teléfono Fijo</label>
                <input type="tel" id="tlf_f" name="tlf_f" class="form-control" value="<?php echo $response[0]['tlf_hab']; ?>">
            </div>
            <div class="col-md-6">
                <label for="tlf_m">Teléfono Movil</label>
                <input type="tel" id="tlf_m" name="tlf_m" class="form-control" value="<?php echo $response[0]['tlf_cell']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label for="email">Correo Electrónico</label>
            <input type="email" id="email" name="email" class="form-control" value="<?php echo $response[0]['correo']; ?>">
        </div>
        <div class="col-md-2">
            <label for="cod">Código</label>
            <input type="text" id="cod" name="cod" class="form-control" value="<?php echo $response[0]['codigo']; ?>">
        </div>
    </div>
    
     <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Datos de Identificación de los miembros del Núcleo Familiar</h2><hr>
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-6">
                <label for="nombres">Nombres</label>
                 <?php for($i=0; $i<4; $i++){ ?> 
                <input type="text" id="nom_ben[]" name="nom_ben[]" class="form-control" value="<?php echo $response[0]['nombres']; ?>">
                
                 <?php } ?>
            </div>
            <div class="col-md-6">
                <label for="apellidos">Apellidos</label>
                <?php for($i=0; $i<4; $i++){ ?> 
                <input type="text" id="apell_ben[]" name="apell_ben[]" class="form-control" value="<?php echo $response[0]['apellidos']; ?>">
                
                 <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-4">
                <label for="t_doc_ben">Tipo de Doc</label>
                <?php for($i=0; $i<4; $i++){ ?> 
                <select name="t_doc_ben[]" id="t_doc_ben[]" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '1') { echo 'Selected'; } ?> value="1">CN</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '2') { echo 'Selected'; } ?> value="2">RC</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '3') { echo 'Selected'; } ?> value="3">TI</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '4') { echo 'Selected'; } ?> value="4">CC</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '5') { echo 'Selected'; } ?> value="5">CE</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '6') { echo 'Selected'; } ?> value="6">PA</option>
                    <option <?php if (isset($response[0]['t_doc_id'])== '7') { echo 'Selected'; } ?> value="7">SC</option>

                </select><br>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <label for="n_identidad_ben">Numero de Identidad</label>
                 <?php for($i=0; $i<4; $i++){ ?> 
                <input type="text" id="n_identidad_ben[]" name="n_identidad_ben[]" class="form-control" value="<?php echo $response[0]['nro_doc_id']; ?>">
                
                <?php } ?>
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-3">
        
            <div class="col-md-6">
                <label for="parentesco">Parentesco</label>
                 <?php for($i=0; $i<4; $i++){ ?> 
                <input type="text" id="parentesco[]" name="parentesco[]" class="form-control" value="<?php echo $response[0]['parentesco']; ?>">
                
                <?php } ?>
            </div>
            <div class="col-md-6">
                <label for="etnia_ben">Etnia</label>
                 <?php for($i=0; $i<4; $i++){ ?> 
                <input type="text" id="etnia_ben[]" name="etnia_ben[]" class="form-control" value="<?php echo $response[0]['etnia']; ?>">
                
                <?php } ?>
            </div>
       
        </div>
        <div class="col-md-8">
            <label for="discapacidad_ben">Discapacidad</label>
             <?php for($i=0; $i<4; $i++){ ?> 
            <input type="text" id="discapacidad_ben[]" name="discapacidad_ben[]" class="form-control" value="<?php echo $response[0]['discapacidad']; ?>">
            
            <?php } ?>
        </div>
    </div>
        <div class="row">
            <h4>Datos de Residencia</h4>
            <div class="col-md-12">
                <div class="col-md-2">
                    <label for="munic">Municipio</label>
                    <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="munic[]" name="munic[]" class="form-control" value="<?php echo $response[0]['municipio']; ?>">
                     <?php } ?>
                </div>
                <div class="col-md-2">
                    <label for="dep_ben">Departamento</label>
                     <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="dep_ben[]" name="dep_ben[]" class="form-control" value="<?php echo $response[0]['depto']; ?>">
                     <?php } ?>
                </div>
                <div class="col-md-2">
                    <label for="zona_ben">Zona</label>
                     <?php for($i=0; $i<4; $i++){ ?> 
                        <select name="zona_ben[]" id="zona_ben[]" class="form-control">
                            <option value="0"></option>
                            <option <?php if (isset($response[0]['zona'])== '1') { echo 'Selected'; } ?> value="1">Rural</option>
                            <option <?php if (isset($response[0]['zona'])== '2') { echo 'Selected'; } ?> value="2">Urbana</option>
                        </select><br>
                     <?php } ?>
                </div>
                <div class="col-md-2">
                    <label for="tlf_m_ben">Teléfono Movil</label>
                    <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="tlf_m_ben[]" name="tlf_m_ben[]" class="form-control" value="<?php echo $response[0]['tlf_cell']; ?>">
                     <?php } ?>
                </div>
                <div class="col-md-2">
                    <label for="tlf_f_ben">Teléfono Fijo</label>
                    <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="tlf_f_ben[]" name="tlf_f_ben[]" class="form-control" value="<?php echo $response[0]['tlf_hab']; ?>">
                     <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label for="upc">Valor UPC afiliado adicional</label>
                        <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="upc[]" name="upc[]" class="form-control" value="<?php echo $response[0]['valor_upc_dicional']; ?>">
                     <?php } ?>
                    </div>
                    <div class="col-md-5">
                        <label for="Ins_salud">Nombre del Inst. Prestador de servicios de salud</label>
                        <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="ins_salud[]" name="ins_salud[]" class="form-control" value="<?php echo $response[0]['inst_servicios_salud']; ?>">
                     <?php } ?>
                    </div>
                    <div class="col-md-2">
                        <label for="n_sisben_ben">Nivel de Sisben</label>
                        <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="n_sisben_ben[]" name="n_sisben_ben[]" class="form-control" value="<?php echo $response[0]['nivel_sisben']; ?>">
                     <?php } ?>
                    </div>
                    <div class="col-md-2">
                        <label for="g_poblacional">Grupo Poblacional</label>
                        <?php for($i=0; $i<4; $i++){ ?> 
                        <input type="text" id="g_poblacionaln_sisben_ben[]" name="g_poblacional[]" class="form-control" value="<?php echo $response[0]['grupo_poblacional']; ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    

    <h2 class="fs-title">Datos de Identificación y otros aportantes</h2><hr>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <label for="rif">Nombre o Razón Social</label>
                <input type="text" id="rif" name="rif" class="form-control" value="<?php echo $response[0]['razon_social']; ?>">
            </div>
            <div class="col-md-2">
                <label for="t_doc_ot">Tipo Documento</label>
                <select name="t_doc_ot" id="t_doc_ot" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['t_doc'])== '1') { echo 'Selected'; } ?> value="1">C.C.</option>
                    <option <?php if (isset($response[0]['t_doc'])== '2') { echo 'Selected'; } ?> value="2">C.E.</option>
                    <option <?php if (isset($response[0]['t_doc'])== '3') { echo 'Selected'; } ?> value="3">P.A.</option>
                    <option <?php if (isset($response[0]['t_doc'])== '4') { echo 'Selected'; } ?> value="4">C.D.</option>
                    <option <?php if (isset($response[0]['t_doc'])== '5') { echo 'Selected'; } ?> value="5">N.I.</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="identidad_ot">Numero de Identidad</label>
                <input type="text" id="identidad_ot" name="identidad_ot" class="form-control" value="<?php echo $response[0]['nro_doc_id']; ?>">
            </div>
            <div class="col-md-4">
                <label for="t_aportante">Tipo de Aportante o pagador de pensiones</label>
                <input type="text" id="t_aportante" name="t_aportante" class="form-control" value="<?php echo $response[0]['tipo_aportante']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-12">

            <div class="col-md-4">
                <h4>Ubicación o Dirección</h4>
                <div class="col-md-6">
                    <input type="text" id="ciudad" name="ciudad" class="form-control" placeholder="Ciudad / Municipio" value="<?php echo $response[0]['ciudad']; ?>">
                </div>
                <div class="col-md-6">
                    <input type="text" id="departa_aport" name="departa_aport" class="form-control" placeholder="Departamento" value="<?php echo $response[0]['dpto']; ?>">
                </div>
                
            </div>
            <div class="col-md-2">
                <label for="tlf_mo">Teléfono Movil</label>
                <input type="text" id="tlf_mo" name="tlf_mo" class="form-control" placeholder="Teleono Movil" value="<?php echo $response[0]['tlf_cell']; ?>">
            </div>
            <div class="col-md-2">
                <label for="tlf_f_aport">Teléfono Fijo</label>
                <input type="text" id="tlf_f_aport" name="tlf_f_aport" class="form-control" placeholder="Telefono Fijo" value="<?php echo $response[0]['tlf_hab']; ?>">
            </div>
            <div class="col-md-4">
                <label for="email_aport">Correo</label>
                <input type="email" id="email_aport" name="email_aport" class="form-control" placeholder="Email"  value="<?php echo $response[0]['correo']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <label for="fech_ing_t">Fecha Inicio de Relacion Laboral</label>
                <input type="date" id="fech_ing_t" name="fech_ing_t" class="form-control" value="<?php echo $response[0]['fecha_ini_relacion_lab']; ?>">
            </div>
            <div class="col-md-4">
                <label for="cargo_t">Cargo</label>
                <input type="text" id="cargo_t" name="cargo_t" class="form-control" value="<?php echo $response[0]['cargo']; ?>">
            </div>
            <div class="col-md-4">
                <label for="salario_t">Salario</label>
                <input type="text" id="salario_t" name="salario_t" class="form-control" value="<?php echo $response[0]['salario']; ?>">
            </div>
        </div>
    </div>
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Reporte de Novedades</h2><hr>
    <div class="row">
        
        
        <div class="col-md-12">
            <label for="tipo_novedad">Tipo de Novedad</label>
            <select name="tipo_novedad" id="tipo_novedad" class="form-control">
                <option value="0"></option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Modificación de Datos Básicos de Identificación') { echo 'Selected'; } ?> value="Modificación de Datos Básicos de Identificación">Modificación de Datos Básicos de Identificación</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Corrección de Datos Básicos de Identificación') { echo 'Selected'; } ?> value="Corrección de Datos Básicos de Identificación">Corrección de Datos Básicos de Identificación</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Actualización de Documento de Identidad') { echo 'Selected'; } ?> value="Actualización de Documento de Identidad">Actualización de Documento de Identidad</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Actualización y/o corrección de Datos complementarios') { echo 'Selected'; } ?> value="Actualización y/o corrección de Datos complementarios">Actualización y/o corrección de Datos complementarios</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Terminación de la Inscripción en la EPS (Código)') { echo 'Selected'; } ?> value="Terminación de la Inscripción en la EPS (Código)">Terminación de la Inscripción en la EPS (Código)</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Reinscripción en la EPS') { echo 'Selected'; } ?> value="Reinscripción en la EPS">Reinscripción en la EPS</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Inclusión de Beneficiario o de Afiliados adicionales') { echo 'Selected'; } ?> value="Inclusión de Beneficiario o de Afiliados adicionales">Inclusión de Beneficiario o de Afiliados adicionales</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Exclusión de Beneficiarios o Afiliados adicionales') { echo 'Selected'; } ?> value="Exclusión de Beneficiarios o Afiliados adicionales">Exclusión de Beneficiarios o Afiliados adicionales</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Inicio de Relación Laboral o Adquisición de Condiciones para Cotizar') { echo 'Selected'; } ?> value="Inicio de Relación Laboral o Adquisición de Condiciones para Cotizar">Inicio de Relación Laboral o Adquisición de Condiciones para Cotizar</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Terminación de la Relación Laboral o pérdida de condiciones para seguir cotizando') { echo 'Selected'; } ?> value="Terminación de la Relación Laboral o pérdida de condiciones para seguir cotizando">Terminación de la Relación Laboral o pérdida de condiciones para seguir cotizando</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Vinculación a una Entidad autorizada para realizar las afiliaciones colectivas') { echo 'Selected'; } ?> value="Vinculación a una Entidad autorizada para realizar las afiliaciones colectivas">Vinculación a una Entidad autorizada para realizar las afiliaciones colectivas</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Desvinculación a una Entidad autorizada para realizar las afiliaciones colectivas') { echo 'Selected'; } ?> value="Desvinculación a una Entidad autorizada para realizar las afiliaciones colectivas">Desvinculación a una Entidad autorizada para realizar las afiliaciones colectivas</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Movilidad') { echo 'Selected'; } ?> value="Movilidad">Movilidad</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Traslado') { echo 'Selected'; } ?> value="Traslado">Traslado</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Reporte de Fallecimiento') { echo 'Selected'; } ?> value="Reporte de Fallecimiento">Reporte de Fallecimiento</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Reporte del trámite de protección al cesante') { echo 'Selected'; } ?> value="Reporte del trámite de protección al cesante">Reporte del trámite de protección al cesante</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Reporte de la calidad de Prepensionado') { echo 'Selected'; } ?> value="Reporte de la calidad de Prepensionado">Reporte de la calidad de Prepensionado</option>
                <option <?php if (isset($response[0]['tipo_novedad'])== 'Reporte de la calidad de Pensionado') { echo 'Selected'; } ?> value="Reporte de la calidad de Pensionado">Reporte de la calidad de Pensionado</option>
            </select>
            
            <div class="col-md-12 text-left align-self-center">
                 <label class="spanlabel" for="movilidad">Movilidad</label>
                <select name="movilidad" id="movilidad" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['movilidad'])== '1') { echo 'Selected'; } ?> value="1">Al régimen contributivo</option>
                    <option <?php if (isset($response[0]['movilidad'])== '2') { echo 'Selected'; } ?> value="2">Al régimen subsidiado</option>
                </select>
            </div>
            <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="traslado">Traslado</label>
                <select name="traslado" id="traslado" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['traslado'])== '1') { echo 'Selected'; } ?> value="1">Al mismo régimen</option>
                    <option <?php if (isset($response[0]['traslado'])== '2') { echo 'Selected'; } ?> value="2">Diferente régimen</option>
                </select>
            </div>

            

        </div>

    </div>
    
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Datos para el reporte de la novedad</h2><hr>
    <div class="row">
        <div class="col-md-3">
            <label for="rep_nom1">1er Nombre</label>
            <input type="text" id="rep_nom1" name="rep_nom1" class="form-control" value="<?php echo $response[0][' nom1_novedad']; ?>">
        </div>
        <div class="col-md-3">
            <label for="rep_nom2">2er Nombre</label>
            <input type="text" id="rep_nom2" name="rep_nom2" class="form-control" value="<?php echo $response[0]['nom2_novedad']; ?>">
        </div>
        <div class="col-md-3">
            <label for="rep_apell1">1er Apellido</label>
            <input type="text" id="rep_apell1" name="rep_apell1" class="form-control" value="<?php echo $response[0]['apellido1_novedad']; ?>">
        </div>
        <div class="col-md-3">
            <label for="rep_apell2">2do Apellido</label>
            <input type="text" id="rep_apell2" name="rep_apell2" class="form-control" value="<?php echo $response[0]['apellido2_novedad']; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <label for="t_doc_rep">Tipo de Documento</label>
            <select name="t_doc_rep" id="t_doc_rep" class="form-control">
                <option value="0"></option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '1') { echo 'Selected'; } ?> value="1">R.C.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '2') { echo 'Selected'; } ?> value="2">T.I.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '3') { echo 'Selected'; } ?> value="3">C.C.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '4') { echo 'Selected'; } ?> value="4">C.E.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '5') { echo 'Selected'; } ?> value="5">P.A.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '6') { echo 'Selected'; } ?> value="6">C.D.</option>
                <option <?php if (isset($response[0]['t_doc_novedad'])== '7') { echo 'Selected'; } ?> value="7">S.C.</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="n_identidad_rep">Número de Identificación</label>
            <input type="text" id="n_identidad_rep" name="n_identidad_rep" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="sexo_rep">Sexo</label>
            <select name="sexo_rep" id="sexo_rep" class="form-control">
                <option value="0"></option>
                <option <?php if (isset($response[0]['sexo'])== '1') { echo 'Selected'; } ?> value="1">Femenino</option>
                <option <?php if (isset($response[0]['sexo'])== '2') { echo 'Selected'; } ?> value="2">Masculino</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="rep_fech_nac">Fecha de Nacimiento</label>
            <input type="date" id="rep_fech_nac" name="rep_fech_nac" class="form-control" value="<?php echo $response[0]['fecha_nac']; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for="rep_fecha_ini">Fecha (a partir de)</label>
            <input type="date" id="rep_fecha_ini" name="rep_fecha_ini" class="form-control" value="<?php echo $response[0]['fecha_ini']; ?>">
        </div>
        <div class="col-md-2">
            <label for="eps_ant">EPS anterior (Código)</label>
            <input type="text" id="eps_ant" name="eps_ant" class="form-control" value="<?php echo $response[0]['eps_anterior']; ?>">
        </div>
        <div class="col-md-2">
            <label for="cod_traslado_rep">Traslado (Código)</label>
            <input type="text" id="cod_traslado_rep" name="cod_traslado_rep" class="form-control" value="<?php echo $response[0]['traslado']; ?>">
        </div>
        <div class="col-md-5">
            <label for="compensacion_rep">Caja de Compensación Familiar o Pagador de Pensiones</label>
            <input type="text" id="compensacion_rep" name="compensacion_rep" class="form-control" value="<?php echo $response[0]['compensacion_familiar']; ?>">
        </div>
        
    </div>
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Datos a ser Diligenciados por la Entidad Territorial</h2><hr>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="id_entidad_terr">Identificación de la Entidad Territorial</label>
                <select name="id_entidad_terr" id="id_entidad_terr" class="form-control">
                    <option value="0"></option>
                    <option <?php if (isset($response[0]['tipo_id_entidad_territ'])== '1') { echo 'Selected'; } ?> value="1">Código del Municipio</option>
                    <option <?php if (isset($response[0]['tipo_id_entidad_territ'])== '2') { echo 'Selected'; } ?> value="2">Código del Departamento</option>
                </select>
            </div>
            <div class="col-md-6">
                <h4>Datos del SISBEN</h4>
                <div class="col-md-4">
                    <label for="ficha">Número de Ficha</label>
                    <input type="text" id="ficha" name="ficha" class="form-control" value="<?php echo $response[0]['nro_ficha']; ?>">
                </div>  
                <div class="col-md-4">
                    <label for="puntaje">Puntaje</label>
                    <input type="text" id="puntaje" name="puntaje" class="form-control" value="<?php echo $response[0]['puntaje']; ?>">
                </div>
                <div class="col-md-4">
                    <label for="nivel">Nivel</label>
                    <input type="text" id="nivel" name="nivel" class="form-control" value="<?php echo $response[0]['nivel']; ?>">
                </div>
            </div>
            <div class="col-md-2">
                <label for="radicacion">Fecha de Radicación</label>
                <input type="date" id="radicacion" name="radicacion" class="form-control" value="<?php echo $response[0]['fecha_radicacion']; ?>">
            </div>
            <div class="col-md-2">
                <label for="validacion">Fecha de Validación</label>
                <input type="date" id="validacion" name="validacion" class="form-control" value="<?php echo $response[0]['fecha_validacion']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>Datos del Funcionario que realiza la validación</h4>
            <div class="col-md-3">
                <label for="nom1_func">1er Nombre</label>
                <input type="text" id="nom1_func" name="nom1_func" class="form-control" value="<?php echo $response[0]['nom1_func']; ?>">
            </div>
            <div class="col-md-3">
                <label for="nom2_func">2do Nombre</label>
                <input type="text" id="nom2_func" name="nom2_func" class="form-control" value="<?php echo $response[0]['nom2_func']; ?>">
            </div>
            <div class="col-md-3">
                <label for="apell1_func">1er Apellido</label>
                <input type="text" id="apell1_func" name="apell1_func" class="form-control" value="<?php echo $response[0]['apell1_func']; ?>">
            </div>
            <div class="col-md-3">
                <label for="apell2">2do Apellido</label>
                <input type="text" id="apell2_func" name="apell2_func" class="form-control" value="<?php echo $response[0]['apell2_func']; ?>">
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <label for="identidad_func">Número de Identificación</label>
                    <input type="text" id="identidad_func" name="identidad_func" class="form-control" value="<?php echo $response[0]['nro_identidad']; ?>">
                </div>
                <div class="col-md-9">
                    <label for="observ_func">Observaciones</label>
                    <textarea name="observ_func" id="observ_func" cols="30" rows="2" value="<?php echo $response[0]['observaciones']; ?>"></textarea>
                </div>
            </div>
        </div>
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
        <input type="button" name="next" class="next action-button" value="Siguiente" />
</fieldset>
<fieldset>
    <h2 class="fs-title">Autorización de Mensajes de Texto</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_derechos_deberes">He recibido la carta de derechos y deberes</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_derechos_deberes" class="onoffswitch-checkbox" id="auth_derechos_deberes" checked>
                    <label class="onoffswitch-label" for="auth_derechos_deberes">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>


                <label for="mod_datos" class="spanlabel">Modificación de Datos Básicos de Identificación</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="mod_datos" class="onoffswitch-checkbox" id="mod_datos" checked>
                    <label class="onoffswitch-label" for="mod_datos">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
            <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_desempeño_eps">He recibido la carta de desempeño con el ranking de NUEVA EPS</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_desempeño_eps" class="onoffswitch-checkbox" id="auth_desempeño_eps" checked>
                    <label class="onoffswitch-label" for="auth_desempeño_eps">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
             <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_contenido_desempeño">Leí el contenido de la carta de desempeño</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_contenido_desempeño" class="onoffswitch-checkbox" id="auth_contenido_desempeño" checked>
                    <label class="onoffswitch-label" for="auth_contenido_desempeño">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
             <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_dudas">Me fueron resueltas las dudas sobre el contenido de las cartas de derechos y deberes y la carta de desempeño</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_dudas" class="onoffswitch-checkbox" id="auth_dudas" checked>
                    <label class="onoffswitch-label" for="auth_dudas">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
             <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_entender">Entendí y comprendí lo enunciado en la carta de derechos y deberes y la carta de desempeño</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_entender" class="onoffswitch-checkbox" id="auth_entender" checked>
                    <label class="onoffswitch-label" for="auth_entender">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
             
             <div class="col-md-12 text-left align-self-center">
                <label class="spanlabel" for="auth_canales_disp"> NUEVA EPS cuenta con canales disponibles y eficaces para resolver las dudas sobre el contenido de las cartas</label>
                <div class="onoffswitch text-center">
                    <input type="checkbox" name="auth_canales_disp" class="onoffswitch-checkbox" id="auth_canales_disp" checked>
                    <label class="onoffswitch-label" for="auth_canales_disp">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    
      <input type="button" name="previous" class="previous action-button" value="Atrás" />
      <input type="submit" name="submit" class="submit action-button" value="Guardar" />
</fieldset>

    
</form>
</div>
<script src="assets/bootstrap/js/select2.full.js"></script>
        <script>
            var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 100)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale('+scale+')'});
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 0, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 100)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            }, 
            duration: 0, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    // $(".submit").click(function(){
    //     return false;
    // })
</script>

<script type="text/javascript">
    $.fn.select2.defaults.set( "theme", "bootstrap" );

    var placeholder = "";


    $( ".select2-single, .select2-multiple" ).select2( {
        placeholder: placeholder,
        width: null,
        containerCssClass: ':all:'
    } );

    $( ".select2-allow-clear" ).select2( {
        allowClear: true,
        placeholder: placeholder,
        width: null,
        containerCssClass: ':all:'
    } );
 </script>