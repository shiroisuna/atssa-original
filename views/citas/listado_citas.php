<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Cita N°</th>
                <th>N° Identidad Paciente</th>
                <th>Especialidad</th>
                <th>Doctor</th>
                <!-- <th>serial</th> -->
                <th>Problema</th>
                <th>Fecha de la Cita</th>
                <th>status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($response as $row) { ?>
                <tr >
                    <td>
                         <?php echo $row['id_cita']; ?>
                    </td>
                    <td> <?php echo $row['id_paciente']; ?></td>
                    <td> <?php echo $row['especialidad']; ?></td>
                    <td> <?php echo $row['doctor']; ?></td>
                    <td> <?php echo $row['fecha_cita']; ?></td>
                    <td> <?php echo $row['problema']; ?></td>
                    <td> <?php echo $row['status_cita']; ?></td>
                   
                    <td><span class="label-success label label-default">Active</span></td>
                    <td>
                        <button class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </td>
                </tr>
            <?php } ?>                            
                        
        </tbody>
    </table>
</div>