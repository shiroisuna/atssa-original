<?php 

include("models/conn.php");
$con = new Conn;

$con->connect();

include("models/afiliaciones.php");

$afiliacion = new afiliaciones;

//Campos Tipo de Trámite
$eps 			= ( isset($_POST['eps']) ) ? $_POST['eps'] : '' ; 							#select
$tipo_tramite 	= ( isset($_POST['tipo_tramite']) ) ? $_POST['tipo_tramite'] : '' ; 		#select
$tipo_afiliacion = ( isset($_POST['tipo_afiliacion']) ) ? $_POST['tipo_afiliacion'] : '' ; 	#select
$sub_tipo_afil 	= ( isset( $_POST['sub_tipo_afil']) ) ?  $_POST['sub_tipo_afil'] : '' ;		#select
$regimen 		= ( isset($_POST['regimen']) ) ? $_POST['regimen'] : '' ;  					#select
$tp_afiliado 	= ( isset($_POST['tp_afiliado']) ) ? $_POST['tp_afiliado'] : '' ; 			#select
$tp_cotizante 	= ( isset($_POST['tp_cotizante']) ) ? $_POST['tp_cotizante'] : '' ; 		#select
$codigo 		= ( isset($_POST['codigo']) ) ? $_POST['codigo'] : '' ;
$fech_sol 		= ( isset($_POST['fech_sol']) ) ? $_POST['fech_sol'] : '' ;

//DATOS BASICOS DE IDENTIFICACION (del cotizante o cabeza de familia)
$nom1_solic =	 ( isset( $_POST['nom1_solic'] ) ) ? $_POST['nom1_solic'] :  '' ; 
$nom2_solic =	 ( isset( $_POST['nom2_solic'] ) ) ? $_POST['nom2_solic'] :  '' ; 
$apell1_solic =	 ( isset( $_POST['apell1_solic'] ) ) ? $_POST['apell1_solic'] :  '' ; 
$apell2_solic =	 ( isset( $_POST['apell2_solic'] ) ) ? $_POST['apell2_solic'] :  '' ; 

$nomsolicompleto 		= $nom1_solic.' '.$nom2_solic.' '.$apell1_solic.' '.$apell2_solic;
$tipodoc 				= ( isset( $_POST['tipo_id_solic'] ) ) ? $_POST['tipo_id_solic'] :  '' ; #select
$tipo_identificacion = ['null','R.C', 'T.I.','C.C.','C.E.','P.A.','C.D.','S.C.'];

foreach ($tipo_identificacion as $key => $identificar) {
	
	if($key == $tipodoc){
		$tipodoc = $identificar;
		break;
	}

}

$numerodocidentidad 	= ( isset( $_POST['n_identidad'] ) ) ? $_POST['n_identidad'] :  '' ; 
$sexo_solic 			= ( isset( $_POST['sexo_solic'] ) ) ? $_POST['sexo_solic'] :  '' ; 		#select

$sexo_identificacion = ['null','Femenino','Masculino'];
foreach ($sexo_identificacion as $llave => $tipo) {
	
	if($llave == $sexo_solic){
		$sexo_solic = $tipo;
		break;
	}
}
$fechanac 				= ( isset( $_POST['f_nac_solic'] ) ) ? $_POST['f_nac_solic'] :  '' ; 


//Datos Complementarios
$ad_pension = $_POST['ad_pension'];

$codetnico = ( isset( $_POST['etnia'] ) ) ? $_POST['etnia'] : '' ; #select

$tocp_etnico = ['null','Indigena','Rrom (Gitano)','Raizal (San Andres y Providencia)','Palenquero (San Basilio de Palenque)','Negro(a), Afrocolombiano(a)'];

foreach ($tocp_etnico as $key => $etnico) {
	if ($key == $codetnico) {
		$codetnico = $etnico;
		break;
	}
}

$t_discapacidad = ( isset( $_POST['t_discapacidad'] ) ) ? $_POST['t_discapacidad'] : '' ;#select
$tipo_discapacidades = ['F'=>'Física','NS'=>'Neuro-sensorial','M'=>'Mental'];
foreach ($tipo_discapacidades as $key => $tipo_discapacidad) {
	if ($key == $t_discapacidad) {
		$t_discapacidad = $tipo_discapacidad;
		break;
	}
}

$condiciondiscapa = ( isset( $_POST['condic'] ) ) ? $_POST['condic'] : '' ; #select
$tipo_consicion_disca = ['null', 'Temporal', 'Permanente'];
$tipo_consicion_disca2 = ['T'=>'Temporal', 'P'=>'Permanente'];
foreach ($tipo_consicion_disca as $key => $tipo_consicion) {
	if ($key == $condiciondiscapa) {
		$condiciondiscapa = $tipo_consicion;
		break;
	}
}    

$grupopoblacion = ( isset( $_POST['g_poblacion_esp'] ) ) ? $_POST['g_poblacion_esp'] : '' ;#select
$tipo_grupo_poblacional = [
	'02'=>'Población Infantil abandonada aa cargo del Instituto Colombiano de Bienestar Familiar',
	'06' => 'Menore desvinculados del conflicto armado, a cargo del ICBF',
	'08' => 'Población Desmovilizada',
	'09' => 'Victimas de Conflicto armado',
	'10' => 'Población Infantil vulnerable bajo protección de instituciones diferentes al ICBF',
	'11' => 'Personas incluidas al programa de prottección a testigos',
	'16' => 'Adultos mayores en centros de protección',
	'17' => 'Comunidad Indigena',
	'18' => 'Poblacion Rrom',
	'22' => 'Población privada de libertad que no este a cargo del Fondo Nacional de Salud de las personas privadas de libertad',
	'23' => 'Personas que dejen de ser madres comunitarias y sean beneficiarias del subsidio de la subcuenta de subsistencia del fondo de solidaridad pensional',
	'24' => 'Personas incluidas en el registro único de Damnificados por la deportación, expulsión, repatriación o retorno desde el territorio Venezolano'
];
foreach ($tipo_grupo_poblacional as $key => $tipo_grupo) {
	if ($key == $grupopoblacion) {
		$grupopoblacion = $tipo_grupo;
		break;
	}
}


$discapacidad = ( isset( $_POST['discapacidad'] ) ) ? $_POST['discapacidad'] : '' ;

$ptsnivel = ( isset( $_POST['pt_sisben'] ) ) ? $_POST['pt_sisben'] : '' ;

$adminriesgos = ( isset( $_POST['ad_riesgos'] ) ) ? $_POST['ad_riesgos'] : '' ;
$ingresocotiza = ( isset( $_POST['ing_cotizacion'] ) ) ? $_POST['ing_cotizacion'] : '' ;
$direccion = ( isset( $_POST['direccionsolic'] ) ) ? $_POST['direccionsolic'] : '' ;

$zona_complementaria = ( isset( $_POST['zona'] ) ) ? $_POST['zona'] : '' ; #select
$tipo_zona_complementaria = ['null', 'Rural', 'Urbana'];
foreach ($tipo_zona_complementaria as $key => $tipo_zona) {
	if ($key == $zona_complementaria) {
		$zona_complementaria = $tipo_zona;
		break;
	}
} 



$localidad = ( isset( $_POST['localidad'] ) ) ? $_POST['localidad'] : '' ;
$barrio = ( isset( $_POST['barrio'] ) ) ? $_POST['barrio'] : '' ;
$municipio_complementario = ( isset( $_POST['municipio'] ) ) ? $_POST['municipio'] : '' ; #selected

$municios_db = $afiliacion->selectMunicipiosTodo();
foreach ($municios_db as $row) {
	if($municipio_complementario == $row['id_munic']){
		$municipio_complementario = $row['cod_munic']." ".$row['municipio'];
		break;
	}
}


$departamento_complementario = ( isset( $_POST['dep'] ) ) ? $_POST['dep'] : '' ; #select

$dptos = $afiliacion->selectDptos();
foreach ($dptos as $row) {
	if($departamento_complementario == $row['id_dpto']){
		$departamento_complementario = $row['cod_dpto']." ".$row['dpto'];
		break;
	}
}

$tlfhab = ( isset( $_POST['tlf_f'] ) ) ? $_POST['tlf_f'] : '' ;
$tlfcell = ( isset( $_POST['tlf_m'] ) ) ? $_POST['tlf_m'] : '' ;
$correo_complentario = ( isset(  $_POST['email'] ) ) ?  $_POST['email'] : '' ;
$codigoips = '' ;

//Datos de Identificación de los miembros del Núcleo Familiar
$nombre_conyugue_1 = ( isset($_POST['p_nom_c']) ) ? $_POST['p_nom_c'] : '' ;
$nombre_conyugue_2 = ( isset($_POST['s_nom_c']) ) ? $_POST['s_nom_c'] : '' ;
$apellido_conyugue_1 = ( isset($_POST['p_apell_c']) ) ? $_POST['p_apell_c'] : '' ;
$apellido_conyugue_2 = ( isset($_POST['s_apell_c']) ) ? $_POST['s_apell_c'] : '' ;

$nombeneficiario = $nombre_conyugue_1.' '.$nombre_conyugue_2.' '.$apellido_conyugue_1.' '.$apellido_conyugue_2;
$tipodocbene = ( isset(  $_POST['t_doc_con'] )) ?  $_POST['t_doc_con'] : '' ;
$tipos_de_docu = ['null','CN','RC','TI','CC','CE','PA','SC'];

foreach ($tipos_de_docu as $key => $docu) {
	if ($key == $tipodocbene) {
		$tipodocbene = $docu;
		break;
	}
}

$numdocidenbene = ( isset( $_POST['nro_doc_con'] )) ? $_POST['nro_doc_con'] : '' ;
$sexobene = ( isset( $_POST['sex_con'])) ? $_POST['sex_con'] : '' ;

foreach ($sexo_identificacion as $llave => $tipo) {
	
	if($llave == $sexobene){
		$sexobene = $tipo;
		break;
	}
}
$fechanacbene = ( isset( $_POST['f_nac_con'] )) ? $_POST['f_nac_con'] : '' ;
$codigoipsbene = '' ;

$nombres_p = ( isset( $_POST['p_nombre_ben'] ) ) ? $_POST['p_nombre_ben'] : '' ;
$nombres_s = ( isset( $_POST['s_nombre_ben'] ) ) ? $_POST['s_nombre_ben'] : '' ;
$apellidos_p = ( isset($_POST['p_apellido_ben']) ) ? $_POST['p_apellido_ben'] : '' ;
$apellidos_s = ( isset($_POST['s_apellido_ben']) ) ? $_POST['s_apellido_ben'] : '' ;

$t_doc_ben  = ( isset($_POST['t_doc_ben']) ) ? $_POST['t_doc_ben'] : '' ; 
$n_identidad_ben  = ( isset($_POST['n_identidad_ben']) ) ? $_POST['n_identidad_ben'] : '' ; 
$sexo_ben  = ( isset($_POST['sexo_ben']) ) ? $_POST['sexo_ben'] : '' ; 
$fecha_nac_ben  = ( isset($_POST['f_nac_ben']) ) ? $_POST['f_nac_ben'] : '' ; 
$parentesco  = ( isset($_POST['parentesco']) ) ? $_POST['parentesco'] : '' ; 
$etnia_ben  = ( isset($_POST['etnia_ben']) ) ? $_POST['etnia_ben'] : '' ; 
$discapacidad_ben  = ( isset($_POST['discapacidad_ben']) ) ? $_POST['discapacidad_ben'] : '' ; 
$condicion_beneficiario  = ( isset($_POST['condicion_ben']) ) ? $_POST['condicion_ben'] : '' ; 
$t_discapacidad_ben  = ( isset($_POST['t_discapacidad_ben']) ) ? $_POST['t_discapacidad_ben'] : '' ; 

$dep_ben = ( isset( $_POST['dep_ben'] ) ) ? $_POST['dep_ben'] : '' ;
$munic = ( isset( $_POST['munic'] ) ) ? $_POST['munic'] : '' ;
$zona_ben = ( isset( $_POST['zona_ben'] ) ) ? $_POST['zona_ben'] : '' ;
$tlf_m_ben = ( isset( $_POST['tlf_m_ben'] ) ) ? $_POST['tlf_m_ben'] : '' ;
$tlf_f_ben = ( isset( $_POST['tlf_f_ben'] ) ) ? $_POST['tlf_f_ben'] : '' ;
$upc = ( isset( $_POST['upc'] ) ) ? $_POST['upc'] : '' ;
$ins_salud = ( isset( $_POST['ins_salud'] ) ) ? $_POST['ins_salud'] : '' ;

$filas = "";
$residencia = "";

if( $nombres_p != "" ){

	for ($i=0; $i<count($nombres_p); $i++) {
				// recorre cada uno de los valores del post contandolos pasandoles $i
		$nom = $nombres_p[$i]." ".$nombres_s[$i];
		$apell = $apellidos_p[$i]." ".$apellidos_s[$i];

		$t_doc = $t_doc_ben[$i];
		foreach ($tipos_de_docu as $key => $docu) {
			if ($key == $t_doc) {
				$t_doc = $docu;
				break;
			}
		}
		$nro_identidad = $n_identidad_ben[$i];
		$sexo = $sexo_ben[$i];
		foreach ($sexo_identificacion as $llave => $tipo) {
	
			if($llave == $sexo){
				$sexo = $tipo;
				break;
			}
		}

		$fecha_nac = $fecha_nac_ben[$i];
		$parent = $parentesco[$i];
		$etnia = $etnia_ben[$i];
		foreach ($tocp_etnico as $key => $etnico) {
			if ($key == $etnia) {
				$etnia = $etnico;
				break;
			}
		}

		$condicion_ben = $condicion_beneficiario[$i];
		foreach ($tipo_consicion_disca2 as $key => $tipo_consicion) {
			if ($key == $condicion_ben) {
				$condicion_ben = $tipo_consicion;
				break;
			}
		}

		$tipo_discapacidad_ben = $t_discapacidad_ben[$i];
		foreach ($tipo_discapacidades as $key => $tipo_discapacidad) {
			if ($key == $tipo_discapacidad_ben) {
				$tipo_discapacidad_ben = $tipo_discapacidad;
				
			}
		}

				// valida que no esten vacios para que guarde
		if ($nom != "" && $apell != "" && $t_doc != "" && $nro_identidad != "" && $sexo != "" && $fecha_nac != "" && $parent != "" && $etnia != "" && $tipo_discapacidad_ben != "" && $condicion_ben != "") 
		{
					// validamos si existe un conyugue en el registro de beneficiarios

			$nomapellfamili = $nom." ".$apell;
			$filas .=" 
			<tr>
			<td class='text-center bt p-5'>
			<p class='pp3'>".$i."</p>
			</td>

			<td class='bl text-center bt p-5' >
			<p class='pp3 capi cortext'>".$nomapellfamili."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $t_doc."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $nro_identidad."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $sexo."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $fecha_nac."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $parent."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'>". $etnia."</p>
			</td>
			<td class='bl text-center bt p-5' >
			<p class='pp3'><strong>Tipo: </strong>". $tipo_discapacidad_ben."<strong> | Condición:</strong> ". $condicion_ben."</p>
			</td>
			</tr>
			";
		}

		$dep = $dep_ben;
		foreach ($dptos as $row) {
			if($dep == $row['id_dpto']){
				$dep = $row['cod_dpto']." ".$row['dpto'];
				break;
			}
		}

		if( !empty($munic[$i]) ){
			$municipio_b = $munic[$i];
			foreach ($municios_db as $row) {
				if($municipio_b == $row['id_munic']){
					$municipio_b = $row['cod_munic']." ".$row['municipio'];
					break;
				}
			}
		}else{
			$municipio_b = "";
		}
		


		$zona_b = $zona_ben[$i];
		foreach ($tipo_zona_complementaria as $key => $tipo_zona) {
			if ($key == $zona_b) {
				$zona_b = $tipo_zona;
				break;
			}
		} 
		$tlf_m = $tlf_m_ben[$i];
		$tlf_f = $tlf_f_ben[$i];
		$upc_b = $upc[$i];
		$ins_salud_b = $ins_salud[$i];

	 if ( $dep != "" && $municipio_b != "" && $zona_b != "" && $tlf_m != "" && $tlf_f != "" && $upc_b != "" && $ins_salud_b != "")  
		{

			$residencia .=" 
			<tr>
				<td class='bt text-center bt p-5'>
					<p class='pp3'>".$i."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 cortext capi'>". $municipio_b."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 capi cortext'>". $dep."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $zona_b."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $tlf_m."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $tlf_f."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'>". $upc_b."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3 cortext'>". $ins_salud_b."</p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'></p>
				</td>
				<td class='bl text-center bt p-5'>
					<p class='pp3'></p>
				</td>
			</tr>
			";
		}

	}
}


//DATOS DE IDENTIFICACIÓN DEL EMPLEADOR Y OTROS APORTANTES O DE LAS ENTIDADES RESPONSABLES DE LA AFILIACIÓN COLECTIVA, INSTITUCIONAL O DE OFICIO
$condicionfamili = "";
$municipio = "";
$depto = "";
$zona = "";
$tlf_cell = "";
$tlf_hab = "";
$valor_upc_dicional = "";
$inst_servicios_salud = "";
$nivel_sisben = "";
$grupo_poblacional = "";

$razon_social = ( isset( $_POST['rif'] ) ) ? $_POST['rif'] : '' ;
$t_doc_empleador = ( isset( $_POST['t_doc_ot'] ) ) ? $_POST['t_doc_ot'] : '' ;

$tipo_doc_empleador = ['NULL', 'C.C','C.E.','P.A.','C.D.','N.I.'];
foreach ($tipo_doc_empleador as $key => $tipo_doc) {
	if($t_doc_empleador == $key){
		$t_doc_empleador = $tipo_doc;
		break;
	}	
}

$nro_doc_id = ( isset( $_POST['identidad_ot'] ) ) ? $_POST['identidad_ot'] : '' ;
$tipo_aportante = ( isset( $_POST['t_aportante'] ) ) ? $_POST['t_aportante'] : '' ;
$ciudad = ( isset( $_POST['direccion'] ) ) ? $_POST['direccion'] : '' ;
$dpto = ( isset( $_POST['dep5'] ) ) ? $_POST['dep5'] : '' ;
$tlf_cell_pensiones = ( isset( $_POST['tlf_mo'] ) ) ? $_POST['tlf_mo'] : '' ;
$tlf_hab_pensiones = ( isset( $_POST['tlf_f_aport'] ) ) ? $_POST['tlf_f_aport'] : '' ;
$correo = ( isset( $_POST['email_aport'] ) ) ? $_POST['email_aport'] : '' ;
$fecha_ini_relacion_lab = ( isset( $_POST['fech_ing_t'] ) ) ? $_POST['fech_ing_t'] : '' ;
$cargo = ( isset( $_POST['cargo_t'] ) ) ? $_POST['cargo_t'] : '' ;
$salario = ( isset(  $_POST['salario_t'] ) ) ?  $_POST['salario_t'] : '' ;


//Reporte de Novedades
$tipo_novedad = ( isset( $_POST['tipo_novedad'] ) ) ? $_POST['tipo_novedad'] : '' ; 
$nom1_novedad = ( isset($_POST['rep_nom1'] ) ) ? $_POST['rep_nom1'] : '' ; 
$nom2_novedad = ( isset( $_POST['rep_nom2']) ) ? $_POST['rep_nom2'] : '' ; 
$apellido1_novedad = ( isset($_POST['rep_apell1'] ) ) ? $_POST['rep_apell1'] : '' ; 
$apellido2_novedad = ( isset($_POST['rep_apell2'] ) ) ? $_POST['rep_apell2'] : '' ; 
$t_doc_novedad = ( isset($_POST['t_doc_rep'] ) ) ? $_POST['t_doc_rep'] : '' ; 
foreach ($tipo_identificacion as $key => $identificar) {
	
	if($key == $t_doc_novedad){
		$t_doc_novedad = $identificar;
		break;
	}

}
$nro_doc_novedad = ( isset( $_POST['n_identidad_rep']) ) ? $_POST['n_identidad_rep'] : '' ; 
$sexo_novedad = ( isset($_POST['sexo_rep'] ) ) ?  $_POST['sexo_rep']: '' ; 
foreach ($sexo_identificacion as $llave => $tipo) {
	if($llave == $sexo_novedad){
		$sexo_novedad = $tipo;
		break;
	}
}
$fecha_nac = ( isset($_POST['rep_fech_nac'] ) ) ? $_POST['rep_fech_nac'] : '' ; 
$fecha_ini = ( isset( $_POST['rep_fecha_ini']) ) ? $_POST['rep_fecha_ini'] : '' ; 
$eps_anterior = ( isset($_POST['eps_ant'] ) ) ?$_POST['eps_ant']  : '' ; 
$traslado = ( isset($_POST['cod_traslado_rep'] ) ) ? $_POST['cod_traslado_rep'] : '' ; 
$compensacion_familiar = ( isset( $_POST['traslado']) ) ? $_POST['traslado'] : '' ; 

//Datos a ser Diligenciados por la Entidad Territorial
$tipo_id_entidad_territ  = ( isset( $_POST['id_entidad_terr'] ) ) ? $_POST['id_entidad_terr'] : '' ;
$tipo_entidad_territorial = ['null','Código del Municipio','Código del Departamento'];
foreach ($tipo_entidad_territorial as $key => $tipo) {
	if ($key == $tipo_id_entidad_territ) {
		$tipo_id_entidad_territ = $tipo;
		break;
	}
}

$nro_ficha  = ( isset( $_POST['ficha'] ) ) ? $_POST['ficha'] : '' ;
$puntaje  = ( isset( $_POST['puntaje'] ) ) ? $_POST['puntaje'] : '' ;
$nivel  = ( isset( $_POST['nivel'] ) ) ? $_POST['nivel'] : '' ;
$fecha_radicacion  = ( isset( $_POST['radicacion'] ) ) ? $_POST['radicacion'] : '' ;
$fecha_validacion  = ( isset( $_POST['validacion'] ) ) ? $_POST['validacion'] : '' ;
$nom1_func  = ( isset( $_POST['nom1_func'] ) ) ? $_POST['nom1_func'] : '' ;
$nom2_func  = ( isset( $_POST['nom2_func'] ) ) ? $_POST['nom2_func'] : '' ;
$apell1_func  = ( isset( $_POST['apell1_func'] ) ) ? $_POST['apell1_func'] : '' ;
$apell2_func  = ( isset( $_POST['apell2_func'] ) ) ? $_POST['apell2_func'] : '' ;
$nro_identidad  = ( isset( $_POST['identidad_func'] ) ) ? $_POST['identidad_func'] : '' ;
$observaciones  = ( isset( $_POST['observ_func'] ) ) ? $_POST['observ_func'] : '' ;


$gestor_eps = "";
$nom_comercial = "";
$ciudad_eps = "";
$fecha_eps = "";


$rpt='';
$rpt.="<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'>
	</head>

	<style type='text/css'>
		body{
			font-family: 'Open Sans', sans-serif;
			margin-right:0;
			margin-left:0;
			width: 100%;
		}

		td{
			padding: 0;
		}

		.border {
		    border: 1px solid gray;
		    border-radius: 3px;
		}

		.bl {
			border-left: 1px solid gray;
		}

		.br {
			border-right: 1px solid gray;
		}

		.bt {
			border-top: 1px solid gray;
		}

		.pp1{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 8px;
			font-size: 10px;
		}

		.pp2{
			margin-top: 5px;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 5px;
		}

		.pp3{
			margin-top: 0;
			margin-bottom: 0;
			font-weight: 400;
			font-size: 7px;
			line-height: 7px;
		}

		.pp4{
			margin-top: 3px;
			margin-bottom: 2px;
			font-weight: 400;
			font-size: 10px;
			line-height: 20px;
		}
		.pp5{
			margin-top: 3px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.pp6{
			margin-top: 70px;
			margin-bottom: 12px;
			font-weight: 400;
			font-size: 12px;
			line-height: 22px;
		}

		.h2{
			font-size: 13px;
			line-height: 13px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.h1{
			font-size: 18px;
			line-height: 14px;
				font-weight: 600;
				margin-top:0;
				margin-bottom:0;
		}

		.pt-20{
			padding-top: -50px;
		}

		.pb-20{
			padding-bottom: -10px;
		}

		.pb-10{
			padding-bottom: 10px;
		}

		.mb-20{
			margin-bottom: 20px;
		}

		.mb-10{
			margin-bottom: 10px;
		}

		.mb-15{
			margin-bottom: 8px;
		}

		.mt-20{
			margin-top:20px;
		}

		.mt-15{
			margin-top:15px;
		}

		.logomin{
			width: 80px;
			margin-left:auto;
			margin-right:auto;
		}

		.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-0,.col-xs-1-5 {
		  float: left;
		  position: relative;
		  min-height: 1px;
		}

		.col-xs-12 {
		  width: 100%;
		}
		.col-xs-11 {
		  width: 91.66666667%;
		}
		.col-xs-10 {
		  width: 83.33333333%;
		}
		.col-xs-9 {
		  width: 75%;
		}
		.col-xs-8 {
		  width: 66.66666667%;
		}
		.col-xs-7 {
		  width: 58.33333333%;
		}
		.col-xs-6 {
		  width: 50%;
		}
		.col-xs-5 {
		  width: 41.66666667%;
		}
		.col-xs-4 {
		  width: 33.33333333%;
		  width: 30%;
		}
		.col-xs-3 {
		  width: 20%;
		}
		.col-xs-2 {
		  width: 15%;
		}

		.col-xs-1-5 {
		  width: 11%;
		}
		.col-xs-1 {
		  width: 8.33333333%;
		}

		.col-xs-0 {
		  width: 5%;
		}

		.p-5{
			padding: 5px;
		}

		.text-center{
			text-align: center;
		}

		.text-right{
			text-align: right;
		}

		.title1{
			font-size: 7px;
			margin-top:0;
			margin-bottom:5px;
		}

		.title3{
			font-size: 12px;
			margin-top:0;
			margin-bottom:2px;
		}

		.autorizacion{
			font-size: 8px;
			margin-top:5px;
			margin-bottom:5px;
		}

		.title2{
			font-size: 7px;
			margin-top:0;
			margin-bottom:0;
		}

		.capi{
			text-transform: capitalize;
		}

		.cortext{
			 text-overflow:ellipsis;
			  white-space:nowrap; 
			  overflow:hidden; 
			  max-width: 100%;
		}
	</style>
	<body>
	<div class='col-xs-12'>
			<div class='col-xs-4 text-center '>
				<p class='pp1'>REPUBLICA DE COLOMBIA</p>
				<p class='pp2'>SISTEMA GENERAL DE SEGURIDAD SOCIAL EN SALUD</p>
				<p class='pp2'>SUPERINTENDENCIA NACIONAL DE SALUD</p>
				<!-- <p class='pp3 mt-15'>'Código Únicamente para dependientes y afiliaciones al Régimen Subsidiado'</p> -->
			</div>

			<div class='col-xs-5 text-center'>
				<h2 class='h2'>FORMULARIO ÚNICO DE AFILIACIÓN Y REGISTRO DE NOVEDADES</h2>
			</div>

			<div class='col-xs-3 text-right'>
				<img class='logomin' src='assets/img/eps/comfachoco.jpg' alt=''>
				<!--
				<h2>LOGO</h2>
				<img class='logomin' src='assets/img/logo-atssa.jpg' alt=''>
				-->
			</div>
	</div>

	<div class='col-xs-12 pt-30 pb-10 text-center'>

		<h1 class='h1'>FORMULARIO ELECTRÓNICO VISTA PREVIA</h1>
	</div>

	<div class='col-xs-12'>

		<div class='col-xs-12 text-right'>
			<h6 class='title1'>FECHA DE SOLICITUD</h6>
			<p class='pp3'>". $fech_sol."</p>
		</div>

		<h6 class='title1'>DATOS DEL TRAMITE</h6>
		
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-2 p-5'>
				<h6 class='title1'>EPS a la que se desea Afiliar</h6>
				<p class='pp3'>" . $eps ."</p>
			</div>
			<div class='col-xs-2 p-5'>
				<h6 class='title1'>TIPO DE TRAMITE</h6>
				<p class='pp3'>". $tipo_tramite ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>TIPO DE AFILIACION</h6>
				<p class='pp3'>". $tp_afiliado.":".$sub_tipo_afil." </p>
			</div>

			
			<div class='col-xs-1 p-5 bl'>
				<h6 class='title1'>RÉGIMEN</h6>
				<p class='pp3'>".$regimen."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>TIPO DE AFILIADO</h6>
				<p class='pp3'>".$tipo_afiliacion."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>TIPO DE COTIZANTE</h6>
				<p class='pp3'>". $tp_cotizante ."</p>
			</div>
		</div>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO</h6>
					<p class='pp3'>". $codigo ."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS BÁSICOS DE IDENTIFICACIÓN ( del cotizante o cabeza de familia)</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $nomsolicompleto ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $tipodoc ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $numerodocidentidad ."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $sexo_solic ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $fechanac ."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS COMPLEMENTARIOS</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-1 p-5'>
				<h6 class='title1' style='width: 50px; margin-bottom: 2px;'>CÓD. ORIGEN ETNICO</h6>
				<p class='pp3 text-right' style='margin-top: -13px;'>". $codetnico ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>DISCAPACIDAD</h6>
				<p class='pp3'><strong>Tipo: </strong>". $t_discapacidad ." | <strong>Condición:</strong> ". $condiciondiscapa ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>PUNTAJE Y NIVEL DEL SISBEN</h6>
				<p class='pp3 cortext'>". $ptsnivel ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>GRUPO DE POBLACIÓN ESPECIAL</h6>
				<p class='pp3 cortext'>".$grupopoblacion."</p>
			</div>

			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>ADMINISTRADORA DE RIESGOS LABORALES</h6>
				<p class='pp3 cortext'>". $adminriesgos ."</p>
			</div>

			<div class='col-xs-3 p-5 bl'>
				<h6 class='title1'>ADMINISTRADORA DE PENSIONES</h6>
				<p class='pp3 cortext'>". $ingresocotiza ."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>INGRESO BASE DE COTIZACIÓN - IBC</h6>
					<p class='pp3'>". $ingresocotiza ."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>UBICACIÓN O DIRECCIÓN DE RESIDENCIA</h6>
					<p class='pp3 cortext'>". $direccion ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>ZONA</h6>
					<p class='pp3 cortext'>". $zona_complementaria ."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>LOCALIDAD / COMUNA</h6>
					<p class='pp3 cortext'>". $localidad ."</p>
				</div>

				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>BARRIO</h6>
					<p class='pp3 cortext'>". $barrio ."</p>
				</div>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-2 p-5'>
					<h6 class='title1'>CIUDAD / MUNICIPIO</h6>
					<p class='pp3 cortext'>". $municipio_complementario ."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>DEPARTAMENTO</h6>
					<p class='pp3 cortext'>". $departamento_complementario ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO FIJO</h6>
					<p class='pp3 cortext'>". $tlfhab ."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO MÓVIL</h6>
					<p class='pp3 cortext'>". $tlfcell ."</p>
				</div>

				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>CORREO ELECTRÓNICO</h6>
					<p class='pp3 cortext'>". $correo_complentario ."</p>
				</div>

				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO IPS</h6>
					<p class='pp3 cortext'>". $codigoips ."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>DATOS DE IDENTIFICACIÓN DE LOS MIEMBROS DEL NÚCLEO FAMILIAR</h6>
		<h6 class='title1'>Datos de identificación del beneficiario (Cónyuge o compañero (a) permanente del cotizante)</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-4 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $nombeneficiario ."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $tipodocbene ."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $numdocidenbene ."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $sexobene ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $fechanacbene ."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO IPS</h6>
					<p class='pp3 cortext'>". $codigoipsbene ."</p>
				</div>
		</div>

		<h6 class='title1'>Datos básicos de identificación de los beneficiarios y de los afiliados adicionales</h6>
		<table class='border mb-10' style='width: 100%;'>
			<tr>
				<td class='text-center p-5' style='width: 0.5%;'>
				</td>
				<td class='text-center p-5' style='width: 29%;'>
					<h6 class='title2'>APELLIDOS Y NOMBRES COMPLETOS DE LOS BENEFICIARIOS</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TIPO DOCUMENTO DE IDENTIDAD</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>NÚMERO DE IDENTIDAD</h6>
				</td>
				<td class='bl text-center p-5' style='width: 5%;'>
					<h6 class='title2'>SEXO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>FECHA DE NACIMIENTO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>PARENTESCO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>ETNIA</h6>
				</td>
				<td class='bl text-center p-5' style='width: 15.5%;'>
					<h6 class='title2'>DISCAPACIDAD</h6>
				</td>
			</tr>";

$rpt .= $filas;

$rpt .= "		
		</table>

		<h6 class='title1'>Datos de Residencia</h6>
		<table class='border mb-15' style='width: 100%;'>
			<tr>
				<td class='text-center p-5' style='width: 0.5%;'>
				</td>
				<td class='text-center p-5' style='width: 10%;'>
					<h6 class='title2'>CIUDAD/MPIO.</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>DEPARTAMENTO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 5%;'>
					<h6 class='title2'>ZONA</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TELEFÓNO FIJO</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10%;'>
					<h6 class='title2'>TELEFÓNO MOVIL</h6>
				</td>
				<td class='bl text-center p-5' style='width: 15%;'>
					<h6 class='title2'>VALOR UPC AFILIADO ADICIONAL (Lo diligencia la EPS)</h6>
				</td>
				<td class='bl text-center p-5' style='width: 21%;'>
					<h6 class='title2'>NOMBRE DE LA INSTITUCIÓN PRESTADORA DE SERVICIOS DE SALUD (IPS)</h6>
				</td>
				<td class='bl text-center p-5' style='width: 8%;'>
					<h6 class='title2'>NIVEL DE SISBÉN</h6>
				</td>
				<td class='bl text-center p-5' style='width: 10.5%;'>
					<h6 class='title2'>GRUPO POBLACIONAL</h6>
				</td>
			</tr>";

			$rpt .= $residencia;	

		
	$rpt .="</table>
		
		<h6 class='title1'>DATOS DE IDENTIFICACIÓN DEL EMPLEADOR Y OTROS APORTANTES O DE LAS ENTIDADES RESPONSABLES DE LA AFILIACIÓN COLECTIVA, INSTITUCIONAL O DE OFICIO</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-4 p-5'>
				<h6 class='title1'>NOMBRE O RAZÓN SOCIAL</h6>
				<p class='pp3 capi cortext'>". $razon_social."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $t_doc_empleador."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $nro_doc_id."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>DV</h6>
				<p class='pp3 cortext'>1</p>
			</div>

			<div class='col-xs-4 p-5 bl'>
				<h6 class='title1'>TIPO DE APORTANTE O PAGADOR DE PENSIONES</h6>
				<p class='pp3'>". $tipo_aportante."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-4 p-5'>
					<h6 class='title1'>UBICACIÓN O DIRECCIÓN</h6>
					<p class='pp3 cortext'>". $ciudad."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>CIUDAD / MUNICIPIO</h6>
					<p class='pp3 cortext'>". $ciudad."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>DEPARTAMENTO</h6>
					<p class='pp3 cortext'>". $dpto."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO FIJO</h6>
					<p class='pp3 cortext'>". $tlf_cell_pensiones."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>TELÉFONO MÓVIL</h6>
					<p class='pp3 cortext'>". $tlf_hab_pensiones."</p>
				</div>

				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CORREO ELECTRÓNICO</h6>
					<p class='pp3 cortext'>". $correo."</p>
				</div>
			</div>
			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>FECHA INICIO DE RELACIÓN LABORAL</h6>
					<p class='pp3 cortext'>". $fecha_ini_relacion_lab."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>CARGO</h6>
					<p class='pp3 cortext'>". $cargo."</p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>SALARIO</h6>
					<p class='pp3 cortext'>". $salario."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>. REPORTE DE NOVEDADES</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>TIPO DE NOVEDAD</h6>
				<p class='pp3 capi cortext'>". $tipo_novedad."</p>
			</div>
		</div>

		<h6 class='title1'>DATOS PARA EL REPORTE DE LA NOVEDAD</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-5 p-5'>
				<h6 class='title1'>APELLIDOS Y NOMBRES COMPLETOS</h6>
				<p class='pp3 capi cortext'>". $nom1_novedad.' '.$nom2_novedad.' '.$apellido1_novedad.' '.$apellido2_novedad."</p>
			</div>
			<div class='col-xs-1-5 p-5 bl'>			
				<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
				<p class='pp3'>". $t_doc_novedad."</p>
			</div>
			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
				<p class='pp3'>". $nro_doc_novedad."</p>
			</div>
			<div class='col-xs-0 p-5 bl'>
				<h6 class='title1'>SEXO</h6>
				<p class='pp3'>". $sexo_novedad."</p>
			</div>

			<div class='col-xs-2 p-5 bl'>
				<h6 class='title1'>FECHA NACIMIENTO</h6>
				<p class='pp3'>". $fecha_nac."</p>
			</div>

			<div class='col-xs-12 bt'>
				<div class='col-xs-1 p-5'>
					<h6 class='title1'>FECHA (a partir de)</h6>
					<p class='pp3 cortext'>". $fecha_ini."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>CÓDIGO EPS ANTERIOR</h6>
					<p class='pp3 cortext'>". $eps_anterior."</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>MOTIVO DE TRASLADO CÓDIGO</h6>
					<p class='pp3 cortext'>". $traslado."</p>
				</div>
				<div class='col-xs-6 p-5 bl'>
					<h6 class='title1'>CAJA DE COMPENSACIÓN FAMILIAR O PAGADOR DE PENSIONES</h6>
					<p class='pp3 cortext'>". $compensacion_familiar."</p>
				</div>
			</div>
		</div>

		<h6 class='title1'>DECLARACIONES Y AUTORIZACIONES</h6>
			
		<div class='col-xs-12 border mb-15'>
			<p class='pp3 cortext'> - Declaración Juramentada de convivencia: Declaro que convivo con el(la) Señor(a): _______________________________________________ identificado(a) con _________________________ N° ___________________ <br>desde el dia _____ del mes ___________ del año ______</p>
			<p class='pp3 cortext'> - Declaración de dependencia económica de los beneficiarios y afiliados adicionales: Declaro bajo la gravedad de juramento que el(los) Beneficiario(s) reportado(s) dependen económicamente de mi.</p>
			<p class='pp3 cortext'> - Declaración de la no obligación de afiliarse al Régimen Contributivo, Especial o de Excepción.</p>
			<p class='pp3 cortext'> - Declaración de existencia de razones de fuerza mayor o caso fortuito que impiden la entrega de los documentos que acreditan la condición de beneficiarios. Anexo soporte de la Entidad.</p>
			<p class='pp3 cortext'> - Declaración de no intermediación del cotizante, cabeza de familia, beneficiarios o afiliados adicionales en una Institución Prestadora de Servicios de Salud</p>
			<p class='pp3 cortext'> - Autorización para que la EPS solicite y obtenga datos y copia de la historia clínica del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS reporte la información que se genere de la afiliación o del reporte de novedades a la base de datos de afiliados vigentes y a las entidades públicas que por sus funciones la requieran.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS maneje los datos personales del cotizante o cabeza de familia y de sus beneficiarios o afiliados adicionales, de acuerdo con lo previsto en la Ley 1581 de 2012 y el Decreto 1377 de 2013.</p>
			<p class='pp3 cortext'> - Autorización para que la EPS envíe información al correo electrónico o al celular como mensajes de texto.</p>
			
		</div>

		<h6 class='title1'>FIRMAS</h6>
		<div class='col-xs-12 border mb-15' align='center'>
			<div class='col-xs-5 p-5'>
				<p class='pp3 cortext' align='center' style='margin-top:20px;'> Firma de cotizante, cabeza de familia o beneficiario</p>
				
			</div>
			<div class='col-xs-6 p-5'>
				<p class='pp3 cortext' align='center' style='margin-top:20px;'> Firma y sello del empleador, aportante o entidad responsable de la afiliación colectiva, institucional o de oficio.</p>
				
			</div>
		</div>

		<h6 class='title1'>ANEXOS</h6>
		<div class='col-xs-12 border mb-15'>
			<div class='col-xs-6 p-5'>
				<p class='pp3 cortext'> Anexo copia del documento de identidad:</p>
				
			</div><br>
			<p class='pp3 cortext'> - Copia del dictamen de incapacidad permanente emitido por la autoridad competente.</p>
				<p class='pp3 cortext'> - Copia del registro civil de matrimonio, o de la Escritura pública, acta de conciliación o sentencia judicial que declare la unión marital</p>
				<p class='pp3 cortext'> - . Copia de la escritura pública o sentencia judicial que declare el divorcio, sentencia judicial que declare la separación de cuerpos y escritura pública, acta de conciliación o sentencia judicial que declare la terminación de la unión marital</p>
				<p class='pp3 cortext'> - Copia del certificado de adopción o acta de entrega del menor.</p>
				<p class='pp3 cortext'> - Copia de la orden judicial o del acto administrativo de custodia.</p>
				<p class='pp3 cortext'> - Documento en que conste la pérdida de la patria potestad o el certificado de defunción de los padres o la declaración suscrita por el cotizante sobre la ausencia de los dos padres.
</p>
				<p class='pp3 cortext'> - Copia de la autorización de traslado por parte de la Superintendencia Nacional de Salud.</p>
				<p class='pp3 cortext'> - Certificación de vinculación a una entidad autorizada para realizar afiliaciones colectivas</p>
				<p class='pp3 cortext'> - Copia del acto administrativo o providencia de las autoridades competentes en la que conste la calidad de beneficiario o se ordene la afiliación de oficio.</p>
		</div>

		<h6 class='title1'>DATOS A SER DILIGENCIADOS POR LA ENTIDAD TERRITORIAL</h6>
		<div class='col-xs-12 border mb-15'>
				<div class='col-xs-4 p-5'>
					<h6 class='title1'>IDENTIFICACIÓN DE LA ENTIDAD TERRITORIAL</h6>
					<p class='pp3 cortext'><strong>CÓDIGO DEL MUNICIPIO: </strong>000 | <strong>CÓDIGO DEL DEPARTAMENTO: </strong>". $tipo_id_entidad_territ."</p>
				</div>
				<div class='col-xs-4 p-5 bl'>
					<h6 class='title1'>DATOS DEL SISBÉN</h6>
					<p class='pp3 cortext'><strong>NÚMERO DE FICHA: </strong>". $nro_ficha." | <strong>PUNTAJE:</strong> ". $puntaje." | <strong>NIVEL: </strong>". $nivel."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>FECHA DE RADICACIÓN</h6>
					<p class='pp3 cortext'>". $fecha_radicacion."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>
					<h6 class='title1'>FECHA DE VALIDACIÓN</h6>
					<p class='pp3 cortext'>". $fecha_validacion."</p>
				</div>

			<div class='col-xs-12 bt'>

				<div class='col-xs-4 p-5'>
					<h6 class='title1'>DATOS DEL FUNCIONARIO QUE REALIZA LA VALIDACIÓN</h6>
					<p class='pp3 capi cortext'>". $nom1_func.' '.$nom2_func.' '.$apell1_func.' '.$apell2_func."</p>
				</div>
				<div class='col-xs-1-5 p-5 bl'>			
					<h6 class='title1'>TIPO DOC. DE IDENT.</h6>
					<p class='pp3'>C.C.</p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>N° DOCUMENTO DE IDENTIDAD</h6>
					<p class='pp3'>". $nro_identidad."</p>
				</div>
			</div>
			<div class='col-xs-12 bt'>
				<div class='col-xs-3 p-5'>
					<h6 class='title1'>OBSERVACIONES:</h6>
					<p class='pp3 cortext'>". $observaciones."</p>
				</div>
			</div>
		</div>
		<h6 class='title3'>AUTORIZACIÓN DE MENSAJES DE TEXTO.</h6>
		<div class='col-xs-12 border mb-15'>
			<p class='pp4 cortext'>Yo,_______________________________________________identificado (a) con ___________________ otro _________________ número _________________ de _________________, <br> certifico que:</p>
			<div class='col-xs-6 p-5'>
				<div class='col-xs-7 p-5'>
					<p class='pp4 cortext'> He recibido la carta de derechos y deberes.  </p>
					<p class='pp4 cortext'> He recibido la carta de desempeño con el ranking de la EPS.  </p>
					<p class='pp4 cortext'> Leí el contenido de la carta de derechos y deberes.  </p>
					<p class='pp4 cortext'> Leí el contenido de la carta de desempeño.  </p>
					<p class='pp4 cortext'> Me fueron resueltas las dudas sobre el contenido de las cartas 	de derechos y deberes y la carta de desempeño.  </p>
					<p class='pp4 cortext'> Entendí y comprendí lo enunciado en la carta de derechos y 	deberes y la carta de desempeño.  </p>
					<p class='pp4 cortext'> La EPS cuenta con canales disponibles y eficaces para resolver las dudas sobre el contenido de las cartas.  </p>
				</div>
				<div class='col-xs-4 p-5'>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p><br>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p><br>
					<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
				</div>
				<div class='co-xs-5'>
					
				</div>
			</div>
			<div class='col-xs-5 p-5'>
				<p class='pp4 cortext'>Así mismo autorizo a NUEVA EPS, para que envíe información al
teléfono celular No. ____________________
y/o correo electrónico: _______________________________________________</p>
				<p class='pp5 cortext'>SI <input type='checkbox'>  NO <input type='checkbox'></p>
			</div>
		</div>
		<div class='col-xs-12 mb-15'>
			<div class='col-xs-5'>
				<p class='pp4 cortext'>Codialmente,</p>

			</div>
			<div class='col-xs-6'>
				<p class='pp4 cortext'>Afiliado manifiesta que no sabe o no puede firmar, autoriza a:</p>

			</div>
			<div class='col-xs-5 p-5'>
				<div class='col-xs-9'>
				<p class='pp4 cortext'>Firma: _________________________________________</p>
				<p class='pp4 cortext'>Tipo ID: ______________ No. ID: ____________________</p>
				<p class='pp4 cortext'>Fecha (dd/mm/aaaa): _____________________________</p>
				<p class='pp4 cortext'>Dirección: _________________________________________</p>
				<p class='pp4 cortext'>Tel: _______________ Municipio: _____________________</p>
				<p class='pp4 cortext'>Departamento: ___________________________________</p>
				</div>
				<div class='col-xs-3 border'>
					<p class='pp6 cortext' align='center'>Huella</p>
				</div>
			</div>
			<div class='col-xs-6 p-5'>
				<div class='col-xs-9'>
				<p class='pp4 cortext'>Firma: _________________________________________</p>
				<p class='pp4 cortext'>Tipo ID: ______________ No. ID: ____________________</p>
				<p class='pp4 cortext'>Fecha (dd/mm/aaaa): _____________________________</p>
				<p class='pp4 cortext'>Dirección: _________________________________________</p>
				<p class='pp4 cortext'>Tel: _______________ Municipio: _____________________</p>
				<p class='pp4 cortext'>Departamento: ___________________________________</p>
				</div>
				<div class='col-xs-3 border'>
					<p class='pp6 cortext' align='center'>Huella</p>
				</div>
			</div>
			<div class='col-xs-12 mb-15'>
				<p class='pp3 cortext'>Manifiesto con mi firma la aceptación de todas las declaraciones y términos incorporados en este formato y el consentimiento expreso, previo e informado en relación con el tratamiento de mis datos personales que hará
NUEVA EPS S.A., en sujeción a la política para el tratamiento de datos personales de la NUEVA EPS S.A. consúltela al reverso de este documento.</p>
			</div>
		</div>
		<div class='col-xs-12 border mb-15' style='border-radius: 5px;'>
			<p class='pp4 cortext'>  Información de la Nueva EPS</p>
			<p class='pp4 cortext'>     Asesor: __________________________________________________________</p>
			<p class='pp4 cortext'>     Código: ____________________ Oficina: _____________________________</p>
		</div>
		<div class='col-xs-12'>
			<div class='col-xs-6'>
				<h3>Autorizaciones</h3>

				<p class='pp3 cortext'>En virtud de la anterior certificación, autorizo expresamente y conforme a lo dispuesto en los artículos 14 y 15 de la Ley 527 de 1995, a la EPS S.A. Para que me remita información en forma de mensajes de datos, a través de mecanismos como sms -short message service - , correos electrónicos y redes sociales, relacionada con los servicios de la EPS S.A. y la carta de derecho de deberes.</p>

				<p class='pp3 cortext'>Autorizo de manera expresa a la EPS S.A. y/o a la persona natural o jurídica a quien ésta encargue, para que recolecte, almacene, use, haga circular, actualice o suprima mis datos personales, para acceder a los servicios de la EPS S.A. y para el cumplimiento de la carta de derechos y deberes.</p><br>

				<h3>Aviso de privacidad</h3>

				<p class='pp3 cortext'>Hago constar expresamente que la EPS S.A. me informó sobre los siguientes derechos que me asisten como titular de datos personales:</p>

				<p class='pp3 cortext'>• Acceder los datos personales que hayan sido objeto de tratamiento conforme a lo dispuesto en la Ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen.</p>

				<p class='pp3 cortext'>• Conocer, actualizar y rectificar los datos personales frente al responsable del tratamiento y al encargado del tratamiento. Este derecho se podrá ejercer, entre otros, datos, en relación con datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos datos cuyo tratamiento esté expresamente prohibido o no haya sido autorizado. </p>

				<p class='pp3 cortext'>• solicitar prueba de la autorización otorgada al responsable del tratamiento, salvo cuando expresamente se exceptúe como requisito para el tratamiento, de conformidad con lo previsto en el artículo 10 de la Ley 1581 de 2012.</p>

				<p class='pp3 cortext'>• Ser informado por el responsable del tratamiento o el encargado del tratamiento, previa solicitud, con respecto del uso que le ha dado a los datos personales</p>

				<p class='pp3 cortext'>• Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la Ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen.</p>

			</div>
			<div class='col-xs-5'><br><br>
				<p class='pp3 cortext'>• Revocar la autorización y/o solicitar la supresión del dato cuando en el tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o supresión procederá cuando la Superintendencia de Industria y  comercio haya determinado que en el tratamiento el responsable o encargado han ncurrido en conductas contrarias a esta ley y a la constitución. En cumplimiento de la Ley 1581 de 2012 y el Decreto 1377 de 2013 y las demás normas que los modifiquen, adicionen o complementen, le informamos que usted puede conocer la política de tratamiento de los datos personales de la EPS S.S., a través de los siguientes mecanismos que ponemos a su disposición:</p><br>

				<p class='pp3 cortext'>Sitio web: www.nuevaeps.com.co <br> Oficinas de atención al afiliado de la EPS S.A. <br> Línea Gratuita: 01 8000 954400 <br> Línea de atención en Bogotá: 307 7022 <br> Conmutador: (57 + 1) 4193000 <br> Dirección Administrativa: Carrera 85k No. 46A – 66 Piso 2</p>

				<p class='pp3 cortext'>Sr.(a) afiliado(a), la información por usted suministrada en este formato, así como la relacionada en el formulario de afiliación y/o demás documentos anexos, serán utilizados por la EPS S.A. como responsable del tratamiento de la información únicamente para los fines específicos relacionados con la administración y prestación de servicios en salud de Plan Obligatorio de Salud, de acuerdo con la Ley 1581 de 2012 ‘’Hábeas Data’’; dicho tratamiento podrá implicar la trasferencia, trasmisión y/o recepción de los datos, y el cual se realizará a través de sí misma, terceros encargados de tratamiento de información o de sus aliados comerciales, para fines comerciales y para la correcta prestación del servicio, de la forma indicada en los Términos y Condiciones de cada uno de sus portales y de la Política de Confidencialidad y Protección de Datos.</p>
			</div>
		</div>
		<h6 class='title1'>INFORMACIÓN PARA SER DILIGENCIADA POR EL TRABAJADOR INDEPENDIENTE CUYO INGRESO SEA IGUAL O SUPERIOR A 1 SMMLV</h6>
		<div class='col-xs-12 mb-15'>
			<div class='col-xs-12 border'>
				
				<div class='col-xs-2  p-5 bl'>
					<h6 class='title1'>ACTIVIDAD ECONÓMICA</h6>
					<p class='pp3'></p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ADMINISTRADORA DE RIESGOS LABORALES</h6>
					<p class='pp3'></p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ADMINISTRADORA DE FONDOS DE PENSIONES</h6>
					<p class='pp3'></p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>INGRESO BASE DE COTIZACIÓN</h6>
					<p class='pp3'></p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>POSEE VIVIENDA PROPIA</h6>
					<p class='pp3'></p>
				</div>
				<div class='col-xs-2 p-5 bl'>
					<h6 class='title1'>ACTIVIDAD ECONÓMICA</h6>
					<p class='pp3'></p>
				</div>
			</div>
			<div class='col-xs-6 border p-5 bl'>
				<p class='pp3 cortext'>Nivel de Escolaridad</p>
				<p class='pp3'></p>
			</div>
			<div class='col-xs-5 p-5 bl'>
				<div class='col-xs-12 p-5'>
					<p class='pp3'>Posición Ocupacional</p>
				</div>
				<div class='col-xs-6 p-5'>
					<p class='pp3'>Inicio de Contrato</p>
				</div>
				<div class='col-xs-5 p-5'>
					<p class='pp3'>Fin de Contrato</p>
				</div>
			</div>
			<div class='col-xs-12 border p-5 bl'>
				<p class='pp3'>SEÑOR USUARIO: TRABAJADOR INDEPENDIENTE O CONTRATISTA, RECUERDE REPORTAR LA NOVEDAD DE RETIRO EN LA PLANILLA DE PAGO A PARTIR DEL MOMENTO QUE DECIDA NO CONTINUAR CON LOS SERVICIOS DE SALUD, EL NO HACERLO  LE GENERA MORA, LA CUAL TENDRÁ VIGENCIA HASTA EL MOMENTO EN EL QUE REPORTE SU RETIRO AL SISTEMA. DECRETO 806 ART. 57 DE 1998 Y ART. 59 DECRETO 1406 DE 1999. </p>
			</div>
			<div class='col-xs-12 border p-5 bt'>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>SÓLO DILIGENCIAR SI ES COTIZANTE INDEPENDIENTE PAGO POR TERCERO</h6>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre del Tercero</h6>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Identificación</h6>

				</div>
			</div>
		</div>
		<h6 class='title1'>INFORMACIÓN PARA SER SUMINISTRADA POR LA EPS</h6>
		<div class='col-xs-12 boder mb-15'>
			<div class='col-xs-12 border p-5 bt'>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre del GESTOR o PROMOTOR</h6>
					<p class='pp3 cortext'>".$gestor_eps."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO GESTOR o PROMOTOR</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Nombre DIRECTOR COMERCIAL</h6>
					<p class='pp3 cortext'>".$nom_comercial."</p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1'>CÓDIGO DIRECTOR</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-3 p-5 bl'>
					<h6 class='title1'>Causal Inconsistencia y/o Observaciones</h6>
					<p class='pp3 cortext'></p>
				</div>
				<div class='col-xs-1 p-5 bl'>
					<h6 class='title1' align='center'>CIUDAD Y FECHA</h6>
					<p class='pp3 cortext'>".$ciudad_eps." ".$fecha_eps."</p>
				</div>
			</div>
			
		</div>
	</body>
</html>";


	// var_dump($rpt);

	$nombrearchivo='Reporte-afiliacion';

		require_once __DIR__ . '/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf([
			'format' => 'Legal',
			'mode' => 'utf-8',
			'orientation' => 'P',
		    'setAutoTopMargin' => 'stretch',
		    'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 10,
			'margin_header' => 0,
			'margin_footer' => 0
		]);

		$html = $rpt;
		$mpdf->autoPageBreak = true;
		$mpdf->AddPage('P');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		$mpdf->Output($nombrearchivo.'.pdf','I'); 


		exit;
?>

