<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from healthadmin.thememinister.com/add-app.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
           <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
            <!-- Header Navbar -->
                    <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                        <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                    <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                        <h1>Citas</h1>
                        <small>Lista de Cistas</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashborad.php"><i class="pe-7s-home"></i> Inicio</a></li>
                            <li class="active">Citas</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- Form controls -->
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="btn-group"> 
                                      <a class="btn btn-primary" href="table.html"> <i class="fa fa-list"></i>  Listado Citas</a>  
                                  </div>
                              </div>
                              <div class="panel-body">

                                
                                    <div class="form-group">
                                        <label>Paciente Id</label>
                                        <input type="text" class="form-control" id="nro_ident_p" name="nro_ident_p" placeholder="paciente_nro" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Especialidad</label>
                                        <input type="text" name="especialidad" id="especialidad" class="form-control" placeholder="Enter Department" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Doctor</label>
                                        <input type="text" class="form-control" name="doctor" id="doctor" placeholder="Nombre del Doctor" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Fecha de la Cista</label>
                                        <input name="fecha_cita" id="fecha_cita" class="datepicker form-control hasDatepicker" type="date" placeholder="Fecha de la Cita" required>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label>Serial No:</label><br>
                                        <div class="pagination">
                                          <a href="#">&laquo;</a>
                                          <a href="#">1</a>
                                          <a href="#" class="active">2</a>
                                          <a href="#">3</a>
                                          <a href="#">4</a>
                                          <a href="#">5</a>
                                          <a href="#">6</a>
                                          <a href="#">&raquo;</a>
                                      </div> -->

                                  </div>

                                  <div class="form-group">
                                     <label>Problema</label><br>
                                     <textarea class="form-control" id="problema" name="problema" rows="6" placeholder="problema">
                                     </textarea>
                                 </div>
                                 <div class="form-check">
                                  <label>Status</label><br>
                                  <label class="radio-inline"><input type="radio" name="status" value="1" checked="checked">Activo</label>
                                  <label class="radio-inline">
                                      <input type="radio" name="status" value="0">Inactivo</label>  
                                  </div>                                    
                                  <div class="reset-button">
                                     <a href="#" class="btn btn-warning">Reset</a>
                                     <a href="#" class="btn btn-success">Save</a>
                                 </div>
                            
                         </div>
                     </div>
                 </div>
             </div>

         </section> <!-- /.content -->
     </div> <!-- /.content-wrapper -->
    
</div> <!-- ./wrapper -->
        <!-- Start Core Plugins
        =====================================================================-->
        <!-- jQuery -->
        <?php include("footer.php"); ?>
         <script src="assets/citas_medicas.js" type="text/javascript"></script>
       
    </body>

<!-- Mirrored from healthadmin.thememinister.com/add-app.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:18:09 GMT -->
</html>
