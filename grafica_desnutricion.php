<?php session_start(); ?>
<html lang="en">
<style type="text/css">


</style>
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="dashboard.php" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
                <!-- Header Navbar -->
               <?php include("menu-top.php"); ?>
            </header>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <?php include("menu-left.php"); ?>
          
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
                        <i class="fa fa-tachometer"></i>
                    </div>
                    <div class="header-title">
                        <h1> Dashboard</h1>
                        <small> Dashboard features</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </section>
            <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="panel panel-bd cardbox">
                                <div class="panel-body">
                                    <div class="statistic-box">
                                        <h2><span class="count-number">15</span>
                                        </h2>
                                    </div>
                                    <div class="items pull-left">
                                        <i class="fa fa-users fa-2x"></i>
                                        <h4>Active Doctors </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="panel panel-bd cardbox">
                                <div class="panel-body">
                                    <div class="statistic-box">
                                        <h2><span class="count-number">19</span>
                                        </h2>
                                    </div>
                                    <div class="items pull-left">
                                        <i class="fa fa-users fa-2x"></i>
                                        <h4>Active Patients</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                            <div class="panel panel-bd cardbox">
                                <div class="panel-body">
                                    <div class="statistic-box">
                                        <h2><span class="count-number">05</span>
                                        </h2>
                                    </div>
                                    <div class="items pull-left">
                                        <i class="fa fa-user-circle fa-2x"></i>
                                        <h4>Representative</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="panel panel-bd lobidisable">
                            <div class="panel-heading">
                                <div class="panel-title">
                                </div>
                            </div>
                           
                            <div class="panel-body" style="cursor:auto;">
                                <div class="row vertical-align">
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-left vcenter">
                                        <img class="img-responsive mb-20" src="assets/img/graficas-riesgo/peso-p-talla-niños-0-a-2años.png" alt="">
                                    </div>

                                     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center vcenter">

                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><label class="control-label mt-20 mb-10" >Desnutrición Aguda Severa
                                                            <span data-toggle="tooltip" data-placement="bottom" data-original-title="< - 3"><i class="fa fa-info-circle infoform"></i></span></label></td>
                                                        <td class="text-center"> 
                                                           <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="1"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Desnutrición Aguda Moderada</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> = - 3 a < - 2"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="2"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                     <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Riesgo de Desnutrición Aguda</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> = - 2 a < - 1"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="3"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                     <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Peso Adecuado para la Talla</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> = - 1 a < 1"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="4"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Riesgo de Sobrepeso</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> 1 a < = 2"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="5"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Sobrepeso</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> 2 a < = 3"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgo[]" value="6"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="panel panel-bd lobidisable">
                            <div class="panel-heading">
                                <div class="panel-title">
                                </div>
                            </div>
                           
                            <div class="panel-body" style="cursor:auto;">
                                <div class="row vertical-align">
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-left vcenter">
                                        <img class="img-responsive mb-20" src="assets/img/graficas-riesgo/talla-p-edad-niños-0-2años.png" alt="">
                                    </div>

                                     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center vcenter">

                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td><label class="control-label mt-20 mb-10" >Retraso en Talla
                                                            <span data-toggle="tooltip" data-placement="bottom" data-original-title="< - 2"><i class="fa fa-info-circle infoform"></i></span></label></td>
                                                        <td class="text-center"> 
                                                           <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgotalla[]" value="1"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Riesgo de Talla baja para la Edad</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> = - 2 a < - 1"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgotalla[]" value="2"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>

                                                     <tr>
                                                        <td><label for="riesgo" class="control-label mt-20 mb-10">Talla Adecuada para la Edad</label>
                                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="> = - 1"><i class="fa fa-info-circle infoform"></i></span></td>

                                                        <td class="text-center">
                                                            <label class="controlper controlper-radio">
                                                                <input type="radio" name="riesgotalla[]" value="3"/>
                                                            <div class="controlper_indicator"></div>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                       
                </div> <!-- /.row -->
            </section> <!-- /.content -->

        </div> <!-- /.content-wrapper -->
           
    </div> <!-- ./wrapper -->

        <?php include("footer.php"); ?>



    </body>

<!-- JAMP -->
</html>