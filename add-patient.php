
<html>

<!-- Mirrored from healthadmin.thememinister.com/add-patient.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
<?php include("header.php"); ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
           <header class="main-header">
            <a href="index.html" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="assets/dist/img/mini-logo.png" alt="">

                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <h4>ATSSA</h4>
                        <!-- <img src="assets/dist/img/logo.png" alt=""> -->
                    </span>
                </a>
            <!-- Header Navbar -->
                <?php include("menu-top.php"); ?>
                        </header>
                        <!-- =============================================== -->
                        <!-- Left side column. contains the sidebar -->
                    <?php include("menu-left.php"); ?>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                           <!--  <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div> -->
                        </form>  
                        <h1>Pacientes</h1>
                        <small>lista de pacientes</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="dashboard.php"><i class="pe-7s-home"></i> Inicio</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- Form controls -->
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="btn-group"> 
                                        <a class="btn btn-primary" href="pt-list.php"> 
                                            <i class="fa fa-list"></i>  Volver a Lista Pacientes </a>  
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        
                                            <div class="form-group">
                                                <label>Nombres</label>
                                                <input type="text" id="nombres" name="nombres" class="form-control" placeholder="Enter First Name" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Apellidos</label>
                                                <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Enter last Name" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Tlf</label>
                                                <input type="tel" name="tlf" id="tlf" class="form-control" placeholder="Enter Mobile" required>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>nro Identidad</label>
                                                <input type="text" id="nro_identidad" name="nro_identidad" class="form-control" placeholder="Ej: x701505-E" required>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Fecha de nacimiento</label>
                                                <input name="fecha_nac" id="fecha_nac" class="datepicker form-control hasDatepicker" type="date" placeholder="Cumpleaños">
                                            </div>
                                            <div class="form-group">
                                                <label>Grupo Sanguineo</label>
                                                <select id="g_sang" class="sang" class="form-control">
                                                    <option>A+</option>
                                                    <option>AB+</option>
                                                    <option>O+</option>
                                                    <option>AB-</option>
                                                    <option>B+</option>
                                                    <option>A-</option>
                                                    <option>B-</option>
                                                    <option>O-</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Sexo</label><br>
                                                <label class="radio-inline"><input name="sex" id="sex" value="1" checked="checked" type="radio">Masculino</label> 
                                                <label class="radio-inline"><input name="sex" id="sex" value="0" type="radio">Femenino</label>

                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" id="email" class="form-control" type="email" placeholder="ejemplo@correo.com">
                                            </div>
                                            <div class="form-group">
                                                <label>Dirección</label>
                                                <textarea class="form-control" id="direccion" name="direccion" rows="3" required></textarea>
                                            </div>
                                         <!-- <div class="form-check">
                                          <label>Status</label><br>
                                          <label class="radio-inline"><input type="radio" name="status" value="1" checked="checked">Active</label> <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label> -->
                                                                        
                        
                                    
                                    <div class="reset-button">
                                     <a href="#" class="btn btn-warning">Volver</a>
                                     <a href="#" class="btn btn-success" onclick="savePatient()">Guardar</a>
                                 </div>
                            
                         </div>
                     </div>
                 </div>
             </div>
             
         </section> <!-- /.content -->
     </div> <!-- /.content-wrapper -->
    
</div> <!-- ./wrapper -->
       <?php include("footer.php"); ?>
        <script src="assets/pacientes.js" type="text/javascript"></script>
    </body>
    
<!-- Mirrored from healthadmin.thememinister.com/add-patient.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Nov 2018 08:17:17 GMT -->
</html>
